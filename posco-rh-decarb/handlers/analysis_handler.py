#!/usr/bin/env python
# -*- coding: utf-8 -*-
import argparse
import configparser
import sys
import os
import random
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.linear_model import LinearRegression
import matplotlib.cm as cm

from lib import data as data_lib
from lib import sys_lib
from lib import learning as learning_lib
import lib.HoonUtils as utils
from handlers import dataset_handler
from refine_model import model_cvt, model_rh

ANALYSIS_TEST = True
EXTRACT_BY_RANGE = False
ANALYSIS_FOLER = '../sys_temp/analysis/'
HIST_SUB_FOLDER = 'HISTOGRAM'
REAL_Y = 'REAL_Y'
TAR_Y = 'TAR_Y'
EST_Y = 'EST_Y'

EST_REAL_Y_DIFF = 'EST_REAL_Y_DIFF'
TAR_REAL_Y_DIFF = 'TAR_REAL_Y_DIFF'

class DataAnalysis:
    """Data Analysis Framework for POSCO Model

    """
    def __init__(self, feature, ini=None):

        self.feature = feature
        self.data_frame = pd.DataFrame(feature.dataset[1:], columns=feature.dataset[0])
        self.tolerance = None
        self.eval_cond_var_list = []
        self.log = ""

        if ini:
            self.init_ini(ini)

    def init_ini(self, ini):
        self.cat = ini['model_info']['model_name']
        self.record_date = ini['ANALYSIS']['record_date']

    def determine_y_var(self, mode):
        """

        :param mode: 1 -> start y / 2 -> end y / 3 -> drop y, 4 -> target y
        """
        if self.cat == 'CVT':
            REAL_START = model_cvt.CONV_ENDPNT_TEMP
            TAR_START = model_cvt.CONV_ENDPNT_TARGET_TEMP
            EST_START = model_cvt.EVAL_TEMP

            REAL_END = model_cvt.RH_ARR_TEMP
            TAR_END = model_cvt.RH_REQ_TEMP
            EST_END = model_cvt.EST_END

            REAL_DROP = model_cvt.REAL_DROP
            TAR_DROP = model_cvt.TAR_DROP
            EST_DROP = model_cvt.EST_DROP

        elif self.cat == 'RH':
            REAL_START = model_rh.RH_LAST_TEMP
            TAR_START = None  # Variable is None
            EST_START = model_rh.EVAL_TEMP

            REAL_END = model_rh.CC_TD_AVG_TEMP
            TAR_END = model_rh.CC_TD_AIM_TEMP
            EST_END = model_rh.EST_END

            REAL_DROP = model_rh.REAL_DROP
            TAR_DROP = model_cvt.TAR_DROP
            EST_DROP = model_rh.EST_DROP

        if self.cat == 'CVT' or self.cat == 'RH':
            if mode == 'start':  # Start y value
                self.data_frame[REAL_Y] = self.data_frame[REAL_START]
                self.data_frame[TAR_Y] = self.data_frame[TAR_START]
                self.data_frame[EST_Y] = self.data_frame[EST_START]
            elif mode == 'end':  # End y value
                self.data_frame[REAL_Y] = self.data_frame[REAL_END]
                self.data_frame[TAR_Y] = self.data_frame[TAR_END]
                self.data_frame[EST_Y] = self.data_frame[EST_END]
            elif mode == 'drop':  # Drop y value
                self.data_frame[REAL_Y] = self.data_frame[REAL_DROP]
                self.data_frame[TAR_Y] = self.data_frame[TAR_DROP]
                self.data_frame[EST_Y] = self.data_frame[EST_DROP]
        else:
            if mode == 'y':
                for var_name, var_obj in self.feature.vars.items():
                    if var_obj.dataset_order == 0:
                        self.data_frame[REAL_Y] = self.data_frame[var_name]
        pass

    def init_out_dirs(self, out_path):

        self.hist_dir = os.path.join(out_path, HIST_SUB_FOLDER) + "/"

        utils.folder_exists(out_path, exit_=False, create_=True, print_=False)
        utils.folder_exists(self.hist_dir, exit_=False, create_=True, print_=False)

    def init_analysis_var_info(self):
        """

        """
        if self.cat == 'CVT':
            self.tolerance = float(model_cvt.TOLERANCE)
            self.eval_cond_var_list = model_cvt.EVAL_COND_VAR_LIST
        elif self.cat == 'RH':
            self.tolerance = float(model_rh.TOLERANCE)
            self.eval_cond_var_list = model_rh.EVAL_COND_VAR_LIST

    def apply_dtype_to_data_frame(self, data_frame=None, logger=None):
        """

        :param data_frame:
        :param logger:
        """

        for key, val in self.feature.vars.items():
            ptr = self.feature.vars[key]
            try:
                if data_lib.is_class_name(ptr, "NumberClass"):
                    data_frame[key] = pd.to_numeric(data_frame[key], errors='coerce')
                    # data_frame[key].astype('float')
                else:
                    data_frame[key].astype('str')
            except Exception as e:
                pass
                # print(e)

        msg = "Apply data type to data frame."
        if logger:
            logger.info(msg)
        self.log = msg

        return data_frame

    def refine_data_frame_row_by_range(self, data_frame=None, var_list=None, logger=None):
        """

        :param data_frame:
        :param logger:
        """
        if var_list is  None:
            var_list = self.feature.vars.keys()

        for key in var_list:
            ptr = self.feature.vars[key]
            if key not in data_frame.columns.tolist():
                continue
            try:
                if data_lib.is_class_name(ptr, "NumberClass"):
                    data_frame = data_frame.loc[(data_frame[key] >= ptr.min_thresh) & (data_frame[key] <= ptr.max_thresh)]
                    print('key : {}, data : {}'.format(key, len(data_frame)))
                    # data_frame[key].astype('float')
                else:
                    data_frame = data_frame[data_frame[key].isin(ptr.domain)]
                    print('key : {}, data : {}'.format(key, len(data_frame)))
            except Exception as e:
                pass
                print(e)

        msg = "\n\n"
        msg += dataset_handler.DIV_LINE
        msg += "\n\n # Dataset refinement process by deleting row vector "
        msg += "in which one of them doesn't satisfy its range requirement."
        if logger:
            logger.info(msg)
        self.log = msg

        return data_frame

    def extract_valid_data_by_eval_condition(self, data_frame=None, record_date=None, logger=None, var_cond=False):
        """

        :param cat:
        :param record_date:
        :param mode: False -> pass / 1 -> date / 2 -> date & variable
        :param data_frame:
        :param logger:
        """

        # Extract row data by specific date
        df_by_date = data_frame.loc[data_frame['TAP_WORK_DATE'] >= record_date].reset_index(drop=True)
        # data_frame = data_frame.loc[data_frame['TAP_WORK_DATE'] <= '20190520'].reset_index(drop=True) ##

        # Extract row data by outliers of variable
        refined_df = self.refine_data_frame_row_by_range(data_frame=df_by_date,
                                                         var_list=self.eval_cond_var_list)

        ## Extract row data by abs(TARGET_TEMP - REAL_TEMP)
        if var_cond:
            tar_real_t1_torr_cond = np.logical_and(-self.tolerance <= refined_df.TAR_REAL_DIFF, refined_df.TAR_REAL_DIFF <= self.tolerance)
            est_real_t2_torr_cond = np.logical_and(-self.tolerance <= refined_df.EST_REAL_DIFF, refined_df.EST_REAL_DIFF <= self.tolerance)

            refined_df = refined_df[(tar_real_t1_torr_cond == True) & (est_real_t2_torr_cond == True)].reset_index(drop=True)

        msg = "\n\n"
        msg += dataset_handler.DIV_LINE
        msg += "\n\n # Extracts rows satisfying the converter evaluation criterion."
        if logger:
            logger.info(msg)
        self.log = msg
        return refined_df


        pass

    def get_statistics_info(self, data_frame=None, var_list=None, range_val=10, logger=None):

        if self.cat =='CVT':
            data_frame['MODEL_EST_REAL_TEMP_DIFF'] = data_frame.EVAL_TEMP - data_frame.CONV_ENDPNT_TEMP
        elif self.cat =='RH':
            data_frame['MODEL_EST_REAL_TEMP_DIFF'] = data_frame.EVAL_TEMP - data_frame.RH_LAST_TEMP

        range_list = np.arange(-28, 29, range_val).tolist()
        bins = pd.cut(data_frame['MODEL_EST_REAL_TEMP_DIFF'], range_list)
        grouped_df = data_frame.groupby(bins)[var_list].agg(['mean', 'std', 'count'])

        msg = "\n\n"
        msg += dataset_handler.DIV_LINE
        msg += "\n\n # Calculate statistical information for high priority variables."
        if logger:
            logger.info(msg)
        self.log = msg
        return grouped_df

def get_out_range_stats_dict(df_data):
    out_range_stats = {}
    for str_data in df_data.EVAL_RANGE_CHECK_VARS:
        if str_data == 'NULL' or str_data == 'None':
            continue
        str_data = str_data.replace(' ', '')
        str_data = str_data[1:-1]
        list_data = str_data.split(',')
        dict_data = {}
        for item in list_data:
            list_key, list_val = item.split(':')
            dict_data[list_key] = list_val
        if dict_data is None:
            continue
        for var_name, val in dict_data.items():
            if var_name not in out_range_stats:
                out_range_stats[var_name] = {'total': 0}
            if val not in out_range_stats[var_name]:
                out_range_stats[var_name][val] = 1
                out_range_stats[var_name]['total'] += 1
            else:
                out_range_stats[var_name][val] += 1
                out_range_stats[var_name]['total'] += 1
    return out_range_stats

def write_dict_to_file(dict_data, fname):
    file_data = ''
    for stat_key, stat_dict in dict_data.items():
        file_data += stat_key + ': ' + str(stat_dict) + '\n'
    with open(fname, 'w') as file:
        file.write(file_data)


def write_dict_dict_to_file(dict_data, fname, key='total'):
    file_data = ''
    for stat_key, stat_dict in dict_data.items():
        file_data += stat_key + ': ' + str(stat_dict[key]) + '\n'
    with open(fname, 'w') as file:
        file.write(file_data)


def generate_distrib_plot(xmin, xmax, margin, real_val, pred_val, c_name, plot_name, save_path="./"):
        xmin = xmin
        xmax = xmax

        under = []
        under.append((xmin - 30) - margin)
        under.append((xmax + 30) - margin)
        ideal = []
        ideal.append(xmin - 30)
        ideal.append(xmax + 30)
        below = []
        below.append((xmin - 30) + margin)
        below.append((xmax + 30) + margin)

        # fig = plt.figure(1)
        fig = plt.figure(figsize=(25, 25))
        # plt.subplot(1, 2, 1)
        plt.plot(ideal, under, color='r', linestyle='--', label='{:3d} margin'.format(int(margin)))
        plt.plot(ideal, below, color='r', linestyle='--')
        plt.xlim(xmin, xmax)
        plt.ylim(xmin, xmax)
        #mod_train_y = np.array([np.random.uniform(-0.5, 0.5) for i in range(len(real_val))]) + real_val
        #mod_predict_y = np.array([np.random.uniform(-0.5, 0.5) for i in range(len(pred_val))]) + pred_val
        eliminate_idx_list = []
        counter = 0
        for (x), val in np.ndenumerate(pred_val):
            if val <= 100:
                eliminate_idx_list.append(x[0])
                counter += 1
        real_val = np.delete(real_val, eliminate_idx_list)
        pred_val = np.delete(pred_val, eliminate_idx_list)

        linear_regressor = LinearRegression()
        linear_regressor.fit(real_val.reshape(-1, 1), pred_val.reshape(-1, 1))
        y_pred = linear_regressor.predict(real_val.reshape(-1, 1))
        plt.plot(real_val.reshape(-1, 1), y_pred, color='blue')

        plt.plot(ideal, ideal, color='r', linewidth=1, label='ideal value')
        plt.scatter(real_val.reshape(-1, 1), pred_val.reshape(-1, 1), color='g', s=0.5, label='real value')

        plt.xlabel('Real value of {}'.format(c_name))
        plt.ylabel('Prediction value of {}'.format(c_name))
        plt.title('{} Distribution Graph'.format(c_name))
        plt.legend()
        plt.show()

        fig_name = c_name + '_' + plot_name
        fig.savefig(save_path + fig_name)
        plt.clf()
        pass


def generate_line_plot(data_frame, var, tar_val, range_val, color='b'):
    """

    :param x_val:
    :param y_val:
    :param max:
    :param c_name:
    :param plot_name:
    :param save_path:
    """
    ax = data_frame[var][tar_val].plot(color=color, linestyle="-", label=var)

    interval_categories = data_frame[var][tar_val].index.categories
    under_tol_range = pd.Interval(left=float(-range_val), right=float(0))
    below_tol_range = pd.Interval(left=float(0), right=float(range_val))

    # Draw outlier vertical line
    for idx, interval_value in enumerate(interval_categories):
        if (interval_value == under_tol_range) or (interval_value == below_tol_range):
            plt.axvline(x=idx, linewidth=1, color='r', linestyle='--')
        else:
            continue

    # plt.plot(x_val, y_val, color=color, linestyle="dashed", label=var)
    plt.legend(shadow=True, fancybox=False, loc="best")
    plt.title('{} value by temp differnece'.format(tar_val))
    plt.xlabel('Range of temp difference')
    for label in ax.xaxis.get_ticklabels():
        label.set_rotation(45)
    plt.ylabel('{} value'.format(tar_val))

    pass


def draw_statistic_plot(data_frame, var_list, range_val, plot_name, save_path="./"):
    tar_val_list = ['mean', 'std']
    for tar_val in tar_val_list:
        for idx, var in enumerate(var_list):
            colors = cm.rainbow(np.linspace(0, 1, len(var_list)+1))
            generate_line_plot(data_frame=data_frame, var=var, tar_val=tar_val, range_val=range_val, color=colors[idx])
        # plt.show()
        plt.tight_layout()
        plt.savefig(save_path + tar_val + '_value_' +  plot_name)
        plt.clf()
    pass


def main():
    """ Data analysis handler main code.
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("--dataset_file", required=True, help="dataset file path")
    parser.add_argument("--var_csv_file", required=True, help="csv file for variables")
    parser.add_argument("--var_ini_file", required=True, help="ini file for configuration")
    parser.add_argument("--check_perf_measure_", default=False, action='store_true', help="Flag to check performance evaluation measure")
    parser.add_argument("--separate_data_", default=False, action='store_true', help="Flag to separate data")
    parser.add_argument("--statistics_", default=False, action='store_true', help="Flag to run statistics function")
    parser.add_argument("--generate_plot_", default=False, action='store_true', help="Flag to run generate plot function")
    parser.add_argument("--generate_histogram_", default=False, action='store_true', help="Flag to run generate histogram function")

    args = parser.parse_args()

    # Read variable, ini and data files
    var_ini = configparser.ConfigParser()
    var_ini.read(args.var_ini_file)

    logger = sys_lib.setup_logger(var_ini['model_info']['model_name'], '_', folder='log', console_=True)

    var_mtx = data_lib.read_csv_file(args.var_csv_file)

    if not os.path.exists(ANALYSIS_FOLER):
        os.makedirs(ANALYSIS_FOLER)

    if var_ini['model_info']['model_name'] == 'CVT' or var_ini['model_info']['model_name'] == 'RH':
        dat_mtx = data_lib.read_csv_file(var_ini['DATASET']['raw_dataset_csv_filename'])
    else:
        model_num_str = var_ini['model_info']['model_number']
        dat_mtx = data_lib.read_csv_file(args.dataset_file)

    start_pos, end_pos = sys_lib.calc_crop_info_from_ini(var_ini['var_info_csv'])
    roi_var_mtx = data_lib.crop_mtx(var_mtx, start_pos, end_pos)

    # Initialize feature info.
    feature = dataset_handler.PoscoTempEstModel(roi_var_mtx)
    feature.init_feat_class(var_ini, offset=start_pos[0])
    feature.dataset = dat_mtx

    feature.refine_dataset_model(model_num=var_ini['model_info']['model_number'],
                                 feature=feature,
                                 mode=dataset_handler.ANALYSIS_MODE)
    feature.refine_dataset_col_by_dataset_order(mode=dataset_handler.ANALYSIS_MODE)  # mode : NORMAL_MODE / ANALYSIS_MODE
    feature.extract_dataset_col_by_priority(var_cnt=len(feature.dataset[0]))

    if EXTRACT_BY_RANGE:
        feature.refine_dataset_row_by_range(dataset_order_list=[0, 100])
        # feature.refine_dataset_row_by_process_type(cat=var_ini['model_info']['model_name'])

    # Create analysis feature object.
    analyisis_feature = DataAnalysis(feature, ini=utils.get_ini_parameters(args.var_ini_file))
    analysis_df = analyisis_feature.apply_dtype_to_data_frame(analyisis_feature.data_frame)

    # Initialize analysis info.
    analyisis_feature.init_analysis_var_info(mode=var_ini['ANALYSIS']['y_type']) # mode : 1 -> start y / 2 -> end y / 3 -> drop y, 4 -> target y

    # Extract row by evaluation condition
    var_cond = True if var_ini['ANALYSIS']['var_cond'].upper() == "TRUE" else False

    analysis_df = analyisis_feature.extract_valid_data_by_eval_condition(data_frame=analysis_df,
                                                                         record_date=analyisis_feature.record_date,
                                                                         var_cond=var_cond)


        # feature.priority_list.remove('TAP_WORK_DATE')
        # feature.priority_list.remove('SM_STEEL_GRD')
        # refine_var_list = feature.priority_list
        # analysis_df = analyisis_feature.refine_data_frame_row_by_range(data_frame=analysis_df, var_list=refine_var_list)
        #
        # refine_dataset_header = analysis_df.columns.tolist()
        # refine_dataset = analysis_df.values.tolist()
        #
        # refine_dataset = [refine_dataset_header] + refine_dataset
        # feature.refine_dataset_col_by_dataset_order(dataset=refine_dataset, mode=dataset_handler.NORMAL_MODE)  # mode : NORMAL_MODE / ANALYSIS_MODE
        # feature.refine_dataset_row_by_range(feature.dataset)
        # sys_lib.print_and_write(feature.log)
        #
        # analysis_df = pd.DataFrame(feature.dataset[1:], columns=feature.dataset[0])

    if args.check_perf_measure_:
        analyisis_feature.check_performance_measure('OUT_RH_BEFORE_DEO_O2', 'RH_BEFORE_DEO_O2',
                                                    30,
                                                    data_frame=analysis_df, logger=logger)
        pass

    if args.separate_data_:
        separate_condition = analysis_df.EVAL_TEMP.astype(float) > 1000
        normal_df = analysis_df[separate_condition == True]
        abnormal_df = analysis_df[separate_condition == False]
        normal_df.to_csv('../sys_temp/analysis/normal_range_1000.csv')
        abnormal_df.to_csv('../sys_temp/analysis/abnormal_range_1000.csv')

    if args.statistics_:
        # Get statistics info
        stat_var_list = feature.priority_list[:5] # select top 5 variable
        grouped_df = analyisis_feature.get_statistics_info(analysis_df, var_list=stat_var_list, range_val=analyisis_feature.tolerance, logger=logger)
        grouped_df.to_csv(ANALYSIS_FOLER + 'data_statistic.csv')

        draw_statistic_plot(data_frame=grouped_df, var_list=stat_var_list, range_val=analyisis_feature.tolerance,
                            plot_name='line_plot', save_path=ANALYSIS_FOLER)

        norm_range_cond = abs(analysis_df.MODEL_EST_REAL_TEMP_DIFF) <= analyisis_feature.tolerance
        norm_range_df = analysis_df[norm_range_cond == True].reset_index(drop=True)
        abnorm_range_df = analysis_df[norm_range_cond == False].reset_index(drop=True)

        describe_list = ['mean', 'std'] # 'min', 'max', 'mean', 'std', 'count', '25%', '50%', '75%'
        norm_range_df.describe(include='all').loc[describe_list].transpose().to_csv(var_ini['ANALYSIS']['norm_csv_filename'])
        abnorm_range_df.describe(include='all').loc[describe_list].transpose().to_csv(var_ini['ANALYSIS']['abnorm_csv_filename'])

        pass

    if args.generate_plot_:
        generate_distrib_plot(xmin=feature.vars['CONV_ENDPNT_TEMP'].min_thresh, xmax=feature.vars['CONV_ENDPNT_TEMP'].max_thresh,
                              margin=model_cvt.TOLERANCE,
                              real_val=analysis_df.CONV_ENDPNT_TEMP.values.astype(np.float), pred_val=analysis_df.EVAL_TEMP.values.astype(np.float),
                              c_name='Temp', plot_name='Temp distribution plot',
                              save_path=ANALYSIS_FOLER)
        pass

    if args.generate_histogram_:

        VAR_NAME = 'RH_SAMPLE_TM'

        refined_series = analysis_df[VAR_NAME].dropna()
        bar_min = refined_series.min() # feature.vars[VAR_NAME].min_thresh
        bar_max = refined_series.max() # feature.vars[VAR_NAME].max_thresh
        bar_array = np.arange(bar_min, bar_max).tolist()

        # Draw outlier vertical line
        [plt.axvline(_x, linewidth=1, color='r', linestyle='--') for _x in [feature.vars[VAR_NAME].min_thresh,
                                                                            feature.vars[VAR_NAME].max_thresh]]
        plt.hist(refined_series, bar_array)
        plt.xlabel(VAR_NAME)
        plt.ylabel('Frequency')
        plt.title('Histogram of {}'.format(VAR_NAME))

        # plt.show()
        plt.savefig(ANALYSIS_FOLER + '{}.png'.format(VAR_NAME))
        plt.clf()

        # plt.hist(norm_range_df['EVAL_TEMP'], bar_array)
        # plt.xlabel('EVAL_TEMP')
        # plt.ylabel('Frequency')
        # plt.title('Histogram of EVAL_TEMP')
        # plt.savefig(ANALYSIS_FOLER + 'EVAL_TEMP.png')
        # plt.clf()

        pass
    pass


if __name__ == "__main__":

    if len(sys.argv) == 1:
        if ANALYSIS_TEST:
            sys.argv.extend([
                             # For temp model
                             # "--var_csv_file", "../sys_temp/cvt_var.csv",
                             # "--var_ini_file", "../sys_temp/cvt_var.ini",

                             # # For temp model
                             # "--var_csv_file", "../sys_temp/rh_var.csv",
                             # "--var_ini_file", "../sys_temp/rh_var.ini",

                             # # For component model
                             # "--var_csv_file", "../sys_component/cvt_macro_var.csv",
                             # "--var_ini_file", "../sys_component/cvt_macro_var.ini",
                             "--dataset_file", "../sys_component/dataset_raw/190717/rh_macro_eval_db.csv",
                             "--var_csv_file", "../sys_component/rh_macro_var.csv",
                             "--var_ini_file", "../sys_component/rh_macro_var.ini",

                             "--check_perf_measure_",
                             # "--separate_data_",
                             # "--statistics_",
                             #"--generate_plot_",
                             #"--generate_histogram_",
                             ])
        else:
            sys.argv.extend(["--~help"])
    main()