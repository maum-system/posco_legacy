#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""
"""
import sys
import os
# os.environ["CUDA_VISIBLE_DEVICES"]="-1" # CPU/GPU option

from operator import itemgetter
import matplotlib.pyplot as plt
import time
from sklearn.ensemble import RandomForestRegressor, BaggingRegressor
from sklearn.tree import DecisionTreeRegressor
from lightgbm.sklearn import LGBMRegressor, LGBMClassifier
from xgboost.sklearn import XGBRegressor, XGBClassifier
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier
from sklearn.tree import DecisionTreeClassifier

from sklearn.multioutput import MultiOutputRegressor
from sklearn.svm import LinearSVR
from sklearn.metrics import mean_squared_error, r2_score
from sklearn.model_selection import GridSearchCV
import scipy.stats as st
import pickle
import numpy as np
from math import sqrt
import tensorflow as tf

from lib import data as data_lib
from handlers import dataset_handler as Dataset
from lib import learning as learning_lib

from tensorflow.python.client import device_lib

__author__ = "Hoon Paek, Dong-gi Kim, Ji-young Moon"
__copyright__ = "Copyright 2007, The Cogent Project"
__credits__ = []
__license__ = "GPL"
__version__ = "0.0.1"
__maintainer__ = "Hoon Paek"
__email__ = "hoon.paek@mindslab.ai"
__status__ = "Prototype"  # Prototype / Development / Production

SVR_C = 1.0
SVR_epsilon = 0.1


class MachineLearner(object):
    """

    """
    def __init__(self, Feat=None, dataset=None, out_idx=0):
        self.Feat = Feat
        self.dataset = dataset
        self.out_idx = 0
        self.log = ""
        self.est_thresh = 0.0

        self.X_name = None
        self.y_name = None
        self.y_num = 0
        self.X_dataset = None
        self.y_dataset = None
        self.y_predict = None
        self.params = None

        self.train_mse = 0
        self.train_mse_2 = 0
        self.train_rmse = 0
        self.train_rmsre = 0
        self.train_rmsre_2 = 0
        self.train_r2_score = 0

        self.test_mse = 0
        self.test_mse_2 = 0
        self.test_rmse = 0
        self.test_rmsre = 0
        self.test_rmsre_2 = 0
        self.test_r2_score = 0

        self.train_acc = 0
        self.train_acc_2 = 0
        self.test_acc = 0
        self.test_acc_2 = 0

        self.acc_thresh = 0
        self.X = [[], []]
        self.y = [[], []]

        self.model = None

    def get_log(self):
        return self.log

    def split_dataset_into_train_and_test(self, dataset_ratio=70):
        """ Split dataset into train and test.

        :param dataset_ratio:
        :return:
        """
        dataset_train, dataset_test = learning_lib.split_dataset_into_train_and_test(self.dataset, dataset_ratio, offset=1)
        trans_dataset_train = data_lib.transpose_list(dataset_train)
        trans_dataset_test  = data_lib.transpose_list(dataset_test)
        self.X[0] = data_lib.transpose_list(trans_dataset_train[1:])[1:]
        self.X[1] = data_lib.transpose_list(trans_dataset_test[1:])[1:]
        self.y[0] = trans_dataset_train[0][1:]
        self.y[1] = trans_dataset_test[0][1:]
        self.X_name = self.dataset[0][1:]
        self.y_name = self.dataset[0][0]
        self.y_num = 1


    def split_dataset_into_train_and_test_for_multiple_y(self, dataset_ratio=70, num_y=2):
        """ Split dataset into train and test.
        :param dataset_ratio:
        :return:
        """
        dataset_train, dataset_test = learning_lib.split_dataset_into_train_and_test(self.dataset, dataset_ratio, offset=1)
        # dataset_train, dataset_test = learning_lib.split_dataset_into_train_and_test_by_k_fold(self.dataset,
        #                                                                                   dataset_ratio,
        #                                                                                   num_y,
        #                                                                                   offset=1,
        #                                                                                   k_fold_idx=3,
        #                                                                                   k_fold_area_idx=3)
        trans_dataset_train = data_lib.transpose_list(dataset_train)
        trans_dataset_test = data_lib.transpose_list(dataset_test)

        self.X[0] = data_lib.transpose_list(trans_dataset_train[num_y:])[1:]
        self.X[1] = data_lib.transpose_list(trans_dataset_test[num_y:])[1:]
        self.y[0] = data_lib.transpose_list(trans_dataset_train[:num_y])[1:]
        self.y[1] = data_lib.transpose_list(trans_dataset_test[:num_y])[1:]
        self.X_name = self.dataset[0][num_y:]
        self.y_name = self.dataset[0][:num_y]
        self.y_num = num_y

        self.X_dataset = self.X[0] + self.X[1]
        self.y_dataset = self.y[0] + self.y[1]

        pass


    def encode_symbol_dataset_by_int(self, dataset=None):
        """ Encode symbol dataset by integer

        :param dataset:
        :return:
        """
        if not dataset:
            dataset = self.dataset

        trans_dataset = data_lib.transpose_list(dataset)
        arr = []
        for col in trans_dataset:
            if col[0] not in self.Feat.vars:
                continue
            if data_lib.is_class_name(self.Feat.vars[col[0]], 'NumberClass'):
                try:
                    arr.append([col[0]] + [float(x) for x in col[1:]])
                except (TypeError, ValueError) as e:
                    print(e)
                    print(col[0])
            else:
                try:
                    arr.append([col[0]] + [self.Feat.vars[col[0]].domain.index(x) for x in col[1:]])
                except (TypeError, ValueError) as e:
                    print(e)
                    print(col[0])

        self.dataset = data_lib.transpose_list(arr)

        return self.dataset

    def decode_symbol_dataset_by_str_domain(self, dataset=None, target_var=None):
        """ Decode symbol dataset by str domain

        :param dataset:
        :return:
        """
        if not dataset:
            dataset = self.dataset

        trans_dataset = data_lib.transpose_list(dataset)
        arr = []
        for col in trans_dataset:
            if col[0] not in self.Feat.vars:
                continue
            if data_lib.is_class_name(self.Feat.vars[col[0]], 'NumberClass'):
                continue
            else:
                try:
                    if target_var:
                        arr.append([col[0]] + [self.Feat.vars[target_var].domain[x] for x in col[1:]])
                        break
                    else:
                        arr.append([col[0]] + [self.Feat.vars[col[0]].domain[x] for x in col[1:]])
                except (TypeError, ValueError) as e:
                    print(e)
                    print(col[0])

        self.dataset = data_lib.transpose_list(arr)

        return self.dataset

    def encode_symbol_dataset_by_one_hot_encoder(self, dataset=None):
        """ Encode symbol dataset by integer

        :param dataset:
        :return:
        """
        if not dataset:
            dataset = self.dataset

        trans_dataset = data_lib.transpose_list(dataset)
        arr = []
        for col in trans_dataset:
            if data_lib.is_class_name(self.Feat.vars[col[0]], 'NumberClass'):
                arr.append(col)
            else:
                width = len(self.Feat.vars[col[0]].domain)
                sub_arr = [[]]
                for i in range(width):
                    sub_arr[-1].append("{}__{:d}".format(col[0], i))
                for val in col[1:]:
                    sub_arr.append([0.]*width)
                    try:
                        sub_arr[-1][self.Feat.vars[col[0]].domain.index(val)] = 1.
                    except ValueError as e:
                        print(e)
                trans_sub_arr = data_lib.transpose_list(sub_arr)
                for i in range(width):
                    arr.append(trans_sub_arr[i])
        self.dataset = data_lib.transpose_list(arr)

        return self.dataset

    def normalize_number_features(self, dataset=None, min_val=-1, max_val=1, offset=0):
        """ Normalize number features by pre-defined range

        :param dataset:
        :param min_val:
        :param max_val:
        :return:
        """
        if not dataset:
            dataset = self.dataset

        trans_dataset = data_lib.transpose_list(dataset)

        # arr = [[trans_dataset[0][0]] + [float(x) for x in trans_dataset[0][1:]]]
        arr = []
        for col in trans_dataset[offset:]:
            if data_lib.is_class_name(self.Feat.vars[col[0]], 'NumberClass'):
                arr.append([col[0]])
                ptr = self.Feat.vars[col[0]]
                val_range = float(ptr.max_thresh - ptr.min_thresh)
                if val_range == 0:
                    print(col[0])
                    for val in col[1:]:
                        arr[-1].append(float(val))
                else:
                    for val in col[1:]:
                        # print(val)

                        try:
                            val = float(val)
                        except ValueError:
                            val = 0
                        if val < ptr.min_thresh:
                            val = ptr.min_thresh
                        elif val > ptr.max_thresh:
                            val = ptr.max_thresh
                        # if val_range == 0:
                        #    arr[-1].append(col[1:])
                        # else:
                        arr[-1].append((val - ptr.min_thresh) / val_range * (max_val - min_val) + min_val)
            else:
                arr.append(col)

        self.dataset = data_lib.transpose_list(arr)

        return self.dataset

    def denormalize_output(self, predicted_output, real_max, real_min, norm_min=-1, norm_max=1):
        return (predicted_output - norm_min) / (norm_max - norm_min) * (real_max - real_min) + real_min

    def check_est_accuracy(self, error_arr, est_thresh=None):
        """ Check estimation accuracy based on estimation threshold.

        :param error_arr:
        :param est_thresh:
        :return:
        """
        if not est_thresh:
            est_thresh = self.est_thresh

        if self.y_num == 1:
            acc_arr = [x < est_thresh for x in error_arr]
            res_acc_arr = 100. * sum(acc_arr) / float(len(acc_arr))
        else:
            res_acc_arr = []
            for i in range(self.y_num):
                acc_arr = [x < int(est_thresh[i]) for x in error_arr[:,i]]
                res_acc_arr.append(100. * sum(acc_arr) / float(len(acc_arr)))

        if len(acc_arr) == 0:
            return 99.99
        else:
            return res_acc_arr

    def draw_ABS_error_plot(self, err_values, acc_values, est_margin_values, category_list, plot_filename_prefix='', plot_=True, num=2):

        for i in range(num):
            train_err, test_err = err_values[i]
            train_acc, test_acc = acc_values[i]
            est_margin = est_margin_values[i]
            category = category_list[i]

            max_range = max(max(train_err), max(test_err), est_margin) + 1
            fig = plt.figure()
            plt.subplot(1,2,1)
            plt.plot(train_err, 'b', [est_margin,] * len(train_err), 'r')
            plt.ylim(0, max_range)
            plt.title('{} Training ABS error({:2d}%)'.format(category, int(train_acc)))
            plt.subplot(1,2,2)
            plt.plot(test_err, 'b', [est_margin,] * len(test_err), 'r')
            plt.ylim(0, max_range)
            plt.title('{} Test ABS error({:2d}%)'.format(category, int(test_acc)))
            if plot_filename_prefix:
                fig.savefig(plot_filename_prefix)
            if plot_:
                plt.show()
            else:
                plt.close(fig)

        pass

    def find_rfr_feature_importance(self, y_name_list=None):

        global msg
        y_num = len(y_name_list)
        for i, y_name in enumerate(y_name_list):
            mdl = self.model
            feat_impt = [[self.dataset[0][y_num+idx], mdl.estimators_[i].feature_importances_[idx]] for idx in range(len(self.dataset[0][y_num:]))]
            sorted_feat_impt = sorted(feat_impt, key=itemgetter(1), reverse=True)

            msg = "\n   > [{}] Sorted feature importance".format(y_name)
            for idx, content in enumerate(sorted_feat_impt):
                msg += "\n   > {:d}. {:<30} : {:6.12f}".format(idx+1, content[0], content[1])

            self.log += msg
            # print(self.log)

            # Draw feature importance
            # index =
            # plt.barh(index, mdl[i].feature_importances_, align='center')
            # plt.yticks(index, cancer.feature_names)
            # plt.ylim(-1, n_feature)
            # plt.xlabel('feature importance', size=15)
            # plt.ylabel('feature', size=15)
            # plt.show()


        pass

    def generate_plot(self, model):

        if self.y_num == 1:
            if data_lib.is_class_name(self.Feat.vars[self.y_name], "NumberClass"):
                xmin = self.Feat.vars[self.y_name].min_thresh
                xmax = self.Feat.vars[self.y_name].max_thresh
                self.gen_plot(xmin=xmin, xmax=xmax, margin=self.acc_thresh,
                              train_val=np.array(self.y[0]), test_val=np.array(self.y[1]),
                              train_pred_val=np.array(self.y_predict[0]),
                              test_pred_val=np.array(self.y_predict[1]),
                              train_acc=self.train_acc, test_acc=self.test_acc,
                              c_name=self.y_name, plot_name='Data Distribution Graph', method=model)
            else:
                xmin = 0
                xmax = len(self.Feat.vars[self.y_name].domain)
                self.gen_plot(xmin=xmin, xmax=xmax, margin=self.acc_thresh,
                              train_val=np.array(self.y[0]), test_val=np.array(self.y[1]),
                              train_pred_val=np.array(self.y_predict[0]),
                              test_pred_val=np.array(self.y_predict[1]),
                              train_acc=self.train_acc, test_acc=self.test_acc,
                              c_name=self.y_name, plot_name='Data Distribution Graph', method=model)
        else:
            for j in range(self.y_num):
                xmin = self.Feat.vars[self.y_name[j]].min_thresh
                xmax = self.Feat.vars[self.y_name[j]].max_thresh
                self.gen_plot(xmin=xmin, xmax=xmax, margin=self.acc_thresh[j],
                              train_val=np.array(self.y[0])[:, j], test_val=np.array(self.y[1])[:, j],
                              train_pred_val=np.array(self.y_predict[0])[:, j],
                              test_pred_val=np.array(self.y_predict[1])[:, j],
                              train_acc=self.train_acc[j], test_acc=self.test_acc[j],
                              c_name=self.y_name[j], plot_name='Data Distribution Graph', method=model)
        pass

    def gen_plot(self, xmin, xmax, margin, train_val, test_val, train_pred_val, test_pred_val, train_acc, test_acc, c_name, plot_name,
                 method):

        xmin = xmin
        xmax = xmax

        under = []
        under.append((xmin - 30) - margin)
        under.append((xmax + 30) - margin)
        ideal = []
        ideal.append(xmin - 30)
        ideal.append(xmax + 30)
        below = []
        below.append((xmin - 30) + margin)
        below.append((xmax + 30) + margin)

        fig = plt.figure(1)
        fig = plt.figure(figsize=(25, 8))
        plt.subplot(1, 2, 1)
        plt.plot(ideal, under, color='r', linestyle='--', label='{:3d}ppm margin'.format(int(margin)))
        plt.plot(ideal, below, color='r', linestyle='--')
        plt.xlim(xmin, xmax)
        plt.ylim(xmin, xmax)
        mod_train_y = np.array([np.random.uniform(-0.5, 0.5) for i in range(len(train_val))]) + train_val
        mod_predict_y = np.array([np.random.uniform(-0.5, 0.5) for i in range(len(train_pred_val))]) + train_pred_val
        plt.plot(ideal, ideal, color='r', linewidth=1, label='ideal value')
        plt.scatter(mod_train_y, mod_predict_y, color='g', s=0.5, label='real value')
        plt.xlabel('Real value of {}'.format(c_name))
        plt.ylabel('Prediction value of {}'.format(c_name))
        plt.title('Training results of {} ({:2d}%)   {} Distribution Graph'.format(method, int(train_acc), c_name))
        plt.legend()

        plt.subplot(1, 2, 2)
        plt.plot(ideal, under, color='r', linestyle='--', label='{:3d}ppm margin'.format(int(margin)))
        plt.plot(ideal, below, color='r', linestyle='--')
        plt.xlim(xmin, xmax)
        plt.ylim(xmin, xmax)
        mod_test_y = np.array([np.random.uniform(-0.5, 0.5) for i in range(len(test_val))]) + test_val
        mod_predict_test_y = np.array(
            [np.random.uniform(-0.5, 0.5) for i in range(len(test_pred_val))]) + test_pred_val
        plt.plot(ideal, ideal, color='r', linewidth=1, label='ideal value')
        plt.scatter(mod_test_y, mod_predict_test_y, color='g', s=0.5, label='real value')
        plt.xlabel('Real value of {} (ppm)'.format(c_name))
        plt.ylabel('Prediction value of {} (ppm)'.format(c_name))
        plt.title('Test results of {} ({:2d}%)   {} Distribution Graph'.format(method, int(test_acc), c_name))
        plt.legend()

        fig_name = c_name + '_' + plot_name
        fig.savefig(fig_name)

        pass

    def random_forest_regressor(self, ml_cfg):
        """ Run random forest regressor with various environments.

        :param dataset_ratio:
        :param n_estimators:
        :param est_thresh:
        :return:
        """
        rfr_cfg = ml_cfg['RFR']
        msg = "\n # Random Forest Regression"
        n_estimators = int(rfr_cfg['n_estimators'])
        min_samples_leaf = int(rfr_cfg['min_samples_leaf'])

        if rfr_cfg['max_depth'] == "none":
            max_depth = None
        else:
            max_depth = int(rfr_cfg['max_depth'])

        if rfr_cfg['max_leaf_nodes'] == "none":
            max_leaf_nodes = None
        else:
            max_leaf_nodes = int(rfr_cfg['max_leaf_nodes'])

        max_features = rfr_cfg['max_features']
        if max_features != "auto" and max_features != "sqrt" and max_features != "log2" :
            max_features = int(max_features)
        # if rfr_cfg['max_features'] == "num_feat":
        #     max_features = len(self.X[0])
        # elif rfr_cfg['max_features'] == "sqrt":
        #     max_features = math.sqrt(len(self.X[0]))
        # elif rfr_cfg['max_features'] == "log2":
        #     max_features = math.log2(len(self.X[0]))
        # else:
        #     max_features = int(rfr_cfg['max_features'])

        est_thresh = float(rfr_cfg['est_thresh'])
        self.acc_thresh = est_thresh

        GRID_SEARCH_ = False
        if GRID_SEARCH_:
            params = {
                "min_samples_leaf": [1, 5, 10, 50, 100, 200, 500],
                "max_depth": [None, 10, 25, 50, 100, 200, 500],
                "max_leaf_nodes": [ None, 10, 25, 50, 100, 200, 500],
            }

            mdl = RandomForestRegressor(n_estimators=n_estimators, max_features=max_features, n_jobs=-1, oob_score=True)
            grid = GridSearchCV(estimator=mdl, param_grid=params, cv=5)
            grid = grid.fit(self.X[0], self.y[0])
            print(grid.best_score_)
            print(grid.best_params_)
            print('Parameter tuning complete')

        else:
            mdl = RandomForestRegressor(n_estimators=n_estimators, min_samples_leaf=min_samples_leaf,
                                        max_depth=max_depth, max_leaf_nodes=max_leaf_nodes,
                                        max_features=max_features)
        mdl.fit(self.X[0], self.y[0])
        self.model = mdl

        if ml_cfg['general']['print_est_time'].upper() == 'ON':
            # 모델의 예측속도 측정
            start_time = time.time()
            mdl_predict = mdl.predict(self.X[0][0])
            print(" ## Model's prediction time : {:.3f} sec".format(time.time() - start_time))

        pred_y = []
        est_err = []
        est_acc = []
        mape_arr = []
        rmse_arr = []
        r2_score_arr = []

        for i in range(2):
            pred_y.append(mdl.predict(self.X[i]))
            # pred_y.append(mdl.predict(self.X[i]) + np.random.normal(0,sigma[i],len(self.X[i])))
            est_err.append(abs(pred_y[i] - np.array(self.y[i])))
            est_acc.append(self.check_est_accuracy(est_err[-1], est_thresh))
            mape_arr.append(learning_lib.calc_MAPE(self.y[i], pred_y[-1]))
            rmse_arr.append(learning_lib.calc_RMSE(self.y[i], pred_y[-1]))

        self.y_predict = pred_y

        self.train_acc = self.check_est_accuracy(est_err[0], est_thresh)
        self.test_acc = self.check_est_accuracy(est_err[1], est_thresh)

        self.train_mse = mean_squared_error(self.y[0], pred_y[0].tolist())
        self.test_mse = mean_squared_error(self.y[1], pred_y[1].tolist())

        self.train_rmse = sqrt(mean_squared_error(self.y[0], pred_y[0].tolist()))
        self.test_rmse = sqrt(mean_squared_error(self.y[1], pred_y[1].tolist()))

        self.train_r2_score = r2_score(self.y[0], pred_y[0].tolist())
        self.test_r2_score = r2_score(self.y[1], pred_y[1].tolist())

        msg += "\n # train acc : {:4.2f}, test acc : {:4.2f}".format(self.train_acc, self.test_acc)
        msg += "\n # train rmse : {:4.2f}, test rmse : {:4.2f}".format(self.train_rmse, self.test_rmse)
        msg += "\n # train r2 score : {:4.2f}, test r2 score : {:4.2f}".format(self.train_r2_score, self.test_r2_score)

        self.log = msg
        self.params = rfr_cfg

        return self.train_acc, self.test_acc

    def light_gbm_regressor(self, ml_cfg):
        """ Run light gbm regressor with various environments.

        :param dataset_ratio:
        :param n_estimators:
        :param est_thresh:
        :return:
        """
        lgbm_cfg = ml_cfg['LGBMR']
        msg = "\n # Light GBM Regression"

        n_estimators = int(lgbm_cfg['n_estimators'])
        learning_rate = float(lgbm_cfg['learning_rate'])
        num_leaves = int(lgbm_cfg['num_leaves'])
        max_depth = int(lgbm_cfg['max_depth'])
        est_thresh = float(lgbm_cfg['est_thresh'])

        self.acc_thresh = est_thresh

        GRID_SEARCH_ = False
        if GRID_SEARCH_:
            param_grid = {
                'learning_rate': [0.01, 0.1, 0.05, 0.5, 1],
                'n_estimators': [100, 300, 500, 1000],
                'num_leaves': [10, 50, 100, 200, 500],
                "max_depth": [10, 50, 100, 200, 500],
            }

            mdl = LGBMRegressor(n_estimators=n_estimators, learning_rate=learning_rate, num_leaves=num_leaves, max_depth=max_depth, n_jobs=-1)
            grid = GridSearchCV(estimator=mdl, param_grid=param_grid, cv=5)
            grid = grid.fit(self.X[0], self.y[0])
            print('Best score is {:4.4f}' .format(grid.best_score_))
            print('Best params is {}' .format(str(grid.best_params_)))
            print('Parameter tuning complete')

        else:
            mdl = LGBMRegressor(n_estimators=n_estimators, learning_rate=learning_rate)


        mdl.fit(self.X[0], self.y[0])
        self.model = mdl

        pred_y = []
        est_err = []
        est_acc = []
        mape_arr = []
        rmse_arr = []


        for i in range(2):
            pred_y.append(mdl.predict(self.X[i]))
            est_err.append(abs(pred_y[i] - np.array(self.y[i])))
            est_acc.append(self.check_est_accuracy(est_err[-1], est_thresh))
            mape_arr.append(learning_lib.calc_MAPE(self.y[i], pred_y[-1]))
            rmse_arr.append(learning_lib.calc_RMSE(self.y[i], pred_y[-1]))

        self.y_predict = pred_y
        self.train_acc = self.check_est_accuracy(est_err[0], est_thresh)
        self.test_acc = self.check_est_accuracy(est_err[1], est_thresh)
        self.train_mse = mean_squared_error(self.y[0], pred_y[0].tolist())
        self.test_mse = mean_squared_error(self.y[1], pred_y[1].tolist())

        msg += "\n # train acc : {:4.2f}, test acc : {:4.2f}".format(self.train_acc, self.test_acc)
        msg += "\n # train mse : {:4.2f}, test mse : {:4.2f}".format(self.train_mse, self.test_mse)

        self.log = msg
        self.params = lgbm_cfg
        print(msg)


        # if True:
        #     self.gen_plot()

        return self.train_acc, self.test_acc

    def light_gbm_regressor_for_multiple_y(self, ml_cfg):
        """ Run light gbm regressor with various environments.

        :param dataset_ratio:
        :param n_estimators:
        :param est_thresh:
        :return:
        """
        lgbm_cfg = ml_cfg['LGBMR']
        msg = "\n # Light GBM Regression"
        estimator__n_estimators = int(lgbm_cfg['n_estimators'])
        estimator__learning_rate = float(lgbm_cfg['learning_rate'])
        estimator__num_leaves = int(lgbm_cfg['num_leaves'])
        estimator__max_depth = int(lgbm_cfg['max_depth'])

        est_thresh = lgbm_cfg['est_thresh'].split(',')
        self.est_thresh = [float(thresh) for thresh in est_thresh]

        self.acc_thresh = est_thresh

        GRID_SEARCH_ = False
        if GRID_SEARCH_:
            param_grid = {
                'estimator__learning_rate': [0.1, 0.05, 0.5, 1],
                'estimator__n_estimators': [300, 500, 1000, 1200, 1300, 1500],
                'estimator__num_leaves': [10, 20, 30, 40, 50, 100, 200],
                "estimator__max_depth": [10, 20, 30, 40, 50, 100, 200]
            }

            mdl = MultiOutputRegressor(LGBMRegressor(n_estimators=estimator__n_estimators,
                                                     learning_rate=estimator__learning_rate,
                                                     num_leaves=estimator__num_leaves,
                                                     max_depth=estimator__max_depth, n_jobs=-1))
            grid = GridSearchCV(estimator=mdl, param_grid=param_grid, cv=5)
            grid = grid.fit(self.X[0], self.y[0])
            print('Best score is {:4.4f}' .format(grid.best_score_))
            print('Best params is {}' .format(str(grid.best_params_)))
            print('Parameter tuning complete')

        else:
            mdl = MultiOutputRegressor(LGBMRegressor(n_estimators=estimator__n_estimators,
                                                     learning_rate=estimator__learning_rate))


        mdl.fit(self.X[0], self.y[0])
        self.model = mdl


        pred_y = []
        est_err = []
        est_acc = []
        mse_arr = []
        rmse_arr = []
        r2_score_arr = []

        for i in range(2):  # Train & Test
            pred_y.append(mdl.predict(self.X[i]))
            est_err.append(abs(pred_y[i] - np.array(self.y[i])))
            est_acc.append(self.check_est_accuracy(est_err[i], self.est_thresh))
            mse_arr.append(mean_squared_error(self.y[i], pred_y[i]))
            rmse_arr.append(learning_lib.calc_RMSE(self.y[i], pred_y[i]))
            r2_score_arr.append(r2_score(self.y[i], pred_y[i], multioutput='raw_values'))

        self.y_predict = pred_y

        self.train_acc, self.test_acc = est_acc
        self.train_mse, self.test_mse = mse_arr
        self.train_rmse, self.test_rmse = rmse_arr
        self.train_r2_score, self.test_r2_score = r2_score_arr

        for j in range(self.y_num):  # For multiple y value
            msg += "\n # type : {}, train acc : {:4.2f}, test acc : {:4.2f}".format(self.y_name[j], self.train_acc[j], self.test_acc[j])
            msg += "\n # type : {}, train rmse : {:4.2f}, test rmse : {:4.2f}".format(self.y_name[j], self.train_rmse[j], self.test_rmse[j])
            msg += "\n # type : {}, train r2 score : {:4.2f}, test r2 score : {:4.2f}".format(self.y_name[j], self.train_r2_score[j], self.test_r2_score[j])

        return self.train_acc, self.test_acc

    def light_gbm_classifier(self, ml_cfg):
        """ Run light gbm regressor with various environments.

        :param dataset_ratio:
        :param n_estimators:
        :param est_thresh:
        :return:
        """
        lgbm_cfg = ml_cfg['LGBMC']
        msg = "\n # Light GBM Classification"
        n_estimators = int(lgbm_cfg['n_estimators'])
        learning_rate = float(lgbm_cfg['learning_rate'])
        num_leaves = int(lgbm_cfg['num_leaves'])
        max_depth = int(lgbm_cfg['max_depth'])
        est_thresh = float(lgbm_cfg['est_thresh'])

        self.acc_thresh = est_thresh

        GRID_SEARCH_ = False
        if GRID_SEARCH_:
            param_grid = {
                'learning_rate': [0.01, 0.1, 0.05, 0.5, 1],
                'n_estimators': [100, 300, 500, 1000],
                'num_leaves': [3, 5, 10, 20, 100],
                "max_depth":  [3, 5, 10, 20, 100],
            }

            mdl = LGBMClassifier(n_estimators=n_estimators, learning_rate=learning_rate, num_leaves=num_leaves, max_depth=max_depth, n_jobs=-1)
            grid = GridSearchCV(estimator=mdl, param_grid=param_grid, cv=5)
            grid = grid.fit(self.X[0], self.y[0])
            print('Best score is {:4.4f}' .format(grid.best_score_))
            print('Best params is {}' .format(str(grid.best_params_)))
            print('Parameter tuning complete')

        else:
            mdl = LGBMClassifier(n_estimators=n_estimators, learning_rate=learning_rate)


        mdl.fit(self.X[0], self.y[0])
        self.model = mdl

        pred_y = []
        est_err = []
        est_acc = []
        mape_arr = []
        rmse_arr = []
        r2_score_arr = []

        for i in range(2):
            pred_y.append(mdl.predict(self.X[i]))
            est_err.append(abs(pred_y[i] - np.array(self.y[i])))
            est_acc.append(self.check_est_accuracy(est_err[-1], est_thresh))
            mape_arr.append(learning_lib.calc_MAPE(self.y[i], pred_y[-1]))
            rmse_arr.append(learning_lib.calc_RMSE(self.y[i], pred_y[-1]))
            r2_score_arr.append(r2_score(self.y[i], pred_y[i]))

        self.y_predict = pred_y
        self.train_acc, self.test_acc = est_acc
        self.train_rmse, self.test_rmse = rmse_arr
        self.train_r2_score, self.test_r2_score = r2_score_arr

        msg += "\n # train acc : {:4.2f}, test acc : {:4.2f}".format(self.train_acc, self.test_acc)
        msg += "\n # train rmse : {:4.2f}, test rmse : {:4.2f}".format(self.train_rmse, self.test_rmse)
        msg += "\n # train r2 score : {:4.2f}, test r2 score : {:4.2f}".format(self.train_r2_score, self.test_r2_score)

        self.log = msg
        self.params = lgbm_cfg

        # if True:
        #     self.gen_plot()

        return

        self.log = msg



    def xgboost_regressor(self, ml_cfg):
        """ Run xgboost regressor with various environments.

        :param dataset_ratio:
        :param n_estimators:
        :param est_thresh:
        :return:
        """
        xgboost_cfg = ml_cfg['XGBR']
        msg = "\n # XGBoost Regression"
        n_estimators = int(xgboost_cfg['n_estimators'])
        learning_rate = float(xgboost_cfg['learning_rate'])
        est_thresh = float(xgboost_cfg['est_thresh'])

        self.acc_thresh = est_thresh

        one_to_left = st.beta(10, 1)
        from_zero_positive = st.expon(0, 50)
        GRID_SEARCH_ = False
        if GRID_SEARCH_:
            param_grid = {
                'n_estimators': [500, 1000, 1500],# [50, 100, 200, 300, 400, 500],
                'learning_rate': [0.1], # [0.01, 0.1, 0.05, 0.5, 1],
                # 'max_depth' : [5, 10, 20, 40, 60],
                # 'colsample_bytree' : one_to_left,
                # 'subsample' : one_to_left,
                # 'gamma' : st.uniform(0, 10),
                # 'reg_alpha' : from_zero_positive,
                # 'min_child_weight' : from_zero_positive
            }

            mdl = XGBRegressor(n_estimators=n_estimators, learning_rate=learning_rate, n_jobs=-1)
            grid = GridSearchCV(estimator=mdl, param_grid=param_grid, cv=5)
            grid = grid.fit(self.X[0], self.y[0])
            print('Best score is {:4.4f}' .format(sum(grid.best_estimator_.predict(self.X[1]) == self.y[1]) / (len(self.y[1])*1.0)))
            print('Best params is {}' .format(str(grid.best_params_)))
            print('Parameter tuning complete')

        else:
            mdl = XGBRegressor(n_estimators=n_estimators, learning_rate=learning_rate)


        mdl.fit(self.X[0], self.y[0])
        self.model = mdl

        pred_y = []
        est_err = []
        est_acc = []
        mape_arr = []
        rmse_arr = []


        for i in range(2):
            pred_y.append(mdl.predict(self.X[i]))
            est_err.append(abs(pred_y[i] - np.array(self.y[i])))
            est_acc.append(self.check_est_accuracy(est_err[-1], est_thresh))
            mape_arr.append(learning_lib.calc_MAPE(self.y[i], pred_y[-1]))
            rmse_arr.append(learning_lib.calc_RMSE(self.y[i], pred_y[-1]))

        self.y_predict = pred_y
        self.train_acc = self.check_est_accuracy(est_err[0], est_thresh)
        self.test_acc = self.check_est_accuracy(est_err[1], est_thresh)
        self.train_mse = mean_squared_error(self.y[0], pred_y[0].tolist())
        self.test_mse = mean_squared_error(self.y[1], pred_y[1].tolist())

        msg += "\n # train acc : {:4.2f}, test acc : {:4.2f}".format(self.train_acc, self.test_acc)
        msg += "\n # train mse : {:4.2f}, test mse : {:4.2f}".format(self.train_mse, self.test_mse)

        self.log = msg
        self.params = xgboost_cfg
        print(msg)

        # if True:
        #     self.gen_plot()

    def xgboost_regressor_for_multiple_y(self, ml_cfg):
        """ Run xgboost regressor with various environments.

        :param dataset_ratio:
        :param n_estimators:
        :param est_thresh:
        :return:
        """
        xgboost_cfg = ml_cfg['XGBR']
        msg = "\n # XGBoost Regression"
        n_estimators = int(xgboost_cfg['n_estimators'])
        learning_rate = float(xgboost_cfg['learning_rate'])

        est_thresh = xgboost_cfg['est_thresh'].split(',')
        self.est_thresh = [float(thresh) for thresh in est_thresh]

        self.acc_thresh = est_thresh

        one_to_left = st.beta(10, 1)
        from_zero_positive = st.expon(0, 50)
        GRID_SEARCH_ = False
        if GRID_SEARCH_:
            param_grid = {
                'n_estimators': [500, 1000, 1500],# [50, 100, 200, 300, 400, 500],
                'learning_rate': [0.1], # [0.01, 0.1, 0.05, 0.5, 1],
                # 'max_depth' : [5, 10, 20, 40, 60],
                # 'colsample_bytree' : one_to_left,
                # 'subsample' : one_to_left,
                # 'gamma' : st.uniform(0, 10),
                # 'reg_alpha' : from_zero_positive,
                # 'min_child_weight' : from_zero_positive
            }

            mdl = MultiOutputRegressor(XGBRegressor(n_estimators=n_estimators, learning_rate=learning_rate, n_jobs=-1))
            grid = GridSearchCV(estimator=mdl, param_grid=param_grid, cv=5)
            grid = grid.fit(self.X[0], self.y[0])
            print('Best score is {:4.4f}' .format(sum(grid.best_estimator_.predict(self.X[1]) == self.y[1]) / (len(self.y[1])*1.0)))
            print('Best params is {}' .format(str(grid.best_params_)))
            print('Parameter tuning complete')

        else:
            mdl = MultiOutputRegressor(XGBRegressor(n_estimators=n_estimators, learning_rate=learning_rate))


        mdl.fit(self.X[0], self.y[0])
        self.model = mdl

        pred_y = []
        est_err = []
        est_acc = []
        mse_arr = []
        rmse_arr = []
        r2_score_arr = []

        for i in range(2):  # Train & Test
            pred_y.append(mdl.predict(self.X[i]))
            est_err.append(abs(pred_y[i] - np.array(self.y[i], dtype='float64')))
            est_acc.append(self.check_est_accuracy(est_err[i], self.est_thresh))
            mse_arr.append(mean_squared_error(np.array(self.y[i], dtype='float64'), pred_y[i]))
            rmse_arr.append(learning_lib.calc_RMSE(np.array(self.y[i], dtype='float64'), pred_y[i]))
            r2_score_arr.append(r2_score(np.array(self.y[i], dtype='float64'), pred_y[i], multioutput='raw_values'))

        self.y_predict = pred_y

        self.train_acc, self.test_acc = est_acc
        self.train_mse, self.test_mse = mse_arr
        self.train_rmse, self.test_rmse = rmse_arr
        self.train_r2_score, self.test_r2_score = r2_score_arr

        for j in range(self.y_num):  # For multiple y value
            msg += "\n # type : {}, train acc : {:4.2f}, test acc : {:4.2f}".format(self.y_name[j], self.train_acc[j],
                                                                                    self.test_acc[j])
            msg += "\n # type : {}, train rmse : {:4.2f}, test rmse : {:4.2f}".format(self.y_name[j],
                                                                                      self.train_rmse[j],
                                                                                      self.test_rmse[j])
            msg += "\n # type : {}, train r2 score : {:4.2f}, test r2 score : {:4.2f}".format(self.y_name[j],
                                                                                              self.train_r2_score[j],
                                                                                              self.test_r2_score[j])
        self.log = msg

        return self.train_acc, self.test_acc
        # if True:
        #     self.gen_plot()
        
    def xgboost_classifier(self, ml_cfg):
        """ Run xgboost regressor with various environments.

        :param dataset_ratio:
        :param n_estimators:
        :param est_thresh:
        :return:
        """
        xgboost_cfg = ml_cfg['XGBC']
        msg = "\n # XGBoost Classification"
        n_estimators = int(xgboost_cfg['n_estimators'])
        learning_rate = float(xgboost_cfg['learning_rate'])
        est_thresh = float(xgboost_cfg['est_thresh'])

        self.acc_thresh = est_thresh

        one_to_left = st.beta(10, 1)
        from_zero_positive = st.expon(0, 50)
        GRID_SEARCH_ = False
        if GRID_SEARCH_:
            param_grid = {
                'n_estimators': [500, 1000, 1500],# [50, 100, 200, 300, 400, 500],
                'learning_rate': [0.1], # [0.01, 0.1, 0.05, 0.5, 1],
                # 'max_depth' : [5, 10, 20, 40, 60],
                # 'colsample_bytree' : one_to_left,
                # 'subsample' : one_to_left,
                # 'gamma' : st.uniform(0, 10),
                # 'reg_alpha' : from_zero_positive,
                # 'min_child_weight' : from_zero_positive
            }

            mdl = XGBClassifier(n_estimators=n_estimators, learning_rate=learning_rate, n_jobs=-1)
            grid = GridSearchCV(estimator=mdl, param_grid=param_grid, cv=5)
            grid = grid.fit(self.X[0], self.y[0])
            print('Best score is {:4.4f}' .format(sum(grid.best_estimator_.predict(self.X[1]) == self.y[1]) / (len(self.y[1])*1.0)))
            print('Best params is {}' .format(str(grid.best_params_)))
            print('Parameter tuning complete')

        else:
            mdl = XGBClassifier(n_estimators=n_estimators, learning_rate=learning_rate)

        mdl.fit(np.array(self.X[0]), np.array(self.y[0]))
        self.model = mdl

        pred_y = []
        est_err = []
        est_acc = []
        mape_arr = []
        rmse_arr = []
        r2_score_arr = []

        for i in range(2):
            pred_y.append(mdl.predict(self.X[i]))
            est_err.append(abs(pred_y[i] - np.array(self.y[i])))
            est_acc.append(self.check_est_accuracy(est_err[-1], est_thresh))
            mape_arr.append(learning_lib.calc_MAPE(self.y[i], pred_y[-1]))
            rmse_arr.append(learning_lib.calc_RMSE(self.y[i], pred_y[-1]))
            r2_score_arr.append(r2_score(self.y[i], pred_y[i]))

        self.y_predict = pred_y
        self.train_acc, self.test_acc = est_acc
        self.train_rmse, self.test_rmse = rmse_arr
        self.train_r2_score, self.test_r2_score = r2_score_arr

        msg += "\n # train acc : {:4.2f}, test acc : {:4.2f}".format(self.train_acc, self.test_acc)
        msg += "\n # train rmse : {:4.2f}, test rmse : {:4.2f}".format(self.train_rmse, self.test_rmse)
        msg += "\n # train r2 score : {:4.2f}, test r2 score : {:4.2f}".format(self.train_r2_score, self.test_r2_score)

        self.log = msg
        self.params = xgboost_cfg

        # if True:
        #     self.gen_plot()	


    def random_forest_regressor_for_multiple_y(self, ml_cfg):
        """ Run random forest regressor with various environments.
        :param dataset_ratio:
        :param n_estimators:
        :param est_thresh:
        :return:
        """
        rfr_cfg = ml_cfg['RFR']
        msg = "\n # Random Forest Regression"
        n_estimators = int(rfr_cfg['n_estimators'])
        min_samples_leaf = int(rfr_cfg['min_samples_leaf'])

        if rfr_cfg['max_depth'] == "none":
            max_depth = None
        else:
            max_depth = int(rfr_cfg['max_depth'])

        if rfr_cfg['max_leaf_nodes'] == "none":
            max_leaf_nodes = None
        else:
            max_leaf_nodes = int(rfr_cfg['max_leaf_nodes'])

        max_features = rfr_cfg['max_features']
        if max_features != "auto" and max_features != "sqrt" and max_features != "log2":
            max_features = int(max_features)

        est_thresh = rfr_cfg['est_thresh'].split(',')
        self.est_thresh = [float(thresh) for thresh in est_thresh]

        mdl = MultiOutputRegressor(RandomForestRegressor(n_estimators=n_estimators, min_samples_leaf=min_samples_leaf,
                                                         max_depth=max_depth, max_leaf_nodes=max_leaf_nodes,
                                                         max_features=max_features))

        mdl.fit(self.X[0], self.y[0])
        self.RFR_num_estimators = n_estimators
        self.model = mdl

        pred_y = []
        est_err = []
        est_acc = []
        mse_arr = []
        rmse_arr = []
        r2_score_arr = []

        for i in range(2):  # Train & Test
            pred_y.append(mdl.predict(self.X[i]))
            est_err.append(abs(pred_y[i] - np.array(self.y[i])))
            est_acc.append(self.check_est_accuracy(est_err[i], self.est_thresh))
            mse_arr.append(mean_squared_error(self.y[i], pred_y[i]))
            rmse_arr.append(learning_lib.calc_RMSE(self.y[i], pred_y[i]))
            r2_score_arr.append(r2_score(self.y[i], pred_y[i], multioutput='raw_values'))

        self.y_predict = pred_y

        self.train_acc, self.test_acc = est_acc
        self.train_mse, self.test_mse = mse_arr
        self.train_rmse, self.test_rmse = rmse_arr
        self.train_r2_score, self.test_r2_score = r2_score_arr


        for j in range(self.y_num):  # For multiple y value
            msg += "\n # type : {}, train acc : {:4.2f}, test acc : {:4.2f}".format(self.y_name[j], self.train_acc[j], self.test_acc[j])
            msg += "\n # type : {}, train rmse : {:4.2f}, test rmse : {:4.2f}".format(self.y_name[j], self.train_rmse[j], self.test_rmse[j])
            msg += "\n # type : {}, train r2 score : {:4.2f}, test r2 score : {:4.2f}".format(self.y_name[j], self.train_r2_score[j], self.test_r2_score[j])
        self.log = msg
        # print(msg)
        return self.train_acc, self.test_acc

    def random_forest_classifier(self, ml_cfg):
        """ Run random forest classifier with various environments.

        :param dataset_ratio:
        :param n_estimators:
        :param est_thresh:
        :return:
        """
        rfc_cfg = ml_cfg['RFC']
        msg = "\n # Random Forest Classification"
        n_estimators = int(rfc_cfg['n_estimators'])
        min_samples_leaf = int(rfc_cfg['min_samples_leaf'])

        if rfc_cfg['max_depth'] == "none":
            max_depth = None
        else:
            max_depth = int(rfc_cfg['max_depth'])

        if rfc_cfg['max_leaf_nodes'] == "none":
            max_leaf_nodes = None
        else:
            max_leaf_nodes = int(rfc_cfg['max_leaf_nodes'])

        max_features = rfc_cfg['max_features']
        if max_features != "auto" and max_features != "sqrt" and max_features != "log2" :
            max_features = int(max_features)
        # if rfr_cfg['max_features'] == "num_feat":
        #     max_features = len(self.X[0])
        # elif rfr_cfg['max_features'] == "sqrt":
        #     max_features = math.sqrt(len(self.X[0]))
        # elif rfr_cfg['max_features'] == "log2":
        #     max_features = math.log2(len(self.X[0]))
        # else:
        #     max_features = int(rfr_cfg['max_features'])

        est_thresh = float(rfc_cfg['est_thresh'])
        self.acc_thresh = est_thresh

        mdl = RandomForestClassifier(n_estimators=n_estimators, min_samples_leaf=min_samples_leaf,
                                    max_depth=max_depth, max_leaf_nodes=max_leaf_nodes,
                                    max_features=max_features)
        mdl.fit(self.X[0], self.y[0])
        self.model = mdl

        if ml_cfg['general']['print_est_time'].upper() == 'ON':
            # 모델의 예측속도 측정
            start_time = time.time()
            mdl_predict = mdl.predict(self.X[0][0])
            print(" ## Model's prediction time : {:.3f} sec".format(time.time() - start_time))

        pred_y = []
        est_err = []
        est_acc = []
        mape_arr = []
        rmse_arr = []
        r2_score_arr = []

        for i in range(2):
            pred_y.append(mdl.predict(self.X[i]))
            est_err.append(abs(pred_y[i] - np.array(self.y[i])))
            est_acc.append(self.check_est_accuracy(est_err[i], est_thresh))
            mape_arr.append(learning_lib.calc_MAPE(self.y[i], pred_y[i]))
            rmse_arr.append(learning_lib.calc_RMSE(self.y[i], pred_y[i]))
            r2_score_arr.append(r2_score(self.y[i], pred_y[i]))

        self.y_predict = pred_y
        self.train_acc, self.test_acc = est_acc
        self.train_rmse, self.test_rmse = rmse_arr
        self.train_r2_score, self.test_r2_score = r2_score_arr

        msg += "\n # train acc : {:4.2f}, test acc : {:4.2f}".format(self.train_acc, self.test_acc)
        msg += "\n # train rmse : {:4.2f}, test rmse : {:4.2f}".format(self.train_rmse, self.test_rmse)
        msg += "\n # train r2 score : {:4.2f}, test r2 score : {:4.2f}".format(self.train_r2_score, self.test_r2_score)

        self.log = msg
        self.params = ml_cfg

        # if True:
        #     self.gen_plot()

        return

    def bagging_regressor_for_multiple_y(self, ml_cfg):

        cat = ml_cfg['general']['methond']
        bagging_cfg = ml_cfg['BAGGING']

        """ Run random forest regressor with various environments.
        :param dataset_ratio:
        :param n_estimators:
        :param est_thresh:
        :return:
        """
        msg = "\n # Bagging Regression"
        n_estimators = int(bagging_cfg['n_estimators'])
        max_samples = int(bagging_cfg['max_samples'])
        max_features = int(bagging_cfg['max_features'])


        if cat == 'RH_PLUS':
            first_est_thresh = float(bagging_cfg['time_est_thresh'])
            second_est_thresh = float(bagging_cfg['temp_est_thresh'])
        elif cat == 'CVT_MACRO_M1':
            first_est_thresh = float(bagging_cfg['carbon_est_thresh'])
            second_est_thresh = float(bagging_cfg['oxygen_est_thresh'])

        mdl = MultiOutputRegressor(BaggingRegressor(base_estimator=DecisionTreeRegressor(),
                                                    n_estimators = n_estimators, max_samples = max_samples,
                                                    bootstrap=False, n_jobs=-1))
                                                    # max_features = max_features))

        # mdl = MultiOutputRegressor(GradientBoostingRegressor(random_state=21, n_estimators=400))

        mdl.fit(self.X[0], self.y[0])
        self.BAGGING_num_estimators = n_estimators
        self.model = mdl
        pred_y = []

        first_est_err = []
        first_est_acc = []
        first_mse_arr = []
        first_rmsre_arr = []

        second_est_err = []
        second_est_acc = []
        second_mse_arr = []
        second_rmsre_arr = []

        for i in range(2):  # Train & Test
            pred_y.append(mdl.predict(self.X[i]))
            # pred_y.append(mdl.predict(self.X[i]) + np.random.normal(0,sigma[i],len(self.X[i])))
            first_est_err.append(abs(np.array(pred_y[i])[:, 0] - np.array(self.y[i])[:, 0]))
            first_est_acc.append(self.check_est_accuracy(first_est_err[-1], first_est_thresh))
            first_mse_arr.append(mean_squared_error(np.array(self.y[i])[:, 0], np.array(pred_y[-1])[:, 0]))
            first_rmsre_arr.append(learning_lib.calc_RMSRE(np.array(self.y[i])[:, 0], np.array(pred_y[-1])[:, 0])) ## check point

            second_est_err.append(abs(np.array(pred_y[i])[:, 1] - np.array(self.y[i])[:, 1]))
            second_est_acc.append(self.check_est_accuracy(second_est_err[-1], second_est_thresh))
            second_mse_arr.append(mean_squared_error(np.array(self.y[i])[:, 1], np.array(pred_y[-1])[:, 1]))
            second_rmsre_arr.append(learning_lib.calc_RMSRE(np.array(self.y[i])[:, 1], np.array(pred_y[-1])[:, 1])) ## check point

        self.train_acc, self.test_acc = first_est_acc
        self.train_acc_2, self.test_acc_2 = second_est_acc

        self.train_mse, self.test_mse = first_mse_arr
        self.train_mse_2, self.test_mse_2 = second_mse_arr

        self.train_rmsre, self.test_rmsre = first_rmsre_arr
        self.train_rmsre_2, self.test_rmsre_2 = second_rmsre_arr

        # Calculate mean, std value
        first_real_y = np.concatenate((np.array(self.y[0])[:, 0], np.array(self.y[1])[:, 0]), axis=0)
        first_y_mean = np.mean(first_real_y)
        first_y_std = np.std(first_real_y)

        second_real_y = np.concatenate((np.array(self.y[0])[:, 1], np.array(self.y[1])[:, 1]), axis=0)
        second_y_mean = np.mean(second_real_y)
        second_y_std = np.std(second_real_y)

        msg += "\n # type : carbon, train acc : {:4.2f}, test acc : {:4.2f}".format(self.train_acc, self.test_acc)
        # msg += "\n # type : carbon, train mse : {:4.2f}, test mse : {:4.2f}".format(self.train_mse, self.test_mse)
        msg += "\n # type : carbon, train rmsre : {:4.2f}, test rmsre : {:4.2f}".format(self.train_rmsre, self.test_rmsre)
        msg += "\n # type : carbon, mean value : {:4.2f}, std value : {:4.2f}".format(first_y_mean, first_y_std)

        msg += "\n # type : oxygen, train acc : {:4.2f}, test acc : {:4.2f}".format(self.train_acc_2, self.test_acc_2)
        # msg += "\n # type : oxygen, train mse : {:4.2f}, test mse : {:4.2f}".format(self.train_mse_2, self.test_mse_2)
        msg += "\n # type : oxygen, train rmsre : {:4.2f}, test rmsre : {:4.2f}".format(self.train_rmsre_2, self.test_rmsre_2)
        msg += "\n # type : oxygen, mean value : {:4.2f}, std value : {:4.2f}".format(second_y_mean, second_y_std)
        self.log = msg
        print(msg)
        return self.train_acc, self.test_acc, self.train_acc_2, self.test_acc_2

    def support_vector_regressor(self, est_margin=sys.float_info.epsilon, plot_filename_prefix='', plot_=True, pkl_filename=''):
        # reg = SVR(C=SVR_C, epsilon=SVR_epsilon)
        reg = LinearSVR()
        reg.fit(self.train_X, self.train_y)
        train_y_predict = reg.predict(self.train_X)
        test_y_predict = reg.predict(self.test_X)
        err_train = abs(train_y_predict - self.train_y)
        err_test  = abs(test_y_predict  - self.test_y)
        train_result = err_train <= est_margin
        test_result  = err_test  <= est_margin
        train_accuracy = 100. * sum(train_result) / float(len(train_result))
        test_accuracy  = 100. * sum(test_result)  / float(len(test_result))

        num_train = len(self.train_X)
        num_test  = len(self.test_X)
        num_ratio = int(100. * num_train / float(num_train + num_test))
        msg  = "\n"
        msg += "\n # Machine learning by support vector regressor"
        msg += "\n   > Train vs test = {:d} vs {:d} ({:d}%)".format(num_train, num_test, num_ratio)
        msg += "\n   > Train dataset statistics: mean = {:5.2f}, var = {:7.2f}".format(err_train.mean(), err_train.var())
        msg += "\n   > Test  dataset statistics: mean = {:5.2f}, var = {:7.2f}".format(err_test.mean(),  err_test.var())
        msg += "\n   > Estimator accuracy for train dataset = {:6.2f}".format(train_accuracy)
        msg += "\n   > Estimator accuracy for test  dataset = {:6.2f}".format(test_accuracy)
        self.log = msg
        print(msg)
        print(" real  = ({:f} {:f}) -> ({:f} {:f})".format(min(self.train_y), max(self.train_y),
                                                           min(train_y_predict), max(train_y_predict)))
        print(" real  = ({:f} {:f}) -> ({:f} {:f})".format(min(self.test_y), max(self.test_y),
                                                           min(test_y_predict), max(test_y_predict)))

        max_range = max(max(err_train), max(err_test), est_margin) + 1
        fig = plt.figure()
        plt.subplot(1,2,1)
        plt.plot(err_train, 'b', [est_margin,] * len(err_train), 'r')
        plt.ylim(0, max_range)
        plt.title('Training ABS error({:2d}%)'.format(int(train_accuracy)))
        plt.subplot(1,2,2)
        plt.plot(err_test, 'b', [est_margin,] * len(err_test), 'r')
        plt.ylim(0, max_range)
        plt.title('Test ABS error({:2d}%)'.format(int(test_accuracy)))
        if plot_filename_prefix:
            fig.savefig(plot_filename_prefix)
        if plot_:
            plt.show()
        else:
            plt.close(fig)

        if pkl_filename:
            pickle.dump([reg, self.name_X, self.name_y, msg], open(pkl_filename, 'wb'))
        pass

    # def deep_neuralnet_regressor(self, dataset_ratio=70, est_margin=sys.float_info.epsilon, plot_filename_prefix='', plot_=True, pkl_filename=''):
    #
    #     self.split_dataset_into_train_and_test(dataset_ratio=dataset_ratio)
    #
    #     trans_train_X = posco.transpose_list(self.X[0])
    #     trans_test_X = posco.transpose_list(self.X[1])
    #
    #     train_X_n = []
    #     test_X_n = []
    #     dat_min = []
    #     dat_max = []
    #
    #     for idx in range(len(trans_train_X)):
    #         train_X_n.append(MinMaxScaler(trans_train_X[idx]))
    #         test_X_n.append(MinMaxScaler(trans_test_X[idx]))
    #         dat_min.append(min(trans_train_X[idx]))
    #         dat_max.append(max(trans_train_X[idx]))
    #
    #     output_min = min(self.y[0])
    #     output_max = max(self.y[0])
    #
    #     train_X_n = posco.transpose_list(train_X_n)
    #     test_X_n = posco.transpose_list(test_X_n)
    #     train_y_n = MinMaxScaler(self.y[0])
    #     test_y_n = MinMaxScaler(self.y[1])
    #
    #     train_X_n = np.array(train_X_n)
    #     test_X_n = np.array(test_X_n)
    #     train_y_n = np.array(train_y_n)
    #     test_y_n = np.array(test_y_n)
    #     train_y_n.shape = (train_y_n.shape[0], 1)
    #     test_y_n.shape = (test_y_n.shape[0], 1)
    #
    #     def get_train_data():
    #         X_train_tf = tf.constant(train_X_n)
    #         Y_train_tf = tf.constant(train_y_n)
    #         return X_train_tf, Y_train_tf
    #
    #     nsteps = 15001
    #
    #     dnn_num_feat = train_X_n.shape[1]
    #     dnn_feat_col = [tf.contrib.layers.real_valued_column("", dimension=dnn_num_feat)]
    #     dnn_active_fn = tf.tanh
    #     dnn_unit = [2]
    #     dnn_learn_rate = 0.2
    #     dnn_optimizer = tf.train.AdagradOptimizer(learning_rate=dnn_learn_rate)
    #
    #     dnn_dir = "dnn_model"
    #
    #     msg = "\n"
    #     msg += "------------------------------------\n"
    #
    #     reg = tf.contrib.learn.DNNRegressor(feature_columns=dnn_feat_col, activation_fn=dnn_active_fn,
    #                                         hidden_units=dnn_unit, model_dir=dnn_dir,
    #                                         optimizer=dnn_optimizer)
    #
    #
    #     reg.fit(input_fn=get_train_data, steps=nsteps)
    #     train_y_predict_n = reg.predict(train_X_n)
    #     train_y_predict_n = np.array(list(train_y_predict_n))
    #     test_y_predict_n = reg.predict(test_X_n)
    #     test_y_predict_n = np.array(list(test_y_predict_n))
    #     train_y_predict = NormToReal(self.y[0], train_y_predict_n)
    #     test_y_predict = NormToReal(self.y[0], test_y_predict_n)
    #     err_train = abs(train_y_predict - self.y[0])
    #     err_test = abs(test_y_predict - self.y[1])
    #     train_result = err_train <= est_margin
    #     test_result = err_test <= est_margin
    #     train_accuracy = 100. * sum(train_result) / float(len(train_result))
    #     test_accuracy = 100. * sum(test_result) / float(len(test_result))
    #
    #     num_train = len(self.X[0])
    #     num_test = len(self.X[1])
    #     num_ratio = int(100. * num_train / float(num_train + num_test))
    #
    #     msg += "{:6.2f}".format(train_accuracy)
    #     msg += "{:6.2f}".format(test_accuracy)
    #
    #     print(msg)
    #     self.log = msg
        #
        # max_range = max(max(err_train), max(err_test), est_margin) + 1
        # fig = plt.figure()
        # plt.subplot(1, 2, 1)
        # plt.plot(err_train, 'b', [est_margin, ] * len(err_train), 'r')
        # plt.ylim(0, max_range)
        # plt.title('Training ABS error({:2d}%)'.format(int(train_accuracy)))
        # plt.subplot(1, 2, 2)
        # plt.plot(err_test, 'b', [est_margin, ] * len(err_test), 'r')
        # plt.ylim(0, max_range)
        # plt.title('Test ABS error({:2d}%)'.format(int(test_accuracy)))
        #
        # dat_minmax = []
        # dat_minmax.append(self.X_name)
        # dat_minmax.append(dat_min)
        # dat_minmax.append(dat_max)
        #
        # if plot_filename_prefix:
        #     fig.savefig(plot_filename_prefix)
        # if plot_:
        #     plt.show()
        # else:
        #     plt.close(fig)
        #
        # if pkl_filename:
        #     pickle.dump([self.X_name, self.y_name, msg, dat_minmax, output_min, output_max, dnn_learn_rate],
        #                 open(pkl_filename, 'wb'))

    def deep_neuralnet_regressor(self, ml_cfg):

        # with tf.device('/cpu:0'):
        dnn_cfg = ml_cfg['DNN']
        train_X_n = np.array(self.X[0])
        test_X_n = np.array(self.X[1])
        train_y_n = np.array(self.y[0])
        test_y_n = np.array(self.y[1])

        train_y_n.shape = (train_y_n.shape[0], 1)
        test_y_n.shape = (test_y_n.shape[0], 1)

        def get_train_data():
            X_train_tf = tf.constant(train_X_n)
            Y_train_tf = tf.constant(train_y_n)
            return X_train_tf, Y_train_tf

        nsteps = int(dnn_cfg['nsteps'])

        dnn_num_feat = train_X_n.shape[1]
        dnn_feat_col = [tf.contrib.layers.real_valued_column("", dimension=dnn_num_feat)]

        if dnn_cfg['active_fn'] == "relu":
            dnn_active_fn = tf.nn.relu
        elif dnn_cfg['active_fn'] == "sigmoid":
            dnn_active_fn = tf.sigmoid
        else:
            dnn_active_fn = tf.tanh
        dnn_unit = dnn_cfg['unit'].split(',')
        dnn_unit = [int(u) for u in dnn_unit]
        dnn_learn_rate = float(dnn_cfg['learn_rate'])
        if dnn_cfg['optimizer'] == "adadelta":
            dnn_optimizer = tf.train.AdadeltaOptimizer(learning_rate=dnn_learn_rate)
        elif dnn_cfg['optimizer'] == "adam":
            dnn_optimizer = tf.train.AdamOptimizer(learning_rate=dnn_learn_rate)
        elif dnn_cfg['optimizer'] == "ftrl":
            dnn_optimizer = tf.train.FtrlOptimizer(learning_rate=dnn_learn_rate)
        elif dnn_cfg['optimizer'] == "gd":
            dnn_optimizer = tf.train.GradientDescentOptimizer(learning_rate=dnn_learn_rate)
        elif dnn_cfg['optimizer'] == "proximalGrad":
            dnn_optimizer = tf.train.ProximalGradientDescentOptimizer(learning_rate=dnn_learn_rate)
        elif dnn_cfg['optimizer'] == "proximalAdagrad":
            dnn_optimizer = tf.train.ProximalAdagradOptimizer(learning_rate=dnn_learn_rate)
        elif dnn_cfg['optimizer'] == "RMSProp":
            dnn_optimizer = tf.train.RMSPropOptimizer(learning_rate=dnn_learn_rate)
        else:
            dnn_optimizer = tf.train.AdagradOptimizer(learning_rate=dnn_learn_rate)

        dnn_dropout = None
        if dnn_cfg['dropout'] != 'None':
            dnn_dropout = float(dnn_cfg['dropout'])

        msg = "\n"
        msg += "------------------------------------\n"


        reg = tf.contrib.learn.DNNRegressor(feature_columns=dnn_feat_col, activation_fn=dnn_active_fn,
                                            hidden_units=dnn_unit, model_dir=dnn_cfg['model_dir'],
                                            optimizer=dnn_optimizer, dropout=dnn_dropout)

        reg.fit(input_fn=get_train_data, steps=nsteps)
        self.model = reg

        if ml_cfg['general']['print_est_time'].upper == 'ON':
            # with tf.device("/cpu:0"):
            print(device_lib.list_local_devices())
            # 모델의 예측속도 측정
            start_time = time.time()
            mdl_predict = reg.predict(train_X_n[:1])
            print(" ## Model's prediction time : {:.3f} sec".format(time.time() - start_time))


        train_y_predict_n = reg.predict(train_X_n)
        train_y_predict_n = np.array(list(train_y_predict_n))
        test_y_predict_n = reg.predict(test_X_n)
        test_y_predict_n = np.array(list(test_y_predict_n))

        train_y_predict = self.denormalize_output(train_y_predict_n, self.Feat.vars[self.y_name].max_thresh,
                                                  self.Feat.vars[self.y_name].min_thresh)
        test_y_predict = self.denormalize_output(test_y_predict_n, self.Feat.vars[self.y_name].max_thresh,
                                                 self.Feat.vars[self.y_name].min_thresh)

        real_y_0, real_y_1 = self.denormalize_y()

        err_train = abs(train_y_predict - real_y_0)
        err_test = abs(test_y_predict - real_y_1)
        self.train_mse = mean_squared_error(real_y_0, train_y_predict)
        self.test_mse = mean_squared_error(real_y_1, test_y_predict)

        train_result = err_train <= float(dnn_cfg['est_margin'])
        test_result = err_test <= float(dnn_cfg['est_margin'])

        train_accuracy = 100. * sum(train_result) / float(len(train_result))
        test_accuracy = 100. * sum(test_result) / float(len(test_result))

        self.train_acc = train_accuracy
        self.test_acc = test_accuracy

        num_train = len(self.X[0])
        num_test = len(self.X[1])
        num_ratio = int(100. * num_train / float(num_train + num_test))

        msg += "{:6.2f}".format(self.train_acc)
        msg += "{:6.2f}".format(self.test_acc)
        msg += "\n"
        msg += "{:8.2f}".format(self.train_mse)
        msg += "{:8.2f}".format(self.test_mse)
        # filename = "result_" + dnn_cfg['train_ratio'] + "_" +\
        #            dnn_cfg['nsteps'] + "_" + dnn_cfg['unit'] + "_" +\
        #            dnn_cfg['learn_rate'] + "_" + dnn_cfg['dropout'] + "_" +\
        #            str(dnn_num_feat) + dnn_cfg['filename_postfix']
        #
        # with open(dnn_cfg['result_dir'] + filename, 'a') as f:
        #     f.write("{:6.2f},{:6.2f},{:6.2f},{:6.2f}\n".
        #             format(train_accuracy,
        #                    test_accuracy,
        #                    mean_squared_err_train,
        #                    mean_squared_err_test
        #             ))
        print(msg)
        self.log = msg

        trans_dataset = data_lib.transpose_list(self.dataset[1:])
        self.X_dataset = data_lib.transpose_list(trans_dataset[1:])
        self.y_dataset = trans_dataset[0]

        self.params = ml_cfg

        return self.train_acc, self.test_acc
        # dat_minmax = []
        # dat_minmax.append(self.X_name)
        # dat_minmax.append(dat_min)
        # dat_minmax.append(dat_max)

    def deep_neuralnet_regressor_for_multiple_y(self, ml_cfg):
        dnn_cfg = ml_cfg['DNN']
        output_num = int(ml_cfg['general']['output_num'])
        train_X_n = np.array(self.X[0])
        test_X_n = np.array(self.X[1])
        train_y_n = np.array(self.y[0])
        test_y_n = np.array(self.y[1])

        train_y_n.shape = (train_y_n.shape[0], output_num)
        test_y_n.shape = (test_y_n.shape[0], output_num)

        def get_train_data():
            X_train_tf = tf.constant(train_X_n)
            Y_train_tf = tf.constant(train_y_n)
            return X_train_tf, Y_train_tf

        nsteps = int(dnn_cfg['nsteps'])

        dnn_num_feat = train_X_n.shape[1]
        dnn_feat_col = [tf.contrib.layers.real_valued_column("", dimension=dnn_num_feat)]

        if dnn_cfg['active_fn'] == "relu":
            dnn_active_fn = tf.nn.relu
        elif dnn_cfg['active_fn'] == "sigmoid":
            dnn_active_fn = tf.sigmoid
        else:
            dnn_active_fn = tf.tanh
        dnn_unit = dnn_cfg['unit'].split(',')
        dnn_unit = [int(u) for u in dnn_unit]
        dnn_learn_rate = float(dnn_cfg['learn_rate'])
        if dnn_cfg['optimizer'] == "adadelta":
            dnn_optimizer = tf.train.AdadeltaOptimizer(learning_rate=dnn_learn_rate)
        elif dnn_cfg['optimizer'] == "adam":
            dnn_optimizer = tf.train.AdamOptimizer(learning_rate=dnn_learn_rate)
        elif dnn_cfg['optimizer'] == "ftrl":
            dnn_optimizer = tf.train.FtrlOptimizer(learning_rate=dnn_learn_rate)
        elif dnn_cfg['optimizer'] == "gd":
            dnn_optimizer = tf.train.GradientDescentOptimizer(learning_rate=dnn_learn_rate)
        elif dnn_cfg['optimizer'] == "proximalGrad":
            dnn_optimizer = tf.train.ProximalGradientDescentOptimizer(learning_rate=dnn_learn_rate)
        elif dnn_cfg['optimizer'] == "proximalAdagrad":
            dnn_optimizer = tf.train.ProximalAdagradOptimizer(learning_rate=dnn_learn_rate)
        elif dnn_cfg['optimizer'] == "RMSProp":
            dnn_optimizer = tf.train.RMSPropOptimizer(learning_rate=dnn_learn_rate)
        else:
            dnn_optimizer = tf.train.AdagradOptimizer(learning_rate=dnn_learn_rate)

        dnn_dropout = None
        if dnn_cfg['dropout'] != 'None':
            dnn_dropout = float(dnn_cfg['dropout'])

        msg = "\n"
        msg += "------------------------------------\n"
        # config_proto = tf.ConfigProto(allow_soft_placement=True, log_device_placement=True)
        # config_proto.gpu_options.allow_growth = True

        reg = tf.contrib.learn.DNNRegressor(feature_columns=dnn_feat_col, activation_fn=dnn_active_fn,
                                            hidden_units=dnn_unit, model_dir=dnn_cfg['model_dir'],
                                            optimizer=dnn_optimizer, dropout=dnn_dropout,
                                            label_dimension=2)
        # config=tf.contrib.learn.RunConfig(session_config=config_proto))

        reg.fit(input_fn=get_train_data, steps=nsteps)
        train_y_predict_n = reg.predict(train_X_n)
        train_y_predict_n = np.array(list(train_y_predict_n))
        test_y_predict_n = reg.predict(test_X_n)
        test_y_predict_n = np.array(list(test_y_predict_n))
        train_y_vals = {}
        test_y_vals = {}
        for idx in range(output_num):
            train_y_vals[self.y_name[idx]] = self.denormalize_output(train_y_predict_n[:, idx],
                                                                     self.Feat.vars[self.y_name[idx]].max_thresh,
                                                                     self.Feat.vars[self.y_name[idx]].min_thresh)
            test_y_vals[self.y_name[idx]] = self.denormalize_output(test_y_predict_n[:, idx],
                                                                    self.Feat.vars[self.y_name[idx]].max_thresh,
                                                                    self.Feat.vars[self.y_name[idx]].min_thresh)
        #real_y_list = self.denormalize_multiple_y()
        real_y_0_time, real_y_1_time, real_y_0_temp, real_y_1_temp = self.denormalize_multiple_y()

        err_train_time = abs(train_y_predict_time - real_y_0_time)
        err_train_temp = abs(train_y_predict_temp - real_y_0_temp)
        err_test_time = abs(test_y_predict_time - real_y_1_time)
        err_test_temp = abs(test_y_predict_temp - real_y_1_temp)

        mean_squared_err_train_time = mean_squared_error(real_y_0_time, train_y_predict_time)
        mean_squared_err_train_temp = mean_squared_error(real_y_0_temp, train_y_predict_temp)
        mean_squared_err_test_time = mean_squared_error(real_y_1_time, test_y_predict_time)
        mean_squared_err_test_temp = mean_squared_error(real_y_1_temp, test_y_predict_temp)

        train_result_time = err_train_time <= float(dnn_cfg['time_est_margin'])
        train_result_temp = err_train_temp <= float(dnn_cfg['temp_est_margin'])
        test_result_time = err_test_time <= float(dnn_cfg['time_est_margin'])
        test_result_temp = err_test_temp <= float(dnn_cfg['temp_est_margin'])

        train_accuracy_time = 100. * sum(train_result_time) / float(len(train_result_time))
        train_accuracy_temp = 100. * sum(train_result_temp) / float(len(train_result_temp))
        test_accuracy_time = 100. * sum(test_result_time) / float(len(test_result_time))
        test_accuracy_temp = 100. * sum(test_result_temp) / float(len(test_result_temp))

        num_train = len(self.X[0])
        num_test = len(self.X[1])
        num_ratio = int(100. * num_train / float(num_train + num_test))

        msg += "{:6.2f}".format(train_accuracy_time)
        msg += "{:6.2f}".format(train_accuracy_temp)
        msg += "{:6.2f}".format(test_accuracy_time)
        msg += "{:6.2f}".format(test_accuracy_temp)
        msg += "{:6.2f}".format(mean_squared_err_train_time)
        msg += "{:6.2f}".format(mean_squared_err_train_temp)
        msg += "{:6.2f}".format(mean_squared_err_test_time)
        msg += "{:6.2f}".format(mean_squared_err_test_temp)

        print(msg)
        self.log = msg

        self.train_acc = train_accuracy_time
        self.train_acc_2 = train_accuracy_temp
        self.test_acc = test_accuracy_time
        self.test_acc_2 = test_accuracy_temp

        self.train_mse = mean_squared_err_train_time
        self.train_mse_2 = mean_squared_err_train_temp
        self.test_mse = mean_squared_err_test_time
        self.test_mse_2 = mean_squared_err_test_temp

        trans_dataset = data_lib.transpose_list(self.dataset[1:])
        self.X_dataset = data_lib.transpose_list(trans_dataset[1:])
        self.y_dataset = trans_dataset[0]

        self.params = ml_cfg

        # # Plot outputs
        # plt.scatter(test_X_n[:, np.newaxis, 0], test_y_n, color='black')
        # plt.plot(test_X_n[:, np.newaxis, 0], test_y_predict_n, color='blue', linewidth=1)
        # plt.xticks(())
        # plt.yticks(())
        # plt.show()

    def deep_neuralnet_classifier(self, ml_cfg):
        dnnc_cfg = ml_cfg['DNNC']
        train_X_n = np.array(self.X[0])
        test_X_n = np.array(self.X[1])
        train_y_n = np.array(self.y[0], dtype=np.int)
        test_y_n = np.array(self.y[1], dtype=np.int)

        train_y_n.shape = (train_y_n.shape[0], 1)
        test_y_n.shape = (test_y_n.shape[0], 1)

        def get_train_data():
            X_train_tf = tf.constant(train_X_n)
            Y_train_tf = tf.constant(train_y_n)
            return X_train_tf, Y_train_tf

        nsteps = int(dnnc_cfg['nsteps'])

        dnn_num_feat = train_X_n.shape[1]
        dnn_feat_col = [tf.contrib.layers.real_valued_column("", dimension=dnn_num_feat)]

        if dnnc_cfg['active_fn'] == "relu":
            dnn_active_fn = tf.nn.relu
        elif dnnc_cfg['active_fn'] == "sigmoid":
            dnn_active_fn = tf.sigmoid
        else:
            dnn_active_fn = tf.tanh
        dnn_unit = dnnc_cfg['unit'].split(',')
        dnn_unit = [int(u) for u in dnn_unit]
        dnn_learn_rate = float(dnnc_cfg['learn_rate'])
        if dnnc_cfg['optimizer'] == "adadelta":
            dnn_optimizer = tf.train.AdadeltaOptimizer(learning_rate=dnn_learn_rate)
        elif dnnc_cfg['optimizer'] == "adam":
            dnn_optimizer = tf.train.AdamOptimizer(learning_rate=dnn_learn_rate)
        elif dnnc_cfg['optimizer'] == "ftrl":
            dnn_optimizer = tf.train.FtrlOptimizer(learning_rate=dnn_learn_rate)
        elif dnnc_cfg['optimizer'] == "gd":
            dnn_optimizer = tf.train.GradientDescentOptimizer(learning_rate=dnn_learn_rate)
        elif dnnc_cfg['optimizer'] == "proximalGrad":
            dnn_optimizer = tf.train.ProximalGradientDescentOptimizer(learning_rate=dnn_learn_rate)
        elif dnnc_cfg['optimizer'] == "proximalAdagrad":
            dnn_optimizer = tf.train.ProximalAdagradOptimizer(learning_rate=dnn_learn_rate)
        elif dnnc_cfg['optimizer'] == "RMSProp":
            dnn_optimizer = tf.train.RMSPropOptimizer(learning_rate=dnn_learn_rate)
        else:
            dnn_optimizer = tf.train.AdagradOptimizer(learning_rate=dnn_learn_rate)

        dnn_dropout = None
        if dnnc_cfg['dropout'] != 'None':
            dnn_dropout = float(dnnc_cfg['dropout'])

        msg = "\n"
        msg += "------------------------------------\n"


        reg = tf.contrib.learn.DNNClassifier(feature_columns=dnn_feat_col, activation_fn=dnn_active_fn,
                                             hidden_units=dnn_unit, model_dir=dnnc_cfg['model_dir'],
                                             optimizer=dnn_optimizer, dropout=dnn_dropout,
                                             n_classes=int(dnnc_cfg['n_classes']))

        reg.fit(input_fn=get_train_data, steps=nsteps)
        self.model = reg

        if ml_cfg['general']['print_est_time'].upper == 'ON':
            # with tf.device("/cpu:0"):
            print(device_lib.list_local_devices())
            # 모델의 예측속도 측정
            start_time = time.time()
            mdl_predict = reg.predict(train_X_n[:1])
            print(" ## Model's prediction time : {:.3f} sec".format(time.time() - start_time))


        train_y_predict_n = reg.predict(train_X_n)
        train_y_predict_n = np.array(list(train_y_predict_n))
        test_y_predict_n = reg.predict(test_X_n)
        test_y_predict_n = np.array(list(test_y_predict_n))

        train_y_predict = train_y_predict_n
        test_y_predict = test_y_predict_n

        real_y_0, real_y_1 = self.y[0], self.y[1]

        err_train = abs(train_y_predict - real_y_0)
        err_test = abs(test_y_predict - real_y_1)
        self.train_mse = mean_squared_error(real_y_0, train_y_predict)
        self.test_mse = mean_squared_error(real_y_1, test_y_predict)

        train_result = err_train < float(dnnc_cfg['est_margin'])
        test_result = err_test < float(dnnc_cfg['est_margin'])

        train_accuracy = 100. * sum(train_result) / float(len(train_result))
        test_accuracy = 100. * sum(test_result) / float(len(test_result))

        self.train_acc = train_accuracy
        self.test_acc = test_accuracy

        num_train = len(self.X[0])
        num_test = len(self.X[1])
        num_ratio = int(100. * num_train / float(num_train + num_test))

        msg += "{:6.2f}".format(self.train_acc)
        msg += "{:6.2f}".format(self.test_acc)

        # filename = "result_" + dnn_cfg['train_ratio'] + "_" +\
        #            dnn_cfg['nsteps'] + "_" + dnn_cfg['unit'] + "_" +\
        #            dnn_cfg['learn_rate'] + "_" + dnn_cfg['dropout'] + "_" +\
        #            str(dnn_num_feat) + dnn_cfg['filename_postfix']
        #
        # with open(dnn_cfg['result_dir'] + filename, 'a') as f:
        #     f.write("{:6.2f},{:6.2f},{:6.2f},{:6.2f}\n".
        #             format(train_accuracy,
        #                    test_accuracy,
        #                    mean_squared_err_train,
        #                    mean_squared_err_test
        #             ))
        print(msg)
        self.log = msg

        trans_dataset = data_lib.transpose_list(self.dataset[1:])
        self.X_dataset = data_lib.transpose_list(trans_dataset[1:])
        self.y_dataset = trans_dataset[0]

        self.params = ml_cfg

    def adaboost_classifier(self, ml_cfg):
        """ Run adaboost classifier with various environments.

        :param dataset_ratio:
        :param n_estimators:
        :param est_thresh:
        :return:
        """
        ada_cfg = ml_cfg['AdaBoostC']
        msg = "\n # AdaBoost Classification"
        n_estimators = int(ada_cfg['n_estimators'])
        min_samples_leaf = int(ada_cfg['min_samples_leaf'])

        if ada_cfg['max_depth'] == "none":
            max_depth = None
        else:
            max_depth = int(ada_cfg['max_depth'])

        if ada_cfg['max_leaf_nodes'] == "none":
            max_leaf_nodes = None
        else:
            max_leaf_nodes = int(ada_cfg['max_leaf_nodes'])

        max_features = ada_cfg['max_features']
        if max_features != "auto" and max_features != "sqrt" and max_features != "log2" :
            max_features = int(max_features)

        # if rfr_cfg['max_features'] == "num_feat":
        #     max_features = len(self.X[0])
        # elif rfr_cfg['max_features'] == "sqrt":
        #     max_features = math.sqrt(len(self.X[0]))
        # elif rfr_cfg['max_features'] == "log2":
        #     max_features = math.log2(len(self.X[0]))
        # else:
        #     max_features = int(rfr_cfg['max_features'])
        learning_rate = float(ada_cfg['learning_rate'])

        est_thresh = float(ada_cfg['est_thresh'])
        self.acc_thresh = est_thresh

        mdl = AdaBoostClassifier(base_estimator=DecisionTreeClassifier(max_depth=max_depth,min_samples_leaf=min_samples_leaf,
                                                                       max_leaf_nodes=max_leaf_nodes, max_features=max_features),
                                 n_estimators=n_estimators, learning_rate=learning_rate)
        mdl.fit(self.X[0], self.y[0])
        self.model = mdl

        if ml_cfg['general']['print_est_time'].upper == 'ON':
            # 모델의 예측속도 측정
            start_time = time.time()
            mdl_predict = mdl.predict(self.X[0][0])
            print(" ## Model's prediction time : {:.3f} sec".format(time.time() - start_time))

        pred_y = []
        est_err = []
        est_acc = []
        mape_arr = []
        rmse_arr = []


        for i in range(2):
            pred_y.append(mdl.predict(self.X[i]))
            est_err.append(abs(pred_y[i] - np.array(self.y[i])))
            est_acc.append(self.check_est_accuracy(est_err[-1], est_thresh))
            mape_arr.append(learning_lib.calc_MAPE(self.y[i], pred_y[-1]))
            rmse_arr.append(learning_lib.calc_RMSE(self.y[i], pred_y[-1]))

        self.y_predict = pred_y
        self.train_acc = self.check_est_accuracy(est_err[0], est_thresh)
        self.test_acc = self.check_est_accuracy(est_err[1], est_thresh)
        self.train_mse = mean_squared_error(self.y[0], pred_y[0].tolist())
        self.test_mse = mean_squared_error(self.y[1], pred_y[1].tolist())

        msg += "\n # train acc : {:4.2f}, test acc : {:4.2f}".format(self.train_acc, self.test_acc)
        msg += "\n # train mse : {:4.2f}, test mse : {:4.2f}".format(self.train_mse, self.test_mse)

        self.log = msg
        self.params = ml_cfg

        # if True:
        #     self.gen_plot()

        return

    def run(self, model, ml_cfg):
        try:
            num_output = int(ml_cfg['general']['output_num'])
        except KeyError:
            num_output = 1

        if num_output >= 2:
            self.split_dataset_into_train_and_test_for_multiple_y(dataset_ratio=int(ml_cfg[model]['train_ratio']),
                                                                  num_y=num_output)
            # For multiple y
            if model == "DNN":
                self.deep_neuralnet_regressor_for_multiple_y(ml_cfg)
            elif model == "RFR":
                self.random_forest_regressor_for_multiple_y(ml_cfg)
            elif model == "BAGGING":
                self.bagging_regressor_for_multiple_y(ml_cfg)
            elif model == "LGBMR":
                self.light_gbm_regressor_for_multiple_y(ml_cfg)
            elif model == "XGBR":
                self.xgboost_regressor_for_multiple_y(ml_cfg)
        else:
            self.split_dataset_into_train_and_test(dataset_ratio=int(ml_cfg[model]['train_ratio']))

            # Regressor Models
            if model == "DNN":
                self.deep_neuralnet_regressor(ml_cfg)
            elif model == "RFR":
                self.random_forest_regressor(ml_cfg)
            elif model == "LGBMR":
                self.light_gbm_regressor(ml_cfg)
            elif model == "XGBR":
                self.xgboost_regressor(ml_cfg)

            # Classifier Models
            elif model == "RFC":
                self.random_forest_classifier(ml_cfg)
            elif model == "DNNC":
                self.deep_neuralnet_classifier(ml_cfg)
            elif model == "AdaBoostC":
                self.adaboost_classifier(ml_cfg)
            elif model == "LGBMC":
                self.light_gbm_classifier(ml_cfg)
            elif model == "XGBC":
                self.xgboost_classifier(ml_cfg)

        if ml_cfg['general']['generate_plot'].upper() == 'ON':
            self.generate_plot(model)

        if ml_cfg['general']['rfr_feature_importance'].upper() == 'ON':
            if num_output == 1:
                y_name_list = [self.y_name]
            elif num_output >= 2:
                y_name_list = self.y_name
            self.find_rfr_feature_importance(y_name_list=y_name_list)
            
        return self.train_acc, self.test_acc

    def denormalize_y(self, min_val=-1, max_val=1):

        max_thresh = self.Feat.vars[self.y_name].max_thresh
        min_thresh = self.Feat.vars[self.y_name].min_thresh
        real_y_0 = []
        real_y_1 = []
        for val in self.y[0]:
            real_y_0.append( (val - min_val) / (max_val - min_val) * (max_thresh - min_thresh) + min_thresh )
        for val in self.y[1]:
            real_y_1.append( (val - min_val) / (max_val - min_val) * (max_thresh - min_thresh) + min_thresh )
        return real_y_0, real_y_1

    # def denormalize_multiple_y(self, min_val=-1, max_val=1):
    #     denrmalized_list = []
    #     for y_name in self.y_name:
    #         min_thresh = self.Feat.vars[y_name].min_thresh
    #         max_thresh = self.Feat.vars[y_name].max_thresh
    #         real_y_list = []
    #         for y_list in self.y:
    #             real_y_list = []
    #             for idx in len(self.y):
    #                 real_y_list.append([(val - min_val) / (max_val - min_val) * (max_thresh - min_thresh) + min_thresh
    #                                     for val in val_list])
    #         denrmalized_list += real_y_list
    #     return denrmalized_list

    def denormalize_multiple_y(self, min_val=-1, max_val=1):
        max_thresh_time = self.Feat.vars[self.y_name[0]].max_thresh
        min_thresh_time = self.Feat.vars[self.y_name[0]].min_thresh
        max_thresh_temp = self.Feat.vars[self.y_name[1]].max_thresh
        min_thresh_temp = self.Feat.vars[self.y_name[1]].min_thresh
        real_y_0_time = []
        real_y_1_time = []
        real_y_0_temp = []
        real_y_1_temp = []

        trans_y_0 = data_lib.transpose_list(self.y[0])
        trans_y_1 = data_lib.transpose_list(self.y[1])

        for val in trans_y_0[0]:
            real_y_0_time.append(
                (val - min_val) / (max_val - min_val) * (max_thresh_time - min_thresh_time) + min_thresh_time)
        for val in trans_y_1[0]:
            real_y_1_time.append(
                (val - min_val) / (max_val - min_val) * (max_thresh_time - min_thresh_time) + min_thresh_time)
        for val in trans_y_0[1]:
            real_y_0_temp.append(
                (val - min_val) / (max_val - min_val) * (max_thresh_temp - min_thresh_temp) + min_thresh_temp)
        for val in trans_y_1[1]:
            real_y_1_temp.append(
                (val - min_val) / (max_val - min_val) * (max_thresh_temp - min_thresh_temp) + min_thresh_temp)
        return real_y_0_time, real_y_1_time, real_y_0_temp, real_y_1_temp

def MinMaxScaler(data):
    numerator = data - np.min(data)
    denominator = np.max(data) - np.min(data)
    return numerator / (denominator + 1e-7)


def NormToReal(given_output, predict_output):
    print(max(given_output), min(given_output))
    return predict_output * (max(given_output) - min(given_output)) + min(given_output)



