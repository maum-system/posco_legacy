import argparse
import pymssql
import numpy as np
from pandas import DataFrame
import pandas as pd
import matplotlib.pyplot as plt
from lib import sys_lib as system_lib
from lib import learning as learning_lib
from lib import data as data_lib
import configparser
import refine_model
from models import est_eval


ml_ini_file = "../sys_component/config_machine_learning/rh_macro_m4-7-C.ini"
ml_ini = configparser.ConfigParser()
ml_ini.read(ml_ini_file)
var_ini_file = "../sys_component/rh_macro_var.ini"
var_ini = configparser.ConfigParser()
var_ini.read(var_ini_file)
var_csv_file = "../sys_component/rh_macro_var.csv"
var_mtx = data_lib.read_csv_file(var_csv_file)
MODEL_REQUEST_TIME = 'MODEL_REQUEST_TIME'
PATTERN_OPERATION = 'PATTERN_OPERATION'
USE_MOD4_7_TIME = 'USE_MOD4_7_TIME'
LOGGER_FOLER = 'log'
server_mode = 'EST'
logger_name = ml_ini['general']['model_name'] + '_' + server_mode
logger = system_lib.setup_logger(logger_name,
                                 logger_name.lower(),
                                 folder=LOGGER_FOLER,
                                 console_=True)
model_number_str = ml_ini['general']['model_number']
# str_dat = 'RH_4S_ES,,,20180101,9961,TTA6Sa,VU3015YCPE1abcd,DS,S60,S52,0,708,188,40,0.025,1C,2,2,4,8,4,4,SQ56204,566846,108.6,276.6,285,282.7,282.7,0,0,1386,,,84.19,1638,1640,277,0,0,213,1,5.1,126,15.28333333,3.7,2.2,16,5.1,2.7,31.4,47,583,,399,193,,,,,,,,,,,,,1610,500,,,,,,0,0,0,,0,28,27,1583,,A,A,388,1574,337,무OB+무냉각,1579,1586,1553,1545,,-4,-7,-8,L,0,48,1,,0,,,,,,0,0,0,0,0,0,0,,0,295,1,1,0,,168.5,126.5,,0,,,,,412,,135,FTS.IM,,66,15,,,,196,,,,,,17,1,10,676,95,26.6,7,98,6.91,3.05,9.96,,,,5,5,,7,1,0,1,139,0,53,0,비대상,1,5.1,4,4,2,1,,,0.0019,,,0.001,,0.022,,0.0229,,0.03,,23,,0.0027,,,0.0088,,,0.146,,,0.014,0.015,0.02,0,0.001,0,0.001,,,,,,4.3,,,13.3,3.6,4187,14,33,1,1810,,,SQ55905,,,W,130,55,,76.6,0,1100,SQ56186,VU3015YCPE1,극저,62.6,0.6,0.3,1.5,7.5,18.1,10,3,5,35,0,33,0,1.21,2.78,1,4,0,1,,,,,,,,,,81,,,,,,,,,,,,,179,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,무OB+무냉각,,,,,,,,,,,,,,,,,,,,,,,,19'
# str_dat = ',,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,'

def get_input_var():
    data = pd.read_csv("../sys_component/rh_macro_var.csv")
    df = data.iloc[4:264, [3]]
    df = df.reset_index(drop=True)
    return df.iloc[:, 0].values.tolist()


def stream_creator(select_var, model_ver='%', save_csv=False):
    # 단, POSCO 코드는 /가 아니고 \
    # conn = pymssql.connect(host="localhost", user="SA", password="Posco17temp", database="MINDS_CVT")
    conn = pymssql.connect(host="172.24.92.148", user="SA", password="sm2_ai_dev!", database="MINDS_CVT")
    cursor = conn.cursor()
    row_str_lst = list()
    sql = "SELECT TOP 20 {} FROM [MINDS_RH].[dbo].[rh_macro_est] " \
          "WHERE TRANSACTION_CD = 'RH_4S_ES' AND TAP_WORK_DATE like '202104%' AND EST_MODEL_VER LIKE '{}'" \
          "ORDER BY CHARGE_NO DESC"
    cursor.execute(sql.format(select_var, model_ver))
    row = cursor.fetchall()
    row_all = [list(r) for r in row]
    for row in row_all:
        row_str_lst.append(','.join(str(e) for e in row))
    if save_csv : # 모델 4-4, 4-7, 4-7-C 순서대로 해보려고 csv 만들 때 사용.
        column_names = select_var.split(", ")
        df = DataFrame(row_all, columns = column_names)
        df.to_csv("ys.csv", mode="w", index=False)
    return row_str_lst

def cal_output(str_dat):
    # output_list = []
    feat_handler = system_lib.get_feat_handler(var_ini, var_mtx, model_number_str)
    est_dict = feat_handler.decode_stream_comma(str_dat, dbg_=False, logger=logger)
    ml_ini_file = "../sys_component/config_machine_learning/rh_macro_m4-7-C.ini"
    ml_ini = configparser.ConfigParser()
    ml_ini.read(ml_ini_file)
    est_eval.update_feat_handler(feat_handler, ml_ini, est_dict, server_mode, logger)
    request_time = 0
    new_filename_words = []
    if model_number_str[0] == '3' or model_number_str[0] == '4':
        new_filename_words.append(model_number_str[0])
        if model_number_str[0] == '3':
            if feat_handler.vars[MODEL_REQUEST_TIME].val < 4:
                request_time = 4
            elif feat_handler.vars[MODEL_REQUEST_TIME].val > 12:
                request_time = 12
            else:
                request_time = feat_handler.vars[MODEL_REQUEST_TIME].val
        if model_number_str[0] == '4':
            if feat_handler.vars[MODEL_REQUEST_TIME].val <= 4:
                request_time = 4
            else:
                request_time = 7
        new_filename_words.append(str(request_time))
        if model_number_str[0] == '3':
            pattern_operation = feat_handler.vars[PATTERN_OPERATION].val
            if pattern_operation == '':
                pattern_number_str = 'C'
                # Todo: exception needs to be added
            else:
                pattern_number_str = refine_model.replace_dict_m2_3_4.pattern_op_num_dict[pattern_operation]
            new_filename_words.append(pattern_number_str)
        elif model_number_str[0] == '4' and request_time == 7 and \
                feat_handler.vars[USE_MOD4_7_TIME].val == 0:
            new_filename_words.append('C')
        ml_ini = system_lib.update_ml_ini_filepath(new_filename_words, ml_ini_file,
                                                   separator='-',
                                                   rh_macro_prefix='rh_macro_m',
                                                   ini_extension='.ini')
        new_model_number_str = ''
        for idx, var in enumerate(new_filename_words):
            if idx == len(new_filename_words) - 1:
                new_model_number_str += var
            else:
                new_model_number_str += var + '-'

        if model_number_str[0] == '3':
            model_number_str_parts = new_model_number_str.split('-')
            new_model_number_str = model_number_str_parts[0] + '-' + model_number_str_parts[1]

        feat_handler = system_lib.get_feat_handler(var_ini, var_mtx, new_model_number_str)
        # try:
        est_dict = feat_handler.decode_stream_comma(str_dat, dbg_=False, logger=logger)

        est_eval.update_feat_handler(feat_handler, ml_ini, est_dict, server_mode, logger)

        if server_mode == 'EST' and model_number_str[0] == '4':  # 모델4 EST의 경우 F_RH_C 대신 F_RH_TARGET_C를 사용해야함.
            est_dict['F_RH_C'] = est_dict['F_RH_TARGET_C']

    # for est_dict in est_dicts:
        output = (learning_lib.calculate_ml_output(est_dict, ml_ini, feat_handler,
                                                           server_mode=server_mode,
                                                           logger=logger))
        return output

# def acc_error(pred, real):

if __name__ == "__main__":
    use_csv = False
    output_list = list()
    var_list = get_input_var()

    var_str = ", ".join(var_list)
    row_str_list = stream_creator(var_str, model_ver='%M4-4\%', save_csv=False)
    # # CSV 파일을 사용할 경우
    if use_csv:
        row_str_list = stream_creator(var_str, save_csv=True)
        data = pd.read_csv("ys.csv")
        for row in [list(row) for row in data.values]:
            r = ",".join(str(e) for e in row)
            output_list.append(cal_output(r))
        print (output_list)
    # DB에서 데이터를 가져올 경우
    else:
        for row in row_str_list:
            output_list.append(cal_output(row))
        print (output_list)

    # 4-7 Dataset 만들기
    my_list = list()
    for row in row_str_list:
        my_list.append(row.split(","))
    df = DataFrame(my_list, columns=var_list)
    charge_list = df['CHARGE_NO'].to_list()
    if np.array(output_list).shape == 1:
        new_output_list = [e for array in output_list for e in array]
    else:
        new_output_list = list()
        for o in output_list:
            new_output_list.append((", ".join(str(e) for e in o)))
    row_str_list =  stream_creator(var_str, model_ver='%M4-7\%', save_csv=False)
    my_list = list()
    for row in row_str_list:
        my_list.append(row.split(","))
    df = DataFrame(my_list, columns=var_list)
    print (df)
    # df[['DEC_TM', 'RH_TOT_OB_INPUT', 'RH_COOLANT_QT', 'RH_RISING_O2_INPUT', 'RH_BEFORE_DEO_AL_INPUT', 'RH_RISING_O2_INPUT']] = new_output_list
    for charge_no, new_output in zip(charge_list, new_output_list):
        l = list()
        for new_out in new_output.split(", "):
            l.append(int(float(new_out)))
        df.loc[df.CHARGE_NO == charge_no, ['DEC_TM', 'RH_TOT_OB_INPUT', 'RH_COOLANT_QT', 'RH_RISING_O2_INPUT',
                                       'RH_BEFORE_DEO_AL_INPUT', 'RH_RISING_O2_INPUT']] = l
    print (df)
    # 4-7 Dataset 으로 예측
    row_str_list = df.values.tolist()
    output_list = list()
    for row in row_str_list:
        # output_list.append(cal_output(",".join(row)))
        output_list.append(cal_output(",".join(str(e) for e in row)))
    print(output_list)

    # 4-7-C Dataset 만들기
    my_list = list()
    for row in row_str_list:
        # my_list.append(row.split(","))
        my_list.append(row)
    df = DataFrame(my_list, columns=var_list)
    charge_list = df['CHARGE_NO'].to_list()
    if np.array(output_list).shape == 1:
        new_output_list = [e for array in output_list for e in array]
    else:
        new_output_list = list()
        for o in output_list:
            new_output_list.append((", ".join(str(e) for e in o)))
    row_str_list =  stream_creator(var_str, model_ver='%M4-7-C\%', save_csv=False)
    my_list = list()
    for row in row_str_list:
        my_list.append(row.split(","))
    df = DataFrame(my_list, columns=var_list)
    print (df)
    # df[['DEC_TM', 'RH_TOT_OB_INPUT', 'RH_COOLANT_QT', 'RH_RISING_O2_INPUT', 'RH_BEFORE_DEO_AL_INPUT', 'RH_RISING_O2_INPUT']] = new_output_list
    for charge_no, new_output in zip(charge_list, new_output_list):
        df.loc[df.CHARGE_NO == charge_no, ['DEC_TM']] = int(float(new_output))
    print (df)
    # 4-7-C Dataset 으로 예측
    row_str_list = df.values.tolist()
    output_list = list()
    for row in row_str_list:
        output_list.append(cal_output(",".join(str(e) for e in row)))
    print(output_list)

    # est_dicts = [{'TRANSACTION_CD': 'RH_4S_ES', 'OP_CODE_N': '', 'CHARGE_NO': '', 'TAP_WORK_DATE': '20180101', 'CC_CAST_NO': '9961', 'MOLTEN_STEEL_CHAR_CD_N': 'TTA6Sa', 'SM_STEEL_GRD_N': 'VU3015YCPE1abcd', 'SM_LD_BLW_METH_TP': 'DS', 'LD_FCE_AC_UBLW_PTRN_NUM': 'S60', 'LD_FCE_LW_BW_PTRN_NUM': 'S52', 'LD_FCE_SCR_OX_QT': 0, 'LD_FCE_N2_COAT_FLOW_QT1': 708, 'LD_FCE_NCT_OX_USE_QT': 188, 'C_MAX': 40, 'TI_AIM': 0.025, 'LD_FCE_OP_SFT': '1C', 'CONV_NUM': '2', 'RH_NO': '2', 'MC_NO': '4', 'CH_SEQ_IN_CAST': 8, 'CONV_TEEM_LD_ME_WGT': 4.0, 'CONV_ME_TAP_WGT': 270.0, 'SM_POSLEAD_CLT_ISQ': 0, 'RTN_RE_CH_M_STE_QT': 566846, 'PRE_SM_TMP': 0, 'LD_SM_SI_QT': 276.6, 'LD_SM_S_QT': 285.0, 'CH_HMR': 282.7, 'CONV_ENDPNT_TEMP': 0, 'CONV_ENDPNT_TARGET_TEMP': 0, 'CONV_ENDPNT_O2': 0, 'CONV_ENDPNT_COOLANT': 1386, 'CONV_REBLOW_INPUT': 0, 'CONV_REBLOW_TIM': 0, 'TAP_TM': 84.19, 'CONV_TAP_HOLE_USE_TIM': 1638, 'LD_WAIT_TM': 1640.0, 'LD_FCE_OX_VW_TM1': 277.0, 'SM_LD_EXCL': 0.0, 'SM_LD_BLW_TM': 0.0, 'LD_FCE_RINS_TM': 213.0, 'LD_FCE_SM_TM': 1.0, 'LD_FCE_TT_TM': 5.1, 'MATERIAL_LINS': 126, 'CONV_TAP_AL_INPUT': 0, 'CONV_TAP_BLI_INPUT': 0, 'CONV_TAP_B_FLUX_INPUT': 0, 'CONV_TAP_C_INPUT': 16, 'FE_SI_J6': 0, 'CONV_TAP_SI_INPUT': 0, 'CONV_TAP_P_INPUT': 0, 'CONV_TAP_HC_CR_INPUT': 47, 'CONV_TAP_MC_CR_INPUT': 0, 'CONV_TAP_LC_CR_INPUT': 0, 'CONV_TAP_NI_INPUT': 399, 'CU_LD_QT_TOT': 193, 'CONV_TAP_MO_INPUT': 0, 'CONV_TAP_SN_INPUT': 0, 'CONV_TAP_SB_INPUT': 0, 'BAP_DEP_TEMP': 0, 'BAP_DEP_O2': 0, 'CHEM05_F_BAP_C_VAL_1': 0, 'BAP_BLI_INPUT': 0, 'BAP_AL_INPUT': 0, 'BAP_COOLANT_INPUT': 0, 'BAP_TB_TM': 0.0, 'BAP_BB_TM': 0.0, 'RH_ARR_TEMP': 0, 'RH_REQ_TEMP': 1610, 'RH_ARR_O2': 500, 'RH_BEFORE_DEO_TEMP': 0, 'RH_BEFORE_DEO_O2': 0, 'PATTERN_OPERATION': 'G_PREC', 'RH_DEP_TEMP': 0, 'RH_TARGET_TEMP': 0, 'TUNDISH_TARGET_TEMP': 0, 'TUNDISH_AVR_TEMP': 0, 'RH_REQ_TEMP_GAP': 0, 'RH_DEP_ARR_TEMP_GAP': 0, 'RH_TOT_OB_INPUT': 0, 'RH_RISING_O2_INPUT': 28, 'RH_COOLANT_QT': 27, 'RH_AL_TOTAL_INPUT': 1583, 'RH_BEFORE_DEO_AL_INPUT': 0, 'DEO_QT': 0, 'REV_QT': 0, 'BEFORE_DEO_C_INPUT_RATIO': 388, 'BEFORE_DEO_C_INPUT_QT': 1574, 'BEFORE_DEO_C_INPUT_TM': 337, 'RH_TOTAL_ALLOY_INPUT': 0, 'RH_TI_INPUT': 1579, 'RH_METAL_MN_INPUT': 1586, 'RH_SI_INPUT': 1553, 'CCW_TM': 0, 'RH_LOWEST_VACUUM': 0, 'DEPOSIT_DEPTH': 0, 'RH_STEAM_PRESSURE': -7.0, 'RH_STEAM_STEAM_QT': -8, 'DEO_CO2': 0, 'DEO_CO': 0.0, 'DEO_CO_CO2': 48.0, 'ARR_VACUUM_500TORR': 1, 'ARR_VACUUM_250TORR': 0, 'ARR_VACUUM_100TORR': 0, 'F_RH_TARGET_C': 0, 'F_RH_C': 0, 'F_CC_C': 0, 'F_BAP_SI': 0, 'F_BAP_P': 0, 'C_PICK_UP': 0, 'LADLE_PREPERATION_TAP': 0.0, 'SM_2ND_REF_BAP_VAC_LAD_SHT_TM': 0.0, 'EMT_LD_TM': 0, 'DSTL_LAD_NUM': 0, 'DSTL_LAD_USAGE': 0, 'LAD_SHT_TM': 0, 'BEFORE_LAD_DISTINCT_STEEL': '', 'RH_VESSEL_DISTINCT': '0', 'RH_LVES_USE_TIM': 295, 'RH_DW_SOAK_NUM': 1, 'SM_SREF_RH_QUE_TM': 0, 'RH_BEFORE_VESSEL_TM': 0, 'RH_AFTER_VESSEL_TM': 0, 'BEFORE_VESSEL_DISTINCT_STEEL': '168.5', 'TAP_BB_TM': 126.5, 'BAP_TM': 0.0, 'TM_BAP_RH': 0.0, 'RH_TM': 0, 'DEC_TM': 0, 'SM_SREF_STAG_TM': 0, 'VACUUM_TM_1M': 0, 'REFLUX_FLOW_TM_1M': 0, 'CO_1M': 0, 'CO2_1M': 0, 'O2_1M': 0, 'GAS_1M': 0, 'VACUUM_TM_2M': 66, 'REFLUX_FLOW_TM_2M': 15, 'CO_2M': 0, 'CO2_2M': 0, 'O2_2M': 0, 'GAS_2M': 196, 'VACUUM_TM_3M': 0, 'REFLUX_FLOW_TM_3M': 0, 'CO_3M': 0, 'CO2_3M': 0, 'O2_3M': 0, 'GAS_3M': 17, 'VACUUM_TM_4M': 1, 'REFLUX_FLOW_TM_4M': 10, 'CO_4M': 0, 'CO2_4M': 0, 'O2_4M': 26.6, 'GAS_4M': 7, 'VACUUM_TM_5M': 98, 'REFLUX_FLOW_TM_5M': 0, 'CO_5M': 3.05, 'CO2_5M': 9.96, 'O2_5M': 0, 'GAS_5M': 0, 'VACUUM_TM_6M': 0, 'REFLUX_FLOW_TM_6M': 5, 'CO_6M': 5.0, 'CO2_6M': 0, 'O2_6M': 7.0, 'GAS_6M': 1, 'VACUUM_TM_7M': 0, 'REFLUX_FLOW_TM_7M': 1, 'CO_7M': 139.0, 'CO2_7M': 0.0, 'O2_7M': 53.0, 'GAS_7M': 0, 'VACUUM_TM_8M': 0, 'REFLUX_FLOW_TM_8M': 1, 'CO_8M': 5.1, 'CO2_8M': 4.0, 'O2_8M': 4.0, 'GAS_8M': 2, 'VACUUM_TM_9M': 1, 'REFLUX_FLOW_TM_9M': 0, 'CO_9M': 0, 'CO2_9M': 0.0019, 'O2_9M': 0, 'GAS_9M': 0, 'VACUUM_TM_10M': 0, 'REFLUX_FLOW_TM_10M': 0, 'CO_10M': 0.022, 'CO2_10M': 0, 'O2_10M': 0.0229, 'GAS_10M': 0, 'VACUUM_TM_11M': 0, 'REFLUX_FLOW_TM_11M': 0, 'CO_11M': 23.0, 'CO2_11M': 0, 'O2_11M': 0.0027, 'GAS_11M': 0, 'VACUUM_TM_12M': 0, 'REFLUX_FLOW_TM_12M': 0, 'CO_12M': 0, 'CO2_12M': 0, 'O2_12M': 0.146, 'GAS_12M': 0, 'VACUUM_TM_13M': 0, 'REFLUX_FLOW_TM_13M': 0, 'CO_13M': 0.015, 'CO2_13M': 0.02, 'O2_13M': 0.0, 'GAS_13M': 0, 'VACUUM_TM_14M': 0, 'REFLUX_FLOW_TM_14M': 0, 'CO_14M': 0, 'CO2_14M': 0, 'O2_14M': 0, 'GAS_14M': 0, 'VACUUM_TM_15M': 0, 'REFLUX_FLOW_TM_15M': 0, 'CO_15M': 0, 'CO2_15M': 0, 'O2_15M': 13.3, 'GAS_15M': 0, 'VACUUM_TM_16M': 4187, 'REFLUX_FLOW_TM_16M': 14, 'CO_16M': 33.0, 'CO2_16M': 1.0, 'O2_16M': 1810.0, 'GAS_16M': 0, 'VACUUM_TM_17M': 0, 'REFLUX_FLOW_TM_17M': 0, 'CO_17M': 0, 'CO2_17M': 0, 'O2_17M': 0, 'GAS_17M': 130, 'VACUUM_TM_18M': 55, 'REFLUX_FLOW_TM_18M': 0, 'CO_18M': 76.6, 'CO2_18M': 0.0, 'O2_18M': 1100.0, 'GAS_18M': 0, 'VACUUM_TM_19M': 0, 'REFLUX_FLOW_TM_19M': 0, 'CO_19M': 62.6, 'CO2_19M': 0.6, 'O2_19M': 0.3, 'GAS_19M': 0, 'VACUUM_TM_20M': 0, 'REFLUX_FLOW_TM_20M': 0, 'CO_20M': 10.0, 'CO2_20M': 3.0, 'O2_20M': 5.0, 'GAS_20M': 35, 'VACUUM_TM_21M': 0, 'REFLUX_FLOW_TM_21M': 33, 'CO_21M': 0.0, 'CO2_21M': 1.21, 'O2_21M': 2.78, 'GAS_21M': 1, 'MODEL_REQUEST_TIME': 4, 'USE_MOD4_EST_VAL': 0, 'USE_MOD4_7_TIME': 1, 'MINUTE_C': 0},
    #              {'TRANSACTION_CD': 'RH_4S_ES', 'OP_CODE_N': '', 'CHARGE_NO': '', 'TAP_WORK_DATE': '20180101', 'CC_CAST_NO': '9961', 'MOLTEN_STEEL_CHAR_CD_N': 'TTA6Sa', 'SM_STEEL_GRD_N': 'VU3015YCPE1abcd', 'SM_LD_BLW_METH_TP': 'DS', 'LD_FCE_AC_UBLW_PTRN_NUM': 'S60', 'LD_FCE_LW_BW_PTRN_NUM': 'S52', 'LD_FCE_SCR_OX_QT': 0, 'LD_FCE_N2_COAT_FLOW_QT1': 708, 'LD_FCE_NCT_OX_USE_QT': 188, 'C_MAX': 40, 'TI_AIM': 0.025, 'LD_FCE_OP_SFT': '1C', 'CONV_NUM': '2', 'RH_NO': '2', 'MC_NO': '4', 'CH_SEQ_IN_CAST': 8, 'CONV_TEEM_LD_ME_WGT': 4.0, 'CONV_ME_TAP_WGT': 270.0, 'SM_POSLEAD_CLT_ISQ': 0, 'RTN_RE_CH_M_STE_QT': 566846, 'PRE_SM_TMP': 0, 'LD_SM_SI_QT': 276.6, 'LD_SM_S_QT': 285.0, 'CH_HMR': 282.7, 'CONV_ENDPNT_TEMP': 0, 'CONV_ENDPNT_TARGET_TEMP': 0, 'CONV_ENDPNT_O2': 0, 'CONV_ENDPNT_COOLANT': 1386, 'CONV_REBLOW_INPUT': 0, 'CONV_REBLOW_TIM': 0, 'TAP_TM': 84.19, 'CONV_TAP_HOLE_USE_TIM': 1638, 'LD_WAIT_TM': 1640.0, 'LD_FCE_OX_VW_TM1': 277.0, 'SM_LD_EXCL': 0.0, 'SM_LD_BLW_TM': 0.0, 'LD_FCE_RINS_TM': 213.0, 'LD_FCE_SM_TM': 1.0, 'LD_FCE_TT_TM': 5.1, 'MATERIAL_LINS': 126, 'CONV_TAP_AL_INPUT': 0, 'CONV_TAP_BLI_INPUT': 0, 'CONV_TAP_B_FLUX_INPUT': 0, 'CONV_TAP_C_INPUT': 16, 'FE_SI_J6': 0, 'CONV_TAP_SI_INPUT': 0, 'CONV_TAP_P_INPUT': 0, 'CONV_TAP_HC_CR_INPUT': 47, 'CONV_TAP_MC_CR_INPUT': 0, 'CONV_TAP_LC_CR_INPUT': 0, 'CONV_TAP_NI_INPUT': 399, 'CU_LD_QT_TOT': 193, 'CONV_TAP_MO_INPUT': 0, 'CONV_TAP_SN_INPUT': 0, 'CONV_TAP_SB_INPUT': 0, 'BAP_DEP_TEMP': 0, 'BAP_DEP_O2': 0, 'CHEM05_F_BAP_C_VAL_1': 0, 'BAP_BLI_INPUT': 0, 'BAP_AL_INPUT': 0, 'BAP_COOLANT_INPUT': 0, 'BAP_TB_TM': 0.0, 'BAP_BB_TM': 0.0, 'RH_ARR_TEMP': 0, 'RH_REQ_TEMP': 1610, 'RH_ARR_O2': 500, 'RH_BEFORE_DEO_TEMP': 0, 'RH_BEFORE_DEO_O2': 0, 'PATTERN_OPERATION': 'G_PREC', 'RH_DEP_TEMP': 0, 'RH_TARGET_TEMP': 0, 'TUNDISH_TARGET_TEMP': 0, 'TUNDISH_AVR_TEMP': 0, 'RH_REQ_TEMP_GAP': 0, 'RH_DEP_ARR_TEMP_GAP': 0, 'RH_TOT_OB_INPUT': 0, 'RH_RISING_O2_INPUT': 28, 'RH_COOLANT_QT': 27, 'RH_AL_TOTAL_INPUT': 1583, 'RH_BEFORE_DEO_AL_INPUT': 0, 'DEO_QT': 0, 'REV_QT': 0, 'BEFORE_DEO_C_INPUT_RATIO': 388, 'BEFORE_DEO_C_INPUT_QT': 1574, 'BEFORE_DEO_C_INPUT_TM': 337, 'RH_TOTAL_ALLOY_INPUT': 0, 'RH_TI_INPUT': 1579, 'RH_METAL_MN_INPUT': 1586, 'RH_SI_INPUT': 1553, 'CCW_TM': 0, 'RH_LOWEST_VACUUM': 0, 'DEPOSIT_DEPTH': 0, 'RH_STEAM_PRESSURE': -7.0, 'RH_STEAM_STEAM_QT': -8, 'DEO_CO2': 0, 'DEO_CO': 0.0, 'DEO_CO_CO2': 48.0, 'ARR_VACUUM_500TORR': 1, 'ARR_VACUUM_250TORR': 0, 'ARR_VACUUM_100TORR': 0, 'F_RH_TARGET_C': 0, 'F_RH_C': 0, 'F_CC_C': 0, 'F_BAP_SI': 0, 'F_BAP_P': 0, 'C_PICK_UP': 0, 'LADLE_PREPERATION_TAP': 0.0, 'SM_2ND_REF_BAP_VAC_LAD_SHT_TM': 0.0, 'EMT_LD_TM': 0, 'DSTL_LAD_NUM': 0, 'DSTL_LAD_USAGE': 0, 'LAD_SHT_TM': 0, 'BEFORE_LAD_DISTINCT_STEEL': '', 'RH_VESSEL_DISTINCT': '0', 'RH_LVES_USE_TIM': 295, 'RH_DW_SOAK_NUM': 1, 'SM_SREF_RH_QUE_TM': 0, 'RH_BEFORE_VESSEL_TM': 0, 'RH_AFTER_VESSEL_TM': 0, 'BEFORE_VESSEL_DISTINCT_STEEL': '168.5', 'TAP_BB_TM': 126.5, 'BAP_TM': 0.0, 'TM_BAP_RH': 0.0, 'RH_TM': 0, 'DEC_TM': 0, 'SM_SREF_STAG_TM': 0, 'VACUUM_TM_1M': 0, 'REFLUX_FLOW_TM_1M': 0, 'CO_1M': 0, 'CO2_1M': 0, 'O2_1M': 0, 'GAS_1M': 0, 'VACUUM_TM_2M': 66, 'REFLUX_FLOW_TM_2M': 15, 'CO_2M': 0, 'CO2_2M': 0, 'O2_2M': 0, 'GAS_2M': 196, 'VACUUM_TM_3M': 0, 'REFLUX_FLOW_TM_3M': 0, 'CO_3M': 0, 'CO2_3M': 0, 'O2_3M': 0, 'GAS_3M': 17, 'VACUUM_TM_4M': 1, 'REFLUX_FLOW_TM_4M': 10, 'CO_4M': 0, 'CO2_4M': 0, 'O2_4M': 26.6, 'GAS_4M': 7, 'VACUUM_TM_5M': 98, 'REFLUX_FLOW_TM_5M': 0, 'CO_5M': 3.05, 'CO2_5M': 9.96, 'O2_5M': 0, 'GAS_5M': 0, 'VACUUM_TM_6M': 0, 'REFLUX_FLOW_TM_6M': 5, 'CO_6M': 5.0, 'CO2_6M': 0, 'O2_6M': 7.0, 'GAS_6M': 1, 'VACUUM_TM_7M': 0, 'REFLUX_FLOW_TM_7M': 1, 'CO_7M': 139.0, 'CO2_7M': 0.0, 'O2_7M': 53.0, 'GAS_7M': 0, 'VACUUM_TM_8M': 0, 'REFLUX_FLOW_TM_8M': 1, 'CO_8M': 5.1, 'CO2_8M': 4.0, 'O2_8M': 4.0, 'GAS_8M': 2, 'VACUUM_TM_9M': 1, 'REFLUX_FLOW_TM_9M': 0, 'CO_9M': 0, 'CO2_9M': 0.0019, 'O2_9M': 0, 'GAS_9M': 0, 'VACUUM_TM_10M': 0, 'REFLUX_FLOW_TM_10M': 0, 'CO_10M': 0.022, 'CO2_10M': 0, 'O2_10M': 0.0229, 'GAS_10M': 0, 'VACUUM_TM_11M': 0, 'REFLUX_FLOW_TM_11M': 0, 'CO_11M': 23.0, 'CO2_11M': 0, 'O2_11M': 0.0027, 'GAS_11M': 0, 'VACUUM_TM_12M': 0, 'REFLUX_FLOW_TM_12M': 0, 'CO_12M': 0, 'CO2_12M': 0, 'O2_12M': 0.146, 'GAS_12M': 0, 'VACUUM_TM_13M': 0, 'REFLUX_FLOW_TM_13M': 0, 'CO_13M': 0.015, 'CO2_13M': 0.02, 'O2_13M': 0.0, 'GAS_13M': 0, 'VACUUM_TM_14M': 0, 'REFLUX_FLOW_TM_14M': 0, 'CO_14M': 0, 'CO2_14M': 0, 'O2_14M': 0, 'GAS_14M': 0, 'VACUUM_TM_15M': 0, 'REFLUX_FLOW_TM_15M': 0, 'CO_15M': 0, 'CO2_15M': 0, 'O2_15M': 13.3, 'GAS_15M': 0, 'VACUUM_TM_16M': 4187, 'REFLUX_FLOW_TM_16M': 14, 'CO_16M': 33.0, 'CO2_16M': 1.0, 'O2_16M': 1810.0, 'GAS_16M': 0, 'VACUUM_TM_17M': 0, 'REFLUX_FLOW_TM_17M': 0, 'CO_17M': 0, 'CO2_17M': 0, 'O2_17M': 0, 'GAS_17M': 130, 'VACUUM_TM_18M': 55, 'REFLUX_FLOW_TM_18M': 0, 'CO_18M': 76.6, 'CO2_18M': 0.0, 'O2_18M': 1100.0, 'GAS_18M': 0, 'VACUUM_TM_19M': 0, 'REFLUX_FLOW_TM_19M': 0, 'CO_19M': 62.6, 'CO2_19M': 0.6, 'O2_19M': 0.3, 'GAS_19M': 0, 'VACUUM_TM_20M': 0, 'REFLUX_FLOW_TM_20M': 0, 'CO_20M': 10.0, 'CO2_20M': 3.0, 'O2_20M': 5.0, 'GAS_20M': 35, 'VACUUM_TM_21M': 0, 'REFLUX_FLOW_TM_21M': 33, 'CO_21M': 0.0, 'CO2_21M': 1.21, 'O2_21M': 2.78, 'GAS_21M': 1, 'MODEL_REQUEST_TIME': 4, 'USE_MOD4_EST_VAL': 0, 'USE_MOD4_7_TIME': 1, 'MINUTE_C': 0}]
    # cal_output(est_dicts)