import argparse
import configparser
import os
import socket
import sys
import queue
from lib import sys_lib as system_lib
from lib import data as data_lib
from lib import learning as learning_lib
from lib import networking

from sys_component import learning_handler

from models import est_eval
import refine_model

DB_OP_ = True
LOGGER_FOLER = 'log'
HEADER_LENGTH = 8
MODEL_REQUEST_TIME = 'MODEL_REQUEST_TIME'
USE_MOD4_7_TIME = 'USE_MOD4_7_TIME'
PATTERN_OPERATION = 'PATTERN_OPERATION'
RH_MACRO_PREFIX = 'rh_macro_m'
INI_EXTENSION = '.ini'


def create_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("--server_mode", required=True, help="server mode - EST or EVAL")
    parser.add_argument("--ml_ini_file", required=True, help="machine learning ini file")
    parser.add_argument("--networking_ini_file", required=True, help="networking ini file")
    parser.add_argument("--var_ini_file", required=True, help="var ini file")
    parser.add_argument("--var_csv_file", required=True, help="var csv file")
    return parser


def send_error_message(error_message, client_ip, client_port):
    print(error_message)
    sock_res = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    client_address = (client_ip,
                      client_port)
    sock_res.connect(client_address)

    sock_res.sendall(error_message.encode('utf-8'))
    sock_res.close()


def main():
    parser = create_parser()
    args = parser.parse_args()

    networking_ini = configparser.ConfigParser()
    networking_ini.read(args.networking_ini_file)
    ml_ini = configparser.ConfigParser()
    ml_ini.read(args.ml_ini_file)

    var_ini = configparser.ConfigParser()
    var_ini.read(args.var_ini_file)

    var_mtx = data_lib.read_csv_file(args.var_csv_file)

    server_mode = args.server_mode

    if not os.path.exists(LOGGER_FOLER):
        os.makedirs(LOGGER_FOLER)
    logger_name = ml_ini['general']['model_name'] + '_' + server_mode
    logger = system_lib.setup_logger(logger_name,
                                     logger_name.lower(),
                                     folder=LOGGER_FOLER,
                                     console_=True)

    logger.info("START " + ml_ini['general']['model_name'] + "_" + server_mode + " server")

    server_address = (networking_ini[server_mode + '_SERVER']['ip'],
                      int(networking_ini[server_mode + '_SERVER']['port']))

    sock_req = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    logger.info("Starting up on %s port %s..." % server_address)
    sock_req.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    sock_req.bind(server_address)
    sock_req.listen(10)

    while True:
        connected, str_dat, client_address = networking.accept(sock_req, logger)

        if not connected:
            continue
        if "Error" in str_dat:
            client_ip = networking_ini[server_mode + '_CLIENT']['ip']
            client_port = int(networking_ini[server_mode + '_CLIENT']['port'])
            send_error_message(str_dat, client_ip, client_port)
            continue

        model_number_str = ml_ini['general']['model_number']
        request_time = 0

        feat_handler = system_lib.get_feat_handler(var_ini, var_mtx, model_number_str)
        est_dict = feat_handler.decode_stream_comma(str_dat, dbg_=False, logger=logger)
        est_eval.update_feat_handler(feat_handler, ml_ini, est_dict, server_mode, logger)

        new_filename_words = []
        if model_number_str[0] == '3' or model_number_str[0] == '4':
            new_filename_words.append(model_number_str[0])
            if model_number_str[0] == '3':
                if feat_handler.vars[MODEL_REQUEST_TIME].val < 4:
                    request_time = 4
                elif feat_handler.vars[MODEL_REQUEST_TIME].val > 12:
                    request_time = 12
                else:
                    request_time = feat_handler.vars[MODEL_REQUEST_TIME].val
            if model_number_str[0] == '4':
                if feat_handler.vars[MODEL_REQUEST_TIME].val <= 4:
                    request_time = 4
                else:
                    request_time = 7
            new_filename_words.append(str(request_time))
            if model_number_str[0] == '3':
                pattern_operation = feat_handler.vars[PATTERN_OPERATION].val
                if pattern_operation == '':
                    pattern_number_str = 'C'
                    # Todo: exception needs to be added
                else:
                    pattern_number_str = refine_model.replace_dict_m2_3_4.pattern_op_num_dict[pattern_operation]
                new_filename_words.append(pattern_number_str)
            elif model_number_str[0] == '4' and request_time == 7 and \
                feat_handler.vars[USE_MOD4_7_TIME].val == 0:
                new_filename_words.append('C')

            ml_ini = system_lib.update_ml_ini_filepath(new_filename_words, args.ml_ini_file,
                                                       separator='-',
                                                       rh_macro_prefix='rh_macro_m',
                                                       ini_extension='.ini')
            new_model_number_str = ''
            for idx, var in enumerate(new_filename_words):
                if idx == len(new_filename_words) - 1:
                    new_model_number_str += var
                else:
                    new_model_number_str += var + '-'

            if model_number_str[0] == '3':
                model_number_str_parts = new_model_number_str.split('-')
                new_model_number_str = model_number_str_parts[0] + '-' + model_number_str_parts[1]

            feat_handler = system_lib.get_feat_handler(var_ini, var_mtx, new_model_number_str)

            # try:
            est_dict = feat_handler.decode_stream_comma(str_dat, dbg_=False, logger=logger)

            est_eval.update_feat_handler(feat_handler, ml_ini, est_dict, server_mode, logger)

            if server_mode == 'EST' and model_number_str[0] == '4':  # 모델4 EST의 경우 F_RH_C 대신 F_RH_TARGET_C를 사용해야함.
                est_dict['F_RH_C'] = est_dict['F_RH_TARGET_C']

        output_list = learning_lib.calculate_ml_output(est_dict, ml_ini, feat_handler,
                                                       server_mode=server_mode,
                                                       logger=logger)
        # # try:
        # test_model_dir = os.path.join(ml_ini['general']['root_dir'], ml_ini['general']['model_name'], 'test_190823')
        # dataset_file_list = ['dataset_file_17',
        #                      'dataset_file_18_19',
        #                      'dataset_file_1801_1806',
        #                      'dataset_file_1804_1809',
        #                      'dataset_file_1807_1812',
        #                      'dataset_file_1810_1903']
        # for model_dir_name in dataset_file_list:
        #     model_dir_name = os.path.join(test_model_dir, model_dir_name)
        #     try:
        #         test_output_list = learning_lib.calculate_ml_output_test(est_dict, ml_ini, model_dir_name, logger=logger)
        #     except FileNotFoundError:
        #         print('Wrong file or file path')
        #         continue
        #     else:
        #         break
        #     result_file_path = os.path.join(model_dir_name, 'result.txt')
        #     if os.path.exists(result_file_path):
        #         option = 'a'
        #     else:
        #         option = 'w'
        #     content = 'port number: ' + networking_ini[server_mode + '_SERVER']['port'] + '\n'
        #     content += str(system_lib.get_datetime()) + '\n'
        #     for output in test_output_list:
        #         if isinstance(output, list):
        #             output = [str(element) for element in output]
        #             output = output.join(' ')
        #         content += '\n' + str(output)
        #     content += '\n\n'
        #     file = open(result_file_path, option)
        #     file.write(content)
        # # except Exception as e:
        # #     print(e)
        # #     print("Test Error")

        feat_handler.vars['EST_END_TIME'].val = system_lib.get_datetime()

        if DB_OP_:
            db_handler = system_lib.get_db_handler(db_ini=networking_ini[server_mode + '_DB'],
                                                   logger=logger,
                                                   DB_OP_=DB_OP_)
            table_info = data_lib.read_csv_file(networking_ini[server_mode + '_DB']['table_info_csv'])
            refined_db_data = system_lib.get_refined_db_data(var_ini, ml_ini, networking_ini[server_mode+'_CLIENT'][server_mode.lower()+'_name'], table_info, db_handler, feat_handler,
                                                             logger)
            if server_mode == 'EVAL' and system_lib.time_to_self_learn(ml_ini) and refined_db_data is not None:

                dataset = data_lib.read_csv_file(ml_ini['general']['dataset_file'])

                org_dataset_len = len(dataset) - 1
                add_dataset_len = len(refined_db_data) - 1

                # sort by local dataset column
                sorted_db_data = data_lib.extract_rows_from_table(refined_db_data, dataset[0])
                dataset += sorted_db_data[1:]
                data_lib.write_list_to_csv(dataset, ml_ini['general']['dataset_file'],
                                           overwrite=True)
                msg = "Write new self-learning dataset, {:d} + {:d}".format(org_dataset_len, add_dataset_len)
                logger.info(msg)

                est_eval.evaluation_start(ml_ini, dataset, feat_handler, msg, logger)

            name = server_mode.lower() + '_' + 'name'
            if str_dat.split(',')[0] == networking_ini[server_mode + '_CLIENT'][name]:
                db_row_dict = data_lib.gen_db_row_dat(table_info, feat_handler, server_mode)
                db_handler.insertOne(db_row_dict)
                logger.info("{}_DB read done".format(server_mode))

                print("{}_DB read back:".format(server_mode) + str(db_handler.selectOne(db_handler.selectLastPK())))

        logger.info("{} {} operation, ({:d}), ({:d}) - {}.".format(ml_ini['general']['model_name'],
                                                                   server_mode,
                                                                   len(str_dat),
                                                                   len(est_dict),
                                                                   str(est_dict)))

        sock_res = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        client_address = (networking_ini[server_mode + '_CLIENT']['ip'],
                          int(networking_ini[server_mode + '_CLIENT']['port']))
        sock_res.connect(client_address)

        out_stream = feat_handler.encode_stream(dbg_=False, dataset_order_list=[1, 0], additional_items=['CHARGE_NO'])
        logger.info("OUTPUT: " + out_stream)
        predict_name = networking_ini[args.server_mode + '_CLIENT']['predict_name']
        logger.info("Predict Name:" + predict_name)

        out_stream = predict_name + out_stream[len(predict_name):]
        while True:
            if len(out_stream.split(',')) < 9:
                out_stream += ','
            else:
                break
        if 'CVT_MACRO' in ml_ini['general']['model_name']:
            out_stream += ',' + ',' + feat_handler.vars[server_mode + '_RANGE_CHECK_VARS'].val
        else:
            out_stream += ',' + str(feat_handler.vars['USE_MOD4_7_TIME'].val) + ',' + feat_handler.vars[server_mode+'_RANGE_CHECK_VARS'].val
        # output_str = ''
        # for output in output_list:
        #     output_str += str(output) + ', '
        # output_str = output_str[:-2]
        # print(str(output_str))
        print(out_stream)
        sock_res.sendall(out_stream.encode('utf-8'))
        sock_res.close()

if __name__ == "__main__":
    if len(sys.argv) == 1:
        sys.argv.extend([
            "--server_mode", "EVAL",
            "--var_ini_file", "../sys_component/cvt_macro_var.ini",
            "--var_csv_file", "../sys_component/cvt_macro_var.csv",
            "--ml_ini_file", "../sys_component/config_machine_learning/cvt_macro_m1.ini",
            "--networking_ini_file", "../sys_component/config_networking/cvt_m1_cfg_sa_posco.ini",
            #
            # "--server_mode", "EVAL",
            # "--var_ini_file", "../sys_component/rh_macro_var.ini",
            # "--var_csv_file", "../sys_component/rh_macro_var.csv",
            # "--ml_ini_file", "../sys_component/config_machine_learning/rh_macro_m4-7-C.ini",
            # "--networking_ini_file", "../sys_component/config_networking/rh_m4_cfg_sa_minds.ini",
        ])

    main()
