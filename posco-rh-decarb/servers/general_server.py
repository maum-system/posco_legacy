#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import sys
import argparse
import configparser
import socket
from lib import sys_lib as lib
from lib import HoonUtils as utils
import json


DB_OP_ = True


def parse_arguments(argv):
    parser = argparse.ArgumentParser()
    parser.add_argument("--cfg_ini_file", required=True, help="configuration ini file")
    parser.add_argument("--log_no_console_", default=False, action='store_true', help="no console flag")
    # parser.add_argument("--ml_ini_file", required=True, help="machine learning ini file")

    return parser.parse_args(argv)


def main(args):
    this_folder = os.path.dirname(os.path.abspath(__file__))
    ini = configparser.ConfigParser()
    ini.read(os.path.join(this_folder, args.cfg_ini_file))
    ini = utils.remove_comments_in_ini(ini, delimiter="###")

    logger = utils.setup_logger(ini['LOG']['name'],
                                ini['LOG']['prefix'],
                                folder=ini['LOG']['folder'],
                                console_=(not args.log_no_console_))
    logger.info("START \"" + ini['LOG']['name'] + "\" server.")

    exec(ini['GENERAL']['import'] + " as " + ini['GENERAL']['name'])
    func = eval(ini['GENERAL']['name'] + "." + ini['GENERAL']['name'] + "()")
    func.initialize(ini)

    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_address = (ini['SERVER']['ip'], int(ini['SERVER']['port']))
    logger.info(" # Starting up \"{}\" SERVER on {}:{}...".
                format(ini['GENERAL']['name'], server_address[0], server_address[1]))
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    sock.bind(server_address)
    sock.listen(5)

    while True:

        status, str_dat = lib.accept_receive_and_check_in_server(sock, logger=logger)

        if status == -1:
            sock.close()
            sys.exit()

        elif status == 1:
            ret, ret_dat = func.run(str_dat)
            if ret_dat:
                lib.send_data_to_server_once(ret_dat, ini['CLIENT']['ip'], int(ini['CLIENT']['port']), logger=logger)
        else:
            pass
        logger.info("")


SELF_TEST_ = True


if __name__ == "__main__":
    if len(sys.argv) == 1:
        if SELF_TEST_:
            sys.argv.extend([
                "--cfg_ini_file", "EchoServer.ini",
            ])
        else:
            sys.argv.extend(["--help"])

    main(parse_arguments(sys.argv[1:]))
