#! /usr/bin/env python
# -*- coding: utf-8 -*-
""" Client Code for testing evaluation module.

    HISTORY
    Ver 0.1

"""
import sys
import argparse
import socket
import configparser
import time

from lib.posco import get_datetime

RECV_BUFF_SIZE = 1024
MODE = 'EST'  # EST / EVAL

def main():

    parser = argparse.ArgumentParser()
    parser.add_argument("--cat", required=True, help="Category, rh, cvt, or rh+")
    parser.add_argument("--cfg_ini_file", required=True, help="configuration ini file")
    parser.add_argument("--data_file", required=True, help="stream data file")

    args = parser.parse_args()
    cfg = configparser.ConfigParser()
    cfg.read(args.cfg_ini_file)

    data_list = open(args.data_file, encoding='utf-8').readlines()

    for i in range(len(data_list)):
        sock_req = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        server_address = (cfg[MODE + '_SERVER']['ip'], int(cfg[MODE + '_SERVER']['port']))

        print(MODE + " Client> connecting to %s port %s..." % server_address)
        try:
            sock_req.connect(server_address)
        except ConnectionRefusedError:
            print(" @ ConnectionRefusedError")
            sys.exit()

        # time.sleep(10)
        try:
            while MODE[0:2] not in data_list[i][:8] or args.cat[-1] not in data_list[i][:8]:
                i += 1
            print(MODE + " Client> {} : sending \"{}\"".format(get_datetime(), data_list[i]))
            # sock_req.sendall(stream.encode('utf-8'))
            sock_req.sendall(data_list[i].encode('utf-8'))
        finally:
            print(MODE + " Client> {} : closing socket...".format(get_datetime()))
            sock_req.close()

        sock_res = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        client_address = (cfg[MODE + '_CLIENT']['ip'], int(cfg[MODE + '_CLIENT']['port']))
        print("Starting up on %s port %s..." % client_address)
        sock_res.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        sock_res.bind(client_address)
        sock_res.listen(1)

        print("Waiting for a connection...")
        connection, server_address = sock_res.accept()
        print("Connection for {}...".format(server_address))
        try:
            str_dat = str(connection.recv(RECV_BUFF_SIZE), encoding='utf-8')
            print("SOCK_REQ: {}".format(str_dat))
        finally:
            connection.close()

        time.sleep(3)

if __name__ == "__main__":
    if len(sys.argv) == 1:
        sys.argv.extend([
                        # "--cat", "CVT_M1",
                        # "--cfg_ini_file", "../sys_component/config_networking/cvt_m1_cfg_sa_posco.ini",
                        # "--data_file", "../sys_component/streams/stream_cvt_evalest.txt"

                        "--cat", "RH_M6",
                        "--cfg_ini_file", "../sys_component/config_networking/rh_m6_cfg_sa_posco.ini",
                        # "--data_file", "../sys_component/streams/stream.txt"
                        "--data_file", "../sys_component/streams/stream_rh_evalest.txt"
        ])
    main()