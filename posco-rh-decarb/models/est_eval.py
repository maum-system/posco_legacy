import configparser
import os
import pickle
from datetime import datetime

from handlers import learning_handler
from lib import learning as learning_lib
from lib import sys_lib as system_lib
from lib import data as data_lib


def get_est_dict(ml_ini,
                 feat_handler,
                 str_dat, logger,
                 DB_OP_=False, server_mode='EVAL'):

    try:
        decode_dict = feat_handler.decode_stream_comma(str_dat, dbg_=False, logger=logger)
    except Exception as e:
        logger.error("FATAL : decode_stream : " + str(e))
        return False

    logger.info("{} {} operation, ({:d}), ({:d}) - {}.".format(ml_ini['general']['model_name'],
                                                               server_mode,
                                                               len(str_dat),
                                                               len(decode_dict),
                                                               str(decode_dict)))

    # Check variable and process type
    check_status, check_list = feat_handler.check_var_ranges(decode_dict, fix_=True, logger=logger)
    check_process_type = feat_handler.check_var_process_type(decode_dict, cat=ml_ini['general']['model_name'], logger=logger)
    feat_handler.check_process_type = str(check_process_type)

    model_dir = system_lib.get_model_dir(ml_ini['general']['root_dir'],
                                         ml_ini['general']['model_name'],
                                         ml_ini['general']['method'],
                                         ml_ini['general']['version'],
                                         ml_ini['general']['result_file'])

    feat_handler.vars[server_mode + '_START_TIME'].val = system_lib.get_datetime()
    feat_handler.vars[server_mode + '_RANGE_CHECK'].val = int(check_status)
    feat_handler.vars[server_mode + '_RANGE_CHECK_VARS'].val = str(check_list).replace("\'", "").replace("\"", "")
    feat_handler.vars[server_mode + '_METHOD'].val = ml_ini['general']['method']
    feat_handler.vars[server_mode + '_MODEL_VER'].val = model_dir

    return decode_dict


def estimate(networking_ini, ml_ini, feat_handler, str_dat, est_dict, logger, DB_OP_=True, server_mode='EST'):
    est_db_handler, out_val = estimation(ml_ini,
                                         networking_ini,
                                         feat_handler,
                                         est_dict,
                                         logger, DB_OP_,
                                         server_mode)

    est_table_info = data_lib.read_csv_file(networking_ini['EST_DB']['table_info_csv'])

    if DB_OP_ and str_dat[:8]:
        db_row_dict = data_lib.gen_db_row_dat(est_table_info, feat_handler)
        est_db_handler.insertOne(db_row_dict)
        logger.info("EST_DB read done")
        if True:
            print("EST_DB read back:" + str(est_db_handler.selectOne(est_db_handler.selectLastPK())))
    return out_val


def estimation(ml_ini, networking_ini, feat_handler, est_dict, logger, DB_OP_=True, server_mode='EST'):

    out_val = learning_lib.calculate_ml_output(est_dict,
                                               ml_ini,
                                               feat_handler,
                                               logger,
                                               server_mode)

    feat_handler.vars['EST_REPLY_TIME'].val = system_lib.get_datetime()

    est_db_handler, eval_db_handler = system_lib.create_db_handlers(networking_ini, logger, DB_OP_)

    return est_db_handler, out_val


# def evaluate(networking_ini, ml_ini, feat_handler, str_dat, est_dict, eval_dict, logger, DB_OP_=True):
#     output, dataset, eval_db_handler, learn_msg = evaluation_setup_get_dataset(networking_ini, ml_ini, feat_handler,
#                                                                                est_dict, eval_dict,
#                                                                                logger, DB_OP_)
#     if dataset:
#         evaluation_start(ml_ini, dataset, feat_handler, learn_msg, logger)
#
#     eval_table_info = data_lib.read_csv_file(networking_ini['EVAL_DB']['table_info_csv'])
#
#     if DB_OP_ and str_dat[:8]:
#         db_row_dict = data_lib.gen_db_row_dat(eval_table_info, feat_handler)
#         eval_db_handler.insertOne(db_row_dict)
#         logger.info("EVAL_DB read done")
#         if True:
#             print("EVAL_DB read back:" + str(eval_db_handler.selectOne(eval_db_handler.selectLastPK())))
#
#         # if args.cat == 'CVT':
#         #     eval_row_dict = lib.gen_row_dat(eval_table_info, Feat)
#         #     compare_status = lib.compare_est_and_eval_dat(est_dict, eval_row_dict, logger=logger)
#     return output


# def evaluation_setup_get_dataset(networking_ini, ml_ini, feat_handler, est_dict, eval_dict, logger=None, DB_OP_=True):
#
#     out_val, eval_val = learning_lib.calculate_ml_output(eval_dict,
#                                                          ml_ini,
#                                                          feat_handler,
#                                                          logger
#                                                          )
#
#     eval_table_info = data_lib.read_csv_file(networking_ini['EVAL_DB']['table_info_csv'])
#
#     if est_dict:
#         ret_status, ret_list = feat_handler.check_var_tolerances(est_dict, eval_dict, logger=logger)
#         feat_handler.vars['EVAL_TOLERANCE_CHECK'].val = int(ret_status)
#         feat_handler.vars['EVAL_TOLERANCE_CHECK'].val = int(ret_status)
#         feat_handler.vars['EVAL_TOLERANCE_CHECK_VARS'].val = str(ret_list).replace("\'", "").replace("\"", "")
#         if ml_ini['general']['model_name'] == 'CVT' or ml_ini['general']['model_name'] == 'RH':
#             feat_handler.vars['EVAL_PERF'].val = abs(est_dict['EST_TEMP'] - feat_handler.vars['EVAL_TEMP'].val)
#         elif ml_ini['general']['model_name'] == 'RH_MACRO_M6':
#             feat_handler.vars['EVAL_PERF'].val = abs(est_dict['EST_TUNDISH_CARBON'] - feat_handler.vars['EVAL_TUNDISH_CARBON'].val)
#     else:
#         feat_handler.vars['EVAL_TOLERANCE_CHECK'].val = -1
#         feat_handler.vars['EVAL_PERF'].val = data_lib.INFINITY
#         logger.info("There is no charge no. in estimation DB matched with {}".format(eval_dict['CHARGE_NO']))
#
#     feat_handler.vars['EVAL_END_TIME'].val = system_lib.get_datetime()
#     latest_dir = system_lib.get_model_dir(ml_ini['general']['root_dir'],
#                                           ml_ini['general']['model_name'],
#                                           ml_ini['general']['method'],
#                                           ml_ini['general']['version'])
#     if not latest_dir:
#         logger.error("Latest model directory does not exist.")
#
#     model_date = os.path.split(latest_dir)[-1]
#
#     present = datetime.now()
#     past = datetime(int("20" + model_date[:2]), int(model_date[2:-2]), int(model_date[-2:]), 0, 0)
#
#     days_passed = (present - past).days
#
#     days_in_month = system_lib.get_days_in_month(model_date)
#
#     dataset = None
#     eval_db_handler = None
#     learn_msg = ''
#
#     est_db_handler, eval_db_handler = system_lib.create_db_handlers(networking_ini, logger, DB_OP_)
#
#     if DB_OP_ and system_lib.time_elapsed(networking_ini['SELF_LEARNING']['condition'], days_passed, days_in_month):
#
#         eval_cond_list = ["{0}<{1}".format("20" + model_date, 'TAP_WORK_DATE'),
#                           "{0}>{1}".format("30000000", 'TAP_WORK_DATE')]
#         db_data = [[]]
#         db_data[0] = eval_db_handler.selectDataByConditions(cond_list=eval_cond_list)
#
#         msg = "Get last updated {:d} data from EVAL DB with {}.".format(len(db_data[-1]), eval_cond_list)
#         logger.info(msg)
#         learn_msg = "\n # " + msg
#         db_data[-1].insert(0, [x[0] for x in eval_table_info[1:]])
#
#         system_lib.calc_y_from_db(ml_ini['general']['model_name'], db_data)
#
#         db_data.append(feat_handler.refine_dataset_col_by_dataset_order(db_data[-1], logger=logger))
#         learn_msg += "\n # " + feat_handler.log
#         db_data.append(feat_handler.refine_dataset_col_by_null_replacement(db_data[-1], logger=logger))
#         learn_msg += "\n # " + feat_handler.log
#         db_data.append(feat_handler.refine_dataset_row_by_null(db_data[-1], logger=logger))
#         learn_msg += "\n # " + feat_handler.log
#         db_data.append(feat_handler.refine_dataset_row_by_process_type(db_data[-1], args.cat, logger=logger)) ## 경처리 데이터로 filtering
#         learn_msg += "\n # " + feat_handler.log
#         db_data.append(feat_handler.refine_dataset_group_by_operation_pattern(db_data[-1], args.cat, logger=logger))  ## 7가지 조업패턴 데이터로 grouping
#         learn_msg += "\n # " + feat_handler.log
#         db_data.append(feat_handler.refine_dataset_row_by_nan_replacement(db_data[-1], args.cat, logger=logger))  ## nan 데이터 처리
#         learn_msg += "\n # " + feat_handler.log
#         db_data.append(feat_handler.refine_dataset_row_by_range(db_data[-1], logger=logger))
#         learn_msg += "\n # " + feat_handler.log
#
#         dataset = data_lib.read_csv_file(ml_ini['general']['dataset_file'])
#         org_dataset_len = len(dataset) - 1
#         add_dataset_len = len(db_data[-1]) - 1
#         # sort by local dataset column
#         sorted_db_data = data_lib.extract_rows_from_table(db_data[-1], dataset[0])
#
#         dataset += sorted_db_data[1:]
#         data_lib.write_list_to_csv(dataset, ml_ini['general']['dataset_file'])
#
#         msg = "Write new self-learning dataset, {:d} + {:d}".format(org_dataset_len, add_dataset_len)
#         logger.info(msg)
#         learn_msg += "\n # " + msg
#
#     return out_val, dataset, eval_db_handler, learn_msg


def evaluation_start(ml_ini, dataset, feat_handler, learn_msg, logger):

    today = system_lib.get_datetime("%y%m%d")
    new_model_dir = os.path.join(ml_ini['general']['root_dir'],
                                 ml_ini['general']['model_name'],
                                 ml_ini['general']['method'],
                                 today)

    if not os.path.exists(new_model_dir):
        os.makedirs(new_model_dir)

    param_dict = {'dataset': dataset}

    learner = learning_handler.MachineLearner(Feat=feat_handler, dataset=dataset, out_idx=0)
    if ml_ini['general']['method'] == "DNN":
        learner.normalize_number_features()
        learner.encode_symbol_dataset_by_one_hot_encoder()
        ml_ini['DNN']['model_dir'] = os.path.join(new_model_dir, ml_ini['DNN']['model_dir'])
    else:
        learner.encode_symbol_dataset_by_int()

    bin_dir = ml_ini['general']['method'] + ml_ini['general']['bin_extension']

    learner.run(ml_ini['general']['method'], ml_ini)

    param_dict['params'] = learner.params
    if int(ml_ini['general']['output_num']) >= 2:
        for j in range(int(ml_ini['general']['output_num'])):
            msg = "{} : train accuracy - {:6.2f}, test accuracy - {:6.2f}"\
                    .format(learner.y_name[j], learner.train_acc[j], learner.test_acc[j])
            logger.info(msg)
            learn_msg += "\n # " + msg

            msg = "{} : train MSE - {:6.2f}, test RMSE - {:6.2f}"\
                    .format(learner.y_name[j], learner.train_rmse[j], learner.test_rmse[j])
            logger.info(msg)
            learn_msg += "\n # " + msg

            msg = "{} : train R2 score - {:6.2f}, test r2score - {:6.2f}" \
                .format(learner.y_name[j], learner.train_r2_score[j], learner.test_r2_score[j])
            logger.info(msg)
            learn_msg += "\n # " + msg + "\n"
    else:
        msg = "{} : train accuracy - {:6.2f}, test accuracy - {:6.2f}" \
            .format(learner.y_name, learner.train_acc, learner.test_acc)
        logger.info(msg)
        learn_msg += "\n # " + msg

        msg = "{} : train MSE - {:6.2f}, test RMSE - {:6.2f}" \
            .format(learner.y_name, learner.train_rmse, learner.test_rmse)
        logger.info(msg)
        learn_msg += "\n # " + msg

        msg = "{} : train R2 score - {:6.2f}, test r2score - {:6.2f}" \
            .format(learner.y_name, learner.train_r2_score, learner.test_r2_score)
        logger.info(msg)
        learn_msg += "\n # " + msg + "\n"

    if ml_ini['general']['method'] == "DNN":
        pickle.dump(param_dict, open(os.path.join(new_model_dir, bin_dir), "wb"))
    else:
        pickle.dump(learner, open(os.path.join(new_model_dir, bin_dir), "wb"))
    with open(os.path.join(new_model_dir, ml_ini['general']['result_file']), "w") as fid:
        fid.write(learn_msg)


def update_feat_handler(feat_handler, ml_ini, est_dict, server_mode, logger):
    # Check variable and process type
    check_status, check_list = feat_handler.check_var_ranges(est_dict, fix_=True, logger=logger)
    check_process_type = feat_handler.check_var_process_type(est_dict,
                                                             cat=ml_ini['general']['model_name'],
                                                             logger=logger)
    feat_handler.check_process_type = str(check_process_type)

    model_dir = system_lib.get_model_dir(ml_ini['general']['root_dir'],
                                         ml_ini['general']['model_name'],
                                         ml_ini['general']['method'],
                                         ml_ini['general']['version'],
                                         ml_ini['general']['result_file'])

    feat_handler.vars[server_mode + '_START_TIME'].val = system_lib.get_datetime()
    feat_handler.vars[server_mode + '_RANGE_CHECK'].val = int(check_status)
    check_list = str(check_list).replace("\'", "")
    feat_handler.vars[server_mode + '_RANGE_CHECK_VARS'].val = str(check_list).replace("\"", "")
    feat_handler.vars[server_mode + '_METHOD'].val = ml_ini['general']['method']
    feat_handler.vars[server_mode + '_MODEL_VER'].val = model_dir