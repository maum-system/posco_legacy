#! /usr/bin/env python
# -*- coding: utf-8 -*-
import sys
import argparse
import configparser
import mssql.pymssqlwrapper
import pandas as pd
import collections
import os
from handlers import dataset_handler
from lib import learning as learning_lib
from lib import sys_lib as sys_lib
from lib import data as data_lib
from refine_model import model_1, model_2, model_3, model_4, model_5, model_6, replace_dict_m2_3_4

_this_folder_ = os.path.dirname(os.path.abspath(__file__))
_this_basename_ = os.path.splitext(os.path.basename(__file__))[0]

class Evaluation:

    def __init__(self, data_path=None, out_path=None, ini=None, logger=None):

        self.data_path = data_path
        self.out_path = out_path
        self.ini = ini
        self.logger = logger

        self.model_name = None
        self.model_num = None
        self.trans_code = None

        self.init_ini(ini)
        self.est_db_handler = None
        self.eval_db_handler = None
        self.est_dataset = None
        self.eval_dataset = None
        self.match_dataset = None

    def init_ini(self, ini):

        self.est_dataset_fname = ini['general']['est_dataset_fname']
        self.eval_dataset_fname = ini['general']['eval_dataset_fname']
        self.save_file_ = bool(ini['general']['save_file_'])

        self.apply_request_time = bool(ini['general']['apply_request_time'])
        c_max_range = ini['general']['c_max_range'].split(',')
        self.c_max_range = [int(u) for u in c_max_range]
        self.c_diff = int(ini['general']['c_diff'])
        check_model_list = ini['general']['check_model_list'].split(',')
        self.check_model_list = [u.strip() for u in check_model_list]

        self.mssql_ini = ini['mssql_db']
        self.mssql_server_name = ini['mssql_db']['server_name']
        self.mssql_port = ini['mssql_db']['port']
        self.mssql_username = ini['mssql_db']['username']
        self.mssql_password = ini['mssql_db']['password']

        self.model_1_1_ini = ini['model_1_1']
        self.model_1_2_ini = ini['model_1_2']
        self.model_2_ini = ini['model_2']
        self.model_3_ini = ini['model_3']
        self.model_4_ini = ini['model_4']
        self.model_5_ini = ini['model_5']
        self.model_6_ini = ini['model_6']
        self.curr_model_ini = None

    def init_logger(self, log_dir=None, logging_=True, console_=True):
        try:
            self.ini['logger']['name'] += "." + SERVER_MODE
            self.ini['logger']['prefix'] = SERVER_MODE
            if log_dir:
                self.ini['logger']['folder'] = log_dir
            self.logger = sys_lib.setup_logger_with_ini(self.ini['logger'],
                                                        logging_=logging_,
                                                        console_=console_)
        except Exception as e:
            print(" @ Error in init_logger: {}".format(e))
            self.logger = sys_lib.get_stdout_logger()

    def get_model_info(self, model_name):
        self.model_name = model_name
        self.model_num = self.model_name.split('_')[1]
        self.curr_model_ini = self.get_model_ini(model=self.model_name)
        self.trans_code = self.curr_model_ini['eval_header']
        tolerances = self.curr_model_ini['tolerance'].split(',')
        self.tolerances = [float(u) for u in tolerances]


    def init_db_handler(self):
        if not DB_OP_:
            return False
        self.est_db_handler = sys_lib.PyMssqlWrapper(server_name=self.mssql_server_name,
                                                     port=self.mssql_port,
                                                     username=self.mssql_username,
                                                     password=self.mssql_password,
                                                     db_name=self.curr_model_ini['db_name'],
                                                     table_name=self.curr_model_ini['EST'.lower() + '_' + 'table_name'],
                                                     logger=self.logger)
        self.eval_db_handler = sys_lib.PyMssqlWrapper(server_name=self.mssql_server_name,
                                                     port=self.mssql_port,
                                                     username=self.mssql_username,
                                                     password=self.mssql_password,
                                                     db_name=self.curr_model_ini['db_name'],
                                                     table_name=self.curr_model_ini['EVAL'.lower() + '_' + 'table_name'],
                                                     logger=self.logger)
    def get_model_ini(self, model):

        model_ini_dict = {
            'model_1_1': self.model_1_1_ini,
            'model_1_2': self.model_1_2_ini,
            'model_2': self.model_2_ini,
            'model_3': self.model_3_ini,
            'model_4': self.model_4_ini,
            'model_5': self.model_5_ini,
            'model_6': self.model_6_ini
        }
        return model_ini_dict[model]

    def load_dataset(self, SERVER_MODE=None):
        if SERVER_MODE == 'EST':
            db_handler = self.est_db_handler
            dataset_fname = self.est_dataset_fname
        elif SERVER_MODE == 'EVAL':
            db_handler = self.eval_db_handler
            dataset_fname = self.eval_dataset_fname

        if DB_OP_:
            db_columns = db_handler.selectColumnNames()
            db_columns = [db_column[0] for db_column in db_columns]
            db_data = db_handler.selectAll()
            dataset = [db_columns] + db_data
            dataset = convert_data_type(dataset, 'df')
        else:
            dataset = pd.read_csv(dataset_fname)
        return dataset

    def load_est_eval_dataset(self):
        for server_mode in ['EST', 'EVAL']:
            dataset = self.load_dataset(SERVER_MODE=server_mode)
            if server_mode is 'EST':
                self.est_dataset = dataset
            elif server_mode is 'EVAL':
                self.eval_dataset = dataset
        return self.est_dataset, self.eval_dataset

    def extract_match_dataset(self):
        est_df = convert_data_type(self.est_dataset, dtype='df')
        eval_df = convert_data_type(self.eval_dataset, dtype='df')

        # 지시/실적 데이터에서 중복행 제거
        if self.curr_model_ini.name == 'model_4':
            model_request_time = self.curr_model_ini['model_request_time']
            est_no_dup_df = est_df.loc[est_df['MODEL_REQUEST_TIME'] == int(model_request_time)].reset_index(drop=True)
            est_no_dup_df = est_no_dup_df.drop_duplicates(subset=['TRANSACTION_CD', 'CHARGE_NO', 'USE_MOD4_7_TIME'])
            eval_no_dup_df = eval_df.loc[eval_df['MODEL_REQUEST_TIME'] == int(model_request_time)].reset_index(drop=True)
            eval_no_dup_df = eval_no_dup_df.drop_duplicates(subset=['TRANSACTION_CD', 'CHARGE_NO', 'USE_MOD4_7_TIME'])


        # MODEL_REQUEST_TIME 으로 데이터 추출하니까 해당 되는 것 만 선택해서 중복행 제거.
        elif self.curr_model_ini.name == 'model_3':
            model_request_time = self.curr_model_ini['model_request_time']
            est_no_dup_df = est_df.loc[est_df['MODEL_REQUEST_TIME'] == int(model_request_time)].reset_index(drop=True)
            est_no_dup_df = est_no_dup_df.drop_duplicates(subset=['TRANSACTION_CD', 'CHARGE_NO'])
            eval_no_dup_df = eval_df.loc[eval_df['MODEL_REQUEST_TIME'] == int(model_request_time)].reset_index(drop=True)
            eval_no_dup_df = eval_no_dup_df.drop_duplicates(subset=['TRANSACTION_CD', 'CHARGE_NO'])

        else:
            est_no_dup_df = est_df.drop_duplicates(subset=['TRANSACTION_CD', 'CHARGE_NO'])
            eval_no_dup_df = eval_df.drop_duplicates(subset=['TRANSACTION_CD', 'CHARGE_NO'])

        if SERVER_MODE == 'EST':
            # 실적 매칭되는 지시의 CHARGE_NO
            match_ch_no = eval_no_dup_df['CHARGE_NO'].loc[eval_no_dup_df['CHARGE_NO'].isin(est_no_dup_df['CHARGE_NO'])].reset_index(drop=True)

            # 실적 데이터에 지시 예측값 추가
            est_y = self.curr_model_ini[SERVER_MODE.lower() + '_y']
            est_values = est_no_dup_df[['CHARGE_NO', est_y]]
            eval_no_dup_df = eval_no_dup_df.merge(est_values, on='CHARGE_NO')

            # 지시/실적 CHARGE_NO 매칭
            match_df = eval_no_dup_df.loc[eval_no_dup_df['CHARGE_NO'].isin(match_ch_no)]
            self.match_dataset = match_df

        elif SERVER_MODE == 'EVAL':
            self.match_dataset = eval_no_dup_df

    def select_data_by_conditions(self, df=None, cond_list=None):

        if df is None:
            df = self.match_dataset

        for cond in cond_list:
            key = cond[0]
            cond_vals = cond[1]
            if key == 'TRANSACTION_CD':
                df = df.loc[df[key] == cond_vals]
            elif key == 'START_DATE':
                df = df.loc[df['TAP_WORK_DATE'].apply(pd.to_numeric) >= int(cond_vals)]
            elif key == 'END_DATE':
                df = df.loc[df['TAP_WORK_DATE'].apply(pd.to_numeric) <= int(cond_vals)]
            elif key == 'NULL_CHECK_VAR_LIST':
                df = df.dropna(subset=cond_vals)
            # elif key == SERVER_MODE + '_RANGE_CHECK_VARS':
            elif key == 'EVAL_RANGE_CHECK_VARS':
                if cond_vals == 'None':
                    df = df.loc[df[key] == 'None']
                else:
                    for range_var in cond_vals:
                        df = df.loc[(df[key].str.contains(range_var)) == False]
            elif key == 'C_MAX':
                df = df.loc[(int(cond_vals[0]) <= df[key]) & (df[key] <= int(cond_vals[1]))]
            elif key == 'C_DIFF':
                df = df.loc[abs(df['F_RH_C'] - df['F_CC_C']) <= cond_vals]
            print("Key : {}, condition : {}, df_cnts : {}".format(key, str(cond_vals), len(df)))
            print("Key : {}, condition : {}, df_cnts : {}".format(key, str(cond_vals), len(df)))
        return df

def get_date_period():
    start_date = input(" \n Please enter start date : ex) 20190507\n :")
    end_date = input(" \n Please enter end date : ex) 20191231\n :")

    return start_date, end_date


def get_cond_list(db_op_,
                  server_mode,
                  trans_code,
                  start_date=None, end_date=None,
                  null_check_var_list=None,
                  range_check_var_list=None
                  ):
    if db_op_ is True:
        cond_list = ["TRANSACTION_CD = '" + trans_code + "'"]
        if start_date:
            cond_list.append("TAP_WORK_DATE >='" + start_date + "'")
        if end_date:
            cond_list.append("TAP_WORK_DATE <='" + end_date + "'")
        if null_check_var_list:
            for var in null_check_var_list:
                cond_list.append(var + " is not NULL")
        if range_check_var_list:
            for var in range_check_var_list:
                cond_list.append(server_mode+"_RANGE_CHECK_VARS NOT LIKE '%" + var + ":%'")
    else:
        cond_list = [['TRANSACTION_CD', trans_code]]
        if start_date:
            cond_list.append(['START_DATE', start_date])
        if end_date:
            cond_list.append(['END_DATE', end_date])
        if null_check_var_list:
            cond_list.append(['NULL_CHECK_VAR_LIST', null_check_var_list])
        if range_check_var_list:
            cond_list.append([server_mode + "_RANGE_CHECK_VARS", range_check_var_list])

    return cond_list

def convert_data_type(data, dtype='list'):
    """

    :param data:
    :param dtype: list / df
    :return:
    """
    if dtype == 'list':
        if isinstance(data, pd.DataFrame):
            res_data = [data.columns.values.tolist()] + data.values.tolist()
        else:
            res_data = data
    elif dtype =='df':
        if isinstance(data, list):
            res_data = pd.DataFrame(data[1:], columns=data[0])
        else:
            res_data = data
    return res_data

def adjust_y_unit(model_num, dataset):

    # convert data type
    dataset = convert_data_type(dataset, 'list')
    if model_num == '1':
        adjusted_dataset = model_1.refine_dataset_by_y_value(dataset=dataset, mode=dataset_handler.ANALYSIS_MODE)
    elif model_num == '2':
        adjusted_dataset = model_2.refine_dataset_by_y_value(dataset=dataset, mode=dataset_handler.ANALYSIS_MODE)
        adjusted_dataset = decode_symbol_by_dict(dataset=adjusted_dataset, decode_vars=['EST_OP_PATTERN', 'EVAL_OP_PATTERN', 'PATTERN_OPERATION'], dict=model_2.decode_symbol_dict)
    elif model_num == '3':
        adjusted_dataset = dataset
        pass
    elif model_num == '4':
        adjusted_dataset = model_4.refine_dataset_by_y_value(dataset)
    elif model_num == '5':
        adjusted_dataset = model_5.refine_dataset_by_y_value(dataset)
    elif model_num == '6':
        adjusted_dataset = model_6.refine_dataset_by_y_value(dataset=dataset, mode=dataset_handler.ANALYSIS_MODE)

    return convert_data_type(adjusted_dataset, 'df')

def decode_symbol_by_dict(dataset, decode_vars, dict):

    for row_idx in range(1, len(dataset)):
        row_dat = dataset[row_idx]
        for col_idx in range(len(row_dat)):
            key = dataset[0][col_idx]
            if key in decode_vars:
                try:
                    dataset[row_idx][col_idx] = model_2.decode_symbol_dict[row_dat[col_idx]]
                except KeyError:
                    print(key + ' ' + 'is' + ' ' + str(row_dat[col_idx]))
                    continue
            else:
                continue
    return dataset

def get_tolerance_char(tol_idx):
    if tol_idx + 1 == 1:
        tol_char = 'first'
    elif tol_idx + 1 == 2:
        tol_char = 'second'
    elif tol_idx + 1 == 3:
        tol_char = 'third'
    return tol_char

def main(args):

    global accuracy, rmse
    eval = Evaluation(ini=sys_lib.get_ini_parameters(args.ini_fname))

    folder = sys_lib.get_datetime()[:-10].replace(":", "-")
    if not args.log_dir:
        args.log_dir = os.path.join(eval.ini['logger']['folder'], folder)
    if not os.path.isdir(args.log_dir):
        os.mkdir(args.log_dir)

    if not args.out_path:
        args.out_path = os.path.join(_this_folder_, "./Output")
    if not os.path.isdir(args.out_path):
        os.mkdir(args.out_path)

    while True:
        ## ans = input(" \n Enter command, evaluation(1) or exit(0) ? ")
        ans = '1' ##
        try:
            ans = int(ans[0])
        except ValueError:
            print("\n You entered {}. Please enter 0, or 1. Thanks".format(ans))
            continue

        if ans == 0:
            print("\n # Bye")
            break
        elif ans == 1:
            eval.init_logger(log_dir=args.log_dir)
            eval.logger.info(" # START {} with {}_DB.".format(_this_basename_, SERVER_MODE))
            model_type = input(" \n Please enter model type : \n"
                                    "ex) 1_1 : (CHEM05_F_BAP_C_VAL_1)\n"
                                    "ex) 1_2 : (RH_ARR_O2)\n"
                                    "ex) 2   : (PATTERN_OPERATION)\n"
                                    "ex) 3   : (MINUTE_C(Model's end point.)\n"
                                    "ex) 4   : (F_RH_C, 7 min)\n"
                                    "ex) 5   : (BEFORE_DEO_O2)\n"
                                    "ex) 6   : (F_CC_C)\n"
                                    "ex) all : (M 1~6)\n"
                                    " : "
                                    )
            model_name = 'model_' + model_type

            # model_type = '1_1' ##
            # model_name = 'model_' + model_type ##

            if model_type != 'all':
                eval.check_model_list = [model_name]
            for model_idx, model_name in enumerate(eval.check_model_list):
                # Get model info.
                eval.get_model_info(model_name=model_name)

                # Init. database
                if DB_OP_:
                    eval.init_db_handler()
                    ## init_db_info()

                eval.load_est_eval_dataset()
                eval.extract_match_dataset()

                ## start_date, end_date = get_date_period()
                start_date = '20210201'  ##
                end_date = "20210325"  ##

                null_check_var_list = [var.strip() for var in eval.curr_model_ini['null_check_var_list'].split(',')] \
                                                        if eval.curr_model_ini['null_check_var_list'] != '' else None
                range_check_var_list = [var.strip() for var in eval.curr_model_ini['range_check_var_list'].split(',')] \
                                                        if eval.curr_model_ini['range_check_var_list'] != '' else None
                filter_y_cond_list = get_cond_list(db_op_=False,
                                                   server_mode='EVAL',
                                                   trans_code=eval.trans_code,
                                                   start_date=start_date, end_date=end_date,
                                                   null_check_var_list=null_check_var_list,
                                                   range_check_var_list=range_check_var_list)

                date_cond_data = eval.select_data_by_conditions(cond_list=filter_y_cond_list[:3])
                filter_y_data = eval.select_data_by_conditions(cond_list=filter_y_cond_list)

                filter_y_cond_list[-1] = ["EVAL_RANGE_CHECK_VARS", 'None']
                refine_cond_list = filter_y_cond_list
                refine_data = eval.select_data_by_conditions(cond_list=refine_cond_list)

                # Adjust the unit.
                filter_y_data = adjust_y_unit(model_num=eval.model_num, dataset=filter_y_data)
                refine_data = adjust_y_unit(model_num=eval.model_num, dataset=refine_data)

                # add condition for RH Models
                if 'rh_macro' in eval.curr_model_ini[SERVER_MODE.lower() + '_table_name']:
                    for df in [filter_y_data, refine_data]:
                        c_max_df = eval.select_data_by_conditions(df=df, cond_list=[['C_MAX', eval.c_max_range]])
                        # c_diff_df = eval.select_data_by_conditions(df=c_max_df, cond_list=[['C_DIFF', eval.c_diff]])
                        if df is filter_y_data:
                            filter_y_df = c_max_df
                        elif df is refine_data:
                            refine_df = c_max_df
                else: # for CVT Model
                    filter_y_df = filter_y_data
                    refine_df = refine_data

                acc_header = []
                acc_list = []
                rmse_header = []
                rmse_list = []
                # Check accuracy per tolerance
                filter_y_df_ori = filter_y_df
                refine_df_ori = refine_df
                for tol_idx, tolerance in enumerate(eval.tolerances):
                    tol_char = get_tolerance_char(tol_idx)

                    # MODEL_REQUEST_TIME 으로 데이터 추출
                    if eval.apply_request_time == True:
                        if eval.model_num == '3' or eval.model_num == '4':
                            model_request_time = eval.curr_model_ini['model_request_time']
                            filter_y_df = filter_y_df_ori.loc[filter_y_df_ori['MODEL_REQUEST_TIME'] == int(model_request_time)].reset_index(drop=True)
                            refine_df = refine_df_ori.loc[refine_df_ori['MODEL_REQUEST_TIME'] == int(model_request_time)].reset_index(drop=True)

                    # 모델4 예측/실적 탈탄시간 1분 이내의 데이터 추출
                    if eval.model_num == '4':
                        # est_time = eval.curr_model_ini[SERVER_MODE.lower() + '_time']
                        est_time = eval.curr_model_ini['eval_time']
                        real_time = eval.curr_model_ini['real_time']
                        tolerance_time = int(eval.curr_model_ini['tolerance_time'])
                        filter_y_time_ch_no = filter_y_df['CHARGE_NO'].loc[(filter_y_df[est_time] - filter_y_df[real_time])  <= tolerance_time].reset_index(drop=True)
                        refine_time_ch_no = refine_df['CHARGE_NO'].loc[(refine_df[est_time] - refine_df[real_time])  <= tolerance_time].reset_index(drop=True)

                        # CHARGE_NO가 같은 데이터 추출
                        filter_y_df = filter_y_df.loc[filter_y_df['CHARGE_NO'].isin(filter_y_time_ch_no)]
                        refine_df = refine_df.loc[refine_df['CHARGE_NO'].isin(refine_time_ch_no)]

                    # 모델 예측값이 999999인 데이터 제거
                    filter_y_df = filter_y_df.loc[filter_y_df[eval.curr_model_ini[SERVER_MODE.lower() + '_y']].apply(pd.to_numeric) < 999999].reset_index(drop=True)
                    refine_df = refine_df.loc[refine_df[eval.curr_model_ini[SERVER_MODE.lower() + '_y']].apply(pd.to_numeric) < 999999].reset_index(drop=True)

                    print("\n[{} results.]".format(eval.model_name))
                    print('\ntotal data : {}, filtered data by Y (Null, Outlier) & C_MAX : {}'.format(len(date_cond_data), len(filter_y_df)))
                    print('\ntotal data : {}, filtered data by total variables (Null, Outlier) & C_MAX : {}'.format(len(date_cond_data), len(refine_df)))

                    # 모델 real 값이 nan 인 데이터 제거 (model2 에서 오류나서 추가함)
                    if eval.model_num == '2':
                        filter_y_df = filter_y_df.loc[filter_y_df[eval.curr_model_ini['real_y']] != ''].reset_index(drop=True)
                        refine_df = refine_df.loc[refine_df[eval.curr_model_ini['real_y']] != ''].reset_index(drop=True)
                        print("\n[모델 real 값이 nan 인 데이터 제거 (model2 에서 오류나서 추가함) results.]")
                        print("\n[{} results.]".format(eval.model_name))
                        print('\ntotal data : {}, filtered data by Y (Null, Outlier) & C_MAX : {}'.format(
                            len(date_cond_data), len(filter_y_df)))
                        print('\ntotal data : {}, filtered data by total variables (Null, Outlier) & C_MAX : {}'.format(
                            len(date_cond_data), len(refine_df)))

                    # Check accuracy about filter_y, refine_df
                    for db_df in [filter_y_df, refine_df]:
                        accuracy = None
                        rmse = None
                        if len(db_df) == 0:
                            print("No data available for the condition.")
                        else:
                            try:
                                if db_df is filter_y_df:
                                    acc_header.append(tol_char + '_acc' + '(filtered_by_y)')
                                elif db_df is refine_df:
                                    acc_header.append(tol_char + '_acc' + '(filtered_outliers)')

                                accuracy, rmse = learning_lib.check_performance_measure(est_val=eval.curr_model_ini[SERVER_MODE.lower() + '_y'],
                                                                                        real_val=eval.curr_model_ini['real_y'],
                                                                                        tolerance=tolerance, data_frame=db_df)
                            except (ValueError):
                                print("Data contains an outlier value.")
                                pass
                        acc_list.append(accuracy)
                        rmse_list.append(rmse)

                acc_dataset = [acc_header] + [acc_list]
                rmse_dataset = [rmse_header] + [rmse_list]
                print("Performance evaluation is completed.")

                # Save used data file
                if eval.save_file_ == True:
                    used_data_fname = start_date[2:] + '_' + end_date[2:] + '_' + model_name + '_' + 'raw_data' + '.csv'
                    date_cond_data.to_csv(os.path.join(OUT_PATH, used_data_fname), mode='w')

                    # Save evaluated result file
                    eval_data_fname = SERVER_MODE.lower() + '_total_model_evaluation_result.csv'
                    res_dict = collections.OrderedDict()
                    res_dict['model'] = model_name
                    res_dict['start_date'] = start_date
                    res_dict['end_date'] = end_date
                    res_dict['total_data'] = len(date_cond_data)
                    res_dict['filtered_by_y'] = len(filter_y_df)
                    res_dict['filtered_by_outliers'] = len(refine_df)
                    res_dict['tolerances'] = eval.tolerances
                    csv_acc_columns = ['first_acc(filtered_by_y)', 'second_acc(filtered_by_y)', 'third_acc(filtered_by_y)',
                                       'first_acc(filtered_outliers)', 'second_acc(filtered_outliers)', 'third_acc(filtered_outliers)']
                    # acc_titles = [s for s in sorted(list(res_dict.keys())) if "_acc" in s]
                    for ca_idx, ca_name in enumerate(csv_acc_columns):
                        for acc_idx, acc_name in enumerate(acc_header):
                            if acc_name == ca_name:
                                res_dict[ca_name] = acc_dataset[1][acc_idx]
                            elif ca_name not in acc_header:
                                res_dict[ca_name] = 'None'
                    res_dict['RMSE(filtered_by_y)'] = rmse_dataset[1][0]
                    res_dict['RMSE(filtered_outliers)'] = rmse_dataset[1][1]


                    res_df = pd.DataFrame.from_dict(res_dict, orient='index').transpose()
                    if model_idx+1 == 1:
                        res_df.to_csv(os.path.join(OUT_PATH, start_date[2:] + '_' + end_date[2:] + '_' + eval_data_fname), mode='w')
                    else:
                        res_df.to_csv(os.path.join(OUT_PATH, start_date[2:] + '_' + end_date[2:] + '_' + eval_data_fname), mode='a', header=False)
        else:
            print("\n You entered {:d}. Please enter 0 or 1. Thanks".format(ans))

def parse_arguments(argv):

    parser = argparse.ArgumentParser()

    parser.add_argument("--op_mode", default='manual', help="operation mode", choices=['manual', 'auto'])
    parser.add_argument("--server_mode", default='EVAL', help="server mode", choices=['EST', 'EVAL', 'ALL'])
    parser.add_argument("--ini_fname", required=True, help="ini filename")
    parser.add_argument("--out_path", default=None, help="Output path")
    parser.add_argument("--logging_", default=False, action='store_true', help="Logging flag")
    parser.add_argument("--console_logging_", default=False, action='store_true', help="Console logging flag")
    parser.add_argument("--log_dir", default=None, help="Log directory")

    args = parser.parse_args(argv)
    args.out_path = sys_lib.unicode_normalize(args.out_path)
    args.log_dir = sys_lib.unicode_normalize(args.log_dir)

    return args

LOG_DIR = None
DB_OP_ = True
OP_MODE = 'manual' # manual / auto
SERVER_MODE = 'EST' # EST / EVAL / ALL
INI_FNAME = '../db_analysis/db_setting.ini'
CRT_DATE = sys_lib.get_datetime("%Y%m%d")
OUT_PATH = "../db_analysis/Output/" + CRT_DATE + "/"


if __name__ == "__main__":

    if len(sys.argv) == 1:
        sys.argv.extend([
            "--op_mode", OP_MODE,
            "--server_mode", SERVER_MODE,
            "--ini_fname", INI_FNAME,
            "--out_path", OUT_PATH,
            "--logging_",
            "--console_logging_",
            "--log_dir", LOG_DIR
                         ])
    main(parse_arguments(sys.argv[1:]))
