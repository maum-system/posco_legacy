# -*- coding: utf-8 -*-
import sys
import argparse
import configparser
import mssql.pymssqlwrapper
import pandas as pd
import os
import numpy as np

from handlers import dataset_handler
from lib import learning as learning_lib
from lib import data as data_lib
from refine_model import model_1, model_2, model_3, model_4, model_5, model_6, replace_dict_m2_3_4

def get_cond_list(mode,
                  transaction_cd,
                  date_start=None, date_end=None,
                  null_check_var_list=None,
                  range_check_var_list=None
                  ):
    cond_list = ["TRANSACTION_CD = '" + transaction_cd + "'"]
    if date_start:
        cond_list.append(mode+"_START_TIME >='" + date_start + "'")
    if date_end:
        cond_list.append(mode+"_START_TIME <='" + date_end + "'")
    if null_check_var_list:
        for var in null_check_var_list:
            cond_list.append(var + " is not NULL")
    if range_check_var_list:
        for var in range_check_var_list:
            cond_list.append(mode+"_RANGE_CHECK_VARS NOT LIKE '%" + var + ":%'")

    return cond_list

def decode_symbol_by_dict(dataset, decode_vars, dict):

    for row_idx in range(1, len(dataset)):
        row_dat = dataset[row_idx]
        for col_idx in range(len(row_dat)):
            key = dataset[0][col_idx]
            if key in decode_vars:
                try:
                    dataset[row_idx][col_idx] = model_2.decode_symbol_dict[row_dat[col_idx]]
                except KeyError:
                    # print(key + 'is' + str(row_dat[col_idx]))
                    continue
            else:
                continue
    return dataset

def main():

    parser = argparse.ArgumentParser()
    parser.add_argument("--analysis_setting", required=True, help="Setting file path")
    parser.add_argument("--mode", required=True, help="EST or EVAL")
    args = parser.parse_args()

    cfg = configparser.ConfigParser()
    cfg.read(args.analysis_setting)

    while True:
        ## ans = input(" \n Enter command, analysis(1) or exit(0) ? ")
        ans = '1'
        try:
            ans = int(ans[0])
        except ValueError:
            print("\n You entered {}. Please enter 0, or 1. Thanks".format(ans))
            continue

        if ans == 0:
            print("\n # Bye")
            break
        elif ans == 1:
            print("Start the analysis.")
            model_num = '6'
            # model_num = input(" \n Please enter model number : \n"
            #               "ex) 2   : (PATTERN_OPERATION)\n"
            #               "ex) 5   : (BEFORE_DEO_O2)\n"
            #               "ex) 6   : (F_CC_C)\n"
            #               " : "
            #                    )
            mode = args.mode
            model = "model_" + str(model_num)
            port_num = cfg[model][mode.lower() + '_port']
            real_y = cfg[model]['real_y']

            # Init. & load db data
            db_handler = mssql.pymssqlwrapper.PyMssqlWrapper(server_name=cfg['mssql_db']['server_name'],
                                                             port=cfg['mssql_db']['port'],
                                                             username=cfg['mssql_db']['username'],
                                                             password=cfg['mssql_db']['password'],
                                                             db_name=cfg[model]['db_name'],
                                                             table_name=cfg[model][args.mode.lower() + '_table_name'])
            db_columns = db_handler.selectColumnNames()
            db_columns = [db_column[0] for db_column in db_columns]
            transaction_cd = cfg[model][mode.lower() + '_header']
            null_check_var_list = [var.strip() for var in cfg[model]['null_check_var_list'].split(',')] if cfg[model]['null_check_var_list'] != '' else None
            range_check_var_list = [var.strip() for var in cfg[model]['range_check_var_list'].split(',')] if cfg[model]['range_check_var_list'] != '' else None
            real_cond_list = get_cond_list(mode,
                                           transaction_cd=transaction_cd,
                                           null_check_var_list=null_check_var_list,
                                           range_check_var_list=range_check_var_list)
            real_cond_db_data = db_handler.selectDataByConditions(cond_list=real_cond_list)
            real_cond_db_df = pd.DataFrame(real_cond_db_data, columns=db_columns)
            # add condition for RH Macro dataset
            if 'rh_macro' in cfg[model][args.mode.lower() + '_table_name']:
                real_cond_db_df = real_cond_db_df.loc[(20 <= real_cond_db_df['C_MAX']) & (real_cond_db_df['C_MAX'] <= 100)].reset_index(drop=True)

            specific_db_columns = [mode + '_START_TIME', real_y]

            # Load result files
            test_model_dir = os.path.join("..\\sys_component\\model\\", "RH_MACRO_M" + model_num, 'test_190823')
            dataset_file_list = ['dataset_file_17',
                                 'dataset_file_18_19',
                                 'dataset_file_1801_1806',
                                 'dataset_file_1804_1809',
                                 'dataset_file_1807_1812',
                                 'dataset_file_1810_1903']
            model_result_list = [['MODEL_DATASET_INFO', 'DATA_COUNTS', 'ACCURACY', 'RMSE']]
            for m_idx, model_dir_name in enumerate(dataset_file_list):
                model_dir_path = os.path.join(test_model_dir, model_dir_name)
                result_file_path = os.path.join(model_dir_path, 'result.txt')
                raw_df = pd.read_csv(result_file_path, names=['raw_data'])

                df_columns = ['PORT_NUM', args.mode + '_START_TIME', 'MODEL_EST_Y']
                df_column_list = []
                for idx, column in enumerate(df_columns):
                    column_df = raw_df.iloc[idx::len(df_columns), :]
                    column_df.columns = [column]
                    column_df.reset_index(drop=True, inplace=True)
                    df_column_list.append(column_df)

                # merge three dataframe
                new_df = pd.concat([df_column_list[0], df_column_list[1], df_column_list[2]], axis=1)

                # extract specific port data(지시 or 실적)
                new_df = new_df.loc[(new_df['PORT_NUM'] == 'port number: ' + port_num)].reset_index(drop=True)


                # match similar operation data by same minute
                real_cond_db_df[mode + '_START_TIME'] = real_cond_db_df[mode + '_START_TIME'].str.slice(0, 16)
                new_df[mode + '_START_TIME'] = new_df[mode + '_START_TIME'].str.slice(0, 16)

                # compare df for model operation time
                new_df['DB_'+ 'REAL' + '_Y'] = real_cond_db_df[real_y].loc[
                    real_cond_db_df[mode + '_START_TIME'].isin(new_df[mode + '_START_TIME'])].reset_index(drop=True)

                # Adjust y value
                if model_num == '2':
                    new_dataset = decode_symbol_by_dict(dataset=[new_df.columns.tolist()] + new_df.values.tolist(),
                                                        decode_vars=['MODEL_EST_Y', 'DB_REAL_Y'],
                                                        dict=model_2.decode_symbol_dict)
                    new_df = pd.DataFrame(new_dataset, columns=new_df.columns)

                # drop 'NaN' and zero value in dataframe
                new_df['DB_REAL_Y'] = pd.to_numeric(new_df['DB_REAL_Y'], errors='coerce').astype(float)
                new_df = new_df[np.isfinite(new_df['DB_REAL_Y'])]
                new_df = new_df.loc[(new_df['DB_REAL_Y'] != 0)].reset_index(drop=True)

                print("model_dir : {}, len_df : {}".format(model_dir_name, len(new_df)))



                # Calculate model accuracy
                model_acc, model_rmse = learning_lib.check_performance_measure(est_val='MODEL_EST_Y', real_val='DB_REAL_Y',
                                                                               tolerance=float(cfg[model]['tolerance']), data_frame=new_df)

                model_result_list.append([model_dir_name, len(new_df), model_acc, model_rmse])

            data_lib.write_list_to_csv(model_result_list, os.path.join(test_model_dir, model + '_result.csv'))
            print("Performance evaluation is completed.")
        else:
            print("\n You entered {:d}. Please enter 0 or 1. Thanks".format(ans))

if __name__ == "__main__":

    if len(sys.argv) == 1:
        sys.argv.extend([
            "--analysis_setting", "db_setting.ini",
            "--mode", "EVAL",
                         ])

    main()
