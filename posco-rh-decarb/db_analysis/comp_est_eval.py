#! /usr/bin/env python
# -*- coding: utf-8 -*-

import pymssql
from pandas import DataFrame
import pandas as pd


def get_input_var(dataset_idx=55):
    # input 으로 쓰이는 변수명을 list 형태로 반환한다.
    data = pd.read_csv("C:/mindslab/posco-temp-model-component/sys_component/rh_macro_var.csv")
    df = data.iloc[:, [3, dataset_idx]]
    df = df[df.iloc[:, 1] == '100']
    df = df.reset_index(drop=True)
    return df.iloc[:,0].values.tolist()

def different(select):
    conn = pymssql.connect(host="172.24.92.148", user="SA", password="sm2_ai_dev!", database="MINDS_CVT")
    cursor = conn.cursor()
    query = "SELECT TOP (100) {} FROM [MINDS_RH].[dbo].[rh_macro_est] as est INNER JOIN [MINDS_RH].[dbo].[rh_macro_eval] as eval " \
          "ON est.CHARGE_NO = eval.CHARGE_NO AND est.EST_MODEL_VER = eval.EVAL_MODEL_VER " \
          "WHERE est.TRANSACTION_CD = 'RH_4S_ES' ORDER BY est.No DESC"
    cursor.execute(
        query.format(select)
    )
    row = cursor.fetchall()
    row_all = [list(r) for r in row]
    # column_names = [item[0] for item in cursor.description]
    column_names = select.split(", ")
    df = DataFrame(row_all, columns = column_names)
    my_list = list()
    for i in range(0, len(df.columns.tolist()), 2):
        if df.iloc[:,i].dtype == 'O':
            print ("{} is object".format(df.iloc[:,i].name))
        else:
            if not (df.iloc[:, i].round(1) == df.iloc[:, i+1].round(1)).all():
                my_list.append(df.iloc[:,i].name)
                my_list.append(df.iloc[:,i+1].name)
    df = df[my_list]
    df.to_csv("ys.csv", mode='w')

if __name__ == "__main__":
    m = get_input_var()
    var_list = list()
    est_var = ["est." + a for a in m]
    eval_var = ["eval." + a for a in m]
    for s, v in zip(est_var, eval_var):
        var_list.append(s)
        var_list.append(v)
    var_str = ", ".join(var_list)
    different(var_str)