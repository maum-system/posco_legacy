#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = "Yeongsun Park"
__date__ = "2020-10-13"
__email__ = "parkys@mindslab.ai"

import sys, os
from pptx import Presentation
from pptx.util import Inches
from pptx.util import Pt
from pptx.enum.text import PP_ALIGN
import cv2
from PIL import Image

class MakeSlide():
    def __init__(self, model_num, img_dir):
        self.model_num = model_num
        self.img_dir = img_dir
        self.title_text = 'rh탈탄 성능 평가'
        self.subtitle_text = '2021.03.31'
        self.re_freq = {'d':'Daily', 'w':'Weekly', 'm':'Monthly'}
        self.re_period = {'d':'일', 'w':'주', 'm':'월'}

    def title_slide(self): # 제목 슬라이드
        title_slide_layout = prs.slide_layouts[0] # 0 : 제목슬라이드에 해당
        slide = prs.slides.add_slide(title_slide_layout) # 제목 슬라이드를 파워포인트 객체에 추가

        title = slide.shapes.title  # 제목
        title.text = self.title_text

        subtitle = slide.placeholders[1] # 제목상자는 placeholders[0], 부제목상자는 [1]
        subtitle.text_frame.text = self.subtitle_text  # subtitle.txt = ""
        subtitle.text_frame.paragraphs[0].alignment = PP_ALIGN.RIGHT

    def one_picture_slide(self, title_text, img_path):
        ### 사진 슬라이드 (이미지 1개)
        blank_slide_layout = prs.slide_layouts[5] # 6 : 제목만 있는 슬라이드
        slide = prs.slides.add_slide(blank_slide_layout)
        shapes = slide.shapes

        # title font size modify
        shapes.title.text_frame.text = title_text  # shapes.title.text = 'Adding an AutoShape'
        shapes.title.text_frame.paragraphs[0].font.size=Pt(30)

        img_path = img_path
        img = cv2.imread(img_path, cv2.IMREAD_UNCHANGED)
        img_height, image_width, channel = img.shape
        print("img_height: "+str(img_height), "image_width: "+str(image_width), "channel: "+str(channel))

        top = Inches(2.5)
        resize_width = Inches(image_width*0.002)
        img_left = (prs.slide_width - resize_width) / 2
        foo = Image.open(img_path)
        rgb_foo = foo.convert('RGB')
        rgb_foo.save("temp.jpg", quality = 20)  # quality=80
        # slide.shapes.add_picture(img_path, img_left, top, width=resize_width)
        slide.shapes.add_picture("temp.jpg", img_left, top, width=resize_width)

        # 기간 정보 추가
        left, top, width, height = Inches(2), Inches(1.5), Inches(2), Inches(2)
        txBox = shapes.add_textbox(left, top, width, height)
        tf = txBox.text_frame
        tf.text = "기간: {}".format(" ~ ".join(img_path.split("/")[-1].split("_")[:2]))

    def two_picture_slide(self, title_text, img_path, img_path2=None):
        ### 사진 슬라이드 (이미지 1개)
        blank_slide_layout = prs.slide_layouts[5] # 6 : 제목만 있는 슬라이드
        slide = prs.slides.add_slide(blank_slide_layout)
        shapes = slide.shapes

        # title font size modify
        shapes.title.text_frame.text = title_text  # shapes.title.text = 'Adding an AutoShape'
        shapes.title.text_frame.paragraphs[0].font.size=Pt(30)

        img_path = img_path
        img_path2 = img_path2
        img = cv2.imread(img_path, cv2.IMREAD_UNCHANGED)
        img_height, image_width, channel = img.shape
        print("img_height: "+str(img_height), "image_width: "+str(image_width), "channel: "+str(channel))

        top = Inches(2)
        resize_width = Inches(image_width*0.0018)
        resize_height = Inches(image_width*0.0018)

        img_left = (prs.slide_width - resize_width) / 2
        foo = Image.open(img_path)
        rgb_foo = foo.convert('RGB')
        rgb_foo.save("temp.jpg")  # quality=80
        # slide.shapes.add_picture(img_path, img_left, top, width=resize_width)
        slide.shapes.add_picture("temp.jpg", img_left, top, width=resize_width)
        if img_path2:
            # top_2 = prs.slide_height - top - resize_height - (prs.slide_height - 2*top - 2*resize_height)
            top_2 = Inches(4.5)
            foo = Image.open(img_path2)
            rgb_foo = foo.convert('RGB')
            rgb_foo.save("temp.jpg")  # quality=80
            # slide.shapes.add_picture(img_path2, img_left, top_2, width=resize_width)
            slide.shapes.add_picture("temp.jpg", img_left, top_2, width=resize_width)

        # 기간 정보 추가
        left, top, width, height = Inches(2), Inches(1.5), Inches(2), Inches(2)
        txBox = shapes.add_textbox(left, top, width, height)
        tf = txBox.text_frame
        tf.text = "기간: {}".format(" ~ ".join(img_path.split("/")[-1].split("_")[:2]))

    def sort_file_name(self, file_names, model_num):
        for file_name in file_names:
            if 'png' in file_name:
                if "model_" + model_num + "_" in file_name:  # 16개 선택됨.
                    # make_slide.one_picture_slide('Adding an AutoShape', img_path)
                    if "data_raw" in file_name:
                        self.data_raw_list.append(file_name)

                    if "x_real_y_eval" in file_name:
                        self.data_real_pred_list.append(file_name)

                    if "data_average" in file_name:
                        if "day" in file_name:
                            self.data_average_dict['d'].append(file_name)
                        elif "week" in file_name:
                            self.data_average_dict['w'].append(file_name)
                        elif "month" in file_name:
                            self.data_average_dict['m'].append(file_name)

                    if "acc" in file_name:
                        if "day" in file_name:
                            self.data_acc_dict['d'].append(file_name)
                        elif "week" in file_name:
                            self.data_acc_dict['w'].append(file_name)
                        elif "month" in file_name:
                            self.data_acc_dict['m'].append(file_name)

    def main(self):
        make_slide.title_slide()  # title slide
        # image slide 작업
        file_names = os.listdir(img_dir)
        file_names.sort()
        for n in range(len(self.model_num)):
            self.data_raw_list = list()
            self.data_real_pred_list = list()
            self.data_average_dict = {'d': list(), 'w': list(), 'm': list()}
            self.data_acc_dict = {'d': list(), 'w': list(), 'm': list()}
            make_slide.sort_file_name(file_names, self.model_num[n])
            """
            # data raw list
            for i in range(len(self.data_raw_list)):
                make_slide.one_picture_slide('Raw Data', img_dir + self.data_raw_list[i])

            # data average dict
            for freq, v in self.data_average_dict.items():
                for i in range(len(v)):
                    make_slide.one_picture_slide('{} 별 평균 데이터'.format(self.re_period[freq]), img_dir + v[i])

            # data acc dict
            for freq, v in self.data_acc_dict.items():
                for i in range(len(v)):
                    make_slide.one_picture_slide('{} 별 평균 정확도 '.format(self.re_period[freq]), img_dir + v[i])

            # data real pred list
            for i in range(len(self.data_real_pred_list)):
                make_slide.one_picture_slide('실측값과 예측값 비교', img_dir + self.data_real_pred_list[i])
            """
            # data raw list
            for i in range(0, len(self.data_raw_list), 2):
                if len(self.data_raw_list) % 2 == 1 and i == len(self.data_raw_list)-1:
                    make_slide.one_picture_slide('Raw Data', img_dir + self.data_raw_list[i])
                else:
                    make_slide.two_picture_slide('Raw Data', img_dir+self.data_raw_list[i],
                                                 img_dir+self.data_raw_list[i+1])
                    print (img_dir+self.data_raw_list[i])
                    print (img_dir+self.data_raw_list[i+1])

            # data average dict
            for freq, v in self.data_average_dict.items():
                for i in range(0, len(v), 2):
                    if len(v) % 2 == 1 and i == len(v) - 1:
                        make_slide.one_picture_slide('{} 별 평균 데이터'.format(self.re_period[freq]), img_dir + v[i])
                    else:
                        make_slide.two_picture_slide('{} 별 평균 데이터'.format(self.re_period[freq]), img_dir + v[i],
                                                 img_dir + v[i + 1])

            # data acc dict
            for freq, v in self.data_acc_dict.items():
                for i in range(0, len(v), 2):
                    if len(v) % 2 == 1 and i == len(v) - 1:
                        make_slide.one_picture_slide('{} 별 평균 정확도 '.format(self.re_period[freq]), img_dir + v[i])
                    else:
                        make_slide.two_picture_slide('{} 별 평균 정확도'.format(self.re_period[freq]), img_dir + v[i],
                                                 img_dir + v[i + 1])


            # data real pred list
            for i in range(0, len(self.data_real_pred_list), 2):
                if len(self.data_real_pred_list) % 2 == 1 and i == len(self.data_real_pred_list)-1:
                    make_slide.one_picture_slide('실측값과 예측값 비교', img_dir + self.data_real_pred_list[i])
                else:
                    make_slide.two_picture_slide('실측값과 예측값 비교', img_dir+self.data_real_pred_list[i],
                                                 img_dir+self.data_real_pred_list[i+1])





        # save
        prs.save('test.pptx')


if __name__ == '__main__':
    prs = Presentation()  # ppt 객체 선언
    model_num = ["4", "5", "6"]  # ex. ["5"] or ["2_50_C", "3_85_OXY"]
    img_dir = "Output/20210511/" # ex. "Output/20201127/"
    make_slide = MakeSlide(model_num, img_dir)
    make_slide.main()



