#! /usr/bin/env python
# -*- coding: utf-8 -*-

import pymssql
from pandas import DataFrame
import pandas as pd

conn = pymssql.connect(host="172.24.92.148", user="SA", password="sm2_ai_dev!", database="MINDS_CVT")
cursor = conn.cursor()

# cursor.execute(
#     "SELECT TOP (1000) Eval.No, eval.TAP_WORK_DATE, eval.CHARGE_NO, est.RH_RISING_O2_INPUT, est.RH_TOT_OB_INPUT, eval.RH_RISING_O2_INPUT, eval.RH_TOT_OB_INPUT "
#     "FROM [MINDS_RH].[dbo].[rh_macro_est] AS est INNER JOIN [MINDS_RH].[dbo].[rh_macro_eval] as eval "
#     "ON est.CHARGE_NO = eval.CHARGE_NO AND /*est.MODEL_REQUEST_TIME = eval.MODEL_REQUEST_TIME AND*/ est.EST_MODEL_VER = eval.EVAL_MODEL_VER "
#     "WHERE /*est.CHARGE_NO = 'SQ49666' AND*/ est.TRANSACTION_CD = 'RH_4S_ES' AND eval.TRANSACTION_CD = 'RH_4S_EV'AND est.MODEL_REQUEST_TIME = 7 AND "
#     "eval.RH_RISING_O2_INPUT != EVAL.RH_TOT_OB_INPUT ORDER BY eval.No DESC")
# row = cursor.fetchall()
# row_all = [list(r) for r in row]
# column_names = [item[0] for item in cursor.description]
# df = DataFrame(row_all, columns = column_names) # (row_all, columns=[])
# print (df)
#
# df.to_csv("ys.csv", mode='w')


def get_input_var(dataset_idx=55):
    # input 으로 쓰이는 변수명을 list 형태로 반환한다.
    data = pd.read_csv("C:/mindslab/posco-temp-model-component/sys_component/rh_macro_var.csv")
    df = data.iloc[:, [3, dataset_idx]]
    df = df[df.iloc[:,1] == '100']
    df = df.reset_index(drop=True)
    return df.iloc[:,0].values.tolist()

def different(select):
    conn = pymssql.connect(host="172.24.92.148", user="SA", password="sm2_ai_dev!", database="MINDS_CVT")
    cursor = conn.cursor()
    sql = "SELECT TOP (10) {} FROM [MINDS_RH].[dbo].[rh_macro_est] as est INNER JOIN [MINDS_RH].[dbo].[rh_macro_eval] as eval " \
          "ON est.CHARGE_NO = eval.CHARGE_NO AND est.EST_MODEL_VER = eval.EVAL_MODEL_VER " \
          "WHERE est.TRANSACTION_CD = 'RH_4S_ES' ORDER BY est.No DESC"
    cursor.execute(
        sql.format(select)
    )
    row = cursor.fetchall()
    row_all = [list(r) for r in row]
    # column_names = [item[0] for item in cursor.description]
    column_names = select.split(", ")
    df = DataFrame(row_all, columns = column_names) # (row_all, columns=[])
    # df_eval = df[eval_var]
    # df_eval.columns = m
    # df_est = df[est_var]
    # df_est.columns = m
    # df = pd.concat([df_est, df_eval])
    my_list = list()
    for i in range(0, len(df.columns.tolist()), 2):
        if df.iloc[:,i].dtype == 'O':
            print ("{} is object".format(df.iloc[:,i].name))
        else:
            if not (df.iloc[:, i].round(1) == df.iloc[:, i+1].round(1)).all():
                my_list.append(df.iloc[:,i].name)
                my_list.append(df.iloc[:,i+1].name)
    df = df[my_list]
    df.to_csv("ys.csv", mode='w')


# df = data[['No', 'TRANSACTION_CD', 'TAP_WORK_DATE', 'CHARGE_NO', 'F_RH_C', 'EST_F_RH_C', 'EVAL_F_R
# H_C', 'EVAL_RANGE_CHECK_VARS']]
# df = df[df['EST_F_RH_C']<10]
# df = df[df['EVAL_F_RH_C']<10]
# df = df[df['F_RH_C']!=0]
# df = df[df.EVAL_RANGE_CHECK_VARS=='None']
# # SQ49666
#
#
# print (df)
# df.to_csv("ys.csv", mode='w')

if __name__ == "__main__":
    m = get_input_var()
    var_list = list()
    est_var = ["est." + a for a in m]
    eval_var = ["eval."+a for a in m]
    for s, v in zip(est_var, eval_var):
        var_list.append(s)
        var_list.append(v)
    var_str = ", ".join(var_list)
    different(var_str)