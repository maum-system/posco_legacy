#! /usr/bin/env python
# -*- coding: utf-8 -*-

import pymssql
from pandas import DataFrame
import pandas as pd
import matplotlib.pyplot as plt

conn = pymssql.connect(host="172.24.92.148", user="SA", password="sm2_ai_dev!", database="MINDS_CVT")
cursor = conn.cursor()

# cursor.execute(
#     "SELECT TOP (1000) Eval.No, eval.TAP_WORK_DATE, eval.CHARGE_NO, est.RH_RISING_O2_INPUT, est.RH_TOT_OB_INPUT, eval.RH_RISING_O2_INPUT, eval.RH_TOT_OB_INPUT "
#     "FROM [MINDS_RH].[dbo].[rh_macro_est] AS est INNER JOIN [MINDS_RH].[dbo].[rh_macro_eval] as eval "
#     "ON est.CHARGE_NO = eval.CHARGE_NO AND /*est.MODEL_REQUEST_TIME = eval.MODEL_REQUEST_TIME AND*/ est.EST_MODEL_VER = eval.EVAL_MODEL_VER "
#     "WHERE /*est.CHARGE_NO = 'SQ49666' AND*/ est.TRANSACTION_CD = 'RH_4S_ES' AND eval.TRANSACTION_CD = 'RH_4S_EV'AND est.MODEL_REQUEST_TIME = 7 AND "
#     "eval.RH_RISING_O2_INPUT != EVAL.RH_TOT_OB_INPUT ORDER BY eval.No DESC")
# row = cursor.fetchall()
# row_all = [list(r) for r in row]
# column_names = [item[0] for item in cursor.description]
# df = DataFrame(row_all, columns = column_names) # (row_all, columns=[])
# print (df)
#
# df.to_csv("ys.csv", mode='w')


def get_input_var(dataset_idx=73):  # 55, 70, 73
    # input 으로 쓰이는 변수명을 list 형태로 반환한다.
    data = pd.read_csv("C:/mindslab/posco-temp-model-component/sys_component/rh_macro_var.csv")
    df = data.iloc[:, [3, dataset_idx]]
    df = df[df.iloc[:,1] == '100']
    df = df.reset_index(drop=True)
    return df.iloc[:,0].values.tolist()

def select_input(select_var):
    conn = pymssql.connect(host="172.24.92.148", user="SA", password="sm2_ai_dev!", database="MINDS_CVT")
    cursor = conn.cursor()
    sql = "SELECT {} FROM [MINDS_RH].[dbo].[rh_macro_est]  " \
          "WHERE TRANSACTION_CD = 'RH_4S_ES' AND TAP_WORK_DATE >='20210101' AND TAP_WORK_DATE <='20210325' ORDER BY No"
    cursor.execute(
        sql.format(select_var)
    )
    row = cursor.fetchall()
    row_all = [list(r) for r in row]
    column_names = select_var.split(", ")
    df = DataFrame(row_all, columns = column_names) # (row_all, columns=[])
    for c_name in column_names:
        plt.rcParams["figure.figsize"] = [15, 5]
        plt.scatter(df['TAP_WORK_DATE'], df["{}".format(c_name)], c='blue', alpha=0.5, label='real_y')
        plt.xticks(rotation=45)
        # plt.savefig(os.path.join(OUT_PATH, fname), dpi=300)
        plt.savefig('yse-ys_{}.jpg'.format(c_name), dpi=300)
        plt.clf()
        # df.to_csv("ys.csv", mode='w')

if __name__ == "__main__":
    var_list = get_input_var()
    var_list.insert(0,'TAP_WORK_DATE')
    var_list.insert(0,'No')
    # var_list = list()
    var_str = ", ".join(var_list)
    select_input(var_str)