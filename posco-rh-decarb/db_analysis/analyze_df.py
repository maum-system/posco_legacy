#! /usr/bin/env python
# -*- coding: utf-8 -*-

import sys, os
import collections
import pandas as pd

class EditDF:
    def __init__(self):
        pass


    def get_statistic(self, idx, df_summary_t, pred_y, real_y, t, db_df, tolerance):
        if idx ==0:
            df_summary_t['{}_AVERAGE'.format(pred_y)] = db_df["{}".format(pred_y)].resample('1{}'.format(t)).mean()
            df_summary_t['{}_AVERAGE'.format(real_y)] = db_df["{}".format(real_y)].resample('1{}'.format(t)).mean()
            df_summary_t['{}_STD'.format(pred_y)] = db_df["{}".format(pred_y)].resample('1{}'.format(t)).std()
            df_summary_t['{}_STD'.format(real_y)] = db_df["{}".format(real_y)].resample('1{}'.format(t)).std()
            df_summary_t['filter_y_ACC_{}'.format(tolerance)] = db_df.model_norm_range_cond.resample('1{}'.format(t)).sum() / db_df.model_norm_range_cond.resample('1{}'.format(t)).count()
            df_summary_t['filter_y_RMSE'] = db_df.EST_REAL_DIFF_POW.resample('1{}'.format(t)).sum() ** (1 / 2) / db_df.model_norm_range_cond.resample('1{}'.format(t)).count()
        else:
            if not db_df.empty:
                df_summary_t['refined_ACC_{}'.format(tolerance)] = db_df.model_norm_range_cond.resample('1{}'.format(t)).sum() / db_df.model_norm_range_cond.resample('1{}'.format(t)).count()
                df_summary_t['refined_RMSE'] = db_df.EST_REAL_DIFF_POW.resample('1{}'.format(t)).sum() ** (1 / 2) / db_df.model_norm_range_cond.resample('1{}'.format(t)).count()


    def order_column(self, refine_df, df_summary_t, pred_y, real_y, tolerances):
        if not refine_df.empty:
            df_summary_t = df_summary_t[['{}_AVERAGE'.format(pred_y),
                                           '{}_AVERAGE'.format(real_y),
                                           '{}_STD'.format(pred_y),
                                           '{}_STD'.format(real_y),
                                           'filter_y_ACC_{}'.format(tolerances[0]),
                                           'filter_y_ACC_{}'.format(tolerances[1]),
                                           'filter_y_RMSE',
                                           'refined_ACC_{}'.format(tolerances[0]),
                                           'refined_ACC_{}'.format(tolerances[1]),
                                           'refined_RMSE']]
        else:
            df_summary_t = df_summary_t[['{}_AVERAGE'.format(pred_y),
                                           '{}_AVERAGE'.format(real_y),
                                           '{}_STD'.format(pred_y),
                                           '{}_STD'.format(real_y),
                                           'filter_y_ACC_{}'.format(tolerances[0]),
                                           'filter_y_ACC_{}'.format(tolerances[1]),
                                           'filter_y_RMSE']]
        return df_summary_t
    def save_evaluated_result(self, SERVER_MODE, model_name, start_date, end_date, date_cond_data_len, filter_y_df_len, refine_df_len, tolerances,
                              acc_header, acc_dataset, rmse_dataset, OUT_PATH):
        eval_data_fname = SERVER_MODE.lower() + '_total_model_evaluation_result.csv'
        res_dict = collections.OrderedDict()
        res_dict['model'] = model_name
        res_dict['start_date'] = start_date
        res_dict['end_date'] = end_date
        res_dict['total_data'] = date_cond_data_len
        res_dict['filtered_by_y'] = filter_y_df_len
        res_dict['filtered_by_outliers'] = refine_df_len
        res_dict['tolerances'] = tolerances
        csv_acc_columns = ['first_acc(filtered_by_y)', 'second_acc(filtered_by_y)', 'third_acc(filtered_by_y)',
                           'first_acc(filtered_outliers)', 'second_acc(filtered_outliers)',
                           'third_acc(filtered_outliers)']
        # acc_titles = [s for s in sorted(list(res_dict.keys())) if "_acc" in s]
        for ca_idx, ca_name in enumerate(csv_acc_columns):
            for acc_idx, acc_name in enumerate(acc_header):
                if acc_name == ca_name:
                    res_dict[ca_name] = acc_dataset[1][acc_idx]
                elif ca_name not in acc_header:
                    res_dict[ca_name] = 'None'
        res_dict['RMSE(filtered_by_y)'] = rmse_dataset[1][0]
        res_dict['RMSE(filtered_outliers)'] = rmse_dataset[1][1]

        res_df = pd.DataFrame.from_dict(res_dict, orient='index').transpose()
        save_file_path = os.path.join(OUT_PATH, start_date[2:] + '_' + end_date[2:] + '_' + eval_data_fname)
        if not (os.path.isfile(save_file_path)):
            res_df.to_csv(save_file_path, mode='w')
        else:
            res_df.to_csv(save_file_path, mode='a', header=False)

def get_data_by_use_mod_est_oxy(eval_model_name, filter_y_df, refine_df):
    if eval_model_name == 'model_3_85_OXY':
        filter_y_df = filter_y_df.loc[filter_y_df['USE_MOD_EST_OXY'] == int(-1)].reset_index(drop=True)
        refine_df = refine_df.loc[refine_df['USE_MOD_EST_OXY'] == int(-1)].reset_index(drop=True)
    elif eval_model_name == 'model_3_85_C' or eval_model_name == 'model_3_85_O':
        filter_y_df = filter_y_df.loc[filter_y_df['USE_MOD_EST_OXY'] >= int(-1)].reset_index(drop=True)
        refine_df = refine_df.loc[refine_df['USE_MOD_EST_OXY'] >= int(-1)].reset_index(drop=True)
    return filter_y_df, refine_df

def get_data_by_within_1_minute(curr_model_ini, filter_y_df, refine_df):
    est_time = curr_model_ini['eval_time']
    real_time = curr_model_ini['real_time']
    tolerance_time = int(curr_model_ini['tolerance_time'])
    filter_y_time_ch_no = filter_y_df['CHARGE_NO'].loc[
        (filter_y_df[est_time] - filter_y_df[real_time]) <= tolerance_time].reset_index(drop=True)
    refine_time_ch_no = refine_df['CHARGE_NO'].loc[
        (refine_df[est_time] - refine_df[real_time]) <= tolerance_time].reset_index(drop=True)

    # CHARGE_NO가 같은 데이터 추출
    filter_y_df = filter_y_df.loc[filter_y_df['CHARGE_NO'].isin(filter_y_time_ch_no)]
    refine_df = refine_df.loc[refine_df['CHARGE_NO'].isin(refine_time_ch_no)]
    return filter_y_df, refine_df

def get_data_by_within_10_ppm(curr_model_ini, filter_y_df, refine_df):
    f_cc_c = 'F_RH_C'
    f_rh_c = 'F_CC_C'
    tolerence_ppm = 0.001 # 10 ppm
    filter_y_time_ch_no = filter_y_df['CHARGE_NO'].loc[
        abs(filter_y_df[f_cc_c] - filter_y_df[f_rh_c]) <= tolerence_ppm].reset_index(drop=True)
    refine_time_ch_no = refine_df['CHARGE_NO'].loc[
        abs(refine_df[f_cc_c] - refine_df[f_rh_c]) <= tolerence_ppm].reset_index(drop=True)

    # CHARGE_NO가 같은 데이터 추출
    filter_y_df = filter_y_df.loc[filter_y_df['CHARGE_NO'].isin(filter_y_time_ch_no)]
    refine_df = refine_df.loc[refine_df['CHARGE_NO'].isin(refine_time_ch_no)]
    return filter_y_df, refine_df


def remove_unpredictable_data(filter_y_df, refine_df, real_y, pred_y):
    # filter_y_df = filter_y_df.loc[(0 < filter_y_df[real_y]) & (filter_y_df[pred_y] < 999999)].reset_index(drop=True)
    # refine_df = refine_df.loc[(0 < refine_df[real_y]) & (refine_df[pred_y] < 999999)].reset_index(drop=True)
    # temp
    filter_y_df = filter_y_df.loc[(filter_y_df[pred_y] < 999999)].reset_index(drop=True)
    refine_df = refine_df.loc[(refine_df[pred_y] < 999999)].reset_index(drop=True)
    return filter_y_df, refine_df

def remove_outlier_real_y(filter_y_df, refine_df, real_y, min_th, max_th):
    filter_y_df = filter_y_df.loc[((min_th <= filter_y_df[real_y]) & (filter_y_df[real_y] <= max_th))].reset_index(drop=True)
    refine_df = refine_df.loc[((min_th <= refine_df[real_y]) & (refine_df[real_y] <= max_th))].reset_index(drop=True)
    return filter_y_df, refine_df


def remove_nan_real_y(filter_y_df, refine_df, real_y):
    filter_y_df = filter_y_df.loc[filter_y_df[real_y] != ''].reset_index(drop=True)
    refine_df = refine_df.loc[refine_df[real_y] != ''].reset_index(drop=True)
    print("\n[모델 real 값이 nan 인 데이터 제거 (model2 에서 오류나서 추가함) results.]")
    return filter_y_df, refine_df


def get_type_df(filter_y_df, refine_df, cond, str_dom, num_cond=1):
    df_by_types = {'filter_y_df': None, 'refine_df': None}
    # for df in zip([filter_y_df, refine_df], ['filter_y_df', 'refine_df']):
    for name, df in zip(['filter_y_df', 'refine_df'], [filter_y_df, refine_df]):
        # df_by_types[df[1]] = df[0].loc[(df[0][cond] == str_dom)].reset_index(drop=True)
        if num_cond == 1:
            df_by_types[name] = df.loc[(df[cond] == str_dom)].reset_index(drop=True)
    return df_by_types['filter_y_df'], df_by_types['refine_df']


# 내가 생각하는 주요 변수가 이상치인 경우 제외
def remove_outlier_var(filter_y_df, refine_df, my_var, min_th, max_th):
    # min_th = eval.feat_handler.vars[my_var].min_thresh  # handler 에서 써야하는 코드
    # max_th = eval.feat_handler.vars[my_var].max_thresh  # handler 에서 써야하는 코드
    filter_y_df = filter_y_df.loc[((min_th <= filter_y_df[my_var]) & (filter_y_df[my_var] <= max_th))].reset_index(drop=True)
    filter_y_df = filter_y_df.loc[(filter_y_df[my_var] == 0)].reset_index(drop=True)
    refine_df = refine_df.loc[((min_th <= refine_df[my_var]) & (refine_df[my_var] <= max_th))].reset_index(drop=True)
    refine_df = refine_df.loc[(refine_df[my_var] == 0)].reset_index(drop=True)
    return filter_y_df, refine_df