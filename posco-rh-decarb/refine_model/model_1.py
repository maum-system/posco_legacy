import pandas as pd
from handlers import dataset_handler

CHEM05_F_BAP_C_VAL_1 = 'CHEM05_F_BAP_C_VAL_1'
OUT_BAP_DEP_CARBON = 'OUT_BAP_DEP_CARBON'
RH_ARR_O2 = 'RH_ARR_O2'
OUT_RH_ARR_OXYGEN = 'OUT_RH_ARR_OXYGEN'
TOLERANCE = 50
EST_OUT_BAP_DEP_CARBON = 'EST_OUT_BAP_DEP_CARBON'
EST_OUT_RH_ARR_OXYGEN = 'EST_OUT_RH_ARR_OXYGEN'

def refine_dataset_by_y_value(dataset, mode=None, logger=None):
    if dataset is None:
        return []

    df = pd.DataFrame(dataset[1:], columns=dataset[0])

    df[CHEM05_F_BAP_C_VAL_1] = pd.to_numeric(df[CHEM05_F_BAP_C_VAL_1], errors='coerce')
    df[CHEM05_F_BAP_C_VAL_1] = df[CHEM05_F_BAP_C_VAL_1].astype(float) * 10000

    if mode == dataset_handler.ANALYSIS_MODE:
        df[EST_OUT_BAP_DEP_CARBON] = df.OUT_BAP_DEP_CARBON.astype(float)
        df[EST_OUT_RH_ARR_OXYGEN] = df.OUT_RH_ARR_OXYGEN.astype(float)

    df[OUT_BAP_DEP_CARBON] = df[CHEM05_F_BAP_C_VAL_1]
    df[OUT_RH_ARR_OXYGEN] = df[RH_ARR_O2]

    refined_dataset_header = df.columns.tolist()
    refined_dataset = df.values.tolist()

    return [refined_dataset_header] + refined_dataset


def run(feature, mode=None, dataset=None, logger=None):
    if not dataset:
        dataset = feature.dataset

    feature.dataset = refine_dataset_by_y_value(dataset, mode, logger)
