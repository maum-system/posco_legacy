import pandas as pd
from handlers import dataset_handler

EXCEPT_OUTPUT_VAR = 'OUT_OP_PATTERN'
PATTERN_OPERATION = 'PATTERN_OPERATION'
OUT_OP_PATTERN = 'OUT_OP_PATTERN'
TOLERANCE = 1
EST_OUT_OP_PATTERN = 'EST_OUT_OP_PATTERN' # ys commented out
# EST_OP_PATTERN = 'EST_OP_PATTERN'

# 선행가탄, 선행가탄+냉각, OB, OB+냉각, Al+OB, 냉각제, 산소온도만족
decode_symbol_dict = { 'G_PREC' : 0,
                       'G_PREC+COOL' : 1,
                       'G_OB' : 2,
                       'G_OB+COOL' : 3,
                       'G_Al+OB' : 4,
                       'G_COOL' : 5,
                       'G_O_TEMP_OK' : 6}


def refine_dataset_by_y_value(dataset, mode=None, logger=None):
    if dataset is None:
        return []

    df = pd.DataFrame(dataset[1:], columns=dataset[0])

    if mode == dataset_handler.ANALYSIS_MODE:
        df[EST_OUT_OP_PATTERN] = df.OUT_OP_PATTERN  # ys commented out
        # df[EST_OP_PATTERN] = df.OP_PATTERN

    df[OUT_OP_PATTERN] = df[PATTERN_OPERATION]  # ys commented out
    # df[PATTERN_OPERATION] = df[PATTERN_OPERATION]

    refined_dataset_header = df.columns.tolist()
    refined_dataset = df.values.tolist()

    return [refined_dataset_header] + refined_dataset


def run(feature, dataset=None, mode=None, logger=None):
    if not dataset:
        dataset = feature.dataset

    feature.dataset = refine_dataset_by_y_value(dataset, mode, logger)