import matplotlib.pyplot as plt

Y_VAR = 'MINUTE_C'

CO_var_list = ['CO_1M', 'CO_2M', 'CO_3M', 'CO_4M', 'CO_5M', 'CO_6M', 'CO_7M', 'CO_8M', 'CO_9M', 'CO_10M', 'CO_11M',
               'CO_12M', 'CO_13M', 'CO_14M', 'CO_15M', 'CO_16M', 'CO_17M', 'CO_18M', 'CO_19M', 'CO_20M', 'CO_21M']

CO2_var_list = ['CO2_1M', 'CO2_2M', 'CO2_3M', 'CO2_4M', 'CO2_5M', 'CO2_6M', 'CO2_7M',
                'CO2_8M', 'CO2_9M', 'CO2_10M', 'CO2_11M', 'CO2_12M', 'CO2_13M', 'CO2_14M',
                'CO2_15M', 'CO2_16M', 'CO2_17M', 'CO2_18M', 'CO2_19M', 'CO2_20M', 'CO2_21M']

gas_flow_var_list = ['GAS_1M', 'GAS_2M', 'GAS_3M', 'GAS_4M', 'GAS_5M', 'GAS_6M', 'GAS_7M',
                     'GAS_8M', 'GAS_9M', 'GAS_10M', 'GAS_11M', 'GAS_12M', 'GAS_13M', 'GAS_14M',
                     'GAS_15M', 'GAS_16M', 'GAS_17M', 'GAS_18M', 'GAS_19M', 'GAS_20M', 'GAS_21M']


def measure_carbon_CO(gas_flow, ratio_CO):

    C = (gas_flow / 60) * (28 / 22.4) * (ratio_CO / 100) * (12 / 28)

    return C


def measure_carborn_CO2(gas_flow, ratio_CO2):

    C = (gas_flow / 60) * (44 / 22.4) * (ratio_CO2 / 100) * (12 / 44)

    return C


def calculate_ground_truth(var_names, row_data, y_name, request_time, logger=None):
    try:
        minute = int(row_data[var_names.index('MODEL_REQUEST_TIME')])
    except ValueError:
        minute = request_time
    remain_c = []  # 용강 내 C값
    ini_c = 0  # 초기 C값(BAP[C])
    fin_c = 0  # 최종 실측 C값
    weight = 0  # 용강량

    # 선행가탄
    c_input_switch = False  # 선행가탄 투입 여부
    c_input_tm = 0  # 선행가탄 투입 시점(분)
    c_input_qt = 0  # 선행가탄 투입량(kg)
    try:
        weight = float(row_data[var_names.index('CONV_ME_TAP_WGT')])
        ini_c = float(row_data[var_names.index('CHEM05_F_BAP_C_VAL_1')])
        fin_c = float(row_data[var_names.index('F_RH_C')])
        ratio_CO_list = [float(row_data[var_names.index(CO_var)]) for CO_var in CO_var_list] # 배가스 CO 농도
        ratio_CO2_list = [float(row_data[var_names.index(CO2_var)]) for CO2_var in CO2_var_list]  # 배가스 CO2 농도
        gas_flow_list = [float(row_data[var_names.index(gas_flow_var)]) for gas_flow_var in gas_flow_var_list]  # 배가스 유량

        c_input_switch = float(row_data[var_names.index('BEFORE_DEO_C_INPUT_RATIO')])
        c_input_qt = float(row_data[var_names.index('BEFORE_DEO_C_INPUT_QT')])
        c_input_tm = float(row_data[var_names.index('BEFORE_DEO_C_INPUT_TM')])
    except ValueError as e:
        if logger:
            logger.info(e)
        return None
    except TypeError as e:
        if logger:
            logger.info(e)
        return None
    # 단위 변환(ppm -> kg)
    ini_c = (ini_c / pow(10, 2)) * (weight * pow(10, 3))
    fin_c = (fin_c / pow(10, 2)) * (weight * pow(10, 3))

    # 계산 후 그래프를 최종 실적값에 맞추기위해 전처리
    convert_fin_c = fin_c - c_input_qt

    cur_c = ini_c
    remain_c.append(ini_c)

    # 배가스 정보를 통해 용강 내 탄소량 계산
    for i, _ in enumerate(gas_flow_list):
        c = cur_c - (measure_carbon_CO(gas_flow_list[i], ratio_CO_list[i]) + measure_carborn_CO2(gas_flow_list[i], ratio_CO2_list[i]))

        # if c_input_switch:
        #     if i == c_input_tm:
        #         c = c + c_input_qt
        cur_c = c
        remain_c.append(cur_c)

    eval_c = remain_c[-1]
    ground_truth_c = remain_c[:]

    if c_input_switch:
        A = (ini_c - convert_fin_c) / (ini_c - eval_c)
    else:
        A = (ini_c - fin_c) / (ini_c - eval_c)

    for i, _ in enumerate(remain_c):
        if i == 0:
            continue
        ground_truth_c[i] = (ini_c - remain_c[i]) * A * (-1) + ini_c

        if -1 < c_input_tm < i:
            ground_truth_c[i] = ground_truth_c[i] + c_input_qt

    # # Draw a graph
    # x = range(0, 22)
    # plt.plot(x, remain_c, color='black', label='origin graph')
    # # plt.plot(x, convert_c, color='blue', label='scaling graph')
    # plt.plot(x, ground_truth_c, color='red', label='add carbon after scaling')
    # plt.legend(loc='upper right')
    # plt.plot([21], [fin_c], 'ro')
    # plt.xlabel('Time[min]')
    # plt.ylabel('Carbon[kg]')
    # plt.show()
    try:
        return ground_truth_c[minute-1]
    except IndexError:
        return 0


def refine_dataset_by_y_value(feature, dataset, request_time, logger=None):

    if feature.dataset is None:
        feature.dataset = dataset

    if feature.dataset is None:
        return []

    y_name = ''
    for var_name, var_value in feature.vars.items():
        if var_value.dataset_order == 0:
            y_name = var_name

    var_names = feature.dataset[0]
    new_dataset = []
    for row_data in feature.dataset[1:]:
        try:
            y_value = calculate_ground_truth(var_names, row_data, y_name, request_time, logger)
        except ZeroDivisionError:
            y_value = -1
        new_dataset.append([y_value] + row_data)
        feature.vars[Y_VAR].val = y_value
    var_names = [y_name] + var_names
    new_dataset.insert(0, var_names)
    return new_dataset


def run(feature, request_time, dataset=None, logger=None):

    dataset = refine_dataset_by_y_value(feature, dataset, request_time, logger=None)
    feature.dataset = dataset

    return dataset
