import pandas as pd
from handlers import dataset_handler

F_CC_C = 'F_CC_C'
OUT_TUNDISH_CARBON = 'OUT_TUNDISH_CARBON'
TOLERANCE = 3
EST_OUT_TUNDISH_CARBON = 'EST_OUT_TUNDISH_CARBON'

def refine_dataset_by_y_value(dataset, mode=None, logger=None):
    if dataset is None:
        return []

    df = pd.DataFrame(dataset[1:], columns=dataset[0])

    df[F_CC_C] = pd.to_numeric(df[F_CC_C], errors='coerce')
    df[F_CC_C] = df[F_CC_C].astype(float) * 10000

    if mode == dataset_handler.ANALYSIS_MODE:
        df[EST_OUT_TUNDISH_CARBON] = df.OUT_TUNDISH_CARBON.astype(float)

    df[OUT_TUNDISH_CARBON] = df[F_CC_C]

    refined_dataset_header = df.columns.tolist()
    refined_dataset = df.values.tolist()

    return [refined_dataset_header] + refined_dataset

def run(feature, dataset=None, mode=None, logger=None):
    if not dataset:
        dataset = feature.dataset

    feature.dataset = refine_dataset_by_y_value(dataset, mode, logger)
