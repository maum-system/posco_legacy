
obj_name = 'PATTERN_OPERATION'

replacement_dict = {

    'PREC' : 'G_PREC', # 선행가탄

    'PREC+COOL' : 'G_PREC+COOL', # '선행가탄+냉각'
    'PREC+COOL_500' : 'G_PREC+COOL', # '선행가탄+냉각>500'

    'OB' : 'G_OB', # 'OB'
    'OB_50' : 'G_OB', # 'OB>50'

    'OB+COOL' : 'G_OB+COOL', # 'OB+냉각'
    'OB+COOL_500' : 'G_OB+COOL', # 'OB+냉각>500'
    'OB_50+COOL' : 'G_OB+COOL', # 'OB>50+냉각'
    'OB_50+COOL_500' : 'G_OB+COOL', # 'OB>50+냉각>500'

    'Al+OB' : 'G_Al+OB', # 'Al+OB'
    'Al+OB_50' : 'G_Al+OB', # 'Al+OB>50'

    'COOL' : 'G_COOL', # '냉각'
    'COOL_500' : 'G_COOL', # '냉각>500'

    'NOT_OB+NOT_COOL' : 'G_O_TEMP_OK', # '무OB+무냉각'

    # same op pattern
    'G_PREC' : 'G_PREC',
    'G_PREC+COOL' : 'G_PREC+COOL',
    'G_OB' : 'G_OB',
    'G_OB+COOL' : 'G_OB+COOL',
    'G_Al+OB' : 'G_Al+OB',
    'G_COOL' : 'G_COOL',
    'G_O_TEMP_OK' : 'G_O_TEMP_OK'

    # Outlier data
    # '선행가탄+OB':'PREC+OB',
    # '선행가탄+OB>50': 'PREC+OB_50',
    # '선행가탄+냉각+OB': 'PREC+COOL+OB',
    # '선행Al':'PREAl',
    # '선행Al+냉각': 'PREAl+COOL',
    # '선행Al+냉각>500':'PREAl+COOL_500',
    # 'Al+OB+냉각': 'Al+OB+COOL',
    # 'Al+OB+냉각>500':'Al+OB+COOL_500',
    # 'Al+OB>50+냉각':'Al+OB_50+COOL',
    # 'Al+OB>50+냉각>500': 'Al+OB_50+COOL_500',
    # '탈산시OB': 'DEO_OB',
}

pattern_op_num_dict = {
    'G_PREC': 'C',  # 선행가탄
    'G_PREC+COOL': 'C',  # '선행가탄+냉각', '선행가탄+냉각>500'

    'G_OB': 'OB',  # 'OB', 'OB>50'
    'G_OB+COOL': 'OB',  # 'OB+냉각', 'OB+냉각>500', 'OB>50+냉각', 'OB>50+냉각>500'
    'G_Al+OB': 'OB',  # 'Al+OB', 'Al+OB>50'

    'G_COOL': 'OTHER',  # '냉각', '냉각>500'
    'G_O_TEMP_OK': 'OTHER',  # '무OB+무냉각'
}