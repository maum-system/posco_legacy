def refine_dataset_by_y_value(dataset):
    return [] if dataset is None else dataset

def run(feature, dataset=None, mode=None, logger=None):
    if not dataset:
        dataset = feature.dataset

    feature.dataset = refine_dataset_by_y_value(dataset)