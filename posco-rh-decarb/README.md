# posco-component

## Environment
- Python 3.8
- pip install -r requirements.txt


## 주요 실행 코드 위치
* dataset handler: handlers/dataset_handler.py
* learning handler: sys_component/learning_handler.py
* 모델이 생성되는 위치: `sys_component/model/CVT_MACRO_M1/LGBMR/200617/LGBMR.bin`
* SERVER: servers/model_server.py (server_mode로 EST, EVAL 선택)
* Client Test: servers/ongoing_client.py

## 성능 평가
*  선택한 기간 동안의 Acc와 RMSE 계산.
* raw 데이터와 그래프를 파일로 저장
* db_analysis/analyze_handler.py 코드 사용
    * analyze_df.py
        * DataFrame을 이용해 DB에서 가져온 데이터를 원하는 기준에 맞게 선택한다.
        * 모델 별 조건에 맞게, 결측치와 이상치 제외 등등 ...  
    * analyze_plt.py
        * 성능 평가 결과물인 그래프를 그린다.
        * 시간에 따른 Raw data와 Acc의 변화, 실측값과 예측값 비교 그래프 등
    * analyze_utils.py
        * 각종 도구들
* analyze_ppt.py
    * 성능 평가 결과물을 이용해 ppt를 제작한다.
* comp_est_and_eval.py
    * EST 와 EVAL 변수 별 값 비교를 쉽게 할 수 있게 csv 파일을 만들어줌.
    * EST와 EVAL의 성능차이가 많이 날 때 데이터의 경향이 다르지는 않은지 확인할 때 사용.
* input_var_graph.py
    * input으로 사용되는 변수를 선택한 기간만큼 그래프 그려줌
    * input data의 경향이 달라졌는지 확인할 때 사용