#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""This module's docstring summary line.
This is a multi-line docstring. Paragraphs are separated with blank lines.
Lines conform to 79-column limit.
Module and packages names should be short, lower_case_with_underscores.
Notice that this in not PEP8-cheatsheet.py
Seriously, use flake8. Atom.io with https://atom.io/packages/linter-flake8
is awesome!
See http://www.python.org/dev/peps/pep-0008/ for more PEP-8 details
Coding style reference
    - https://www.python.org/dev/peps/pep-0008/
    - http://web.archive.org/web/20111010053227/http://jaynes.colorado.edu
    /PythonGuidelines.html
    WHAT TO DO
    > Normalization before learning.
    > RFR, SVR, DNR
    > apply string features to estimation.
"""
import os
import sys
import argparse
import configparser
from lib import sys_lib
from lib import data as data_lib
import pickle

import handlers.dataset_handler as Dataset
import handlers.learning_handler as Learner

__author__ = "Hoon Paek, Dong-gi Kim, Ji-young Moon"
__copyright__ = "Copyright 2007, The Cogent Project"
__credits__ = []
__license__ = "GPL"
__version__ = "0.0.1"
__maintainer__ = "Hoon Paek"
__email__ = "hoon.paek@mindslab.ai"
__status__ = "Prototype"  # Prototype / Development / Production

LEARNING_TEST = True

DATE_FORMAT = "%y%m%d"

DNN_LIST = ['DNN', 'DNNC']

INT_ENCODING_LIST = ['RFR', 'BAGGING', 'LGBMR', 'XGBR', 'RFC', 'DNNC', 'AdaBoostC']
ONE_HOT_ENCODING_LIST = ['DNN']


def create_ml_model(feature, dataset, general_cfg, ml_cfg):
    Learn = Learner.MachineLearner(Feat=feature, dataset=dataset, out_idx=0)

    today = sys_lib.get_datetime(DATE_FORMAT)

    ml_model = general_cfg['method']
    if ml_model in DNN_LIST:
        Learn.normalize_number_features()

    if ml_model in INT_ENCODING_LIST:
        Learn.encode_symbol_dataset_by_int()
    elif ml_model in ONE_HOT_ENCODING_LIST:
        Learn.encode_symbol_dataset_by_one_hot_encoder()

    Learn.run(general_cfg['method'], ml_cfg)

    dump_data = Learn if ml_model in INT_ENCODING_LIST else {'dataset': dataset, 'params': ml_cfg[ml_model]}

    dump_dir = os.path.join(general_cfg['root_dir'], general_cfg['model_name'], general_cfg['method'], today)
    if not os.path.exists(dump_dir):
        os.makedirs(dump_dir)

    dump_filename = os.path.join(dump_dir, ml_model + general_cfg['bin_extension'])
    pickle.dump(dump_data, open(dump_filename, 'wb'))

    result_file = os.path.join(dump_dir, general_cfg['result_file'])
    sys_lib.print_and_write(Learn.log, console=True, filename=result_file)


def main():
    """ Learning handler main code.
    """

    parser = argparse.ArgumentParser()
    parser.add_argument("--var_csv_file", required=True, help="csv file for variables")
    parser.add_argument("--var_ini_file", required=True, help="ini file for configuration")
    parser.add_argument("--ml_ini_file", default="", help="ini file for machine learner")

    args = parser.parse_args()

    # if args.result_file:
    #     RESULT_FILE = args.result_file
    # with open(RESULT_FILE, 'a') as fid:
    #     fid.write("\n")

    var_cfg = configparser.ConfigParser()
    var_cfg.read(args.var_ini_file)

    ml_cfg = configparser.ConfigParser()

    ml_cfg.read(args.ml_ini_file)
    general_cfg = ml_cfg['general']
    # dataset_cfg = args

    var_mtx = data_lib.read_csv_file(args.var_csv_file)
    start_pos, end_pos = sys_lib.calc_crop_info_from_ini(var_cfg[Dataset.VAR_INFO_CSV])
    roi_var_mtx = data_lib.crop_mtx(var_mtx, start_pos, end_pos)
    feature = Dataset.PoscoTempEstModel(roi_var_mtx)
    feature.init_feat_class(var_cfg, model_number_str=general_cfg['model_number'], offset=start_pos[0])

    dataset = data_lib.read_csv_file(general_cfg['dataset_file'])

    create_ml_model(feature, dataset, general_cfg, ml_cfg)

if __name__ == "__main__":

    if len(sys.argv) == 1:
        if LEARNING_TEST:
            sys.argv.extend([
                             # "--var_csv_file", "../sys_component/cvt_macro_var.csv",
                             # "--var_ini_file", "../sys_component/cvt_macro_var.ini",
                             # "--ml_ini_file", "../sys_component/config_machine_learning/cvt_macro_m1.ini",

                             "--var_csv_file", "../sys_component/rh_macro_var.csv",
                             "--var_ini_file", "../sys_component/rh_macro_var.ini",
                             "--ml_ini_file", "../sys_component/config_machine_learning/rh_macro_m4-7.ini",
                             ])
        else:
            sys.argv.extend(["--~help"])
    main()
