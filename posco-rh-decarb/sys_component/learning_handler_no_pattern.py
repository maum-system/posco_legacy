#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""This module's docstring summary line.
This is a multi-line docstring. Paragraphs are separated with blank lines.
Lines conform to 79-column limit.
Module and packages names should be short, lower_case_with_underscores.
Notice that this in not PEP8-cheatsheet.py
Seriously, use flake8. Atom.io with https://atom.io/packages/linter-flake8
is awesome!
See http://www.python.org/dev/peps/pep-0008/ for more PEP-8 details
Coding style reference
    - https://www.python.org/dev/peps/pep-0008/
    - http://web.archive.org/web/20111010053227/http://jaynes.colorado.edu
    /PythonGuidelines.html
    WHAT TO DO
    > Normalization before learning.
    > RFR, SVR, DNR
    > apply string features to estimation.
"""
import os
import sys
import argparse
import configparser
from lib import sys_lib
from lib import data as data_lib
import pickle
import math
import numpy as np
from dateutil.parser import parse as date_parse
import datetime
import matplotlib.pyplot as plt

import handlers.dataset_handler as Dataset
import handlers.learning_handler as Learner

__author__ = "Hoon Paek, Dong-gi Kim, Ji-young Moon"
__copyright__ = "Copyright 2007, The Cogent Project"
__credits__ = []
__license__ = "GPL"
__version__ = "0.0.1"
__maintainer__ = "Hoon Paek"
__email__ = "hoon.paek@mindslab.ai"
__status__ = "Prototype"  # Prototype / Development / Production

LEARNING_TEST = True

DATE_FORMAT = "%y%m%d"

DNN_LIST = ['DNN', 'DNNC']

INT_ENCODING_LIST = ['RFR', 'BAGGING', 'LGBMR', 'XGBR', 'RFC', 'DNNC', 'AdaBoostC', 'XGBC', 'LGBMC', 'Lasso']
ONE_HOT_ENCODING_LIST = ['DNN']


def create_ml_model(feature, dataset, general_cfg, ml_cfg, model_save_path=None, num_train=None):
    Learn = Learner.MachineLearner(Feat=feature, dataset=dataset, out_idx=0)

    today = sys_lib.get_datetime(DATE_FORMAT)

    ml_model = general_cfg['method']
    if ml_model in DNN_LIST:
        Learn.normalize_number_features()

    if ml_model in INT_ENCODING_LIST:
        Learn.encode_symbol_dataset_by_int()
    elif ml_model in ONE_HOT_ENCODING_LIST:
        Learn.encode_symbol_dataset_by_one_hot_encoder()

    dump_dir = os.path.join(general_cfg['root_dir'], general_cfg['model_name'], general_cfg['method'], '000711')
    if not os.path.exists(dump_dir):
        os.makedirs(dump_dir)
    Learn.run(general_cfg['method'], ml_cfg, dump_dir, num_train=num_train)

    dump_data = Learn if ml_model in INT_ENCODING_LIST else {'dataset': dataset, 'params': ml_cfg[ml_model]}

    if not os.path.exists(dump_dir):
        os.makedirs(dump_dir)

    if not model_save_path:
        model_save_path = os.path.join(dump_dir, ml_model + general_cfg['bin_extension'])

    pickle.dump(dump_data, open(model_save_path, 'wb'))

    result_file = os.path.join(dump_dir, general_cfg['result_file'])
    sys_lib.print_and_write(Learn.log, console=True, filename=result_file)

    train_acc_list = []
    test_acc_list = []
    train_rmse_list = []
    test_rmse_list = []

    if isinstance(Learn.train_acc, list):
        train_acc_list.append(Learn.train_acc[0])
        train_acc_list.append(Learn.train_acc[1])
        test_acc_list.append(Learn.test_acc[0])
        test_acc_list.append(Learn.test_acc[1])
        train_rmse_list.append(Learn.train_rmse[0])
        train_rmse_list.append(Learn.train_rmse[1])
        test_rmse_list.append(Learn.test_rmse[0])
        test_rmse_list.append(Learn.test_rmse[1])

    else:
        train_acc_list.append(Learn.train_acc)
        test_acc_list.append(Learn.test_acc)
        train_rmse_list.append(Learn.train_rmse)
        test_rmse_list.append(Learn.test_rmse)

    return Learn.y_name, train_acc_list, test_acc_list, train_rmse_list, test_rmse_list, Learn


def make_sliding_window_dataset(dataset, traindata_size, testdata_size, step_size):

    train_dataset_list = []
    test_dataset_list = []
    col_names = dataset[0]
    for y in range(1, len(dataset)-traindata_size-testdata_size-1, step_size):
        train_dataset = [col_names] + dataset[y:traindata_size+y]
        test_dataset = [col_names] + dataset[traindata_size+y:traindata_size+y+testdata_size]
        train_dataset_list.append(train_dataset)
        test_dataset_list.append(test_dataset)
    return train_dataset_list, test_dataset_list


def make_sliding_window_dataset_by_days(dataset, traindata_size, testdata_size, step_size,
                                        date_var='TAP_WORK_DATE', fix_start_day=False):

    train_dataset_list = []
    test_dataset_list = []
    col_names = dataset[0]
    dataset = dataset[1:]
    date_idx = col_names.index(date_var)
    initial_idx = 0
    idx = initial_idx
    start_date = date_parse(dataset[0][date_idx])

    train_max_date = start_date + datetime.timedelta(days=traindata_size - 1)
    test_max_date = train_max_date + datetime.timedelta(days=testdata_size)

    sub_train_dataset_list = [col_names]
    sub_test_dataset_list = [col_names]
    found_next_step_idx = False
    next_step_idx = 0
    while idx < len(dataset):

        cur_date = date_parse(dataset[idx][date_idx])

        if not found_next_step_idx and (cur_date - start_date).days >= step_size:
            next_step_idx = idx
            found_next_step_idx = True

        if cur_date <= train_max_date:
            sub_train_dataset_list.append(dataset[idx])
        elif cur_date <= test_max_date:
            sub_test_dataset_list.append(dataset[idx])

        else:
            train_dataset_list.append(sub_train_dataset_list)
            test_dataset_list.append(sub_test_dataset_list)
            sub_train_dataset_list = [col_names]
            sub_test_dataset_list = [col_names]
            train_max_date = date_parse(dataset[idx][date_idx]) + datetime.timedelta(days=step_size - testdata_size - 1)
            test_max_date = train_max_date + datetime.timedelta(days=testdata_size)

            found_next_step_idx = False
            if fix_start_day:
                idx = initial_idx
            else:
                idx = next_step_idx
            start_date = date_parse(dataset[idx][date_idx])

        idx += 1
        if idx >= len(dataset):
            train_dataset_list.append(sub_train_dataset_list)
            test_dataset_list.append(sub_test_dataset_list)
            break

    return train_dataset_list, test_dataset_list


def sliding_window(train_dataset_list, test_dataset_list, args):
    msg = ''
    pred_y_list = []
    real_y_list = []
    diff_list = []
    diff_power_list = []

    var_cfg = configparser.ConfigParser()
    var_cfg.read(args.var_ini_file)

    ml_cfg = configparser.ConfigParser()
    ml_cfg.read(args.ml_ini_file)
    general_cfg = ml_cfg['general']
    method = general_cfg['method']
    ml_model = ml_cfg[method]
    est_thresh = ml_model['est_thresh']

    var_mtx = data_lib.read_csv_file(args.var_csv_file)
    start_pos, end_pos = sys_lib.calc_crop_info_from_ini(var_cfg[Dataset.VAR_INFO_CSV])
    roi_var_mtx = data_lib.crop_mtx(var_mtx, start_pos, end_pos)
    feature = Dataset.PoscoTempEstModel(roi_var_mtx)
    feature.init_feat_class(var_cfg, model_number_str=general_cfg['model_number'], offset=start_pos[0])

    for idx, dataset in enumerate(train_dataset_list):
        y_name, train_acc_list, test_acc_list, train_rmse_list, test_rmse_list, Learn \
            = create_ml_model(feature, dataset, general_cfg, ml_cfg)
        for i, test_dataset in enumerate(test_dataset_list[idx][1:]):
            pred_y = Learn.model.predict(np.array([test_dataset[2:]]).astype('float32'))
            pred_y_list.append(pred_y)
            real_y = test_dataset[0]
            real_y_list.append(real_y)

    with open(os.path.join(general_cfg['sliding_window_result'], 'sliding_window_result.csv'), 'w') as new_file:
        new_file.write('Real_y,Pred_y,Diff\n')
        for i in range(len(pred_y_list)):
            new_file.write('{},'.format(real_y_list[i]))
            new_file.write('{},'.format(pred_y_list[i][0]))
            diff = abs(float(real_y_list[i]) - float(pred_y_list[i][0]))
            diff_list.append(diff)
            new_file.write('{}\n'.format(diff))

    cnt = 0
    for val in diff_list:
        if val < float(est_thresh):
            cnt += 1

    for diff in diff_list:
        diff_power = diff**2
        diff_power_list.append(diff_power)

    sliding_window_accuracy = round((cnt / len(diff_list)) * 100, 2)
    sliding_window_RMSE = math.sqrt(np.mean(diff_power_list))
    msg += 'sliding_window_accuracy : {} %\n'.format(sliding_window_accuracy)
    msg += 'sliding_window_RMSE : {}\n'.format(sliding_window_RMSE)

    return msg


def main():
    """ Learning handler main code.
    """

    parser = argparse.ArgumentParser()
    parser.add_argument("--var_csv_file", required=True, help="csv file for variables")
    parser.add_argument("--var_ini_file", required=True, help="ini file for configuration")
    parser.add_argument("--ml_ini_file", default="", help="ini file for machine learner")
    parser.add_argument("--sliding_window_", default=False, action='store_true', help="activating sliding window")

    args = parser.parse_args()

    var_cfg = configparser.ConfigParser()
    var_cfg.read(args.var_ini_file)

    ml_cfg = configparser.ConfigParser()
    ml_cfg.read(args.ml_ini_file)
    general_cfg = ml_cfg['general']

    var_mtx = data_lib.read_csv_file(args.var_csv_file)
    start_pos, end_pos = sys_lib.calc_crop_info_from_ini(var_cfg[Dataset.VAR_INFO_CSV])
    roi_var_mtx = data_lib.crop_mtx(var_mtx, start_pos, end_pos)
    feature = Dataset.PoscoTempEstModel(roi_var_mtx)
    feature.init_feat_class(var_cfg, model_number_str=general_cfg['model_number'], offset=start_pos[0])

    dataset = data_lib.read_csv_file(general_cfg['dataset_file'])

    if args.sliding_window_:

        traindata_size = 7
        testdata_size = 1
        step_size = 1
        train_dataset_list, test_dataset_list = make_sliding_window_dataset_by_days(dataset,
                                                                                    traindata_size=traindata_size,
                                                                                    testdata_size=testdata_size,
                                                                                    step_size=step_size,
                                                                                    fix_start_day=True)
        msg = sliding_window(train_dataset_list, test_dataset_list, args)
        print(msg)



    msg = "\n"
    y_names = ''
    train_acc_avg = []
    test_acc_avg = []
    train_rmse_avg = []
    test_rmse_avg = []
    train_acc_list_per_iter = []
    test_acc_list_per_iter = []
    train_rmse_list_per_iter = []
    test_rmse_list_per_iter = []

    for i in range(int(general_cfg['iter_num'])):
        y_name, train_acc_list, test_acc_list, train_rmse_list, test_rmse_list, Learn = create_ml_model(feature, dataset
                                                                                                 , general_cfg, ml_cfg)
        train_acc_list_per_iter.append(train_acc_list)
        test_acc_list_per_iter.append(test_acc_list)
        train_rmse_list_per_iter.append(train_rmse_list)
        test_rmse_list_per_iter.append(test_rmse_list)
        y_names = y_name

    transpose_train_acc_list = data_lib.transpose_list(train_acc_list_per_iter)
    transpose_test_acc_list = data_lib.transpose_list(test_acc_list_per_iter)
    transpose_train_rmse_list = data_lib.transpose_list(train_rmse_list_per_iter)
    transpose_test_rmse_list = data_lib.transpose_list(test_rmse_list_per_iter)

    for i in range(len(transpose_train_acc_list)):
        cal_train_acc_avg = sum(transpose_train_acc_list[i]) / len(transpose_train_acc_list[i])
        cal_test_acc_avg = sum(transpose_test_acc_list[i]) / len(transpose_test_acc_list[i])
        cal_train_mse_avg = sum(transpose_train_rmse_list[i]) / len(transpose_train_rmse_list[i])
        cal_test_mse_avg = sum(transpose_test_rmse_list[i]) / len(transpose_test_rmse_list[i])
        train_acc_avg.append(cal_train_acc_avg)
        test_acc_avg.append(cal_test_acc_avg)
        train_rmse_avg.append(cal_train_mse_avg)
        test_rmse_avg.append(cal_test_mse_avg)

    for i in range(len(train_acc_avg)):
        msg += "\n # type : {}, train acc average  : {:4.2f}, test acc average  : {:4.2f}".format(y_names[i]
                                                                                                  , train_acc_avg[i]
                                                                                                  , test_acc_avg[i])
        msg += "\n #type  : {}, train rmse average : {:4.8f}, test rmse average : {:4.8f}".format(y_names[i],
                                                                                                  train_rmse_avg[i],
                                                                                                  test_rmse_avg[i])
    print(msg)
    # Apppend Model EST_VALUE to total dataset
    if False:
        if int(ml_cfg['general']['output_num']) == 1:
            y_dataset = np.concatenate((Learn.y[0], Learn.y[1]),axis=0).reshape(-1, 1)
            y_pred_dataset = np.concatenate((Learn.y_predict[0], Learn.y_predict[1]), axis=0).reshape(-1, 1)
            tot_header = [Learn.y_name] + ['EST_' + Learn.y_name] + Learn.X_name
        else:
            y_dataset = np.concatenate((Learn.y[0], Learn.y[1]), axis=0)
            y_pred_dataset = np.concatenate((Learn.y_predict[0], Learn.y_predict[1]), axis=0)
            tot_header = Learn.y_name + ['EST_'+Learn.y_name[0], 'EST_'+Learn.y_name[1]] + Learn.X_name

        X_dataset = np.concatenate((Learn.X[0], Learn.X[1]), axis=0)
        tot_y_dataset = np.concatenate((y_dataset, y_pred_dataset), axis=1)
        tot_dataset = np.concatenate(([tot_y_dataset, X_dataset]), axis=1)

        res_dataset = [tot_header] + tot_dataset.tolist()
        data_lib.write_list_to_csv(res_dataset, 'result_'+ml_cfg['general']['model_name']+'.csv')
    pass


if __name__ == "__main__":

    if len(sys.argv) == 1:
        if LEARNING_TEST:
            sys.argv.extend([

                             # "--var_csv_file", "../sys_component/rh_macro_var_no_pattern.csv",
                             # "--var_ini_file", "../sys_component/rh_macro_var_no_pattern.ini",
                             # "--ml_ini_file", "../sys_component/config_machine_learning_no_pattern/rh_macro_m4-7-C.ini",

                             "--var_csv_file", "../sys_component/rh_macro_var.csv",
                             "--var_ini_file", "../sys_component/rh_macro_var.ini",
                             "--ml_ini_file", "../sys_component/config_machine_learning/rh_macro_m4-7-C.ini",

                             # "--sliding_window_"
                             ])
        else:
            sys.argv.extend(["--~help"])
    main()
