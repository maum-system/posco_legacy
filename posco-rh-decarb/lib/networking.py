import sys
import time
import socket

from lib import sys_lib as system_lib
from lib import data as data_lib
import json
import time


def accept(sock, logger=None):
    print("Waiting for a connection...")
    connection, client_address = sock.accept()
    # logger.info("Connection for {}...".format(client_address))

    str_dat = b''
    try:
        while True:
            buf_str = connection.recv(1024)
            # buf_str = str(connection.recv(1024), encoding='utf-8')
            if len(buf_str) == 0:
                break
            str_dat += buf_str
        # str_dat = system_lib.recv_all(connection, logger=logger).decode('utf-8')
        print(str_dat)
        if data_lib.is_json(str(str_dat)):
            json_dat = json.loads(str_dat)['cmd']
            return True, json_dat, client_address
        # logger.info("SOCK_REQ: {}".format(str_dat))
    except Exception as e:
        print(e)
        # logger.error(e)
    finally:
        connection.close()

    try:
        str_dat = str_dat.decode('utf-8')
    except UnicodeDecodeError:
        str_dat = 'UnicodeError'
    return True, str_dat, client_address


def run_server(server_ip, server_port, mode, model_num, desc):
    print("Running Server...")
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_address = (server_ip, server_port)
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    sock.bind(server_address)
    sock.listen(10)

    connected = True

    while connected:
        connected, data, client_address = accept(sock)
        if data == 'check':
            print(desc + ": Test succeeded")

        result = system_lib.get_ml_result(mode, model_num, data)

        sock_response = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            time.sleep(1000)
            sock_response.connect(client_address)
        except ConnectionRefusedError:
            print(" @ ConnectionRefusedError")
            sys.exit()
        try:
            print(" EstClient> sending \"{}\"".format(result))
            sock_response.sendall(result.encode('utf-8'))
        finally:
            print(" EstClient> closing socket...")
            sock_response.close()
