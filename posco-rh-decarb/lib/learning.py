import os
import pickle
import random

import numpy as np
import tensorflow as tf
import pandas as pd
from pandas.api.types import is_string_dtype, is_numeric_dtype

from lib import data as data_lib
from handlers import dataset_handler as dataset_handler
from handlers import learning_handler as learn_handler


def split_dataset_into_train_and_test(dataset, percent_ratio, offset=1, mode='random', num_train=None):
    """

    :param dataset:
    :param percent_ratio:
    :param offset:
    :param mode: random, seq
    :param num_train:
    :return:
    """
    train_dataset = []
    test_dataset = []
    for idx in range(offset):
        train_dataset.append(dataset[idx])
        test_dataset.append(dataset[idx])
    cnt_train = 0
    cnt_test = 0
    if mode == 'random':
        for idx in range(offset, len(dataset)):
            if random.randint(0, 100) <= percent_ratio:
                train_dataset.append(dataset[idx])
                cnt_train += 1
            else:
                test_dataset.append(dataset[idx])
                cnt_test += 1
    elif mode == 'seq':
        for idx in range(offset, len(dataset)):
            if idx <= num_train:
                train_dataset.append(dataset[idx])
                cnt_train += 1
            else:
                test_dataset.append(dataset[idx])
                cnt_test += 1
    # print("\n # data set division: ({:d}, {:d}) -> {:2d}".format(
    # cnt_train, cnt_test, int(cnt_train * 100. / float(cnt_train + cnt_test))))
    return train_dataset, test_dataset


def normalize_list(stream_list, minmax_list, min_val=0, max_val=1):
    minmax_dict = {}
    for i in range(len(minmax_list[0])):
        minmax_dict[minmax_list[0][i]] = (minmax_list[1][i], minmax_list[2][i])

    for idx in range(len(stream_list)):

        val = stream_list[idx][1]

        if isinstance(val, (int, float)):  # if number

            val = float(val)

            var_name = stream_list[idx][0]
            minmax = minmax_dict[var_name]
            max_thresh = minmax[1]
            min_thresh = minmax[0]

            val_range = float(max_thresh - min_thresh)

            if val > max_thresh:
                val = max_thresh
            elif val < min_thresh:
                val = min_thresh
            elif val_range != 0:
                val = (val - min_thresh) / val_range * (max_val - min_val) + min_val

        stream_list[idx][1] = val


def denormalize_output(predicted_output, real_max, real_min, norm_min=-1, norm_max=1):
    return (predicted_output - norm_min) / (norm_max - norm_min) * (real_max - real_min) + real_min


def MinMaxScaler(data):
    numerator = data - np.min(data)
    denominator = np.max(data) - np.min(data)
    return numerator / (denominator + 1e-7)


def get_optimizer(learn_rate, optimizer):

    if optimizer == "adadelta":
        dnn_optimizer = tf.train.AdadeltaOptimizer(learning_rate=learn_rate)
    elif optimizer == "adam":
        dnn_optimizer = tf.train.AdamOptimizer(learning_rate=learn_rate)
    elif optimizer == "ftrl":
        dnn_optimizer = tf.train.FtrlOptimizer(learning_rate=learn_rate)
    elif optimizer == "gd":
        dnn_optimizer = tf.train.GradientDescentOptimizer(learning_rate=learn_rate)
    elif optimizer == "proximalGrad":
        dnn_optimizer = tf.train.ProximalGradientDescentOptimizer(learning_rate=learn_rate)
    elif optimizer == "proximalAdagrad":
        dnn_optimizer = tf.train.ProximalAdagradOptimizer(learning_rate=learn_rate)
    elif optimizer == "RMSProp":
        dnn_optimizer = tf.train.RMSPropOptimizer(learning_rate=learn_rate)
    else:
        dnn_optimizer = tf.train.AdagradOptimizer(learning_rate=learn_rate)

    return dnn_optimizer


def dnn_estimation(model_dir, stream_dict,
                   source_var='', target_var='',
                   Feat=None, logger=None, dimension=None, category=''):
    """ Run DNN estimation.

    :param model_dir:
    :param stream_dict:
    :param filename:
    :param Feat:
    :param logger:
    :param dimension:
    :return:
    """

    reg_args = pickle.load(open(os.path.join(model_dir, 'DNN.bin'), 'rb'))
    msg = 'DNN operation'
    logger.info(msg) if logger else print(msg)

    dataset = reg_args['dataset'] # original code
    params = reg_args['params']
    # dataset = reg_args.dataset # 181029 modified
    # params = reg_args.params # 181029 modified
    name_list = []
    val_list = []
    for var_name in dataset[0]:  # dataset[0] contains variable names
        try:
            val = stream_dict[target_var] if (category == 'CVT' or category == 'RH') and var_name == source_var else stream_dict[var_name]
            name_list.append(var_name)
            val_list.append(val)
        except KeyError as e:
            print(e)
    target_dataset = [name_list, val_list]

    Learn = learn_handler.MachineLearner(Feat=Feat, dataset=target_dataset)
    Learn.normalize_number_features()
    Learn.encode_symbol_dataset_by_one_hot_encoder()

    target_dataset = Learn.dataset[1]
    target_dataset = np.array([target_dataset[1:]])
    # target_dataset.shape = (1, len(target_dataset))

    dnn_feat_col = [tf.contrib.layers.real_valued_column("", dimension=len(target_dataset[0]))]

    dnn_unit = params['unit'].split(',')
    dnn_unit = [int(u) for u in dnn_unit]

    if params['active_fn'] == "relu":
        dnn_active_fn = tf.nn.relu
    elif params['active_fn'] == "sigmoid":
        dnn_active_fn = tf.sigmoid
    else:
        dnn_active_fn = tf.tanh

    dnn_optimizer = get_optimizer(float(params['learn_rate']), params['optimizer'])

    dnn_dropout = None
    if params['dropout'] != 'None':
        dnn_dropout = float(params['dropout'])

    config_proto = tf.ConfigProto(allow_soft_placement=True, log_device_placement=True)
    config_proto.gpu_options.allow_growth = False
    if dimension == 2:
        reg = tf.contrib.learn.DNNRegressor(feature_columns=dnn_feat_col,
                                            activation_fn=dnn_active_fn,
                                            hidden_units=dnn_unit,
                                            model_dir=model_dir + "/dnn_model",
                                            optimizer=dnn_optimizer, dropout=dnn_dropout,
                                            label_dimension=dimension,
                                            config=tf.contrib.learn.RunConfig(session_config=config_proto))

        predict_arr = reg.predict(target_dataset)
        predict_arr = np.array(list(predict_arr))

        result_time = denormalize_output(predict_arr[:, 0],
                                         Feat.vars[dataset[0][0]].max_thresh,
                                         Feat.vars[dataset[0][0]].min_thresh)
        result_temp = denormalize_output(predict_arr[:, 0],
                                         Feat.vars[dataset[0][1]].max_thresh,
                                         Feat.vars[dataset[0][1]].min_thresh)

        print("DNN estimation: time - " + str(result_time[0]) + ", temp - " + str(result_temp[0]))

        return result_time[0], result_temp[0]
    else:
        reg = tf.contrib.learn.DNNRegressor(feature_columns=dnn_feat_col,
                                            activation_fn=dnn_active_fn,
                                            hidden_units=dnn_unit,
                                            model_dir=os.path.join(model_dir, params['model_dir']),
                                            optimizer=dnn_optimizer, dropout=dnn_dropout)

        predict_arr = reg.predict(target_dataset)
        predict_arr = np.array(list(predict_arr))

        result = denormalize_output(predict_arr,
                                    Feat.vars[dataset[0][0]].max_thresh,
                                    Feat.vars[dataset[0][0]].min_thresh)
        print("DNN estimation: " + str(result[0]))
        return result[0]


def rfr_estimation(model_dir, stream_dict, source_var='', target_var='', logger=None, category=''):
    msg = 'RFR estimation operation with {}'.format(model_dir)
    logger.info(msg) if logger else print(msg)
    model = pickle.load(open(os.path.join(model_dir, 'RFR.bin'), 'rb'))
    inputs = []
    for key in model.X_name:
        try:
            if (category == 'CVT' or category == 'RH') and key == source_var:
                inputs.append([source_var, stream_dict[target_var]])
            else:
                inputs.append([key, stream_dict[key]])
        except ValueError as e:
            logger.error(e) if logger else print(e)
        except KeyError as e:
            logger.error(e) if logger else print(e)
    inputs = data_lib.transpose_list(inputs)
    model.encode_symbol_dataset_by_int(dataset=inputs)
    try:
        return model.model.predict([model.dataset[1]])[0] ## model to ml_model
    except ValueError as e:
        logger.error(e) if logger else print(e)
        return -1

def rfc_estimation(model_dir, stream_dict, source_var='', target_var='', logger=None, category=''):
    msg = 'RFC estimation operation with {}'.format(model_dir)
    logger.info(msg) if logger else print(msg)
    model = pickle.load(open(os.path.join(model_dir, 'RFC.bin'), 'rb'))
    inputs = []
    for key in model.X_name:
        try:
            if (category == 'CVT' or category == 'RH') and key == source_var:
                inputs.append([source_var, stream_dict[target_var]])
            else:
                inputs.append([key, stream_dict[key]])
        except ValueError as e:
            logger.info(e) if logger else print(e)
        except KeyError as e:
            logger.info(e) if logger else print(e)
    inputs = data_lib.transpose_list(inputs)
    model.encode_symbol_dataset_by_int(dataset=inputs)
    model_output = model.model.predict([model.dataset[1]])[0]  ## model to ml_model
    model.decode_symbol_dataset_by_str_domain(dataset=[[model.y_name], [model_output]], target_var=model.y_name)
    return model.dataset[1][0]


def lgbmc_estimation(model_dir, stream_dict, source_var='', target_var='', logger=None, category=''):
    msg = 'LGBMC estimation operation with {}'.format(model_dir)
    logger.info(msg) if logger else print(msg)
    model = pickle.load(open(os.path.join(model_dir, 'LGBMC.bin'), 'rb'))
    inputs = []
    for key in model.X_name:
        try:
            if (category == 'CVT' or category == 'RH') and key == source_var:
                inputs.append([source_var, stream_dict[target_var]])
            else:
                inputs.append([key, stream_dict[key]])
        except ValueError as e:
            logger.info(e) if logger else print(e)
        except KeyError as e:
            logger.info(e) if logger else print(e)
    inputs = data_lib.transpose_list(inputs)
    model.encode_symbol_dataset_by_int(dataset=inputs)
    model_output = model.model.predict([model.dataset[1]])[0]  ## model to ml_model
    model.decode_symbol_dataset_by_str_domain(dataset=[[model.y_name], [model_output]], target_var=model.y_name)
    return model.dataset[1][0]


def lgbmr_estimation(model_dir, stream_dict, file_name = 'LGBMR.bin', source_var='', target_var='', logger=None, process_type=None, category=''):
    msg = 'LGBMR estimation operation with {}'.format(model_dir)
    logger.info(msg) if logger else print(msg)

    if category == 'CVT' and process_type == 'L':
        file_name = 'LGBMR_L.bin'
        model = pickle.load(open(os.path.join(model_dir, file_name), 'rb'))
    elif category == 'CVT' and process_type == 'H':
        file_name = 'LGBMR_H.bin'
        model = pickle.load(open(os.path.join(model_dir, file_name), 'rb'))
    else:
        model = pickle.load(open(os.path.join(model_dir, file_name), 'rb'))
    inputs = []
    for key in model.X_name:
        try:
            if (category == 'CVT' or category == 'RH') and key == source_var:
                inputs.append([source_var, stream_dict[target_var]])
            else:
                inputs.append([key, stream_dict[key]])
        except ValueError as e:
            logger.info(e) if logger else print(e)
        except KeyError as e:
            logger.info(e) if logger else print(e)
    inputs = data_lib.transpose_list(inputs)
    model.encode_symbol_dataset_by_int(dataset=inputs)
    try:
        return model.model.predict([model.dataset[1]])[0] ## model to ml_model
    except ValueError as e:
        return 1

def xgbr_estimation(model_dir, stream_dict, file_name='XGBR.bin', source_var='', target_var='', logger=None, process_type=None, category=''):
    msg = 'XGBR estimation operation with {}'.format(model_dir)
    logger.info(msg) if logger else print(msg)

    if category == 'CVT' and process_type == 'L':
        file_name = 'XGBR_L.bin'
        model = pickle.load(open(os.path.join(model_dir, file_name), 'rb'))
    elif category == 'CVT' and process_type == 'H':
        file_name = 'XGBR_H.bin'
        model = pickle.load(open(os.path.join(model_dir, file_name), 'rb'))
    else:
        model = pickle.load(open(os.path.join(model_dir, file_name), 'rb'))
    inputs = []
    for key in model.X_name:
        try:
            if (category == 'CVT' or category == 'RH') and key == source_var:
                inputs.append([source_var, stream_dict[target_var]])
            else:
                inputs.append([key, stream_dict[key]])
        except ValueError as e:
            logger.info(e) if logger else print(e)
        except KeyError as e:
            logger.info(e) if logger else print(e)
    inputs = data_lib.transpose_list(inputs)
    model.encode_symbol_dataset_by_int(dataset=inputs)
    return model.model.predict([model.dataset[1]])[0] ## model to ml_model

def calc_MAPE(actual_arr, estimate_arr):
    """ calculate Mean Absolute Percentage Error.

    :param actual_arr:
    :param estimate_arr:
    :return:
    """

    arr = []
    for i in range(len(actual_arr)):
        if actual_arr[i] == 0:
            arr.append(0)
        else:
            arr.append(abs((actual_arr[i] - estimate_arr[i]) / float(actual_arr[i])))

    return 100. * sum(arr) / float(len(arr))


def calc_RMSE(actual_arr, estimate_arr=None):
    """ calculate Root Mean Square Error.
    If the second argument is NOT defined,
    it is assumed that the first argument is error value instead of actual value.

    :param actual_arr:
    :param estimate_arr:
    :return:
    """

    if estimate_arr is not None:
        arr = []
        for i in range(len(actual_arr)):
            arr.append((actual_arr[i] - estimate_arr[i]) * (actual_arr[i] - estimate_arr[i]))
        return np.sqrt(sum(arr) / float(len(arr)))
    else:
        arr = []
        for i in range(len(actual_arr)):
            arr.append(actual_arr[i] * actual_arr[i])
        return np.sqrt(sum(arr) / float(len(arr)))

def calc_RMSRE(actual_arr, estimate_arr=None):
    """ calculate Root Mean Square Ratio Error.
    If the second argument is NOT defined,
    it is assumed that the first argument is error value instead of actual value.
    :param actual_arr:
    :param estimate_arr:
    :return:
    """

    if estimate_arr is not None:
        arr = []
        for i in range(len(actual_arr)):
            arr.append(((actual_arr[i] - estimate_arr[i]) / actual_arr[i]) ** 2)
        return np.sqrt(sum(arr) / float(len(arr)))
    else:
        arr = []
        for i in range(len(actual_arr)):
            arr.append((actual_arr[i] / actual_arr[i]) * (actual_arr[i] / actual_arr[i]))
        return np.sqrt(sum(arr) / float(len(arr)))


def check_performance_measure(est_val, real_val, tolerance, data_frame=None, logger=None, ys_test = False):
    """

    :param cat:
    :param data_frame:
    :param logger:
    """
    pd.options.mode.chained_assignment = None  # default='warn'

    # if is_string_dtype(data_frame[est_val]) and is_string_dtype(data_frame[real_val]):
    data_frame[est_val] = pd.to_numeric(data_frame[est_val], errors='coerce').astype(float)
    data_frame[real_val] = pd.to_numeric(data_frame[real_val], errors='coerce').astype(float)

    data_frame['EST_REAL_DIFF'] = data_frame[est_val] - data_frame[real_val]
    if ys_test:
        data_frame['EST_REAL_DIFF_POW'] = data_frame['EST_REAL_DIFF'] * data_frame['EST_REAL_DIFF']
    model_norm_range_cond = np.logical_and(-tolerance <= data_frame['EST_REAL_DIFF'],
                                           data_frame['EST_REAL_DIFF'] <= tolerance)

    if ys_test:
        data_frame['model_norm_range_cond'] = model_norm_range_cond
    model_est_accuracy = model_norm_range_cond.sum() / len(model_norm_range_cond) * 100
    model_est_rmse = calc_RMSE(data_frame[est_val].tolist(), data_frame[real_val].tolist())

    msg = ""
    msg += "\n # Check the model's performance measurement results, tolerance : {:4.8f}".format(tolerance)
    msg += "\n # Model acc : {:4.2f} %".format(model_est_accuracy)
    msg += "\n # Model RMSE : {:4.4f}".format(model_est_rmse)
    if logger:
        logger.info(msg)
    print(msg)

    return model_est_accuracy, model_est_rmse


def calculate_ml_output(est_dict, ml_ini, feat_handler, logger=None, server_mode='EVAL'):
    category = ml_ini['general']['model_name']
    ml_method = ml_ini['general']['method']
    model_dir = feat_handler.vars[server_mode + '_MODEL_VER'].val
    process_type = feat_handler.check_process_type

    output = None

    if ml_method == 'DNN':
        output = dnn_estimation(model_dir,
                                est_dict,
                                feat_handler,
                                logger=logger,
                                category=category)
    elif ml_method == 'RFR':
        output = rfr_estimation(model_dir,
                                est_dict,
                                logger=logger,
                                category=category)
    elif ml_method == 'RFC':
        output = rfc_estimation(model_dir,
                                est_dict,
                                logger=logger,
                                category=category)
    elif ml_method == 'XGBR':
        output = xgbr_estimation(model_dir,
                                  est_dict,
                                  logger=logger,
                                  process_type=process_type,
                                  category=category)
    elif ml_method == 'LGBMR':
        output = lgbmr_estimation(model_dir,
                                  est_dict,
                                  logger=logger,
                                  process_type=process_type,
                                  category=category)
    elif ml_method == 'LGBMC':
        output = lgbmc_estimation(model_dir,
                                  est_dict,
                                  logger=logger,
                                  category=category)

    if type(output) is np.ndarray:
        output = output.tolist()
    else:
        output = [output]

    vars_dict = {}
    for key, value in feat_handler.vars.items():
        if value.enum_idx >= 700 and value.dataset_order == 0:
            if len(output) > 1:
                vars_dict[value.priority] = key
            else:
                vars_dict[0] = key
                break

    for idx in range(len(output)):
        post_fix = vars_dict[idx].split('_', 1)[-1]
        est_eval_var = server_mode + '_' + post_fix
        feat_handler.vars[vars_dict[idx]].val = output[idx]
        feat_handler.vars[est_eval_var].val = output[idx]
        if isinstance(output[idx], str):
            logger.info("Estimation result = {} : {}, {} : {}".format(vars_dict[idx], feat_handler.vars[vars_dict[idx]].val,
                                                                      est_eval_var, feat_handler.vars[est_eval_var].val))
        else:
            print("est_eval_var : " + str(est_eval_var))
            print("est_eval_var value : " + str(feat_handler.vars[est_eval_var].val))
            logger.info("Estimation result = {} : {:5.2f}, {} : {:7.2f}".format(vars_dict[idx], feat_handler.vars[vars_dict[idx]].val,
                                                                                est_eval_var, feat_handler.vars[est_eval_var].val))

    return output

def calculate_ml_output_test(est_dict, ml_ini, model_dir, logger=None):
    category = ml_ini['general']['model_name']
    ml_method = ml_ini['general']['method']

    output = None

    if ml_method == 'RFR':
        output = rfr_estimation(model_dir,
                                est_dict,
                                logger=logger,
                                category=category)
    elif ml_method == 'RFC':
        output = rfc_estimation(model_dir,
                                est_dict,
                                logger=logger,
                                category=category)
    elif ml_method == 'XGBR':
        output = xgbr_estimation(model_dir,
                                  est_dict,
                                  logger=logger,
                                  category=category)
    elif ml_method == 'LGBMR':
        output = lgbmr_estimation(model_dir,
                                  est_dict,
                                  logger=logger,
                                  category=category)
    elif ml_method == 'LGBMC':
        output = lgbmc_estimation(model_dir,
                                  est_dict,
                                  logger=logger,
                                  category=category)

    if type(output) is np.ndarray:
        output = output.tolist()
    else:
        output = [output]

    return output