#! /usr/bin/env python
# -*- coding: utf-8 -*-
import mssql.DDL.query
import mssql.DML.query


class PyMssqlWrapper:

    def __init__(self, server_name="", port="", username="", password="", db_name="", table_name="", pk_name="no", logger=None):
        self.ddl_query = mssql.DDL.query.DdlQuery(server_name=server_name, port=port,
                                                   username=username, password=password,
                                                   db_name=db_name, logger=logger, pk_name=pk_name)
        self.dml_query = mssql.DML.query.DmlQuery(server_name=server_name, port=port,
                                                   username=username, password=password,
                                                   db_name=db_name, logger=logger, pk_name=pk_name)
        self.table_name = table_name

    def selectTables(self):
        return self.dml_query.selectTables()

    def dropTable(self, table_name):
        return self.ddl_query.dropTable(table_name)

    def createTable(self, table_name, cvs_file):
        return self.ddl_query.createTable(table_name, cvs_file)

    def insertAll(self, cvs_file, table_name=""):
        if not table_name:
            table_name = self.table_name
        return self.dml_query.insertAll(table_name, cvs_file)

    def selectAll(self, table_name=""):
        if not table_name:
            table_name = self.table_name
        return self.dml_query.selectAll(table_name)

    def selectOne(self, pk, table_name=""):
        if not table_name:
            table_name = self.table_name
        return self.dml_query.selectOne(table_name, pk)

    def selectLastPK(self, table_name=""):
        if not table_name:
            table_name = self.table_name
        return self.dml_query.selectLastPK(table_name)

    def update(self, dict_data, pk, table_name=""):
        if not table_name:
            table_name = self.table_name
        return self.dml_query.update(table_name, dict_data, pk)

    def insertOne(self, dict_data, table_name="", logger=None):
        if not table_name:
            table_name = self.table_name
        return self.dml_query.insertOne(table_name, dict_data, logger=logger)

    def DeleteLastRow(self, pk, table_name=""):
        if not table_name:
            table_name = self.table_name
        pk = self.selectLastPK()
        return self.dml_query.deleteOne(table_name, pk)

    def deleteOne(self, pk, table_name=""):
        if not table_name:
            table_name = self.table_name
        return self.dml_query.deleteOne(table_name, pk)

    def createDatabase(self, db_name):
        return self.ddl_query.createDatabase(db_name)

    def dropDatabase(self, db_name):
        return self.ddl_query.dropDatabase(db_name)

    def selectColumnType(self, table_name=''):
        if not table_name:
            table_name = self.table_name
        return self.dml_query.selectColumnType(table_name)

    def selectColumnNames(self, table_name=''):
        if not table_name:
            table_name = self.table_name
        return self.dml_query.selectColumnNames(table_name)

    def selectDataByConditions(self, table_name='', col_list='*', cond_list='', sort_col='', sort_desc=True, logger=None):
        if not table_name:
            table_name = self.table_name
        return self.dml_query.selectDataByConditions(table_name, col_list, cond_list, sort_col, sort_desc, logger=logger)
