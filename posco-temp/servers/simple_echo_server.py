#!/usr/bin/env python
# -*- coding: utf-8 -*-
import socket
import sys

# Create a TCP/IP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# Bind the socket to the port
server_address = ('localhost', 10000)
print('Simple Echo Server : starting up on %s port %s' % server_address)
sock.bind(server_address)

# Listen for incoming connections
sock.listen(1)

while True:
    # Wait for a connection
    print('Simple Echo Server : waiting for a connection')
    connection, client_address = sock.accept()
    try:
        print('Simple Echo Server : connection from ', client_address)

        # Receive the data in small chunks and retransmit it
        while True:
            data = connection.recv(256).decode('utf-8')
            print('Simple Echo Server : received "%s"' % data)
            if data:
                print('Simple Echo Server : sending data back to the client')
                connection.sendall(data.encode('utf-8'))
            else:
                print('Simple Echo Server : no more data from', client_address)
                break
    finally:
        # Clean up the connection
        connection.close()
    print('Simple Echo Server')


