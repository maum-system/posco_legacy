stream  = ""
stream += "        "            #  8 ->    8 : TRANSACTION_CD
stream += "  "                  #  2 ->   10 : OP_CODE
stream += "             "       # 13 ->   23 : CHARGE_NO
stream += "     "               #  5 ->   28 : MOLTEN_STEEL_CHAR_CD
stream += "           "         # 11 ->   39 : SM_STEEL_GRD
stream += "        "            #  8 ->   47 : TAP_WORK_DATE
stream += "8 "                  #  2 ->   49 : WORK_MONTH
stream += "L"                   #  1 ->   50 : RH_TREAT_PATTERN
stream += "   "                 #  3 ->   53 : RH_DEO_METHOD
stream += "  0.001000"          # 10 ->   63 : C_AIM
stream += "  0.070000"          # 10 ->   73 : SI_AIM
stream += "  0.000000"          # 10 ->   83 : MN_AIM
stream += "          "          # 10 ->   93 : AL_AIM
stream += "  "                  #  2 ->   95 : LAST_OP
stream += "11"                  #  2 ->   97 : SM_LD_NO
stream += "153"                 #  3 ->  100 : LD_AF_MID_USE_TIM
stream += " 77"                 #  3 ->  103 : LD_AF_SMALL_USE_TIM
stream += " 1"                  #  2 ->  105 : LD_SMALL_TIM
stream += "    "                #  4 ->  109 : LD_PRE_HEAT_TEMP
stream += "LS"                  #  2 ->  111 : LD_STATUS_CD
stream += " 7"                  #  2 ->  113 : LD_U_TOP_NOZ_USE_TIM
stream += " 7"                  #  2 ->  115 : LD_U_PLATE_USE_TIM
stream += "  73"                #  4 ->  119 : EMT_LD_TM
stream += "    "                #  4 ->  123 : LD_SM_STAY_TIM
stream += "    "                #  4 ->  127 : LAD_CYCLE_TM
stream += "   0"                #  4 ->  131 : LD_KEEP_WARM_TM
stream += "114000"              #  6 ->  137 : LD_WGT
stream += "  "                  #  2 ->  139 : LD_REPAIR_CD
stream += "  "                  #  2 ->  141 : LD_RASH_NO
stream += "  "                  #  2 ->  143 : LD_RASH_GUBUN_CD
stream += "W  "                 #  3 ->  146 : RH_PRE_CHARGE_DEO_METHOD
stream += "  0.001000"          # 10 ->  156 : RH_PRE_CHARGE_C_AIM
stream += "  0.070000"          # 10 ->  166 : RH_PRE_CHARGE_SI_AIM
stream += "  0.000000"          # 10 ->  176 : RH_PRE_CHARGE_MN_AIM
stream += "  0.025000"          # 10 ->  186 : RH_PRE_CHARGE_AL_AIM
stream += "103000"              #  6 ->  192 : CONV_TEEM_LD_ME_WGT
stream += "2"                   #  1 ->  193 : CONV_NUM
stream += "3   "                #  4 ->  197 : CONV_FUR_STAND_NO
stream += " 4004"               #  5 ->  202 : CONV_USE_TIM
stream += "21"                  #  2 ->  204 : CONV_O2_LANCE_NO
stream += "  85"                #  4 ->  208 : CONV_O2_LANCE_USE_TIM
stream += " 89"                 #  3 ->  211 : CONV_TAP_HOLE_USE_TIM
stream += "     "               #  5 ->  216 : CONV_TOT_O2_INPUT
stream += "      "              #  6 ->  222 : CONV_REBLOW_INPUT
stream += " "                   #  1 ->  223 : CONV_REBLOW_TIM
stream += "      "              #  6 ->  229 : CONV_ME_TAP_WGT
stream += "   "                 #  3 ->  232 : CONV_TAP_TM
stream += "    "                #  4 ->  236 : CONV_ENDPNT_TEMP
stream += "    "                #  4 ->  240 : CONV_ENDPNT_O2
stream += "    "                #  4 ->  244 : CONV_TAP_TEMP
stream += "    "                #  4 ->  248 : CONV_ENDPNT_TARGET_TEMP
stream += "    "                #  4 ->  252 : CONV_ENDPNT_TARGET_REV_TEMP
stream += "    "                #  4 ->  256 : CONV_SUPPLY_HEAT_TM
stream += "    "                #  4 ->  260 : CONV_TAP_BB_TM
stream += "        "            #  8 ->  268 : CONV_LP_TOT_ALLOY_INPUT
stream += "     "               #  5 ->  273 : CONV_PREC_INPUT
stream += "     "               #  5 ->  278 : CONV_CU_INPUT
stream += "     "               #  5 ->  283 : CONV_FEMN_HC_INPUT
stream += "     "               #  5 ->  288 : CONV_FEMN_MC_INPUT
stream += "     "               #  5 ->  293 : CONV_FEMN_LC_INPUT
stream += "     "               #  5 ->  298 : CONV_MN_METAL_INPUT
stream += "     "               #  5 ->  303 : CONV_MN_ORE_INPUT
stream += "     "               #  5 ->  308 : CONV_SIMN_HC_INPUT
stream += "     "               #  5 ->  313 : CONV_SIMN_LC_INPUT
stream += "     "               #  5 ->  318 : CONV_FESI_INPUT
stream += "     "               #  5 ->  323 : CONV_FECR_HC_INPUT
stream += "     "               #  5 ->  328 : CONV_FECR_MC_INPUT
stream += "     "               #  5 ->  333 : CONV_FECR_LC_INPUT
stream += "     "               #  5 ->  338 : CONV_NI_CLASS_INPUT
stream += "     "               #  5 ->  343 : CONV_SB_METAL_INPUT
stream += "     "               #  5 ->  348 : CONV_SI_METAL_INPUT
stream += "     "               #  5 ->  353 : CONV_FETI_INPUT
stream += "     "               #  5 ->  358 : CONV_FEP_INPUT
stream += "     "               #  5 ->  363 : CONV_FEV_INPUT
stream += "     "               #  5 ->  368 : CONV_COBALT_INPUT
stream += "     "               #  5 ->  373 : CONV_FEMN_NM_INPUT
stream += "     "               #  5 ->  378 : CONV_FESI_CR_INPUT
stream += "     "               #  5 ->  383 : CONV_MOLY_INPUT
stream += "     "               #  5 ->  388 : CONV_SNTIN_INPUT
stream += "     "               #  5 ->  393 : CONV_TUNGSTEN_INPUT
stream += "     "               #  5 ->  398 : CONV_TAP_AL_INPUT
stream += "  399"               #  5 ->  403 : CONV_TAP_BLI_INPUT
stream += "     "               #  5 ->  408 : CONV_TAP_FELDSPAR_INPUT
stream += "  193"               #  5 ->  413 : CONV_TAP_FLUX_B_INPUT
stream += "    "                #  4 ->  417 : BAP_TB_TM
stream += "    "                #  4 ->  421 : BAP_TB_FLOW
stream += "    "                #  4 ->  425 : BAP_BB_TM
stream += "    "                #  4 ->  429 : BAP_BB_FLOW
stream += "    "                #  4 ->  433 : BAP_ARR_TEMP
stream += "    "                #  4 ->  437 : BAP_ARR_O2
stream += "    "                #  4 ->  441 : BAP_DEP_TEMP
stream += "    "                #  4 ->  445 : BAP_DEP_O2
stream += "3"                   #  1 ->  446 : RH_NO
stream += " "                   #  1 ->  447 : RH_VESSEL_GUBUN_CD
stream += "    "                #  4 ->  451 : RH_COVER_USE_TIM
stream += "    "                #  4 ->  455 : RH_COVER_TOT_USE_TIM
stream += " 366"                #  4 ->  459 : RH_UP_VESSEL_USE_TIM
stream += "3064"                #  4 ->  463 : RH_UP_VESSEL_TOT_USE_TIM
stream += "  88"                #  4 ->  467 : RH_LOW_VESSEL_USE_TIM
stream += "1116"                #  4 ->  471 : RH_LOW_VESSEL_TOT_USE_TIM
stream += "    "                #  4 ->  475 : RH_PRE_TREAT_VESSEL_TEMP
stream += "    "                #  4 ->  479 : RH_AF_TREAT_VESSEL_TEMP
stream += "    8"               #  5 ->  484 : RH_SNR_USE_TIM
stream += "    "                #  4 ->  488 : RH_TOT_TREAT_TM
stream += "   "                 #  3 ->  491 : RH_AF_AL_CIRGAS_TM
stream += "   "                 #  3 ->  494 : RH_AF_COOL_CIRGAS_TM
stream += "    "                #  4 ->  498 : RH_ARR_TEMP
stream += "1587"                #  4 ->  502 : RH_REQ_TEMP
stream += "    "                #  4 ->  506 : RH_ARR_O2
stream += "    "                #  4 ->  510 : RH_BEFORE_DEO_TEMP
stream += "    "                #  4 ->  514 : RH_BEFORE_DEO_O2
stream += "    "                #  4 ->  518 : RH_TOT_OB_INPUT
stream += "             5"      # 14 ->  532 : RH_SAMPLE_TM
stream += "  35"                #  4 ->  536 : RH_BURN_O2_INPUT
stream += "    "                #  4 ->  540 : RH_AVR_CIRGAS_INPUT
stream += "     "               #  5 ->  545 : RH_AVR_VACUUM_PNT
stream += "     "               #  5 ->  550 : RH_LAST_AL_ALLOY_INPUT
stream += "     "               #  5 ->  555 : RH_LAST_COOL_INPUT
stream += "     "               #  5 ->  560 : RH_AL_ALLOY_INPUT
stream += "      "              #  6 ->  566 : RH_PRE_CARBON_INPUT
stream += "      "              #  6 ->  572 : RH_PRE_CU_INPUT
stream += "      "              #  6 ->  578 : RH_PRE_FEMN_HC_INPUT
stream += "      "              #  6 ->  584 : RH_PRE_FEMN_MC_INPUT
stream += "      "              #  6 ->  590 : RH_PRE_FEMN_LC_INPUT
stream += "      "              #  6 ->  596 : RH_PRE_MNMETAL_INPUT
stream += "      "              #  6 ->  602 : RH_PRE_SIMN_HC_INPUT
stream += "      "              #  6 ->  608 : RH_PRE_SIMN_LC_INPUT
stream += "      "              #  6 ->  614 : RH_PRE_FESI_INPUT
stream += "      "              #  6 ->  620 : RH_PRE_FECR_HC_INPUT
stream += "      "              #  6 ->  626 : RH_PRE_FECR_MC_INPUT
stream += "      "              #  6 ->  632 : RH_PRE_NI_INPUT
stream += "      "              #  6 ->  638 : RH_PRE_AL_INPUT
stream += "      "              #  6 ->  644 : RH_PRE_COOLANT_INPUT
stream += "      "              #  6 ->  650 : RH_PRE_FLUXB_INPUT
stream += "      "              #  6 ->  656 : RH_PRE_CASI_INPUT
stream += "      "              #  6 ->  662 : RH_PRE_FETI_INPUT
stream += "      "              #  6 ->  668 : RH_PRE_FE_P_INPUT
stream += "      "              #  6 ->  674 : RH_PRE_FE_V_INPUT
stream += "      "              #  6 ->  680 : RH_PRE_FEB_INPUT
stream += "      "              #  6 ->  686 : RH_PRE_FES_INPUT
stream += "      "              #  6 ->  692 : RHPRE_MOLY_INPUT
stream += "      "              #  6 ->  698 : RH_PRE_NB_INPUT
stream += "      "              #  6 ->  704 : RH_PRE_V_INPUT
stream += "      "              #  6 ->  710 : RH_PRE_TISPONGE_INPUT
stream += "    "                #  4 ->  714 : BAP_ARR_TM_TAP_START_TM
stream += "    "                #  4 ->  718 : BAP_TREAT_TM
stream += "    "                #  4 ->  722 : RH_TRT_START_BAP_DEP_TM
stream += "     "               #  5 ->  727 : SUM_TM
stream += "   15"               #  5 ->  732 : TAP_TO_RH_TM
stream += "   5"                #  4 ->  736 : TAP_TM
stream += "  10"                #  4 ->  740 : SM_2ND_REF_BAP_VAC_LAD_SHT_TM
stream += "  25"                #  4 ->  744 : SM_SREF_RH_QUE_TM
stream += "L"                   #  1 ->  745 : SM_2ND_REF_RH_RQR_TMP_REM_TP
stream += "35"                  #  2 ->  747 : BE_CHARGE_CAST_TM
stream += "1"                   #  1 ->  748 : BE_CHARGE_MC
stream += "70"                  #  2 ->  750 : BE_CHARGE_LEAD_TM
stream += "100"                 #  3 ->  753 : SM_DSTL_LAD_IN_SLU_JDG_GD_TP
stream += "100"                 #  3 ->  758 : SM_DSTL_LAD_SLU_EA_JDG_GD_TP
stream += "                                                                                      "     # 86 ->  842 : SPARE