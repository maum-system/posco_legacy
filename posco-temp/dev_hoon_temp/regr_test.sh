#!/bin/bash

export PYTHONPATH="${HOME}/Workspace/posco-temp-model:${PYTHONPATH}"

if [ "$#" -ne 1 ]; then
    printf "\n # Category argument (rh or cvt) is needed\n"
    exit
fi

if [ $1 == "rh" ]; then
    DATASET_FILE="posco3_91429.csv"
elif [ $1 == "cvt" ]; then
    DATASET_FILE="posco4_91429.csv"
else
    printf "\n @ Error: invalid category, $1\n"
    exit
fi

GEN_DB_TABLE=false
RENAME_DATASET=false
GEN_DATASET=false
HIST_FILE=false
TEST_STREAM=false
LEARNING=true

VAR_CSV_FILE="$1_var.csv"
VAR_INI_FILE="$1_var.ini"
RESULT_FILE="$1_result.txt"
HIST_FOLDER="$1_hist"
STREAM_FILE="$1_test_stream.py"
RENAME_DATASET_NAME="renamed_dataset.csv"

if ${GEN_DB_TABLE}; then

    printf "\n #### Flag test of gen_db_table...\n"
    python data_handler_template.py --var_csv_file ${VAR_CSV_FILE} \
                                    --var_ini_file ${VAR_INI_FILE} \
                                    --dat_csv_file ${DATASET_FILE} \
                                    --ana_file     ${RESULT_FILE} \
                                    --feature_selection_ \
                                    --machine_learning_ \
                                    --gen_db_table_ \
                                    # --rename_dataset_col ${RENAME_DATASET_NAME} \
                                    # --hist_folder ${HIST_FOLDER} \
                                    # --test_stream_file ${STREAM_FILE} \
                                    # --gen_dataset_
fi

if ${RENAME_DATASET}; then

    printf "\n #### Flag test of rename_dataset_col...\n"
    python data_handler_template.py --var_csv_file ${VAR_CSV_FILE} \
                                    --var_ini_file ${VAR_INI_FILE} \
                                    --dat_csv_file ${DATASET_FILE} \
                                    --ana_file     ${RESULT_FILE} \
                                    --feature_selection_ \
                                    --machine_learning_ \
                                    --rename_dataset_col ${RENAME_DATASET_NAME} \
                                    # --gen_db_table_ \
                                    # --hist_folder ${HIST_FOLDER} \
                                    # --test_stream_file ${STREAM_FILE} \
                                    # --gen_dataset_
fi

if ${HIST_FILE}; then

    printf "\n #### Flag test of hist_file...\n"
    python data_handler_template.py --var_csv_file ${VAR_CSV_FILE} \
                                    --var_ini_file ${VAR_INI_FILE} \
                                    --dat_csv_file ${DATASET_FILE} \
                                    --ana_file     ${RESULT_FILE} \
                                    --feature_selection_ \
                                    --machine_learning_ \
                                    --hist_folder ${HIST_FOLDER} \
                                    # --gen_db_table_ \
                                    # --rename_dataset_col ${RENAME_DATASET_NAME} \
                                    # --test_stream_file ${STREAM_FILE} \
                                    # --gen_dataset_
fi

if ${TEST_STREAM}; then

    printf "\n #### Flag test of test_stream_file...\n"
    python data_handler_template.py --var_csv_file ${VAR_CSV_FILE} \
                                    --var_ini_file ${VAR_INI_FILE} \
                                    --dat_csv_file ${DATASET_FILE} \
                                    --ana_file     ${RESULT_FILE} \
                                    --feature_selection_ \
                                    --machine_learning_ \
                                    --test_stream_file ${STREAM_FILE} \
                                    # --gen_db_table_ \
                                    # --rename_dataset_col ${RENAME_DATASET_NAME} \
                                    # --hist_folder ${HIST_FOLDER} \
                                    # --gen_dataset_
fi

if ${GEN_DATASET}; then

    printf "\n #### Flag test of gen_dataset...\n"
    python data_handler_template.py --var_csv_file ${VAR_CSV_FILE} \
                                    --var_ini_file ${VAR_INI_FILE} \
                                    --dat_csv_file ${DATASET_FILE} \
                                    --ana_file     ${RESULT_FILE} \
                                    --feature_selection_ \
                                    --machine_learning_ \
                                    --gen_dataset_ \
                                    # --gen_db_table_ \
                                    # --rename_dataset_col ${RENAME_DATASET_NAME} \
                                    # --hist_folder ${HIST_FOLDER} \
                                    # --test_stream_file ${STREAM_FILE} \

fi

if ${LEARNING}; then

    printf "\n #### Flag test of feature_selection & machine_learning ...\n"
    python data_handler_template.py --var_csv_file ${VAR_CSV_FILE} \
                                    --var_ini_file ${VAR_INI_FILE} \
                                    --dat_csv_file ${DATASET_FILE} \
                                    --ana_file     ${RESULT_FILE} \
                                    --feature_selection_ \
                                    --machine_learning_ \
                                    # --gen_db_table_ \
                                    # --rename_dataset_col ${RENAME_DATASET_NAME} \
                                    # --hist_folder ${HIST_FOLDER} \
                                    # --test_stream_file ${STREAM_FILE} \
                                    # --gen_dataset_

fi
