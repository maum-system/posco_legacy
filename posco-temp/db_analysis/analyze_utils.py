#! /usr/bin/env python
# -*- coding: utf-8 -*-

import sys, os
import collections
import pandas as pd
import configparser
from lib import learning as learning_lib
from lib import sys_lib_2 as sys_lib
from lib import data as data_lib

class Evaluation:

    def __init__(self, data_path=None, out_path=None, ini=None, logger=None):

        self.data_path = data_path
        self.out_path = out_path
        self.ini = ini
        self.logger = logger

        self.model_name = None
        self.model_num = None
        self.trans_code = None

        self.init_ini(ini)
        self.est_db_handler = None
        self.eval_db_handler = None
        self.est_dataset = None
        self.eval_dataset = None
        self.match_dataset = None

        self.curr_ml_ini = None

    def init_ini(self, ini):  # part

        self.est_dataset_fname = ini['general']['est_dataset_fname']
        self.eval_dataset_fname = ini['general']['eval_dataset_fname']
        self.save_file_ = bool(ini['general']['save_file_'])

        check_model_list = ini['general']['check_model_list'].split(',')
        self.check_model_list = [u.strip() for u in check_model_list]

        self.mssql_ini = ini['mssql_db']
        self.mssql_server_name = ini['mssql_db']['server_name']
        self.mssql_port = ini['mssql_db']['port']
        self.mssql_username = ini['mssql_db']['username']
        self.mssql_password = ini['mssql_db']['password']

        self.model_1_ini = ini['model_1']
        self.model_2_ini = ini['model_2']
        self.curr_model_ini = None

    def init_logger(self, log_dir=None, logging_=True, console_=True, SERVER_MODE=None):
        try:
            self.ini['logger']['name'] += "." + SERVER_MODE
            self.ini['logger']['prefix'] = SERVER_MODE
            if log_dir:
                self.ini['logger']['folder'] = log_dir
            self.logger = sys_lib.setup_logger_with_ini(self.ini['logger'],
                                                        logging_=logging_,
                                                        console_=console_)
        except Exception as e:
            print(" @ Error in init_logger: {}".format(e))
            self.logger = sys_lib.get_stdout_logger()

    def get_model_info(self, model_name):
        self.model_name = model_name
        self.model_num = self.model_name.split('_')[1]
        self.curr_model_ini = self.get_model_ini(model=self.model_name)
        self.trans_code = self.curr_model_ini['eval_header']
        tolerances = self.curr_model_ini['tolerance'].split(',')
        self.tolerances = [float(u) for u in tolerances]

    def get_ml_info(self, var_ini_file, var_csv_file):
        # Get ML Info
        self.curr_ml_ini_file = self.get_ml_ini_file(model=self.model_name)
        ml_ini = configparser.ConfigParser()
        ml_ini.read(self.curr_ml_ini_file)
        var_ini = configparser.ConfigParser()
        var_ini.read(var_ini_file)
        var_mtx = data_lib.read_csv_file(var_csv_file)
        self.feat_handler = sys_lib.get_feat_handler(var_ini_file, var_csv_file)


    def init_db_handler(self, DB_OP_):
        if not DB_OP_:
            return False
        self.est_db_handler = sys_lib.PyMssqlWrapper(server_name=self.mssql_server_name,
                                                     port=self.mssql_port,
                                                     username=self.mssql_username,
                                                     password=self.mssql_password,
                                                     db_name=self.curr_model_ini['db_name'],
                                                     table_name=self.curr_model_ini['EST'.lower() + '_' + 'table_name'],
                                                     logger=self.logger)
        self.eval_db_handler = sys_lib.PyMssqlWrapper(server_name=self.mssql_server_name,
                                                     port=self.mssql_port,
                                                     username=self.mssql_username,
                                                     password=self.mssql_password,
                                                     db_name=self.curr_model_ini['db_name'],
                                                     table_name=self.curr_model_ini['EVAL'.lower() + '_' + 'table_name'],
                                                     logger=self.logger)

    def get_model_ini(self, model):  # part

        model_ini_dict = {
            'model_1': self.model_1_ini,
            'model_2': self.model_2_ini,
        }
        return model_ini_dict[model]

    def get_ml_ini_file(self, model):
        model_ml_ini_dict = {
            'model_1'   : 'cvt_cfg_posco.ini',
            'model_2'   : 'rh_cfg_posco.ini',
        }
        ml_ini_file_path = '../sys_temp/'
        return ml_ini_file_path + model_ml_ini_dict[model]

    def load_dataset(self, SERVER_MODE=None, DB_OP_=None):
        if SERVER_MODE == 'EST':
            db_handler = self.est_db_handler
            dataset_fname = self.est_dataset_fname
        elif SERVER_MODE == 'EVAL':
            db_handler = self.eval_db_handler
            dataset_fname = self.eval_dataset_fname

        if DB_OP_:
            db_columns = db_handler.selectColumnNames()
            db_columns = [db_column[0] for db_column in db_columns]
            db_data = db_handler.selectAll()
            dataset = [db_columns] + db_data
            dataset = convert_data_type(dataset, 'df')
        else:
            dataset = pd.read_csv(dataset_fname)
        return dataset

    def load_est_eval_dataset(self, DB_OP_):
        for server_mode in ['EST', 'EVAL']:
            dataset = self.load_dataset(SERVER_MODE=server_mode, DB_OP_=DB_OP_)
            if server_mode is 'EST':
                self.est_dataset = dataset
            elif server_mode is 'EVAL':
                self.eval_dataset = dataset
        return self.est_dataset, self.eval_dataset

    def extract_match_dataset(self, SERVER_MODE):  # part
        est_df = convert_data_type(self.est_dataset, dtype='df')
        eval_df = convert_data_type(self.eval_dataset, dtype='df')

        # # 지시/실적 데이터에서 중복행 제거
        est_no_dup_df = est_df.drop_duplicates(subset=['TRANSACTION_CD','CHARGE_NO'])
        eval_no_dup_df = eval_df.drop_duplicates(subset=['TRANSACTION_CD','CHARGE_NO'])

        if SERVER_MODE == 'EST':
            # 실적 매칭되는 지시의 MTL_NO
            match_ch_no = eval_no_dup_df['CHARGE_NO'].loc[eval_no_dup_df['CHARGE_NO'].isin(est_no_dup_df['CHARGE_NO'])].reset_index(drop=True)

            # 실적 데이터에 지시 예측값 추가
            est_y = self.curr_model_ini[SERVER_MODE.lower() + '_y']
            est_values = est_no_dup_df[['CHARGE_NO', est_y]]
            eval_no_dup_df = eval_no_dup_df.merge(est_values, on='CHARGE_NO')

            # 지시/실적 MTL_NO 매칭
            match_df = eval_no_dup_df.loc[eval_no_dup_df['CHARGE_NO'].isin(match_ch_no)]
            self.match_dataset = match_df

        elif SERVER_MODE == 'EVAL':
            self.match_dataset = eval_no_dup_df

    def select_data_by_conditions(self, df=None, cond_list=None):  # part

        if df is None:
            df = self.match_dataset

        for cond in cond_list:
            key = cond[0]
            cond_vals = cond[1]
            if key == 'TRANSACTION_CD':
                df = df.loc[df[key] == cond_vals]
            elif key == 'START_DATE':
                df = df.loc[df['TAP_WORK_DATE'].apply(pd.to_numeric) >= int(cond_vals)]
            elif key == 'END_DATE':
                df = df.loc[df['TAP_WORK_DATE'].apply(pd.to_numeric) <= int(cond_vals)]
            elif key == 'NULL_CHECK_VAR_LIST':
                df = df.dropna(subset=cond_vals)
            # elif key == SERVER_MODE + '_RANGE_CHECK_VARS':
            elif key == 'EVAL_RANGE_CHECK_VARS':
                if cond_vals == 'None':
                    df = df.loc[df[key] == 'None']
                else:
                    for range_var in cond_vals:
                        df = df.loc[(df[key].str.contains(range_var)) == False]
            print("Key : {}, condition : {}, df_cnts : {}".format(key, str(cond_vals), len(df)))
            print("Key : {}, condition : {}, df_cnts : {}".format(key, str(cond_vals), len(df)))
        return df

def get_date_period():
    start_date = input(" \n Please enter start date : ex) 20190507\n :")
    end_date = input(" \n Please enter end date : ex) 20191231\n :")

    return start_date, end_date


def get_cond_list(db_op_,
                  server_mode,
                  trans_code,
                  start_date=None, end_date=None,
                  null_check_var_list=None,
                  range_check_var_list=None
                  ):
    if db_op_ is True:  # part
        cond_list = ["TRANSACTION_CD = '" + trans_code + "'"]
        if start_date:
            cond_list.append("TAP_WORK_DATE >='" + start_date + "'")
        if end_date:
            cond_list.append("TAP_WORK_DATE <='" + end_date + "'")
        if null_check_var_list:
            for var in null_check_var_list:
                cond_list.append(var + " is not NULL")
        if range_check_var_list:
            for var in range_check_var_list:
                cond_list.append(server_mode+"_RANGE_CHECK_VARS NOT LIKE '%" + var + ":%'")
                # cond_list.append("EVAL_RANGE_CHECK_VARS NOT LIKE '%" + var + ":%'")
    else:
        cond_list = [['TRANSACTION_CD', trans_code]]
        if start_date:
            cond_list.append(['START_DATE', start_date])
        if end_date:
            cond_list.append(['END_DATE', end_date])
        if null_check_var_list:
            cond_list.append(['NULL_CHECK_VAR_LIST', null_check_var_list])
        if range_check_var_list:
            cond_list.append([server_mode + "_RANGE_CHECK_VARS", range_check_var_list])
            # cond_list.append(["EVAL_RANGE_CHECK_VARS", range_check_var_list])

    return cond_list

def convert_data_type(data, dtype='list'):
    if dtype == 'list':
        if isinstance(data, pd.DataFrame):
            res_data = [data.columns.values.tolist()] + data.values.tolist()
        else:
            res_data = data
    elif dtype =='df':
        if isinstance(data, list):
            res_data = pd.DataFrame(data[1:], columns=data[0])
        else:
            res_data = data
    return res_data

def get_tolerance_char(tol_idx):
    if tol_idx + 1 == 1:
        tol_char = 'first'
    elif tol_idx + 1 == 2:
        tol_char = 'second'
    elif tol_idx + 1 == 3:
        tol_char = 'third'
    return tol_char

def get_check_var_list(ini_check_var_list):
    check_var_list = [var.strip() for var in ini_check_var_list.split(',')] if ini_check_var_list != ''\
        else None
    return check_var_list