import pandas as pd
import matplotlib.pyplot as plt

acc_df = pd.read_csv("Output/20210526/200101_210515_model_2_d_EVAL_None_.csv")
zero_df = pd.read_csv("count_zero.csv")
zero_df['DATE2'] = pd.to_datetime(zero_df.TAP_WORK_DATE, format='%Y%m%d')
zero_df['DATE2'] = zero_df['DATE2'].astype(str)

df_merge_how_inner = acc_df.merge(zero_df,
                              how = 'inner',
                              on='DATE2')
print (df_merge_how_inner)
df_merge_how_inner['count_zero_norm'] = (df_merge_how_inner['count_zero'] - df_merge_how_inner['count_zero'].min())/\
                                        (df_merge_how_inner['count_zero'].max()-df_merge_how_inner['count_zero'].min())

# 일/주/월 적중률
df_merge_how_inner['DATE2'] = pd.to_datetime(df_merge_how_inner['DATE2'], errors='coerce')
tolerances = [3.0, 7.0]
plt.rcParams["figure.figsize"] = [15, 5]
plt.scatter(df_merge_how_inner['DATE2'].dt.strftime('%y-%m-%d'),
            df_merge_how_inner["filter_y_ACC_{}".format(tolerances[0])], c='red', alpha=0.5,
            label='filter_y_ACC_{}'.format(tolerances[0]))
plt.scatter(df_merge_how_inner['DATE2'].dt.strftime('%y-%m-%d'),
            df_merge_how_inner["filter_y_ACC_{}".format(tolerances[1])], c='darkorange', alpha=0.5,
            label='filter_y_ACC_{}'.format(tolerances[1]))
plt.scatter(df_merge_how_inner['DATE2'].dt.strftime('%y-%m-%d'),
            df_merge_how_inner["refined_ACC_{}".format(tolerances[0])], c='green', alpha=0.5,
            label='refined_ACC_{}'.format(tolerances[0]))
plt.scatter(df_merge_how_inner['DATE2'].dt.strftime('%y-%m-%d'),
            df_merge_how_inner["refined_ACC_{}".format(tolerances[1])], c='blue', alpha=0.5,
            label='refined_ACC_{}'.format(tolerances[1]))
plt.scatter(df_merge_how_inner['DATE2'].dt.strftime('%y-%m-%d'),
            df_merge_how_inner["count_zero_norm"], c='black', alpha=0.5,
            label='refined_ACC_{}'.format(tolerances[1]))
plt.ylim(0, 1 + 1 * 0.05)
plt.legend(loc='upper left')
fname_dict_t = "일 별 적중률"
plt.title(label=" ".join(fname_dict_t.split(".")[0].split("_")[2:]), loc='center', fontsize=20)
plt.xlabel('Date', fontsize=20)
plt.ylabel('{} Accuracy'.format('온도 RH 모델'), fontsize=20)
plt.gcf().autofmt_xdate()
plt.xticks(rotation=45)
# if 'day' in fname_dict_t:
plt.xticks(df_merge_how_inner['DATE2'].dt.strftime('%y-%m').duplicated(keep='first').replace(True, "")
           .replace(False, df_merge_how_inner['DATE2'].dt.strftime('%y-%m-%d')))
# plt.savefig(os.path.join(OUT_PATH, fname_dict_t), dpi=300)
plt.savefig('comp_zero_and_acc.jpg', dpi=300)
plt.clf()