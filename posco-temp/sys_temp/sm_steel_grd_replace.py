import pandas as pd

def make_conv_dict(conv_data_csv):
    conv_dict = dict()
    conv_data = open(conv_data_csv, "r")
    c_data = conv_data.readlines()
    for d in c_data:
        d = d.replace("\n", "")
        d = d.split(",")
        conv_dict[d[0]] = d[1]
    conv_data.close()
    # print(conv_dict)
    return conv_dict

def main():
    # replace
    df = pd.read_csv(input_file, keep_default_na=False)  # NA 라는 값이 있으면 빈 칸으로 return 하는 거 방지
    df2 = df.deepcopy()


    if category == "CVT":
        df2['SM_STEEL_GRD'] = df['SM_STEEL_GRD'].map(sm_steel_conv_dict)
        df2.rename(columns={"SM_STEEL_GRD":"SM_STEEL_GRD_N"}, inplace=True)
        df2['CONV_PRE_CHARGE_DEO_METHOD'] = df['CONV_PRE_CHARGE_DEO_METHOD'].map(deo_method_conv_dict)
        df2.rename(columns={"CONV_PRE_CHARGE_DEO_METHOD":"CONV_PRE_CHARGE_DEO_METHOD_N"}, inplace=True)

    elif category == "RH":
        df2['RH_DEO_METHOD'] = df['RH_DEO_METHOD'].map(deo_method_conv_dict)
        df2.rename(columns={"RH_DEO_METHOD":"RH_DEO_METHOD_N"}, inplace=True)
        df2['RH_PRE_CHARGE_DEO_METHOD'] = df['RH_PRE_CHARGE_DEO_METHOD'].map(deo_method_conv_dict)
        df2.rename(columns={"RH_PRE_CHARGE_DEO_METHOD":"RH_PRE_CHARGE_DEO_METHOD_N"}, inplace=True)

    # SAVE CSV
    df2.to_csv(output_file, index=False)

# make dict
sm_steel_conv_dict = make_conv_dict("sm_steel_grd_dict.csv")
deo_method_conv_dict = {'B': 'R', 'W': 'A', 'V': 'C', 'P': 'S'}

### Set parameters
category = "RH" # CVT / RH
input_file = "model/RH/rh_dataset_self.csv"
# input_file = "model/CVT/190509_cvt_dataset_all_light.csv"
output_file = "model/RH/201105_rh_dataset_self.csv"
# output_file = "model/CVT/201105_cvt_dataset_all_light.csv"

if __name__ == '__main__':
    main()