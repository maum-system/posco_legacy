#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""This module's docstring summary line.
This is a multi-line docstring. Paragraphs are separated with blank lines.
Lines conform to 79-column limit.
Module and packages names should be short, lower_case_with_underscores.
Notice that this in not PEP8-cheatsheet.py
Seriously, use flake8. Atom.io with https://atom.io/packages/linter-flake8
is awesome!
See http://www.python.org/dev/peps/pep-0008/ for more PEP-8 details

Coding style reference
    - https://www.python.org/dev/peps/pep-0008/
    - http://web.archive.org/web/20111010053227/http://jaynes.colorado.edu
    /PythonGuidelines.html

    WHAT TO DO
    > Normalization before learning.
    > RFR, SVR, DNR
    > apply string features to estimation.

"""
import os
import sys
import shutil
import random
import argparse
import configparser
import copy
import numpy as np
import matplotlib.pyplot as plt
import datetime
import pandas as pd
import ast
from PIL import Image
from operator import itemgetter
from lib import posco as lib
from collections import defaultdict

from sys_temp.feature_selector import FeatureSelector
# from dev_hoon_temp.machine_learner import MachineLearner
from mssql.pymssqlwrapper import PyMssqlWrapper


__author__ = "Hoon Paek, Dong-gi Kim, Ji-young Moon"
__copyright__ = "Copyright 2007, The Cogent Project"
__credits__ = []
__license__ = "GPL"
__version__ = "0.0.1"
__maintainer__ = "Hoon Paek"
__email__ = "hoon.paek@mindslab.ai"
__status__ = "Prototype"  # Prototype / Development / Production

RUN_RH_  = True
RUN_CVT_ = ~RUN_RH_

ANALYTICS_TEST = True
PARSING_TEST = ~ANALYTICS_TEST

DB_OP_ = False

if RUN_RH_:
    CAT = "rh"
    DATASET_FILE = "posco3_91429.csv"

elif RUN_CVT_:
    CAT = "cvt"
    DATASET_FILE = "posco4_91429.csv"
else:
    print(" @ Error: something wrong in category...\n")
    sys.exit()

VAR_CSV_FILE = CAT + "_var.csv"
VAR_INI_FILE = CAT + "_var.ini"
RESULT_FILE  = CAT + "_result.txt"
HIST_FOLDER  = CAT + "_hist"

infinity = 999999.0     # 10e100

"""
    The string variable names come from init file.
    Please make them the same as those in init file.
"""
VAR_INFO_CSV = 'var_info_csv'
ENUM_INDEX = 'no.'
DEFINITION = 'definition'
OBJECT_NAME = 'object_name'
DTYPE_POSCO = 'data_type_posco'
LENGTH = 'length'
DECIMAL = 'decimal'
POSITION = 'position'
UNIT = 'unit'
REMARKS = 'remarks'
CATEGORY = 'category'
DIVISION = 'division'
SECTION = 'section'
DATA_TYPE = 'data_type'
RANGE_MIN = 'range_min'
RANGE_MAX = 'range_max'
RANGE_DEFAULT = 'range_default'
STR_DOMAIN = 'str_domain'
NULL_IS = 'null_is'
DATASET = 'dataset'
PRIORITY = 'priority'
TOLERANCE = 'tolerance'
VAR_IF_NAME = 'var_if_name'

REFINEMENT = 'refinement'
NULL_NUM_THRESHOLD = 'null_num_threshold'
FS_VAR_THRESHOLD = 'FS_var_threshold'
FS_VAR_THRESHOLD_NORM = 'FS_var_threshold_norm'
FS_OUTPUT_NAME = 'FS_output_name'
FS_F_REGRESSION_P_THRESHOLD = 'FS_f_regression_p_threshold'
FS_MI_REGRESSION_THRESHOLD = 'FS_mi_regression_threshold'

DATE_STR_DOMAIN = "YYYYMMDD"

DIV_0_ERR = "#DIV/0!"
ANALYTICS_RESULT_FILE = ''

PRIORITY_ON = 100
PRIORITY_OFF = -1
DIV_CHAR_NUM = 120
DIV_CHAR = '*'
DIV_LINE = ''
for _ in range(DIV_CHAR_NUM):
    DIV_LINE += DIV_CHAR
DIV_PATTERN = "\n\n" + DIV_LINE + "\n"


class PoscoTempEstModel(object):
    """RH Start Temp Estimation Model

    """

    def __init__(self, mtx):
        self.mtx = mtx
        self.trans_mtx = list(map(list, zip(*self.mtx)))
        self.cell_y = len(self.mtx)
        self.cell_x = len(self.mtx[0])
        self.num_vectors = None
        self.vars = {}
        self.vars_num_in = {}
        self.vars_num_out = {}
        self.vars_str = {}
        self.output_idx = None
        self.dataset = None
        self.datasets = None
        self.req_stream_data = {}

        self.recv_enum_list = []
        self.send_enum_list = []
        self.db_est_enum_list  = []
        self.db_eval_enum_list = []

        self.log = ""

    def get_log(self):
        return self.log

    def is_number_class(self, key):
        """ Is the input key the number class ?

        :param key:
        :return:
        """
        if self.vars[key].__class__.__name__ == "NumClass":
            return True
        else:
            return False

    def get_key_from_enum_idx(self, enum_num):
        """ Return key by order.

        :return:
        """
        for key in self.vars:
            if self.vars[key].enum_idx == enum_num:
                return key
        return False

    def get_key_from_if_name(self, if_name):
        """ Get feature name from feature interface name.

        :param if_name:
        :return:
        """
        for key in self.vars:
            if self.vars[key].if_name == if_name:
                return key
        return False

    def rename_if_name_by_key(self, indexes):
        """ Rename dataset if_name by key name.

        :param indexes:
        :return:
        """
        arr = []
        for idx in indexes:
            arr.append(self.get_key_from_if_name(idx))

        return arr

    @staticmethod
    def analyze_dataset(dataset, console=True):
        """ Analyze sample vectors.

        :param dataset:
        :param console:
        :return:
        """
        num_vars = len(dataset[0])
        msg = "\n\n # Total number of input vectors = {:d}".format(len(dataset) - 1)
        msg += "\n # Total number of variables = {:d}".format(num_vars)

        null_list = [0, ] * num_vars
        for sample in dataset[1:]:
            null_cnt = sample.count("")
            null_list[null_cnt] += 1

        msg += "\n # Total null count in input dataset = {}".format(sum(null_list))
        for idx in range(num_vars):
            if null_list[idx] != 0:
                msg += "\n {:3d} = {:d}".format(idx, null_list[idx])

        print_and_write(msg, console=console)
        pass

    def print_analytics_result(self, console=True):
        """ Print attribute(s) of data in a smart way.

        :return:
        """
        prt = "\n\n # Total data sample number = {:d}".format(self.num_vectors)
        prt += "\n\n| {:>30} |".format("Variable IF name")
        prt += " {:>30} |".format("Variable name")
        prt += " {:>6} |".format("Type")
        prt += " {:>6} |".format("Index")
        prt += " {:>8} |".format("Priority")
        prt += " {:>10} |".format("Null num")
        prt += " {:>10} |".format("Outer num")
        prt += " {:>10} |".format("Null+Outer")
        prt += " {:>10} |".format("Mean")
        prt += " {:>10} |".format("Std")
        prt += DIV_PATTERN

        for idx in range(1, len(self.vars) + 1):
            key = self.get_key_from_enum_idx(idx)
            ptr = self.vars[key]
            if ptr.if_name:
                if ptr.__class__.__name__ == "NumClass":
                    var_type = "number"
                    outer_num = str(ptr.min_below + ptr.max_above)
                    waste_num = str(ptr.null_num + ptr.min_below + ptr.max_above)
                    mean_val = "{:.3f}".format(ptr.mean)
                    std_val = "{:.3f}".format(ptr.std)
                else:
                    var_type = "string"
                    outer_num = ""
                    waste_num = str(ptr.null_num)
                    mean_val = ""
                    std_val = ""

                prt += "\n| {:>30} |".format(ptr.if_name)
                prt += " {:>30} |".format(key)
                prt += " {:6} |".format(var_type)
                prt += " {:6d} |".format(ptr.enum_idx)
                prt += " {:8d} |".format(ptr.priority)
                prt += " {:10d} |".format(ptr.null_num)
                prt += " {:>10} |".format(outer_num)
                prt += " {:>10} |".format(waste_num)
                prt += " {:>10} |".format(mean_val)
                prt += " {:>10} |".format(std_val)

        print_and_write(prt, console=console)
        pass

    def generate_hist(self, dataset, folder='.', bin_num=256):
        """ Generate histograms of all variables.

        :param dataset:
        :param folder:
        :param bin_num:
        :return:
        """
        import matplotlib
        matplotlib.use('Agg')
        all_hist_file = "all.png"

        filenames = []
        print("")
        for if_name in dataset[0]:
            key = self.get_key_from_if_name(if_name)
            ptr = self.vars[key]
            if ptr.__class__.__name__ == "NumClass":
                idx = dataset[0].index(if_name)
                filename = str(idx) + "_hist_" + key + ".png"
                filenames.append(filename)
                plt.hist(ptr.dat_arr, bin_num)
                plt.title("Histogram of " + key + "({:d})".format(idx))
                # plt.show()
                plt.savefig(filename)
                print(" # Saving the histogram of {} ({})...".format(key, if_name))
                plt.close()

        images = list(map(Image.open, filenames))
        widths, heights = zip(*(i.size for i in images))
        total_height = sum(heights)
        max_width = max(widths)
        new_im = Image.new('RGB', (max_width, total_height))
        x_offset = 0
        for im in images:
            new_im.paste(im, (0, x_offset))
            x_offset += im.size[1]
        new_im.save(all_hist_file)

        if os.path.exists(folder):
            shutil.rmtree(folder)
        os.makedirs(folder)

        for file in filenames:
            shutil.move(file, folder + "/.")
        shutil.move(all_hist_file, folder + "/.")

        return True

    def refine_dataset_column_by_null(self, thresh, console=True):
        """ Refine dataset by deleting feature(s) which has null number greater than threshold.

        :param thresh:
        :param console:
        :return:
        """
        dataset_dim = [len(self.dataset) - 1, len(self.dataset[0])]
        num_thresh = dataset_dim[0] * thresh
        del_list = []
        for key in self.vars:
            if self.vars[key].null_num > num_thresh:
                del_list.append(key)
        for key in del_list:
            try:
                idx = self.dataset[0].index(self.vars[key].if_name)
            except ValueError:
                # print(key)
                continue
            for row in self.dataset:
                del row[idx]

        msg = "\n\n"
        msg += DIV_LINE
        msg += "\n\n # Dataset refinement process by deleting columns(s) " \
               "which has null number greater than threshold, {:.2f}".format(thresh)
        msg += "\n\n    > {:d} columns detected and deleted.".format(len(del_list))
        for key in del_list:
            msg += "\n      - {:5d}, {} ({})".format(self.vars[key].null_num, self.vars[key].if_name, key)
        msg += "\n\n    > dataset dim : {:d} x {:d} -> {:d} x {:d}".format(dataset_dim[0], dataset_dim[1],
                                                                           dataset_dim[0], len(self.dataset[0]))
        print_and_write(msg, console)
        pass

    def refine_dataset_by_row_range(self, console=True):
        """ Refine dataset row
        by deleting the row vector in which at least one sample doesn't satisfy its range requirement.

        :param console:
        :return:
        """
        dataset_dim = [len(self.dataset) - 1, len(self.dataset[0])]
        error_list = [[], [], []]
        del_list = []
        # temp_cnt = 0
        key_list = []
        for if_name in self.dataset[0]:
            key_list.append(self.get_key_from_if_name(if_name))
        for row_idx in range(1, dataset_dim[0] + 1):
            row_dat = self.dataset[row_idx]
            for idx in range(len(row_dat)):
                key = key_list[idx]
                ptr = self.vars[key_list[idx]]
                if row_dat[idx] == "" and self.vars[key].dataset_prio > PRIORITY_OFF:
                    # if key == 'TD_Weight_CastStart':
                    #     temp_cnt += 1
                    #     print("! {:d} -> {:d} and {:d}".format(temp_cnt, row_idx, idx))
                    error_list[0].append(key)
                    del_list.append(row_idx)
                    break
                if ptr.__class__.__name__ == "NumClass" and self.vars[key].dataset_prio > PRIORITY_OFF:
                    val = float(row_dat[idx])
                    if not (ptr.min_thresh <= val <= ptr.max_thresh):
                        error_list[1].append(key)
                        del_list.append(row_idx)
                        break
                else:
                    # if not (self.get_key_from_if_name(ptr.if_name) == 'Date'):
                    if not ptr.if_name == 'Input_Date_1' and self.vars[key].dataset_prio > PRIORITY_OFF:
                        try:
                            ptr.domain.index(row_dat[idx])
                        except ValueError:
                            error_list[2].append(key)
                            del_list.append(row_idx)
                            break

        del_list = sorted(del_list, reverse=True)
        for idx in del_list:
            del self.dataset[idx]

        msg = "\n\n"
        msg += DIV_LINE
        msg += "\n\n # Dataset refinement process by deleting row vector "
        msg += "in which one of them doesn't satisfy its range requirement."

        features = [[], [], []]
        frequencies = [[], [], []]
        for idx in range(len(error_list)):
            errors = error_list[idx]
            if errors:
                errors = sorted(errors)
                for feature in list(set(errors)):
                    features[idx].append(feature)
                    frequencies[idx].append(errors.count(feature))

        try:
            msg += "\n > Null data in sample vector"
            for idx in range(len(features[0])):
                msg += "\n   - {} : {:d}".format(features[0][idx], frequencies[0][idx])
        except ValueError:
            pass
        try:
            msg += "\n > Number type data in sample vector not satisfying range information"
            for idx in range(len(features[1])):
                msg += "\n   - {} : {:d}".format(features[1][idx], frequencies[1][idx])
        except ValueError:
            pass
        try:
            msg += "\n > String type data in sample vector not satisfying range information"
            for idx in range(len(features[2])):
                msg += "\n   - {} : {:d}".format(features[2][idx], frequencies[2][idx])
        except IndexError:
            pass
        msg += "\n\n    > dataset dim  : {:d} x {:d} -> {:d} x {:d}".format(dataset_dim[0], dataset_dim[1],
                                                                            len(self.dataset) - 1, len(self.dataset[0]))
        print_and_write(msg, console)
        pass

    def write_test_stream(self, dataset=None, filename=""):
        """ Write test stream based on reception and sending datagram.

        :param dataset:
        :param filename:
        :return:
        """
        if not dataset:
            dataset = self.dataset

        if not filename:
            return

        fid = open(filename, 'w')
        fid.write('stream  = ""\n')
        tot_len = 0
        for idx in self.recv_enum_list:
            key = self.get_key_from_enum_idx(idx)
            # print(key)
            try:
                val = dataset[1][dataset[0].index(key)]
            except ValueError:
                val = '' if self.vars[key].stream_dtype.lower() == 'char' else 0
            tot_len += self.vars[key].stream_length
            width = self.vars[key].stream_length
            space_width = max(20 - width, 5)
            if self.vars[key].stream_dtype.lower() == 'char':
                fid.write('stream += "{v:<{w}}"{s:{sw}}# {w:2d} -> {t:4d} : {key}\n'.
                          format(v=val, w=width, s='', sw=space_width, key=key, t=tot_len))
            elif self.vars[key].stream_dtype.lower() == 'number':
                fid.write('stream += "{v:{w}.{i}f}"{s:{sw}}# {w:2d} -> {t:4d} : {key}\n'.
                          format(v=float(val), w=width, i=self.vars[key].stream_decimal, s='', sw=space_width, key=key,
                                 t=tot_len))
            else:
                print(" @ Error: dtype is incorrect, {}".format(self.vars[key].stream_dtype))
        fid.close()
        print(" % Some information such as TRANSACTION CD must be filled out manually.")

        return True

    def gen_csv_by_dataset_prio(self):
        """ Divide dataset input vs output.

         :return:
         """
        dataset_trans = transpose_list(self.dataset)
        datasets = {'input': [], 'output': []}
        for idx in range(len(self.dataset[0])):
            key = self.get_key_from_if_name(self.dataset[0][idx])
            if self.vars[key].dataset_prio == '':
                continue
            dataset_prio = int(self.vars[key].dataset_prio)
            if dataset_prio < 0:
                continue
            if dataset_prio == 0:
                if self.is_number_class(key):
                    datasets['output'].append(
                        [self.vars[key].dataset_prio] + [key] + [float(x) for x in dataset_trans[idx][1:]])
            else:
                datasets['input'].append([self.vars[key].dataset_prio] + [key] + [x for x in dataset_trans[idx][1:]])

        datasets['input'] = sorted(datasets['input'], key=itemgetter(0))

        datasets['input'] = list(map(list, zip(*datasets['input'])))[1:]
        datasets['output'] = list(map(list, zip(*datasets['output'])))[1:]

        sample = transpose_list(datasets['input'])
        sample.append(transpose_list(datasets['output'])[0])
        sample = transpose_list(sample)
        file_name = 'dat_by_dataset_prio.csv'
        write_list_to_csv(sample, file_name)

        return True

    def split_dataset_by_type_and_priority(self, console=True):
        """ Divide dataset by type such as number vs string and input vs output.

        :param console:
        :return:
        """
        dataset_trans = transpose_list(self.dataset)
        datasets = {'in_num': [], 'in_str': [], 'out_num': [], 'out_str': []}
        for idx in range(len(self.dataset[0])):
            key = self.get_key_from_if_name(self.dataset[0][idx])
            if self.vars[key].dataset_prio == '':
                continue
            dataset_prio = int(self.vars[key].dataset_prio)
            if dataset_prio < 0:
                continue
            if dataset_prio == 0:
                if self.is_number_class(key):
                    datasets['out_num'].append(
                        # [self.vars[key].dataset_prio] + [dataset_trans[idx][0]] + [float(x) for x in dataset_trans[idx][1:]])
                        [self.vars[key].dataset_prio] + [key] + [float(x) for x in dataset_trans[idx][1:]])

                else:
                    datasets['out_str'].append([self.vars[key].dataset_prio] + [key] + [k for k in dataset_trans[idx][1:]])
            else:
                if self.is_number_class(key):
                    datasets['in_num'].append(
                        # [self.vars[key].dataset_prio] + [dataset_trans[idx][0]] + [float(x) for x in dataset_trans[idx][1:]])
                        [self.vars[key].dataset_prio] + [key] + [float(x) for x in dataset_trans[idx][1:]])
                else:
                    # datasets['in_str'].append([self.vars[key].dataset_prio] + [k for k in dataset_trans[idx]])
                    datasets['in_str'].append([self.vars[key].dataset_prio] + [key] + [k for k in dataset_trans[idx][1:]])

        datasets['in_num'] = sorted(datasets['in_num'], key=itemgetter(0))
        datasets['in_str'] = sorted(datasets['in_str'], key=itemgetter(0))

        len_datasets = {'in_num': len(datasets['in_num']), 'in_str': len(datasets['in_str']),
                        'out_num': len(datasets['out_num']), 'out_str': len(datasets['out_str'])}

        datasets['in_num'] = list(map(list, zip(*datasets['in_num'])))[1:]
        datasets['in_str'] = list(map(list, zip(*datasets['in_str'])))[1:]
        datasets['out_num'] = list(map(list, zip(*datasets['out_num'])))[1:]
        datasets['out_str'] = list(map(list, zip(*datasets['out_str'])))[1:]
        self.datasets = datasets

        msg = "\n\n"
        msg += DIV_LINE
        msg += "\n\n # Divide dataset by type, input vs output and number vs string."
        msg += "\n   > Input  number features = {:d}".format(len_datasets['in_num'])
        msg += "\n   > Input  string features = {:d}".format(len_datasets['in_str'])
        msg += "\n   > Output number features = {:d}".format(len_datasets['out_num'])
        msg += "\n   > Output string features = {:d}".format(len_datasets['out_str'])
        print_and_write(msg, console)

    def refine_dataset_col_by_priority(self, dataset=None, filename=None, logger=None):
        """ Refine dataset column by priority,
        generating the dataset having the features used in training and testing.

        :param dataset:
        :param filename:
        :param logger:
        :return:
        """
        if not dataset:
            dataset = self.dataset

        dataset_trans = transpose_list(dataset)

        arr = []
        for idx in range(len(dataset[0])):
            try: ##
                key = dataset[0][idx]
                # print(key)
                if self.vars[key].dataset_prio == '':
                    continue
                dataset_prio = int(self.vars[key].dataset_prio)
                if dataset_prio < 0:
                    continue
                else:
                    arr.append([self.vars[key].dataset_prio] + [key] + dataset_trans[idx][1:])
            except KeyError:
                print(key) ##

        arr_sorted = transpose_list(sorted(arr, key=itemgetter(0)))

        if filename:
            write_list_to_csv(arr_sorted[1:], filename)

        msg  = "Processing refine_dataset_col_by_priority : "
        msg += "{:d} x {:d} -> {:d} x {:d}".format(len(dataset), len(dataset[0]),
                                                   len(arr_sorted[1:]), len(arr_sorted[0]))
        if logger:
            logger.info(msg)
        self.log = msg

        return arr_sorted[1:]

    def refine_dataset_col_by_null_replacement(self, dataset=None, filename=None, logger=None):
        """ Refine dataset column by null replacement
        if the column feature has null replacement value.

        :param dataset:
        :param filename:
        :param logger:
        :return:
        """
        if not dataset:
            dataset = self.dataset

        trans_dataset = transpose_list(dataset)
        for idx in range(len(dataset[0])):
            key = dataset[0][idx]
            if self.vars[key].null_is:
                trans_dataset[idx] = [self.vars[key].null_is if x == "" else x for x in trans_dataset[idx]]

        msg = "Processing refine_dataset_col_by_null_replacement..."
        if logger:
            logger.info(msg)
        self.log = msg

        arr = lib.transpose_list(trans_dataset)
        if filename:
            write_list_to_csv(arr, filename)

        return arr

    def refine_dataset_row_by_null(self, dataset=None, filename=None, logger=None):
        """ Refine dataset row by removing the row having at least one null.

        :param dataset:
        :param filename:
        :param logger:
        :return:
        """
        if not dataset:
            dataset = self.dataset

        arr = []
        for row in dataset:
            try:
                row.index("")
            except ValueError:
                arr.append(row)

        if filename:
            write_list_to_csv(arr, filename)

        msg = "Processing refine_dataset_row_by_null : "
        msg += "{:d} x {:d} -> {:d} -> {:d}".format(len(dataset), len(dataset[0]), len(arr), len(arr[0]))
        if logger:
            logger.info(msg)
        self.log = msg

        return arr

    def refine_dataset_row_by_range(self, dataset=None, logger=None):
        """ Refine dataset row by deleting the row vector
        in which at least one sample doesn't satisfy its range requirement.

        :param dataset:
        :param logger:
        :return:
        """
        if not dataset:
            dataset = self.dataset

        dim = [len(dataset), len(dataset[0])]

        del_list = []
        for row_idx in range(1, len(dataset)):
            row_dat = dataset[row_idx]
            for col_idx in range(len(row_dat)):
                key = dataset[0][col_idx]
                ptr = self.vars[key]
                if key == 'SM_DSTL_LAD_SLU_EA_JDG_GD_TP' or key == 'SM_DSTL_LAD_IN_SLU_JDG_GD_TP':
                    break
                if self.is_number_class(key):
                    try:
                        val = float(row_dat[col_idx])
                        if not (ptr.min_thresh <= val <= ptr.max_thresh):
                            del_list.append(row_idx)
                            # print(key, row_dat[col_idx])
                            break
                    except TypeError:
                        del_list.append(row_idx)
                        # print(key, row_dat[col_idx])
                        break
                else:
                    try:
                        ptr.domain.index(row_dat[col_idx].lstrip('0'))
                    except ValueError:
                        del_list.append(row_idx)
                        # print(key, row_dat[col_idx])
                        break
                    except AttributeError as e:
                        print(e)
                        # print(key, row_dat[col_idx])

        del_list = sorted(del_list, reverse=True)
        print("Deleted " + str(len(del_list)) + " rows.")
        for idx in del_list:
            del dataset[idx]

        msg  = "Processing refine_dataset_row_by_range : "
        msg += "{:d} x {:d} -> {:d} -> {:d}".format(dim[0], dim[1], len(dataset), len(dataset[0]))
        if logger:
            logger.info(msg)
        self.log = msg

        return dataset

    def refine_dataset_row_by_process_type(self, dataset=None, cat=None, logger=None, option='L'):
        """ Refine dataset row by deleting the row vector
        in which at least one sample doesn't satisfy its processing type requirement.

        :param dataset:
        :param logger:
        :return:
        """

        if cat == 'RH':
            return dataset
        elif cat == 'CVT':
            pass

        if not dataset:
            dataset = self.dataset

        dim = [len(dataset), len(dataset[0])]

        del_list = []
        for row_idx in range(1, len(dataset)):
            row_dat = dataset[row_idx]
            for col_idx in range(len(row_dat)):
                key = dataset[0][col_idx]
                ptr = self.vars[key]
                # 중처리 제거조건 : 경처리 데이터 추출
                if key in ['CONV_TAP_AL_INPUT', 'BAP_ARR_O2', 'SM_STEEL_GRD']:
                    if self.is_number_class(key):
                        val = float(row_dat[col_idx])
                        if key == 'CONV_TAP_AL_INPUT':
                            if not (val <= 100): # 50 to 100
                                del_list.append(row_idx)
                                break
                        # elif key == 'BAP_ARR_O2':
                        #     if not (val >= 100):
                        #         del_list.append(row_idx)
                        #         break

                    # else:
                    #     if 'U' not in row_dat[col_idx]:
                    #         del_list.append(row_idx)
                    #         break
                else:
                    continue

        del_list = sorted(del_list, reverse=True)
        heavy_list = []
        for idx in del_list:
            if option == 'L':
                del dataset[idx]
            elif option == 'H':
                heavy_list.append(dataset[idx])
            elif option == 'B': # both
                pass

        if option == 'H':
            dataset = [dataset[0]] + heavy_list

        msg  = "Processing refine_dataset_row_by_process_type({}) : ".format(option)
        msg += "{:d} x {:d} -> {:d} x {:d}".format(dim[0], dim[1], len(dataset), len(dataset[0]))
        if logger:
            logger.info(msg)
        self.log = msg

        return dataset

    def check_var_process_type(self, var_dict, cat=None, logger=None):
        """ Check variable process type.

        :param cat:
        :param var_dict:
        :param logger:
        :return:
        """
        if cat == 'RH':
            return None
        elif cat == 'CVT':
            pass

        PROCESS_TYPE = None
        CONV_TAP_AL_INPUT_ON = False
        BAP_ARR_O2_ON = False

        for key in var_dict:
            # print(key)
            if self.is_number_class(key):
                try:
                    val = abs(float(var_dict[key]))
                    if key == 'CONV_TAP_AL_INPUT':
                        if val > 100: # 50 to 100
                            CONV_TAP_AL_INPUT_ON = True
                    # elif key == 'BAP_ARR_O2':
                    #     if val < 100:
                    #         BAP_ARR_O2_ON = True

                except (KeyError, TypeError):
                    pass
                    print(key)

        if CONV_TAP_AL_INPUT_ON == True:
            PROCESS_TYPE = 'H'
        else:
            PROCESS_TYPE = 'L'

        msg = "Operation process type is {}".format(PROCESS_TYPE)
        if logger:
            logger.info(msg)
        else:
            print(msg)

        return PROCESS_TYPE


    def check_var_ranges(self, var_dict, fix_=False, logger=None):
        """ Check variable statistics.

        :param var_dict:
        :param fix_:
        :param logger:
        :return:
        """
        range_error_list = {}
        for key in var_dict:
            # print(key)
            if self.vars[key].dataset_prio < 0:
                continue
            if self.is_number_class(key):
                if self.vars[key].min_thresh is not None \
                        and self.vars[key].max_thresh is not None:
                    try:
                        val = float(var_dict[key])
                    except Exception as e:
                        logger.error("check_var_ranges : number : " + str(e))
                        val = 0
                    if val < self.vars[key].min_thresh or val > self.vars[key].max_thresh:
                        if fix_:
                            if key == "CONV_TAP_AL_ALLOY_INPUT":
                                var_dict[key] = 0 if self.vars['RH_TREAT_PATTERN'].val == 'L' else 300
                            elif key == "CONV_TAP_QUICKLIME_ALLOY_INPUT":
                                var_dict[key] = 900 if self.vars['RH_TREAT_PATTERN'].val == 'L' else 400
                            elif self.vars[key].default_val:
                                var_dict[key] = float(self.vars[key].default_val)
                            else:
                                var_dict[key] = 0
                        range_error_list[key] = val
            else:
                if self.vars[key].domain is not None:
                    try:
                        # print("domain: {} <- {}".format(str(var_dict[key]), str(self.vars[key].domain)))
                        self.vars[key].domain.index(var_dict[key])
                    except ValueError:
                        if fix_:
                            if self.vars[key].default_val:
                                var_dict[key] = self.vars[key].default_val
                            else:
                                var_dict[key] = self.vars[key].domain[0]
                        range_error_list[key] = var_dict[key]
                    except Exception as e:
                        logger.error("check_var_ranges : number : " + str(e))
                        var_dict[key] = self.vars[key].domain[0]
        msg = "range error list: {}".format(range_error_list)
        if range_error_list:
            logger.warning(msg) if logger else print(msg)
            return False, range_error_list
        else:
            return True, None

    def check_var_tolerances(self, est_dict, var_dict, logger=None):
        """ Check variable tolerances.

        :param est_dict:
        :param var_dict:
        :param logger:
        :return:
        """
        range_error_list = {}
        for key in var_dict:
            # print(key)
            if self.vars[key].dataset_prio < 0:
                continue
            if self.is_number_class(key):
                if self.vars[key].tolerance:
                    try:
                        val = abs(float(var_dict[key]) - float(est_dict[key]))
                    except (KeyError, TypeError, ValueError):
                        val = 0
                        print(key)
                    if val < self.vars[key].tolerance[0] or val > self.vars[key].tolerance[1]:
                        range_error_list[key] = val
        msg = "Tolerance error list: {}".format(range_error_list)
        if range_error_list:
            if logger:
                logger.error(msg)
            else:
                print(msg)
            return False, range_error_list
        else:
            return True, None

    def init_feat_class(self, cfg, offset=0):
        """ Initialize feature classes.

        :param cfg:
        :param offset:
        :return:
        """
        self.recv_enum_list = lib.complete_enum_list(cfg['stream']['req_enum_list'])
        self.send_enum_list = lib.complete_enum_list(cfg['stream']['rep_enum_list'])
        self.db_est_enum_list  = lib.complete_enum_list(cfg['DB']['est_enum_list'])
        self.db_eval_enum_list = lib.complete_enum_list(cfg['DB']['eval_enum_list'])

        config = cfg[VAR_INFO_CSV]
        enum_pos = convert_char_to_num(config[ENUM_INDEX]) - offset
        category_pos = convert_char_to_num(config[CATEGORY]) - offset
        object_name_pos = convert_char_to_num(config[OBJECT_NAME]) - offset
        dtype_posco_pos = convert_char_to_num(config[DTYPE_POSCO]) - offset
        length_pos = convert_char_to_num(config[LENGTH]) - offset
        decimal_pos = convert_char_to_num(config[DECIMAL]) - offset
        position_pos = convert_char_to_num(config[POSITION]) - offset
        data_type_pos = convert_char_to_num(config[DATA_TYPE]) - offset
        range_min_pos = convert_char_to_num(config[RANGE_MIN]) - offset
        range_max_pos = convert_char_to_num(config[RANGE_MAX]) - offset
        range_default_pos = convert_char_to_num(config[RANGE_DEFAULT]) - offset
        str_domain_pos = convert_char_to_num(config[STR_DOMAIN]) - offset
        null_is_pos = convert_char_to_num(config[NULL_IS]) - offset
        dataset_pos = convert_char_to_num(config[DATASET]) - offset
        prio_pos = convert_char_to_num(config[PRIORITY]) - offset
        tolerance_pos = convert_char_to_num(config[TOLERANCE]) - offset
        var_if_name_pos = convert_char_to_num(config[VAR_IF_NAME]) - offset

        for row_dat in self.mtx[1:]:
            enum_idx = int(row_dat[enum_pos])
            category = row_dat[category_pos].strip()
            var_name = row_dat[object_name_pos].strip()
            var_dtype = row_dat[dtype_posco_pos].strip()
            length = row_dat[length_pos].strip()
            decimal = row_dat[decimal_pos].strip()
            position = row_dat[position_pos].strip()
            var_type = row_dat[data_type_pos].strip()
            prio_posco = row_dat[prio_pos]
            null_is = row_dat[null_is_pos]
            dataset_prio = int(row_dat[dataset_pos]) if row_dat[dataset_pos].strip() else PRIORITY_OFF
            prio_posco = int(prio_posco) if prio_posco else -1
            tolerance = [float(x.strip()) for x in row_dat[tolerance_pos].split(',')] if row_dat[tolerance_pos] else ''
            var_if_name = row_dat[var_if_name_pos].strip()
            range_default = row_dat[range_default_pos]

            # print(' # Initializing {} ({}) ...'.format(var_if_name, var_name))
            if var_type == 'Int' or var_type == 'Float':
                min_val = float(row_dat[range_min_pos])
                max_val = float(row_dat[range_max_pos])
                min_val = int(min_val) if var_type == 'Int' else min_val
                max_val = int(max_val) if var_type == 'Int' else max_val
                self.vars[var_name] = self.NumClass(name=var_name,
                                                    if_name=var_if_name,
                                                    category=category,
                                                    idx=enum_idx,
                                                    stream_dtype=var_dtype,
                                                    stream_length=length,
                                                    stream_decimal=decimal,
                                                    stream_position=position,
                                                    var_type=var_type,
                                                    min_val=min_val,
                                                    max_val=max_val,
                                                    default_val=range_default,
                                                    priority=prio_posco,
                                                    null_is=null_is,
                                                    dataset_prio=dataset_prio,
                                                    tolerance=tolerance)
            elif var_type == 'String':
                str_domain = self.parse_string_domain(row_dat[str_domain_pos])
                self.vars[var_name] = self.StrClass(name=var_name,
                                                    if_name=var_if_name,
                                                    category=category,
                                                    idx=enum_idx,
                                                    stream_dtype=var_dtype,
                                                    stream_length=length,
                                                    stream_decimal=decimal,
                                                    stream_position=position,
                                                    default_val=range_default,
                                                    domain=str_domain,
                                                    priority=prio_posco,
                                                    null_is=null_is,
                                                    dataset_prio=dataset_prio)
            else:
                pass
        pass

    def init_statistical_properties(self, dataset):
        """ Initialize the statistical properties of all features.

        :param dataset:
        :return:
        """
        self.dataset = dataset
        self.num_vectors = len(self.dataset) - 1
        trans_dat_mtx = list(map(list, zip(*self.dataset)))
        for key in self.vars:
            try: #
                if self.vars[key].if_name:
                    self.vars[key].get_statistical_properties(trans_dat_mtx[dataset[0].index(self.vars[key].if_name)][1:])
            except ValueError: #
                continue #
        pass

    def update_dataset_by_null_is(self, dataset):
        trans_dataset = transpose_list(dataset)
        for if_name in dataset[0]:
            try: #
                key = self.get_key_from_if_name(if_name)
                if self.vars[key].null_is:
                    idx = dataset[0].index(if_name)
                    trans_dataset[idx] = [self.vars[key].null_is if x == "" else x for x in trans_dataset[idx]]
            except KeyError: #
                print(key)
                continue #
        return list(map(list, zip(*trans_dataset)))

    class NumClass(object):
        """ Class for number feature.

        """

        def __init__(self,
                     name='',
                     if_name='',
                     category='',
                     idx=None,
                     stream_dtype='',
                     stream_length=-1,
                     stream_decimal=-1,
                     stream_position=-1,
                     var_type=None,
                     min_val=None,
                     max_val=None,
                     default_val=None,
                     priority=None,
                     null_is='',
                     tolerance='',
                     dataset_prio=-1):
            self.name = name
            self.if_name = if_name
            self.category = category
            self.enum_idx = idx
            self.stream_dtype = stream_dtype
            self.stream_length = int(stream_length) if stream_length else -1
            self.stream_decimal = int(stream_decimal) if stream_decimal else -1
            self.stream_position = int(stream_position) if stream_position else -1
            self.var_type = var_type
            self.min_thresh = min_val
            self.max_thresh = max_val
            self.default_val = default_val
            self.priority = priority
            self.null_is = null_is
            self.tolerance = tolerance if tolerance else [0, 0]
            self.dataset_prio = dataset_prio

            self.null_num = -1
            self.min_below = -1
            self.max_above = -1
            self.mean = infinity
            self.std = infinity
            self.val = infinity
            self.dat_arr = []

        def get_statistical_properties(self, dat):
            dat = ["" if val == DIV_0_ERR else val for val in dat]
            dat = [self.null_is if val == "" else val for val in dat]
            self.null_num = dat.count('')
            self.min_below = 0
            self.max_above = 0
            dat_arr = []
            for val in dat:
                if val:
                    if float(val) < self.min_thresh:
                        self.min_below += 1
                    elif self.max_thresh < float(val):
                        self.max_above += 1
                    else:
                        dat_arr.append(float(val))

            dat_arr = np.array(dat_arr)
            self.mean = dat_arr.mean()
            self.std = dat_arr.std()
            self.dat_arr = dat_arr


    class StrClass(object):
        """ Class for string feature.

        """

        def __init__(self,
                     name='',
                     if_name='',
                     category='',
                     idx=None,
                     stream_dtype='',
                     stream_length=-1,
                     stream_decimal=-1,
                     stream_position=-1,
                     default_val=None,
                     domain='',
                     priority=None,
                     null_is='',
                     dataset_prio=-1):
            self.name = name
            self.if_name = if_name
            self.category = category
            self.enum_idx = idx
            self.stream_dtype = stream_dtype
            self.stream_length = int(stream_length) if stream_length else -1
            self.stream_decimal = int(stream_decimal) if stream_decimal else -1
            self.stream_position = int(stream_position) if stream_position else -1
            self.default_val = default_val
            self.domain = domain
            self.priority = priority
            self.null_is = null_is
            self.dataset_prio = dataset_prio
            self.val = ''

            self.null_num = -1

        def get_statistical_properties(self, dat):
            self.null_num = dat.count("")

    @staticmethod
    def parse_string_domain(str_domain):
        if not str_domain:
            return ''
        domain = [val.strip() for val in str_domain.split(',')]
        try:
            idx = domain.index('~')
            if len(domain[0]) >= 8:
                return "YYYYMMDD"
            if 0 < idx < len(domain):
                del domain[idx]
                for val in range(int(domain[idx])-1, int(domain[idx-1]), -1):
                    domain.insert(idx, str(val))
            return domain
        except ValueError:
            return domain

    def check_null(self):
        pass

    def extract_num_variables(self):
        """ Extract number type input and output variables from class.

        :return:
        """
        for key in self.vars:
            if self.vars[key].__class__.__name__ == "NumClass":
                var_dict = {'idx': self.vars[key].enum_idx,
                            'dat': [x if x != "" else self.vars[key].mean for x in self.vars[key].dat]}
                if self.vars[key].if_name.find('Output') != -1:
                    self.vars_num_out[key] = var_dict
                else:
                    self.vars_num_in[key] = var_dict
        pass

    def sort_features_by_var(self, console=True):
        for key in self.vars_num_in:
            self.vars_num_in[key]['var'] = np.array(self.vars_num_in[key]['dat'], dtype=np.float).var()
        for key in self.vars_num_out:
            self.vars_num_out[key]['var'] = np.array(self.vars_num_out[key]['dat'], dtype=np.float).var()
        pass

        """
        prt = '\n\n*'
        for k in range(140):
            prt += '*'
        prt += "\n\n # Input variable enumeration based on STD normalized by output variables"
        prt += "\n\n| {:>30} |".format("Variable name")
        prt += "| {:>5} |".format("Index")
        for out_key in std_dict_out:
            prt += " {:>30} |".format("STD by {}".format(out_key))
        prt += '\n-'
        for k in range(140):
            prt += '-'
        for col_idx in range(len(std_dicts[0])):
            prt += "\n"
            prt += "| {:>30} |".format(std_dicts[0][col_idx][0])
            prt += "| {:5d} |".format(std_dicts[0][col_idx][1])
            for row_idx in range(len(std_dicts)):
                prt += " {:30.2f} |".format(std_dicts[row_idx][col_idx][2])
        """
        msg = ""
        print_and_write(msg, console=console)
        pass

    def sort_vars_by_feature_selection_test(self, method='chi2', console=False):
        pass

    def write_db_table_info_csv(self, enum_list, filename):
        """ Write DB table information csv file.

        :param enum_list:
        :param filename:
        :return:
        """
        if not filename:
            return False
        with open(filename, 'w') as f:
            f.write("{:<64} {}".format("column_name,", "data_type"))
            for idx in enum_list:
                key = self.get_key_from_enum_idx(idx)
                # print(key)
                if self.is_number_class(key):
                    if self.vars[key].var_type == 'Float':
                        data_type = 'float'
                    elif self.vars[key].var_type == 'Int':
                        data_type = 'int'
                    else:
                        data_type = ''
                else:
                    data_type = "varchar(1024)"
                key += ','
                f.write("\n{:<64} {}".format(key, data_type))
        return True

    def rename_dataset_col(self, filename, dataset=''):
        if not dataset:
            dataset = copy.deepcopy(self.dataset)
        for idx in range(len(dataset[0])):
            dataset[0][idx] = self.get_key_from_if_name(dataset[0][idx])
        with open(filename, 'w') as f:
            for row_data in dataset:
                f.write(','.join(row_data) + '\n')
        pass

    def add_dataset_col(self, filename, dataset='', added_dataset=''):
        if not dataset:
            dataset = copy.deepcopy(self.dataset)

        dataset_df = pd.DataFrame(dataset, columns=dataset[0])
        added_df = pd.DataFrame(added_dataset, columns=added_dataset[0])

        dataset_df_no_dups = dataset_df # .drop_duplicates(subset='CHARGE_NO')
        added_df_no_dups = added_df.drop_duplicates(subset='CHARGE_NO')

        refined_df = pd.merge(added_df_no_dups, dataset_df_no_dups, on='CHARGE_NO', how='inner') # 'inner' : 두 데이터의 교집합, 'outer' :  두 데이터의 합집합

        # refined_dataset_header = refined_df.columns.tolist()
        refined_dataset = refined_df.values.tolist()

        lib.write_list_to_csv(refined_dataset, filename)

        pass

    def decode_stream_comma(self, stream, dbg_=False, logger=None):
        stream = [var.strip() for var in stream.split(',')]
        for idx in self.recv_enum_list:
            key = self.get_key_from_enum_idx(idx)
            if not key:
                continue
            try:
                val = stream[idx - 1]
            except IndexError:
                print('IndexError:', key)
                val = -1
            try:
                var = self.vars[key]
            except KeyError:
                print('KeyError:', key)
                val = -2

            if var.stream_dtype.lower() == 'number':
                if val == '' and var.null_is:
                    val = var.null_is
                try:
                    val = float(val) if self.vars[key].var_type == 'Float' else int(val)
                except ValueError as e:
                    logger.error("decode_stream exception of number : " + str(e))
                    val = 0
                except Exception as e:
                    logger.error("decode_stream exception of number : {} = {}".format(key, str(val)) + str(e))

                self.req_stream_data[key] = self.vars[key].val = val

            else:
                dm = self.vars[key].domain
                if ''.join(map(str,dm)).isdigit():
                    try:
                        val = str(int(val))
                    except Exception as e:
                        if logger:
                            logger.error("change number format exception of string : " + str(e))
                        val = val
                try:
                    self.req_stream_data[key] = self.vars[key].val = str(val)
                except Exception as e:
                    if logger:
                        logger.error("decode_stream exception of string : " + str(e))
                    self.req_stream_data[key] = self.vars[key].val = ''

        self.exception_RH_VESSEL_CD(logger=logger)

        if dbg_:
            print("\n # Parsed stream data")
            for key, value in self.req_stream_data.items():
                print(" > {:>30} : {}".format(key, value))

        return self.req_stream_data

    def exception_RH_VESSEL_CD(self, logger):
        try:
            list(self.vars.keys()).index('RH_VESSEL_CD')
            try:
                self.vars['RH_VESSEL_CD'].domain.index(self.req_stream_data['RH_VESSEL_CD'])
            except ValueError:
                val = ''
                if self.req_stream_data['LAST_OP'] == "R1" or self.req_stream_data['LAST_OP'] == "R3":
                    if self.req_stream_data['RH_VESSEL_CD'] == '1':
                        val = 'N'
                    elif self.req_stream_data['RH_VESSEL_CD'] == '2':
                        val = 'S'
                    else:
                        pass
                elif self.req_stream_data['LAST_OP'] == 'R2':
                    if self.req_stream_data['RH_VESSEL_CD'] == '1':
                        val = 'E'
                    elif self.req_stream_data['RH_VESSEL_CD'] == '2':
                        val = 'W'
                    else:
                        pass
                else:
                    pass
                if not val:
                    logger.error("RH_VESSEL_CD error: {} and {}".format(self.req_stream_data['LAST_OP'],
                                                                        self.req_stream_data['RH_VESSEL_CD']))
                self.req_stream_data['RH_VESSEL_CD'] = self.vars['RH_VESSEL_CD'].val = val
            except Exception as e:
                logger.error("decode_stream exception RH domain : {}".format(self.req_stream_data['RH_VESSEL_CD']) +
                             str(e))
        except ValueError:
            pass
        except Exception as e:
            logger.error("decode_stream exception RH check : " + str(e))

    def decode_stream(self, stream, dbg_=False, logger=None):
        """ Parse request stream.

        :param stream:
        :param dbg_:
        :param logger:
        :return:
        """
        self.req_stream_data = {}
        position = 1
        for idx in self.recv_enum_list:
            key = self.get_key_from_enum_idx(idx)
            val = stream[position - 1:position + self.vars[key].stream_length - 1].strip()
            position += self.vars[key].stream_length
            # print("{} : {} : {:d}".format(key, val, position))
            if self.vars[key].stream_dtype.lower() == 'number':
                if not val:
                    if self.vars[key].null_is:
                        val = self.vars[key].null_is
                        if logger:
                            logger.warning("decode_stream : {} is NULL".format(key))
                    else:
                        val = 0
                        if logger:
                            logger.error("decode_stream : {} is NULL".format(key))
                try:
                    val = float(val)
                except ValueError as e:
                    logger.error("decode_stream exception of number : " + str(e))
                    val = 0
                except Exception as e:
                    logger.error("decode_stream exception of number : {} = {}".format(key, str(val)) + str(e))
                self.req_stream_data[key] = self.vars[key].val = val
            else:
                try:
                    val = str(int(val))
                except ValueError:
                    pass
                try:
                    self.req_stream_data[key] = self.vars[key].val = str(val)
                except Exception as e:
                    if logger:
                        logger.error("decode_stream exception of string : " + str(e))
                    self.req_stream_data[key] = self.vars[key].val = ''

        try:
            list(self.vars.keys()).index('RH_VESSEL_CD')
            try:
                self.vars['RH_VESSEL_CD'].domain.index(self.req_stream_data['RH_VESSEL_CD'])
            except ValueError:
                val = ''
                if self.req_stream_data['LAST_OP'] == "R1" or self.req_stream_data['LAST_OP'] == "R3":
                    if self.req_stream_data['RH_VESSEL_CD'] == '1':
                        val = 'N'
                    elif self.req_stream_data['RH_VESSEL_CD'] == '2':
                        val = 'S'
                    else:
                        pass
                elif self.req_stream_data['LAST_OP'] == 'R2':
                    if self.req_stream_data['RH_VESSEL_CD'] == '1':
                        val = 'E'
                    elif self.req_stream_data['RH_VESSEL_CD'] == '2':
                        val = 'W'
                    else:
                        pass
                else:
                    pass
                if not val:
                    logger.error("RH_VESSEL_CD error: {} and {}".format(self.req_stream_data['LAST_OP'],
                                                                        self.req_stream_data['RH_VESSEL_CD']))
                self.req_stream_data['RH_VESSEL_CD'] = self.vars['RH_VESSEL_CD'].val = val
            except Exception as e:
                logger.error("decode_stream exception RH domain : {}".format(self.req_stream_data['RH_VESSEL_CD']) +
                             str(e))
        except ValueError:
            pass
        except Exception as e:
            logger.error("decode_stream exception RH check : " + str(e))

        if dbg_:
            print("\n # Parsed stream data")
            for key, value in self.req_stream_data.items():
                print(" > {:>30} : {}".format(key, value))

        return self.req_stream_data

    def encode_stream(self, dbg_=False):
        """ Encode reply stream.

        :param dbg_:
        :return:
        """
        stream = []
        for idx in self.send_enum_list:
            key = self.get_key_from_enum_idx(idx)
            val = self.vars[key].val
            if key == 'TRANSACTION_CD':
                if self.vars[key].val == 'SM2SRHOB':
                    val = 'SM2RRHOB'
                elif self.vars[key].val == 'SM2SCONV':
                    val = 'SM2RCONV'
            if self.vars[key].stream_dtype.lower() == 'char':
                stream.append("{v:<{l}}".format(v=val, l=self.vars[key].stream_length))
            else:
                stream.append("{v:{l}.{d}f}".format(v=val, l=self.vars[key].stream_length,
                                                    d=self.vars[key].stream_decimal))
        stream = ''.join(stream)

        if dbg_:
            print("\n # Encoded stream")
            print(' > "{}"'.format(stream))
            print(' > length : {:d}'.format(len(stream)))

        return stream

    def analyze_dataset_in_eval_range(self, dataset=None):
        """

        :param dataset:
        """
        if not dataset:
            dataset = self.dataset

        dim = [len(dataset), len(dataset[0])]

        eval_range_check_vars_idx = dataset[0].index('EVAL_RANGE_CHECK_VARS')

        analysis_dict = {}
        count_dict = defaultdict(int)
        for row_idx in range(1, len(dataset)):
            eval_range_check_vars_dat = dataset[row_idx][eval_range_check_vars_idx]

            contents = eval_range_check_vars_dat[1:-1]      # strip off leading { and trailing }
            items = contents.split(', ')                    # each individual item looks like key:value
            pairs = [item.split(': ', 1) for item in items] # ("key","value"), both strings

            for (key, value) in pairs:
                try:
                    if key in analysis_dict:
                        analysis_dict[key].append(eval(value))  # evaluate values but not strings
                    else:
                        analysis_dict[key] = [eval(value)]

                    count_dict[key] += 1
                except NameError:
                    # analysis_dict[key] = value  # evaluate values but not strings
                    pass

        return count_dict




def convert_char_to_num(char):
    """Convert one-digit alphabet character to number.

    :param char:
    :return:
    """
    if len(char) > 1:
        print(" @ Error: length MUST be less than 'AA', {}\n".format(char))
        sys.exit()

    val = ord(char) - ord('A')
    if val >= 0:
        return val
    else:
        return int(char)


def crop_mtx(csv_mtx, start_pos, end_pos):
    """Crop the rectangle shaped cells from  csv format list

    :param csv_mtx:
    :param start_pos:
    :param end_pos:
    :return:
    """
    roi_mtx = []
    for yi in range(end_pos[0], end_pos[1] + 1):
        roi_mtx.append(csv_mtx[yi][start_pos[0]:start_pos[1] + 1])
    return roi_mtx


def print_and_write(msg, console=True):
    if console:
        print(msg)
    if ANALYTICS_RESULT_FILE:
        with open(ANALYTICS_RESULT_FILE, 'a') as fid:
            fid.write(msg)
    pass


def calc_crop_info_from_ini(cfg):
    x_start = convert_char_to_num(cfg['x_start'])
    x_end = convert_char_to_num(cfg['x_end'])
    y_start = int(cfg['y_start']) - 1
    y_end = int(cfg['y_end']) - 1
    return (x_start, x_end), (y_start, y_end)


def transpose_list(in_list):
    return list(map(list, zip(*in_list)))


def write_list_to_csv(dataset, filename):
    if filename:
        with open(filename, 'w') as f:
            for row_dat in dataset:
                f.write(','.join([str(x) for x in row_dat]) + '\n')
    pass


def split_dataset_into_train_and_test(dataset, percent_ratio, offset=1):
    train_dataset = []
    test_dataset = []
    for idx in range(offset):
        train_dataset.append(dataset[idx])
        test_dataset.append(dataset[idx])
    cnt_train = 0
    cnt_test = 0
    for idx in range(offset, len(dataset)):
        if random.randint(0, 100) < percent_ratio:
            train_dataset.append(dataset[idx])
            cnt_train += 1
        else:
            test_dataset.append(dataset[idx])
            cnt_test += 1
    print("\n # data set division: ({:d}, {:d}) -> {:2d}".format(
        cnt_train, cnt_test, int(cnt_train * 100. / float(cnt_train + cnt_test))))
    return train_dataset, test_dataset


def main_stream_parsing(args):
    config = configparser.ConfigParser()
    config.read(args.var_ini_file)

    var_mtx = lib.read_csv_file(args.var_csv_file)
    start_pos, end_pos = calc_crop_info_from_ini(config[VAR_INFO_CSV])
    roi_var_mtx = crop_mtx(var_mtx, start_pos, end_pos)

    Feat = PoscoTempEstModel(roi_var_mtx)
    Feat.init_feat_class(config, offset=start_pos[0])

    with open(args.posco_stream_file, 'r') as f:
        posco_stream = f.read()
    Feat.decode_stream(posco_stream, dbg_=True)

def select_data_by_condition(dataset, prev_time_thresh, curr_time_thresh):
    """

    :param dataset:
    :param prev_time_thresh:
    :param curr_time_thresh:
    """

    out_list = []
    tap_work_date_idx = dataset[0].index('TAP_WORK_DATE')

    out_list.append(dataset[0])
    for idx in range(1, len(dataset)):
        tap_work_date = dataset[idx][tap_work_date_idx]

        if prev_time_thresh <= tap_work_date <= curr_time_thresh:
            out_list.append(dataset[idx])
        else:
            continue

    return out_list



def calculate_temp_diff(dataset):
    """

    :param in_list:
    """

    out_list = []
    trans_dataset = lib.transpose_list(dataset)
    tap_work_date_idx = dataset[0].index('TAP_WORK_DATE')
    eval_temp_idx = dataset[0].index('EVAL_TEMP')
    conv_endpnt_temp_idx = dataset[0].index('CONV_ENDPNT_TEMP')

    outlier_cnt = 0
    normal_cnt = 0
    out_list.append(['TAP_WORK_DATE', 'TEMP_DIFF'])
    for idx in range(1, len(dataset)):
        tap_work_date = dataset[idx][tap_work_date_idx]
        eval_temp = dataset[idx][eval_temp_idx]
        conv_endpnt_temp = dataset[idx][conv_endpnt_temp_idx]
        temp_diff = abs(float(eval_temp) - float(conv_endpnt_temp))
        if temp_diff >= 1000:
            outlier_cnt += 1
            continue
        elif temp_diff <= 10:
            normal_cnt += 1
        out_list.append([tap_work_date, temp_diff])

    print("\n # Total data : {}, Outlier data(>1000) : {} -> {}, Normal data(<=10) : {}".format(len(dataset)-1,
                                                                                                outlier_cnt,
                                                                                                len(dataset)-1-outlier_cnt,
                                                                                                normal_cnt))
    print("\n # Data accuracy : Norm / Outlier data -> {}".format(normal_cnt / (len(dataset) - 1 - outlier_cnt) * 100))

    return out_list

def avg_value_in_same_column(in_list):
    """

    :param in_list:
    """

    out_list = []
    # tap_work_date_idx = in_list[0].index('TAP_WORK_DATE')
    # temp_diff_idx = in_list[0].index('TEMP_DIFF')

    out_list.append(['TAP_WORK_DATE', 'LAST_TEMP_DIFF', 'MEAN_TEMP_DIFF'])
    in_list_columns = in_list[0]

    df = pd.DataFrame(in_list[1:], columns=in_list_columns)

    df['MEAN_TEMP_DIFF'] = df.groupby(['TAP_WORK_DATE'])['TEMP_DIFF'].transform('mean')
    df_refine = df.groupby(['TAP_WORK_DATE'], as_index=False).last()

    refine_dataset = df_refine.values.tolist()
    for row in refine_dataset:
        out_list.append(row)

    return out_list

def main():
    """ Main code.

    """
    global ANALYTICS_RESULT_FILE
    global g_logger

    parser = argparse.ArgumentParser()
    parser.add_argument("--var_csv_file", required=True, help="csv file for variables")
    parser.add_argument("--var_ini_file", required=True, help="ini file for configuration")
    parser.add_argument("--dat_csv_file", default="", help="csv file for data set")
    parser.add_argument("--result_file",  default="", help="result file")
    parser.add_argument("--cfg_ini_file", required=True, help="configuration ini file")

    parser.add_argument("--analytics_db_table_", default=False, action='store_true',
                        help="flag to analyze DB est/eval tables")
    parser.add_argument("--gen_db_table_", default=False, action='store_true',
                        help="flag to generate DB est/eval tables")
    parser.add_argument("--statistics_",   default=False, action='store_true',
                        help="Flag to run statistics function")
    parser.add_argument("--gen_dataset_", default=False, action='store_true',
                        help="flag to save train and test datasets")

    parser.add_argument("--feature_selection_", default=False, action='store_true',
                        help="flag to run feature selection functions")

    parser.add_argument("--rename_dataset_col", default="", help="csv file with renamed column names")
    parser.add_argument("--add_dataset_col", default="", help="csv file with added column datas")
    parser.add_argument("--hist_folder", default="", help="variable histogram image file folder")

    parser.add_argument("--posco_stream_file", default="", help="posco stream text file")
    parser.add_argument("--test_stream_file", default="", help="test stream filename")
    args = parser.parse_args()

    g_logger = lib.setup_logger(CAT,
                                '_',
                                folder='log',
                                console_=True)
    if args.posco_stream_file:
        main_stream_parsing(args)
        return

    if args.test_stream_file:
        args.gen_dataset_ = True

    ANALYTICS_RESULT_FILE = args.result_file
    if ANALYTICS_RESULT_FILE:
        with open(ANALYTICS_RESULT_FILE, 'w') as fid:
            fid.write("\n # Analytics results")

    cfg = configparser.ConfigParser()
    cfg.read(args.var_ini_file)

    ## 181219
    ini = configparser.ConfigParser()
    ini.read(args.cfg_ini_file)

    var_mtx = lib.read_csv_file(args.var_csv_file)
    dat_mtx = lib.read_csv_file(args.dat_csv_file)

    start_pos, end_pos = calc_crop_info_from_ini(cfg[VAR_INFO_CSV])
    roi_var_mtx = crop_mtx(var_mtx, start_pos, end_pos)

    Feat = PoscoTempEstModel(roi_var_mtx)
    Feat.init_feat_class(cfg, offset=start_pos[0])

    ## For analytics_db_table_
    est_db_handler = None
    if DB_OP_:
        est_db_handler = PyMssqlWrapper(server_name=ini['EST_DB']['server_name'],
                                        port=ini['EST_DB']['port'],
                                        username=ini['EST_DB']['username'],
                                        password=ini['EST_DB']['password'],
                                        db_name=ini['EST_DB']['db_name'],
                                        table_name=ini['EST_DB']['table_name'])
        eval_db_handler = PyMssqlWrapper(server_name=ini['EVAL_DB']['server_name'],
                                         port=ini['EVAL_DB']['port'],
                                         username=ini['EVAL_DB']['username'],
                                         password=ini['EVAL_DB']['password'],
                                         db_name=ini['EVAL_DB']['db_name'],
                                         table_name=ini['EVAL_DB']['table_name'])

    if args.analytics_db_table_:
        print("\n # Analyze Est/Eval DB table information...")

        prev_time_thresh = '20181127' #
        curr_time_thresh = (datetime.datetime.now()).strftime('%Y%m%d')

        time_conds = ["{0}<{1}".format(prev_time_thresh, "TAP_WORK_DATE"),
                      "{0}<={1}".format("TAP_WORK_DATE", curr_time_thresh)]

        if DB_OP_:
            eval_db_columns = [x[0] for x in eval_db_handler.selectColumnNames()]
            eval_db_data = eval_db_handler.selectDataByConditions(cond_list=time_conds)
            eval_dataset = [eval_db_columns] + eval_db_data
        else:
            eval_raw_dataset = lib.read_csv_file("181219_CVT_EVAL_DB.csv") #
            eval_dataset = select_data_by_condition(eval_raw_dataset, prev_time_thresh, curr_time_thresh)

        # Step 1. Calculate temperature difference
        temp_dff_arr = calculate_temp_diff(eval_dataset)
        # mean_temp_dff_arr = avg_value_in_same_column(temp_dff_arr)

        ## Draw temp diff plot
        x_range = np.arange(0, len(temp_dff_arr[1:]), )
        plt.plot(x_range, lib.transpose_list(temp_dff_arr)[1][1:], 'r-', label='temp_diff')

        plt.title('Difference between EVAL_TEMP and CONV_ENDPNT_TEMP')
        plt.xlabel('Time difference')
        plt.ylabel('Temperatue difference')
        plt.legend()
        plt.savefig('temp_diff.png')
        # plt.show(block=True)
        plt.close()

        # Step 2. Analyze out of range datas
        count_dict = Feat.analyze_dataset_in_eval_range(eval_dataset)
        print(count_dict)
        print('Success')


    if args.gen_db_table_:
        print("\n # Write Est/Eval DB table information...")
        Feat.write_db_table_info_csv(Feat.db_est_enum_list,  cfg['DB']['est_table_info_csv'])
        Feat.write_db_table_info_csv(Feat.db_eval_enum_list, cfg['DB']['eval_table_info_csv'])
        return

    if args.rename_dataset_col:
        print("\n # Write new dataset after replacing column name...")
        Feat.rename_dataset_col(args.rename_dataset_col, copy.deepcopy(dat_mtx))
        return

    if args.add_dataset_col:
        ans = input('Please enter a filename to add:')
        added_dat_mtx = lib.read_csv_file("dataset_raw/190412_data/" + ans)

        print("\n # Write new dataset after adding column datas...")
        Feat.add_dataset_col(args.add_dataset_col, copy.deepcopy(dat_mtx), copy.deepcopy(added_dat_mtx))
        return

    if args.statistics_:
        dat_mtx = Feat.update_dataset_by_null_is(dat_mtx)
        Feat.init_statistical_properties(dat_mtx)
        Feat.analyze_dataset(dat_mtx)
        Feat.print_analytics_result(console=True)
        # if args.hist_folder:
        #     print("\n # Write the histogram plots of all features...")
        # Feat.generate_hist(dat_mtx,
        #                    folder=args.hist_folder,
        #                    bin_num=int(cfg['HISTOGRAM']['bin_num']))
        # return

    if args.feature_selection_:

        Feat.dataset = Feat.refine_dataset_col_by_priority(Feat.dataset)
        print_and_write(DIV_PATTERN + Feat.get_log())

        Feat.refine_dataset_col_by_null_replacement()
        Feat.refine_dataset_column_by_null(float(cfg[REFINEMENT][NULL_NUM_THRESHOLD]))
        Feat.refine_dataset_row_by_process_type(cat=ini['TYPE']['name'],
                                                option='L')  ## 경처리 데이터로 filtering
        print_and_write(DIV_PATTERN + Feat.get_log())

        Feat.refine_dataset_by_row_range()
        # Feat.gen_csv_by_dataset_prio()
        Feat.split_dataset_by_type_and_priority()

        FeatSel = FeatureSelector(Feat.datasets['in_num'], Feat.datasets['out_num'])
        FeatSel.variance_threshold_method(thresh=float(cfg['refinement']['FS_var_threshold']),
                                          norm_cond=bool(cfg['refinement']['FS_var_threshold_norm']),
                                          selection_=True, log_=True)
        print_and_write(DIV_PATTERN + FeatSel.get_log())

        FeatSel.f_regression_method(p_thresh=float(cfg['refinement']['FS_f_regression_p_threshold']),
                                    selection_=False, log_=True)
        print_and_write(DIV_PATTERN + FeatSel.get_log())

        FeatSel.mutual_info_regression_method(mi_thresh=float(cfg['refinement']['FS_mi_regression_threshold']),
                                              selection_=False, log_=True)
        print_and_write(DIV_PATTERN + FeatSel.get_log())

        FeatSel.random_forest_regressor_method(thresh=float(cfg['refinement']['FS_random_forest_threshold']),
                                               selection_=False, log_=True)
        print_and_write(DIV_PATTERN + FeatSel.get_log())

    datasets = []
    if args.gen_dataset_:
        datasets.append(lib.read_csv_file(args.dat_csv_file))
        # datasets[-1][0] = Feat.rename_if_name_by_key(datasets[-1][0])
        datasets.append(Feat.refine_dataset_col_by_priority(datasets[-1]))
        print_and_write(DIV_PATTERN + Feat.get_log())

        datasets.append(Feat.refine_dataset_col_by_null_replacement(datasets[-1]))
        print_and_write(DIV_PATTERN + Feat.get_log())

        datasets.append(Feat.refine_dataset_row_by_process_type(datasets[-1], cat=ini['TYPE']['name'],
                                                                option='L'))  ## 경처리 데이터로 filtering
        print_and_write(DIV_PATTERN + Feat.get_log())

        datasets.append(Feat.refine_dataset_row_by_null(datasets[-1]))
        print_and_write(DIV_PATTERN + Feat.get_log())

        datasets.append(Feat.refine_dataset_row_by_range(datasets[-1]))
        print_and_write(DIV_PATTERN + Feat.get_log())

        dataset_train, dataset_test = \
            split_dataset_into_train_and_test(datasets[-1], int(cfg['DATASET']['percent_ratio']), offset=1)

        print("\n # Write splitted dataset by {:d} %".format(int(cfg['DATASET']['percent_ratio'])))
        write_list_to_csv(datasets[-1],  cfg['DATASET']['dataset_csv_filename'])
        # write_list_to_csv(dataset_train, cfg['DATASET']['train_dataset_csv_filename'])
        # write_list_to_csv(dataset_test,  cfg['DATASET']['test_dataset_csv_filename'])

        if args.test_stream_file:
            print("\n # Write test stream...")
            Feat.write_test_stream(dataset=datasets[-1], filename=args.test_stream_file)
            return
    return True


if __name__ == "__main__":

    if len(sys.argv) == 1:
        if ANALYTICS_TEST:
            sys.argv.extend([
                            "--var_csv_file", "../sys_temp/cvt_var.csv",
                            "--var_ini_file", "../sys_temp/cvt_var.ini",
                            # "--dat_csv_file", "../sys_temp/180823_cvt_dataset_modified4.csv",
                            # "--dat_csv_file", "../sys_temp/dataset_raw/190509_CVT_EVAL_DB_modified_CALC_OUTPUT.csv",
                            "--dat_csv_file", "../sys_temp/dataset_raw/190509_CVT_EVAL_DB_modified_CALC_OUTPUT_Y2_LF_REMOVED.csv",
                            "--cfg_ini_file", "../sys_temp/cvt_cfg_sa_minds.ini",
                            "--result_file", "cvt_result.txt",

                             # "--var_csv_file", "../sys_temp/rh_var.csv",
                             # "--var_ini_file", "../sys_temp/rh_var.ini",
                             # "--dat_csv_file", "../sys_temp/dataset_raw/190109_RH_EVAL_DB_modified_CALC_OUTPUT_2.csv",
                             # "--cfg_ini_file", "../sys_temp/rh_cfg_sa_minds.ini",
                             # "--result_file",  "rh_result.txt",


                             # "--gen_db_table_",
                             "--statistics_",
                             "--gen_dataset_",


                             # "--feature_selection_",
                             # "--rename_dataset_col", "renamed_dataset.csv",
                             # "--add_dataset_col", "190509_refined_dataset_LF_REMOVED.csv",
                             # "--hist_folder", HIST_FOLDER,
                             # "--test_stream_file", "rh_test_stream_171216.py",
                             ])
        elif PARSING_TEST:
            sys.argv.extend(["--var_csv_file", "rh_var_t2.csv",
                             "--var_ini_file", "rh_cfg_template.ini",
                             "--posco_stream_file", "posco_stream_1.txt",
                             ])
        else:
            sys.argv.extend(["--~help"])
    main()

