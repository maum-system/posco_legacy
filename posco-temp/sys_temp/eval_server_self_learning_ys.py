#! /usr/bin/env python
# -*- coding: utf-8 -*-
""" EVAL Server

"""
import os
import sys
import argparse
import socket
import threading
import configparser
import json
import time
import pickle
import copy
import pandas as pd
import tensorflow as tf
from datetime import datetime, timedelta
from random import randint

import numpy as np
import csv
import lib.posco as lib
from mssql.pymssqlwrapper import PyMssqlWrapper
import sys_temp.dataset_handler as feat_handler
import sys_temp.machine_learner_2 as learn_handler
from sklearn.ensemble import RandomForestRegressor
import mssql.DDL.query
import mssql.DML.query


__author__ = "Hoon Paek, Dong-gi Kim, Ji-young Moon"
__copyright__ = "Copyright 2007, The POSCO Project"
__credits__ = []
__license__ = "GPL"
__version__ = "0.0.1"
__maintainer__ = "Dong-gi Kim"
__email__ = "motive@mindslab.ai"
__status__ = "Prototype"  # Prototype / Development / Production

DB_OP_ = True


def get_feat_handler(var_cfg_file, var_csv_file):

    config = configparser.ConfigParser()
    config.read(var_cfg_file)

    var_mtx = lib.read_csv_file(var_csv_file)
    start_pos, end_pos = lib.calc_crop_info_from_ini(config['var_info_csv'])
    roi_var_mtx = lib.crop_mtx(var_mtx, start_pos, end_pos)

    obj = feat_handler.PoscoTempEstModel(roi_var_mtx)
    obj.init_feat_class(config, offset=start_pos[0])
    obj.req_enum_list = lib.complete_enum_list(config['stream']['req_enum_list'])
    obj.rep_enum_list = lib.complete_enum_list(config['stream']['rep_enum_list'])

    return obj


def main():
    """ Main method of evaluation server.

    :return:
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("--cat", required=True, help="Category, rh, cvt or rh+")
    parser.add_argument("--cfg_ini_file", required=True, help="configuration ini file")
    parser.add_argument("--log_no_console_", default=False, action='store_true', help="no console flag")
    parser.add_argument("--ml_ini_file", required=True, help="machine learning ini file")

    args = parser.parse_args()
    args.cat = args.cat.upper()
    ini = configparser.ConfigParser()
    ini.read(args.cfg_ini_file)

    logger = lib.setup_logger(args.cat + '_EVAL',
                              ini['LOG']['eval_prefix'],
                              folder=ini['LOG']['folder'],
                              console_=(not args.log_no_console_))

    # lib.kill_the_process(os.path.basename(__file__), pause_time=5, logger=logger)

    est_db_handler = None
    if DB_OP_:
    #     est_db_handler = PyMssqlWrapper(server_name=ini['EST_DB']['server_name'],
    #                                     port=ini['EST_DB']['port'],
    #                                     username=ini['EST_DB']['username'],
    #                                     password=ini['EST_DB']['password'],
    #                                     db_name=ini['EST_DB']['db_name'],
    #                                     table_name=ini['EST_DB']['table_name'],
    #                                     logger=logger)
        eval_db_handler = PyMssqlWrapper(server_name=ini['EVAL_DB']['server_name'],
                                         port=ini['EVAL_DB']['port'],
                                         username=ini['EVAL_DB']['username'],
                                         password=ini['EVAL_DB']['password'],
                                         db_name=ini['EVAL_DB']['db_name'],
                                         table_name=ini['EVAL_DB']['table_name'],
                                         logger=logger)
    #
    # est_table_info = lib.get_db_table_info(ini['EST_DB']['table_info_csv'])
    eval_table_info = lib.get_db_table_info(ini['EVAL_DB']['table_info_csv'])
    #
    Feat = get_feat_handler(ini['DATA_ANALYTICS']['var_ini_file'],
                            ini['DATA_ANALYTICS']['var_csv_file'])
    # logger.info("START " + args.cat + "_EVAL server")
    #
    # sock_req = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    # server_address = (ini['EVAL_SERVER']['ip'], int(ini['EVAL_SERVER']['port']))
    # logger.info("Starting up on %s port %s..." % server_address)
    # sock_req.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    # sock_req.bind(server_address)
    # sock_req.listen(5)

    while True:

        # logger.info("Waiting for a connection...")
        # connection, client_address = sock_req.accept()
        # logger.info("Connection for {}...".format(client_address))
        #
        # try:
        #     str_dat = lib.recv_all(connection, logger=logger).decode('utf-8')
        #     logger.info("SOCK_REQ: {}".format(str_dat))
        # except Exception as e:
        #     logger.error(e)
        # finally:
        #     connection.close()

        # try:
        #     # eval_dict = Feat.decode_stream(str_dat, dbg_=False, logger=logger)
        #     eval_dict = Feat.decode_stream_comma(str_dat, dbg_=False, logger=logger)
        # except Exception as e:
        #     logger.error("FATAL : decode_stream : " + str(e))
        #     continue
        #
        #     Feat.vars['EVAL_START_TIME'].val = lib.get_datetime()
        #     logger.info("{} EVAL operation, ({:d}) - {}.".format(args.cat, len(str_dat), str(eval_dict)))
        #     est_dict = None
        #
        #     if DB_OP_:
        #         cond_list = ['CHARGE_NO = \'' + eval_dict['CHARGE_NO'] + '\'']
        #         try:
        #             est_columns = [column[0] for column in est_table_info[1:]]
        #             est_db_columns = [db_column[0] for db_column in est_db_handler.selectColumnNames() if db_column[0] != 'No']
        #             est_db = est_db_handler.selectDataByConditions(cond_list=cond_list)[0][1:]
        #             extract_est_db = lib.extract_rows_from_table([est_db_columns] + [est_db], est_columns)
        #             logger.info("EST_DB read done")
        #             est_dict = lib.convert_db_to_dict(extract_est_db[1:][0], est_table_info[1:])
        #         except BaseException as e:
        #             logger.error(str(cond_list) + " : " + str(e))
        #
        #     check_status, check_list = Feat.check_var_ranges(eval_dict, fix_=True, logger=logger)
        #     Feat.vars['EVAL_RANGE_CHECK'].val = int(check_status)
        #     Feat.vars['EVAL_RANGE_CHECK_VARS'].val = str(check_list).replace("\'", "").replace("\"", "")
        #
        #     ## 19.04.17 modified
        #     check_process_type = Feat.check_var_process_type(eval_dict, cat=args.cat, logger=logger)
        #
        #     Feat.vars['EVAL_METHOD'].val = ini['MACHINE_LEARNING_MODEL']['method']
        #     # model_dir = lib.get_model_dir(ini['MACHINE_LEARNING_MODEL']['root_dir'],
        #     #                               args.cat,
        #     #                               ini['MACHINE_LEARNING_MODEL']['method'],
        #     #                               ini['MACHINE_LEARNING_MODEL']['version'],
        #     #                               ini['SELF_LEARNING']['result_file'])
        #     # model_dir = "model\\CVT\\LGBMR\\000711"  # cvt 일 경우
        #     model_dir = "model\\RH\\DNN\\000711"  # rh 일 경우
        #     Feat.vars['EVAL_MODEL_VER'].val = model_dir

            def evaluate():

                # output = None
                # if ini['MACHINE_LEARNING_MODEL']['method'] == 'DNN':
                #     if args.cat == 'RH':
                #         old_var = 'CC_TD_AVG_TEMP'
                #         new_var = 'CC_TD_AIM_TEMP'
                #     else: # CVT
                #         old_var = 'RH_ARR_TEMP'
                #         new_var = 'RH_REQ_TEMP'
                #
                #     if args.cat == 'RH_PLUS':
                #         output = lib.dnn_estimation_multiply_y(model_dir,
                #                                                eval_dict,
                #                                                Feat,
                #                                                logger=logger)
                #     else:
                #         output = lib.dnn_estimation(model_dir,
                #                                     eval_dict,
                #                                     Feat,
                #                                     old_var=old_var,
                #                                     new_var=new_var,
                #                                     logger=logger)
                #
                # elif ini['MACHINE_LEARNING_MODEL']['method'] == 'RFR':
                #     if args.cat == 'RH':
                #         old_var = 'CC_TD_AVG_TEMP'
                #         new_var = 'CC_TD_AIM_TEMP'
                #     else: # CVT
                #         old_var = 'RH_ARR_TEMP'
                #         new_var = 'RH_REQ_TEMP'
                #
                #     output = lib.rfr_estimation(model_dir,
                #                                 eval_dict,
                #                                 old_var=old_var,
                #                                 new_var=new_var,
                #                                 logger=logger)
                #
                # elif ini['MACHINE_LEARNING_MODEL']['method'] == 'LGBMR':
                #     if args.cat == 'RH':
                #         old_var = 'CC_TD_AVG_TEMP'
                #         new_var = 'CC_TD_AIM_TEMP'
                #     else: # CVT
                #         old_var = 'RH_ARR_TEMP'
                #         new_var = 'RH_REQ_TEMP'
                #
                #     output = lib.lgbmr_estimation(model_dir,
                #                                   eval_dict,
                #                                   old_var=old_var,
                #                                   new_var=new_var,
                #                                   process_type=check_process_type,
                #                                   logger=logger)
                # if args.cat == 'RH':
                #
                #     Feat.vars['CALC_OUTPUT_2'].val = output
                #     Feat.vars['EVAL_TEMP'].val = Feat.vars['CALC_OUTPUT_2'].val + Feat.vars['CC_TD_AIM_TEMP'].val
                #     logger.info("Estimation result = {:5.2f}, {:7.2f}".format(Feat.vars['CALC_OUTPUT_2'].val,
                #                                                               Feat.vars['EVAL_TEMP'].val))
                #
                # elif args.cat == 'CVT':
                #
                #     Feat.vars['OUT_DROP'].val = output
                #     Feat.vars['EVAL_TEMP'].val = Feat.vars['OUT_DROP'].val + Feat.vars['RH_REQ_TEMP'].val
                #     logger.info("Estimation result = {:5.2f}, {:7.2f}".format(Feat.vars['OUT_DROP'].val,
                #                                                               Feat.vars['EVAL_TEMP'].val))
                #
                # elif args.cat == 'RH_PLUS':
                #
                #     Feat.vars['RH_TOT_TREAT_TM'].val, Feat.vars['RH_LAST_TEMP'].val = output
                #     logger.info("Estimation result = {:5.2f}, {:7.2f}".format(Feat.vars['RH_TOT_TREAT_TM'].val,
                #                                                                   Feat.vars['RH_LAST_TEMP'].val))
                #
                # if est_dict:
                #     ret_status, ret_list = Feat.check_var_tolerances(est_dict, eval_dict, logger=logger)
                #     Feat.vars['EVAL_TOLERANCE_CHECK'].val = int(ret_status)
                #     Feat.vars['EVAL_TOLERANCE_CHECK_VARS'].val = str(ret_list).replace("\'", "").replace("\"", "")
                #     Feat.vars['EVAL_PERF'].val = abs(est_dict['EST_TEMP'] - Feat.vars['EVAL_TEMP'].val)
                # else:
                #     Feat.vars['EVAL_TOLERANCE_CHECK'].val = -1
                #     Feat.vars['EVAL_PERF'].val = lib.INFINITY
                #     logger.info("There is no charge no. in estimation DB matched with {}".format(eval_dict['CHARGE_NO']))
                # Feat.vars['EVAL_END_TIME'].val = lib.get_datetime()

                latest_dir = lib.get_model_dir(ini['MACHINE_LEARNING_MODEL']['root_dir'],
                                               args.cat,
                                               ini['MACHINE_LEARNING_MODEL']['method'],
                                               ini['MACHINE_LEARNING_MODEL']['version'])

                today = lib.get_datetime("%y%m%d")
                model_date = os.path.split(latest_dir)[-1]
                now = datetime.now()
                past = datetime(int("20"+model_date[:2]), int(model_date[2:-2]), int(model_date[-2:]), 0, 0)

                days = (now - past).days

                if model_date[2:-2] in ['01', '03', '05', '07', '08', '10', '12']:
                    total_month = 31
                elif model_date[2:-2] in ['04', '06', '09', '11']:
                    total_month = 30
                else:
                    if int(model_date[:2]) % 4 == 0:
                        total_month = 29
                    else:
                        total_month = 28

                if DB_OP_ :
                #     and \
                # (
                #     (ini['SELF_LEARNING']['condition'] == 'everyday' and days > 1) or
                #     (ini['SELF_LEARNING']['condition'] == 'weekly' and days > 7) or
                #     (ini['SELF_LEARNING']['condition'] == 'biweekly' and days > 14) or
                #     (ini['SELF_LEARNING']['condition'] == 'monthly' and days > total_month)
                # ):
                    eval_cond_list = ["{0}<{1}".format("20" + model_date, 'TAP_WORK_DATE'),
                                      "{0}>{1}".format("30000000",        'TAP_WORK_DATE')]
                    db_data = [[]]
                    eval_columns = [column[0] for column in eval_table_info[1:]]

                    eval_db_columns = [db_column[0] for db_column in eval_db_handler.selectColumnNames() if
                                       db_column[0] != 'No']
                    db_data[0] = eval_db_handler.selectDataByConditions(cond_list=eval_cond_list)  # 날짜 조건에 맞는 데이터
                    db_data.append(lib.transpose_list(lib.transpose_list(db_data[-1])[1:]))

                    db_data.append(lib.extract_rows_from_table([eval_db_columns] + db_data[-1], eval_columns))
                    # db_data의 len 은 3이고 각각 날자 조건에 맞는 데이터(180), 거기서 No 뺀 거(179), extract_rows_from_table(120) 한 것이다.
                    msg = "Get last updated {:d} data from EVAL DB with {}.".format(len(db_data[-1]), eval_cond_list)
                    logger.info(msg)
                    learn_msg = "\n # " + msg

                    if args.cat == 'RH':

                        idx_RH_LAST_TEMP = db_data[-1][0].index('RH_LAST_TEMP')
                        idx_TD_AVG_TEMP = db_data[-1][0].index('CC_TD_AVG_TEMP')
                        idx_CALC_OUTPUT = db_data[-1][0].index('CALC_OUTPUT_2')
                        for i in range(1, len(db_data[-1])):
                            db_data[-1][i][idx_CALC_OUTPUT] = db_data[-1][i][idx_RH_LAST_TEMP] - \
                                                              db_data[-1][i][idx_TD_AVG_TEMP]
                    elif args.cat == 'CVT':
                        idx_CONV_ENDPNT_TEMP = db_data[-1][0].index('CONV_ENDPNT_TEMP')
                        idx_RH_ARR_TEMP = db_data[-1][0].index('RH_ARR_TEMP')
                        idx_OUT_DROP = db_data[-1][0].index('OUT_DROP')
                        for i in range(1, len(db_data[-1])):
                            db_data[-1][i][idx_OUT_DROP] = db_data[-1][i][idx_CONV_ENDPNT_TEMP] - \
                                                           db_data[-1][i][idx_RH_ARR_TEMP]
                    else:
                        pass
                    # BUG.181029: END
                    db_data.append(Feat.refine_dataset_col_by_priority(db_data[-1], logger=logger)) # 120개가 97개가 된다.
                    learn_msg += "\n # " + Feat.log
                    db_data.append(Feat.refine_dataset_col_by_null_replacement(db_data[-1], logger=logger))
                    learn_msg += "\n # " + Feat.log
                    db_data.append(Feat.refine_dataset_row_by_null(db_data[-1], logger=logger))
                    learn_msg += "\n # " + Feat.log
                    refined_db_data = Feat.refine_dataset_row_by_range(db_data[-1], logger=logger)
                    learn_msg += "\n # " + Feat.log

                    refined_db_data_h = copy.deepcopy(refined_db_data) # deepcopy dataset for H

                    process_db_data = Feat.refine_dataset_row_by_process_type(refined_db_data, cat=args.cat, logger=logger)
                    learn_msg += "\n # " + Feat.log
                    process_db_data_h = Feat.refine_dataset_row_by_process_type(refined_db_data_h, cat=args.cat, logger=logger, option='H')
                    learn_msg += "\n # " + Feat.log

                    dataset = lib.read_csv_file(ini['SELF_LEARNING']['dataset_file'])
                    org_dataset_len = len(dataset) - 1
                    add_dataset_len = len(process_db_data) - 1

                    # sort by local dataset column
                    sorted_db_data = lib.extract_rows_from_table(process_db_data, dataset[0])
                    # sorted_db_data = sorted(db_data[-1], key=dataset[0])

                    dataset += sorted_db_data[1:]
                    # dataset_dir = lib.get_learning_dataset_dir(ini['MACHINE_LEARNING_MODEL']['root_dir'], args.cat)
                    # lib.write_list_to_csv(dataset, ini['SELF_LEARNING']['dataset_file']) # 위험해 !!!
                    msg = "Write new self-learning dataset, {:d} + {:d}".format(org_dataset_len, add_dataset_len)
                    logger.info(msg)
                    learn_msg += "\n # " + msg

                    ## For process type is heavy
                    if args.cat == 'CVT':
                        dataset_h = lib.read_csv_file(ini['SELF_LEARNING']['dataset_file_h'])
                        org_dataset_h_len = len(dataset_h) - 1
                        add_dataset_h_len = len(process_db_data_h) - 1

                        # sort by local dataset column
                        sorted_db_data_h = lib.extract_rows_from_table(process_db_data_h, dataset[0])
                        # sorted_db_data = sorted(db_data[-1], key=dataset[0])

                        dataset_h += sorted_db_data_h[1:]

                        ## 19.04.19 extract data by 'Y2'
                        dataset_h_df = pd.DataFrame(dataset_h[1:], columns=dataset_h[0])
                        dataset_h[1:] = dataset_h_df.loc[dataset_h_df['PROCESS_SEP'] == 'Y2'].values.tolist()

                        # dataset_dir = lib.get_learning_dataset_dir(ini['MACHINE_LEARNING_MODEL']['root_dir'], args.cat)
                        lib.write_list_to_csv(dataset_h, ini['SELF_LEARNING']['dataset_file_h'])
                        msg = "Write new self-learning dataset for heavy process, {:d} + {:d}".format(org_dataset_h_len, add_dataset_h_len)
                        logger.info(msg)
                        learn_msg += "\n # " + msg

                    new_model_dir = os.path.join(ini['MACHINE_LEARNING_MODEL']['root_dir'],
                                                 args.cat,
                                                 ini['MACHINE_LEARNING_MODEL']['method'],
                                                 today)
                    if not os.path.exists(new_model_dir):
                        os.makedirs(new_model_dir)

                    ml_cfg = configparser.ConfigParser()
                    ml_cfg.read(args.ml_ini_file)

                    param_dict = {'dataset': dataset}

                    if args.cat == 'CVT':
                        Learn = learn_handler.MachineLearner(Feat=Feat, dataset=dataset, out_idx=0)
                        Learn_h = learn_handler.MachineLearner(Feat=Feat, dataset=dataset_h, out_idx=0)
                        if ini['MACHINE_LEARNING_MODEL']['method'] == "RFR":
                            Learn.encode_symbol_dataset_by_int()
                            Learn_h.encode_symbol_dataset_by_int()
                        elif ini['MACHINE_LEARNING_MODEL']['method'] == "LGBMR":
                            Learn.encode_symbol_dataset_by_int()
                            Learn_h.encode_symbol_dataset_by_int()

                        bin_dir = ini['MACHINE_LEARNING_MODEL']['method'] + "_L.bin"
                        bin_dir_h = ini['MACHINE_LEARNING_MODEL']['method'] + "_H.bin"
                        Learn.run(ini['MACHINE_LEARNING_MODEL']['method'], ml_cfg)
                        Learn_h.run(ini['MACHINE_LEARNING_MODEL']['method'], ml_cfg)
                    else:
                        Learn = learn_handler.MachineLearner(Feat=Feat, dataset=dataset, out_idx=0)
                        if ini['MACHINE_LEARNING_MODEL']['method'] == "DNN":
                            Learn.normalize_number_features()
                            Learn.encode_symbol_dataset_by_one_hot_encoder()
                            ml_cfg['DNN']['model_dir'] = os.path.join(new_model_dir, ml_cfg['DNN']['model_dir'])
                        elif ini['MACHINE_LEARNING_MODEL']['method'] == "RFR":
                            Learn.encode_symbol_dataset_by_int()
                        elif ini['MACHINE_LEARNING_MODEL']['method'] == "LGBMR":
                            Learn.encode_symbol_dataset_by_int()

                        bin_dir = ini['MACHINE_LEARNING_MODEL']['method'] + ".bin"
                        Learn.run(ini['MACHINE_LEARNING_MODEL']['method'], ml_cfg)

                    param_dict['params'] = Learn.params

                    if args.cat == 'RH_PLUS':
                        msg = "Time : train accuracy - {:6.2f}, test accuracy - {:6.2f}, Temperature : train accuracy - {:6.2f}, test accuracy - {:6.2f}". \
                            format(Learn.train_acc, Learn.test_acc, Learn.train_acc_2, Learn.test_acc_2)
                        logger.info(msg)
                        learn_msg += "\n # " + msg
                        msg = "Time : train MSE - {:6.2f}, test MSE - {:6.2f}, Temperature : train MSE - {:6.2f}, test MSE - {:6.2f}". \
                            format(Learn.train_mse, Learn.test_mse, Learn.train_mse_2, Learn.test_mse_2)
                        logger.info(msg)
                        learn_msg += "\n # " + msg
                    elif args.cat == 'RH':
                        msg = "Train accuracy : {:6.2f}, Test accuracy : {:6.2f}".format(Learn.train_acc,
                                                                                         Learn.test_acc)
                        logger.info(msg)
                        learn_msg += "\n # " + msg
                        msg = "Train MSE : {:6.2f}, Test MSE : {:6.2f}".format(Learn.train_mse, Learn.test_mse)
                        logger.info(msg)
                        learn_msg += "\n # " + msg
                    elif args.cat == 'CVT':
                        msg = "Model(L) Train accuracy : {:6.2f}, Test accuracy : {:6.2f}".format(Learn.train_acc,
                                                                                                  Learn.test_acc)
                        msg_h = ", Model(H) Train accuracy : {:6.2f}, Test accuracy : {:6.2f}".format(Learn_h.train_acc,
                                                                                                    Learn_h.test_acc)
                        logger.info(msg + msg_h)
                        learn_msg += "\n # " + msg + msg_h
                        msg = "Train MSE : {:6.2f}, Test MSE : {:6.2f}".format(Learn.train_mse, Learn.test_mse)
                        msg_h = ", Train MSE : {:6.2f}, Test MSE : {:6.2f}".format(Learn_h.train_mse, Learn_h.test_mse)
                        logger.info(msg + msg_h)
                        learn_msg += "\n # " + msg + msg_h

                    if ini['MACHINE_LEARNING_MODEL']['method'] == "DNN":
                        pickle.dump(param_dict, open(os.path.join(new_model_dir, bin_dir), "wb"))
                    else:
                        if args.cat == 'CVT':
                            pickle.dump(Learn, open(os.path.join(new_model_dir, bin_dir), "wb"))
                            pickle.dump(Learn_h, open(os.path.join(new_model_dir, bin_dir_h), "wb"))
                        else:
                            pickle.dump(Learn, open(os.path.join(new_model_dir, bin_dir), "wb"))
                    with open(os.path.join(new_model_dir, ini['SELF_LEARNING']['result_file']), "w") as fid:
                        fid.write(learn_msg)

                # if DB_OP_:
                #     if str_dat[:8] == ini['EVAL_CLIENT']['name']:
                #         db_row_dict = lib.gen_db_row_dat(eval_table_info, Feat)
                #         try:
                #             connected  = eval_db_handler.insertOne(db_row_dict)
                #             while not connected:
                #                 eval_db_handler.ddl_query = mssql.DDL.query.DdlQuery(
                #                     server_name=ini['EST_DB']['server_name'],
                #                     port=ini['EST_DB']['port'],
                #                     username=ini['EST_DB']['username'],
                #                     password=ini['EST_DB']['password'],
                #                     db_name=ini['EST_DB']['db_name'],
                #                     logger=logger, pk_name='no')
                #                 eval_db_handler.dml_query = mssql.DML.query.DmlQuery(
                #                     server_name=ini['EST_DB']['server_name'],
                #                     port=ini['EST_DB']['port'],
                #                     username=ini['EST_DB']['username'],
                #                     password=ini['EST_DB']['password'],
                #                     db_name=ini['EST_DB']['db_name'],
                #                     logger=logger, pk_name='no')
                #                 connected = eval_db_handler.insertOne(db_row_dict)
                #
                #         except Exception as e:
                #             logger.error("FATAL : DB write error : " + str(e))
                #
                #         logger.info("EVAL_DB read done")
                #         if True:
                #             print("EVAL_DB read back:" + str(eval_db_handler.selectOne(eval_db_handler.selectLastPK())))
                #         pass
                #
                #         ## modified 181120
                #         if args.cat == 'CVT':
                #             eval_row_dict = lib.gen_row_dat(eval_table_info, Feat)
                #             compare_status = lib.compare_est_and_eval_dat(est_dict, eval_row_dict, logger=logger)
                #     else:
                #         pass

            # t = threading.Thread(target=evaluate)
            # t.start()
            evaluate()


if __name__ == "__main__":
    if len(sys.argv) == 1:
        sys.argv.extend([
                         # "--cfg_ini_file", "rh_cfg_posco.ini",
                         # "--ml_ini_file", "rh_ml_setting.ini",
                         # "--cat", "rh",

                         "--cfg_ini_file", "cvt_cfg_posco.ini",
                         "--ml_ini_file", "cvt_ml_setting.ini",
                         "--cat", "cvt"
                        ])

    main()

