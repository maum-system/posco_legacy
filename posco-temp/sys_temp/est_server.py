#! /usr/bin/env python
# -*- coding: utf-8 -*-
""" EST Server

"""
# import os
import sys
import argparse
import socket
import threading
import configparser
import json
import time
import lib.posco as lib
from mssql.pymssqlwrapper import PyMssqlWrapper
import sys_temp.dataset_handler as feat_handler
import mssql.DDL.query
import mssql.DML.query

__author__ = "Hoon Paek, Dong-gi Kim, Ji-young Moon"
__copyright__ = "Copyright 2007, The POSCO Project"
__credits__ = []
__license__ = "GPL"
__version__ = "0.0.1"
__maintainer__ = "Dong-gi Kim"
__email__ = "motive@mindslab.ai"
__status__ = "Prototype"  # Prototype / Development / Production

g_logger = None
DB_OP_ = False


def get_feat_handler(var_cfg_file, var_csv_file):

    config = configparser.ConfigParser()
    config.read(var_cfg_file)

    var_mtx = lib.read_csv_file(var_csv_file)
    start_pos, end_pos = lib.calc_crop_info_from_ini(config['var_info_csv'])
    roi_var_mtx = lib.crop_mtx(var_mtx, start_pos, end_pos)

    obj = feat_handler.PoscoTempEstModel(roi_var_mtx)
    obj.init_feat_class(config, offset=start_pos[0])
    obj.req_enum_list = lib.complete_enum_list(config['stream']['req_enum_list'])
    obj.rep_enum_list = lib.complete_enum_list(config['stream']['rep_enum_list'])

    return obj


def main():
    """ Main method of estimation_server

    :return:
    """
    global g_logger

    parser = argparse.ArgumentParser()
    parser.add_argument("--cat", required=True, help="Category, rh, cvt, or rh_plus")
    parser.add_argument("--cfg_ini_file", required=True, help="configuration ini file")
    parser.add_argument("--log_no_console_", default=False, action='store_true', help="no console flag")

    args = parser.parse_args()
    args.cat = args.cat.upper()
    ini = configparser.ConfigParser()
    ini.read(args.cfg_ini_file)

    g_logger = lib.setup_logger(args.cat + '_EST',
                                ini['LOG']['est_prefix'],
                                folder=ini['LOG']['folder'],
                                console_=(not args.log_no_console_))

    # lib.kill_the_process(os.path.basename(__file__), pause_time=5, logger=g_logger)

    if DB_OP_:
        db_handler = PyMssqlWrapper(server_name=ini['EST_DB']['server_name'],
                                    port=ini['EST_DB']['port'],
                                    username=ini['EST_DB']['username'],
                                    password=ini['EST_DB']['password'],
                                    db_name=ini['EST_DB']['db_name'],
                                    table_name=ini['EST_DB']['table_name'],
                                    logger=g_logger)

    est_table_info  = lib.get_db_table_info(ini['EST_DB']['table_info_csv'])
    var_handler  = get_feat_handler(ini['DATA_ANALYTICS']['var_ini_file'],
                                    ini['DATA_ANALYTICS']['var_csv_file'])
    g_logger.info("START " + args.cat + "_EST server")

    sock_req = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_address = (ini['EST_SERVER']['ip'], int(ini['EST_SERVER']['port']))
    g_logger.info("Starting up on %s port %s..." % server_address)
    sock_req.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    sock_req.bind(server_address)
    sock_req.listen(5)

    while True:
        g_logger.info("Waiting for a connection...")
        connection, client_address = sock_req.accept()
        g_logger.info("Connection for {}...".format(client_address))

        try:
            str_dat = lib.recv_all(connection, logger=g_logger).decode('utf-8')
            g_logger.info("SOCK_REQ: {:d} \"{}\"".format(len(str_dat), str_dat))
        except Exception as e:
            g_logger.error(e)
        finally:
            connection.close()

        # continue
        if str_dat == "":
            continue

        if lib.is_json(str_dat):
            dict_dat = json.loads(str_dat)
            if dict_dat['cmd'].lower() == "check":
                g_logger.info("Check operation ")
                rep_stream = "Healthy"
            elif dict_dat['cmd'].lower() == 'stop':
                g_logger.info("Stop operation")
                rep_stream = "Bye"
            else:
                g_logger.info("Invalid command, {}.".format(dict_dat['cmd']))
                rep_stream = "Invalid"

            time.sleep(1)
            sock_rep = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            server_address = (ini['CHECK_SERVER']['ip'], int(ini['CHECK_SERVER']['port']))
            g_logger.info("connecting to %s port %s..." % server_address)
            try:
                sock_rep.connect(server_address)
            except ConnectionRefusedError:
                g_logger.error("ConnectionRefusedError: %s port %s..." % server_address)
                continue

            try:
                sock_rep.sendall(rep_stream.encode('utf-8'))
                g_logger.info("SOCK_REP: {}".format(rep_stream))
            finally:
                sock_rep.close()

            if rep_stream == "Bye":
                return

            continue

        else:
            try:
                # stream_dict = var_handler.decode_stream(str_dat, dbg_=False, logger=g_logger)
                print(str_dat)
                stream_dict = var_handler.decode_stream_comma(str_dat, dbg_=False, logger=g_logger)
            except Exception as e:
                g_logger.error("FATAL : decode_stream : " + str(e))
                # sys.exit()
                continue

            var_handler.vars['EST_REQUEST_TIME'].val = lib.get_datetime()
            g_logger.info("{} EST operation, {:d}, {}.".format(args.cat, len(str_dat), str(stream_dict)))
            model_dir = lib.get_model_dir(ini['MACHINE_LEARNING_MODEL']['root_dir'],
                                          args.cat,
                                          ini['MACHINE_LEARNING_MODEL']['method'],
                                          ini['MACHINE_LEARNING_MODEL']['version'],
                                          ini['SELF_LEARNING']['result_file'])
            # model_dir = "model\\CVT\\LGBMR\\000711"  # cvt 일 경우
            # model_dir = "model\\RH\\DNN\\000711"  # rh 일 경우

            check_status, check_list = var_handler.check_var_ranges(stream_dict, fix_=True, logger=g_logger)
            var_handler.vars['EST_RANGE_CHECK'].val = int(check_status)
            var_handler.vars['EST_RANGE_CHECK_VARS'].val = str(check_list).replace("\'", "").replace("\"", "")

            ## 19.04.17 modified
            check_process_type = var_handler.check_var_process_type(stream_dict, cat=args.cat, logger=g_logger)

            var_handler.vars['EST_METHOD'].val = ini['MACHINE_LEARNING_MODEL']['method']
            var_handler.vars['EST_MODEL_VER'].val = model_dir

            def estimate():
                global est_val_time, est_val_temp, est_val, est_result
                if args.cat == 'RH':
                    old_var = 'CC_TD_AVG_TEMP'
                    new_var = 'CC_TD_AIM_TEMP'
                    est_val = lib.dnn_estimation(model_dir,
                                                 stream_dict,
                                                 var_handler,
                                                 old_var=old_var,
                                                 new_var=new_var,
                                                 logger=g_logger)  # rh
                    # est_val = lib.lgbmr_estimation(model_dir,
                    #                                stream_dict,
                    #                                old_var=old_var,
                    #                                new_var=new_var,
                    #                                process_type=check_process_type,
                    #                                logger=g_logger)
                    est_result = est_val + var_handler.vars['CC_TD_AIM_TEMP'].val
                elif args.cat == 'CVT':
                    old_var = 'RH_ARR_TEMP'
                    new_var = 'RH_REQ_TEMP'
                    # est_val = lib.dnn_estimation(model_dir,
                    #                              stream_dict,
                    #                              var_handler,
                    #                              logger=g_logger)
                    # est_val = lib.rfr_estimation(model_dir,
                    #                              stream_dict,
                    #                              old_var=old_var,
                    #                              new_var=new_var,
                    #                              logger=g_logger)
                    est_val = lib.lgbmr_estimation(model_dir,
                                                   stream_dict,
                                                   old_var=old_var,
                                                   new_var=new_var,
                                                   process_type=check_process_type,
                                                   logger=g_logger)
                    est_result = est_val + var_handler.vars['RH_REQ_TEMP'].val
                elif args.cat == 'RH_PLUS':
                    est_val_time, est_val_temp = lib.rfr_estimation(model_dir,
                                                 stream_dict,
                                                 logger=g_logger)
                else:
                    g_logger.error(" @ Error: Invalid cat, {}".format(args.cat))
                    est_val = est_result = -1

                if args.cat == 'RH_PLUS':
                    g_logger.info("Estimation result = time: {:5.2f}, temp: {:7.2f}".format(est_val_time, est_val_temp))
                    var_handler.vars['OUT_AI_CALC_TEMP'].val = est_val_temp
                    var_handler.vars['OUT_AI_CALC_TIME'].val = est_val_time
                    var_handler.vars['EST_TEMP'].val = est_val_temp
                    var_handler.vars['EST_TIME'].val = est_val_time
                    var_handler.vars['EST_REPLY_TIME'].val = lib.get_datetime()
                else:
                    g_logger.info("Estimation result = {:5.2f}, {:7.2f}".format(est_val, est_result))
                    var_handler.vars['OUT_AI_CALC_TEMP'].val = est_result
                    var_handler.vars['EST_TEMP'].val = est_result
                    var_handler.vars['EST_REPLY_TIME'].val = lib.get_datetime()
                rep_stream = var_handler.encode_stream(dbg_=False)

                sock_rep = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                server_address = (ini['EST_CLIENT']['ip'], int(ini['EST_CLIENT']['port']))
                g_logger.info("connecting to %s port %s..." % server_address)
                try:
                    sock_rep.connect(server_address)
                except ConnectionRefusedError:
                    g_logger.error("ConnectionRefusedError: %s port %s..." % server_address)
                    return

                try:
                    sock_rep.sendall(rep_stream.encode('utf-8'))
                    g_logger.info("SOCK_REP: Sent {}".format(len(rep_stream)))
                finally:
                    sock_rep.close()

                if DB_OP_:
                    if str_dat[:8] == ini['EST_CLIENT']['name']:
                        db_row_dict = lib.gen_db_row_dat(est_table_info, var_handler)
                        try:
                            connected = db_handler.insertOne(db_row_dict)
                            # if not connected:
                            while not connected:
                                db_handler.ddl_query = mssql.DDL.query.DdlQuery(
                                    server_name=ini['EST_DB']['server_name'],
                                    port=ini['EST_DB']['port'],
                                    username=ini['EST_DB']['username'],
                                    password=ini['EST_DB']['password'],
                                    db_name=ini['EST_DB']['db_name'],
                                    logger=g_logger, pk_name='no')
                                db_handler.dml_query = mssql.DML.query.DmlQuery(
                                    server_name=ini['EST_DB']['server_name'],
                                    port=ini['EST_DB']['port'],
                                    username=ini['EST_DB']['username'],
                                    password=ini['EST_DB']['password'],
                                    db_name=ini['EST_DB']['db_name'],
                                    logger=g_logger, pk_name='no')
                                connected = db_handler.insertOne(db_row_dict)

                        except Exception as e:
                            g_logger.error("FATAL : DB write error : " + str(e))

                        g_logger.info("EST_DB write done")
                        if True:
                            g_logger.info("EST_DB read back :" + str(db_handler.selectOne(db_handler.selectLastPK())))
                        pass
                    else:
                        pass

            t = threading.Thread(target=estimate)
            t.start()

if __name__ == "__main__":
    if len(sys.argv) == 1:
        sys.argv.extend(["--cat", "rh",  "--cfg_ini_file",  "rh_cfg_sa_minds.ini"])
        # sys.argv.extend(["--cat", "rh_plus",  "--cfg_ini_file",  "rh_plus_cfg_posco.ini"])
        # sys.argv.extend(["--cat", "rh_plus", "--cfg_ini_file", "rh_plus_cfg_sa_posco.ini"])
        # sys.argv.extend(["--cat", "cvt", "--cfg_ini_file", "cvt_cfg_sa_minds.ini"])
        # sys.argv.extend(["--cat", "cvt", "--cfg_ini_file", "cvt_cfg_sa_posco.ini"])
        # sys.argv.extend(["--cat", "cvt", "--cfg_ini_file", "cvt_cfg_posco_new.ini"])
    main()
