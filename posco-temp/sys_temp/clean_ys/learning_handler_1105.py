#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""This module's docstring summary line.
This is a multi-line docstring. Paragraphs are separated with blank lines.
Lines conform to 79-column limit.
Module and packages names should be short, lower_case_with_underscores.
Notice that this in not PEP8-cheatsheet.py
Seriously, use flake8. Atom.io with https://atom.io/packages/linter-flake8
is awesome!
See http://www.python.org/dev/peps/pep-0008/ for more PEP-8 details
Coding style reference
    - https://www.python.org/dev/peps/pep-0008/
    - http://web.archive.org/web/20111010053227/http://jaynes.colorado.edu
    /PythonGuidelines.html
    WHAT TO DO
    > Normalization before learning.
    > RFR, SVR, DNR
    > apply string features to estimation.
"""
import os
import sys
import shutil
import random
import argparse
import configparser
import copy
import numpy as np
import matplotlib.pyplot as plt
from PIL import Image
from operator import itemgetter
from lib.posco import read_csv_file
import pickle

import sys_temp.dataset_handler as Dataset
import sys_temp.machine_learner_2 as Learner

import lib.posco as posco


__author__ = "Hoon Paek, Dong-gi Kim, Ji-young Moon"
__copyright__ = "Copyright 2007, The Cogent Project"
__credits__ = []
__license__ = "GPL"
__version__ = "0.0.1"
__maintainer__ = "Hoon Paek"
__email__ = "hoon.paek@mindslab.ai"
__status__ = "Prototype"  # Prototype / Development / Production

LEARNING_TEST = True
RESULT_FILE = "result.txt"


def main():
    """ Learning handler main code.
    """
    global RESULT_FILE

    parser = argparse.ArgumentParser()
    parser.add_argument("--var_csv_file", required=True, help="csv file for variables")
    parser.add_argument("--var_ini_file", required=True, help="ini file for configuration")
    parser.add_argument("--dataset_csv_file", default="", help="csv file for data set")
    parser.add_argument("--result_file", default="", help="Result filename")

    parser.add_argument("--ml_method", required=True, help="Machine learning method, RFR or DNN")

    parser.add_argument("--ml_ini_file", default="", help="ini file for machine learner")

    args = parser.parse_args()

    if args.result_file:
        RESULT_FILE = args.result_file
    with open(RESULT_FILE, 'a') as fid:
        fid.write("\n")

    cfg = configparser.ConfigParser()
    cfg.read(args.var_ini_file, encoding="utf-8")

    ml_cfg = configparser.ConfigParser()
    ml_cfg.read(args.ml_ini_file)

    var_mtx = read_csv_file(args.var_csv_file)
    start_pos, end_pos = Dataset.calc_crop_info_from_ini(cfg[Dataset.VAR_INFO_CSV])
    roi_var_mtx = Dataset.crop_mtx(var_mtx, start_pos, end_pos)
    Feat = Dataset.PoscoTempEstModel(roi_var_mtx)
    Feat.init_feat_class(cfg, offset=start_pos[0])

    datasets = []
    datasets.append(read_csv_file(args.dataset_csv_file))
    datasets.append(Feat.refine_dataset_col_by_priority(datasets[-1]))

    Learn = Learner.MachineLearner(Feat=Feat, dataset=datasets[-1], out_idx=0)

    # today = posco.get_datetime("%y%m%d")
    today = "000711"
    if args.ml_method == 'RFR':
        Learn.encode_symbol_dataset_by_int()
        Learn.run("RFR", ml_cfg)
        pickle.dump(Learn, open(today + "_RFR.bin", 'wb'))

    elif args.ml_method == 'DNN':
        Learn.normalize_number_features()
        Learn.encode_symbol_dataset_by_one_hot_encoder()
        Learn.run("DNN", ml_cfg)

        # for DNN pickle dump
        param_dict = {'dataset': datasets[-1], 'params': ml_cfg['DNN']}
        pickle.dump(param_dict, open(today + "_DNN.bin", 'wb'))

    elif args.ml_method == 'LGBMR':
        Learn.encode_symbol_dataset_by_int()
        Learn.run("LGBMR", ml_cfg)
        pickle.dump(Learn, open(today + "_LGBMR.bin", 'wb'))

    elif args.ml_method == 'XGBR':
        Learn.encode_symbol_dataset_by_int()
        Learn.run("XGBR", ml_cfg)
        pickle.dump(Learn, open(today + "_XGBR.bin", 'wb'))

    posco.print_and_write(Learn.log, console=True, filename=RESULT_FILE)
    pass


if __name__ == "__main__":

    if len(sys.argv) == 1:
        if LEARNING_TEST:
            sys.argv.extend([
                             "--var_ini_file", "../sys_temp/rh_var.ini",
                             # "--var_ini_file", "../sys_temp/cvt_var.ini",
                             "--var_csv_file", "../sys_temp/rh_var.csv",
                             # "--var_csv_file", "../sys_temp/cvt_var.csv",
                             # "--dataset_csv_file", "../sys_temp/dataset_raw/190416_cvt_dataset_all_both.csv",
                             # "--dataset_csv_file", "../sys_temp/dataset_raw/190509_cvt_dataset_all_light.csv",
                             # "--dataset_csv_file", "../sys_temp/analysis/normal_data.csv",
                             # "--dataset_csv_file", "../sys_temp/dataset_raw/190502_CVT_EVAL_DB_modified_CALC_OUTPUT_Y2_LF_REMOVED.csv",  # ys
                             "--dataset_csv_file", "../sys_temp/model/RH/201105_rh_dataset_self.csv",
                             # "--dataset_csv_file", "../sys_temp/model/CVT/201105_cvt_dataset_all_light.csv",

                             "--result_file", "rh_result.txt",
                             # "--result_file", "cvt_result.txt",

                             # "--var_ini_file", "../sys_temp/rh_var.ini",
                             # "--var_csv_file", "../sys_temp/rh_var_new.csv",
                             # "--dataset_csv_file", "../sys_temp/dataset_raw/190109_rh_dataset_all.csv",
                             # "--result_file", "rh_result.txt",

                             "--ml_method", "DNN",
                             # "--ml_method", "LGBMR",

                             # "--ml_ini_file", "RFR_setting.ini",
                             # "--ml_ini_file", "DNN_setting.ini",
                             # "--ml_ini_file", "../sys_temp/cvt_ml_setting.ini",
                             "--ml_ini_file", "../sys_temp/rh_ml_setting.ini",

                             # "--gen_db_table_",
                             # "--rename_dataset_col", "renamed_dataset.csv",
                             # "--hist_folder", HIST_FOLDER,
                             # "--test_stream_file", STREAM_FILE,
                             # "--gen_dataset_",
                             ])
        else:
            sys.argv.extend(["--~help"])
    main()