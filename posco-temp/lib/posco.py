#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""
"""
import os
import sys
import json
import random
import csv
import datetime
import logging
# import imageio
import pickle
from logging.handlers import TimedRotatingFileHandler

import psutil
import time
import traceback
import configparser
import tensorflow as tf
from tensorflow.python.client import device_lib
from tensorflow.python.framework.errors_impl import InternalError
from sys_temp import machine_learner_2 as learn_handler

import numpy as np
from PIL import Image
from operator import itemgetter
from random import randint

__author__ = "Hoon Paek, Dong-gi Kim, Ji-young Moon"
__copyright__ = "Copyright 2007, The Cogent Project"
__credits__ = []
__license__ = "GPL"
__version__ = "0.0.1"
__maintainer__ = "Hoon Paek"
__email__ = "hoon.paek@mindslab.ai"
__status__ = "Prototype"  # Prototype / Development / Production

"""
    The string variable names come from init file.
    Please make them the same as those in init file.
"""

DIV_0_ERR = "#DIV/0!"
RECV_BUFF_SIZE = 1024
INFINITY = 999999.0


def read_csv_file(csv_file, delimiter=',', encoding='utf-8'):
    """ Read csv file

    :param csv_file:
    :param delimiter:
    :return:
    """
    mtx = []
    with open(csv_file, "r", encoding=encoding) as f:
        for row in csv.reader(f, delimiter=delimiter):
            row = [x.strip() if x != DIV_0_ERR else "" for x in row]
            mtx.append(row)
    return mtx


def convert_char_to_num(char):
    """Convert one-digit alphabet character to number.

    :param char:
    :return:
    """
    if len(char) > 1:
        print(" @ Error: length MUST be less than 'AA', {}\n".format(char))
        sys.exit()

    val = ord(char) - ord('A')
    if val >= 0:
        return val
    else:
        return int(char)


def crop_mtx(csv_mtx, start_pos, end_pos):
    """Crop the rectangle shaped cells from  csv format list

    :param csv_mtx:
    :param start_pos:
    :param end_pos:
    :return:
    """
    roi_mtx = []
    for yi in range(end_pos[0], end_pos[1] + 1):
        roi_mtx.append(csv_mtx[yi][start_pos[0]:start_pos[1] + 1])
    return roi_mtx


def print_and_write(msg, console=True, filename=''):
    if console:
        print(msg)
    if filename:
        with open(filename, 'a') as fid:
            fid.write(msg)
    pass


def calc_crop_info_from_ini(cfg):
    x_start = convert_char_to_num(cfg['x_start'])
    x_end = convert_char_to_num(cfg['x_end'])
    y_start = int(cfg['y_start']) - 1
    y_end = int(cfg['y_end']) - 1
    return (x_start, x_end), (y_start, y_end)


def transpose_list(in_list):
    try:
        return list(map(list, zip(*in_list)))
    except TypeError:
        return in_list


def get_col_list(list_obj, n):
    return list(map(itemgetter(n), list_obj))


def write_list_to_csv(dataset, filename):
    if filename:
        with open(filename, 'w') as f:
            for row_dat in dataset:
                f.write(','.join([str(x) for x in row_dat]) + '\n')
    pass


def split_dataset_into_train_and_test(dataset, percent_ratio, offset=1):
    train_dataset = []
    test_dataset = []
    for idx in range(offset):
        train_dataset.append(dataset[idx])
        test_dataset.append(dataset[idx])
    cnt_train = 0
    cnt_test = 0
    for idx in range(offset, len(dataset)):
        if random.randint(0, 100) < percent_ratio:
            train_dataset.append(dataset[idx])
            cnt_train += 1
        else:
            test_dataset.append(dataset[idx])
            cnt_test += 1
    # print("\n # data set division: ({:d}, {:d}) -> {:2d}".format(
    # cnt_train, cnt_test, int(cnt_train * 100. / float(cnt_train + cnt_test))))
    return train_dataset, test_dataset


def get_datetime(fmt="%Y-%m-%d-%H-%M-%S"):
    """ Get datetime with format argument.

    :param fmt:
    :return:
    """
    return datetime.datetime.now().strftime(fmt)


# def setup_logger(logger_name, log_prefix_name, level=logging.INFO, folder='.', console_=True):
#     """ Setup logger supporting two handlers of stdout and file.
#
#     :param logger_name:
#     :param log_prefix_name:
#     :param level:
#     :param folder:
#     :param console_:
#     :return:
#     """
#     crt_time = get_datetime()
#     log_file = os.path.join(*folder.split('/'), log_prefix_name + crt_time + '.log')
#     log_setup = logging.getLogger(logger_name)
#     formatter = logging.Formatter('%(name)-10s | %(asctime)s | %(levelname)-7s | %(message)s',
#                                   datefmt='%Y-%m-%d %H:%M:%S')
#     fileHandler = logging.FileHandler(log_file, mode='a')
#     fileHandler.setFormatter(formatter)
#     streamHandler = None
#     if console_:
#         streamHandler = logging.StreamHandler()
#         streamHandler.setFormatter(formatter)
#     log_setup.setLevel(level)
#     log_setup.addHandler(fileHandler)
#     if console_:
#         log_setup.addHandler(streamHandler)
#     return logging.getLogger(logger_name)

def setup_logger(logger_name, log_prefix_name, level=logging.INFO, folder='.', console_=True):
    """ Setup logger supporting two handlers of stdout and file.

    :param logger_name:
    :param log_prefix_name:
    :param level:
    :param folder:
    :param console_:
    :return:
    """
    crt_time = get_datetime()
    log_file = os.path.join(*folder.split('/'), log_prefix_name + crt_time + '.log')
    log_setup = logging.getLogger(logger_name)
    
    # prev code at 181119
	# formatter = logging.Formatter('%(name)-10s | %(asctime)s | %(levelname)-7s | %(message)s')
    # fileHandler = TimedRotatingFileHandler(log_file, when="midnight", interval=1, backupCount=30)
    #fileHandler.suffix = crt_time
    formatter = logging.Formatter('%(name)-10s | %(asctime)s | %(levelname)-7s | %(message)s',
                                  datefmt='%Y-%m-%d %H:%M:%S')
    fileHandler = logging.FileHandler(log_file, mode='a')
    fileHandler.setFormatter(formatter)
    streamHandler = None
    if console_:
        streamHandler = logging.StreamHandler()
        streamHandler.setFormatter(formatter)
    log_setup.setLevel(level)
    log_setup.addHandler(fileHandler)
    if console_:
        log_setup.addHandler(streamHandler)
    return logging.getLogger(logger_name)


def recv_all(connection, timeout_val=10., logger=None, file_prefix=None):
    byte_data = b''
    data_len_list = None  # [16, 15, 527, 837, 842]
    while True:
        try:
            connection.settimeout(timeout_val)
            part = connection.recv(RECV_BUFF_SIZE)
            # if logger:
            #     logger.info("recv_all ({:d} : {}".format(len(part), str(part)))
            if len(part) > 0:
                byte_data += part
                if data_len_list:
                    try:
                        if data_len_list.index(len(byte_data)) >= 0:
                            logger.info("Total packet length is {:d}".format(len(byte_data)))
                            return byte_data
                    except ValueError:
                        pass
            else:
                break
        except Exception as e:
            if logger:
                logger.error(str(e) + "\n" + traceback.format_exc())
            else:
                print(str(e) + "\n" + traceback.format_exc())
            break

    if file_prefix:
        with open(os.path.join("log", file_prefix + "_" + get_datetime() + ".txt"), "wb") as fid:
            fid.write(byte_data)

    return byte_data


def is_json(my_json):
    try:
        json.loads(my_json)
    except ValueError:
        return False
    return True


def complete_enum_list(enum_list_str=''):
    """ Complete enumeration list.

    :param enum_list_str:
    :return:
    """
    enum_list = [val.strip() for val in enum_list_str.split(',')]
    try:
        while True:
            pos = enum_list.index('~')
            if 0 < pos < len(enum_list):
                del enum_list[pos]
                enum_list = enum_list[0:pos] + [x for x in range(int(enum_list[pos - 1]) + 1,
                                                                 int(enum_list[pos]), 1)] + enum_list[pos:]
    except ValueError:
        pass
    return [int(x) for x in enum_list]


def make_gif(gif_filename, filenames, duration=1):
    """ Make gif based on the list composed of image filenames or numpy image objects.

    :param gif_filename:
    :param filenames:
    :param duration:
    :return:
    """
    if isinstance(filenames[0], str):
        images = [np.array(Image.open(fn)) for fn in filenames]
    else:
        images = filenames
    # imageio.mimwrite(gif_filename, images, format='GIF', duration=duration)
    return True


def gen_db_row_dat(table_info, handler):
    row_dat = {}
    for info in table_info[1:]:
        key = info[0]
        # print(key)
        if 'int' in info[1]:
            row_dat[key] = str(int(handler.vars[key].val))
        elif 'float' in info[1]:
            row_dat[key] = str(float(handler.vars[key].val))
        else:
            row_dat[key] = str("'" + handler.vars[key].val + "'")
    return row_dat

def gen_row_dat(table_info, handler):
    row_dat = {}
    for info in table_info[1:]:
        key = info[0]
        # print(key)
        if 'int' in info[1]:
            row_dat[key] = str(int(handler.vars[key].val))
        elif 'float' in info[1]:
            row_dat[key] = str(float(handler.vars[key].val))
        else:
            row_dat[key] = str(handler.vars[key].val)
    return row_dat

def compare_est_and_eval_dat(est_dict, eval_dict, logger=None):

    eval_dict['EST_TEMP'] = -1
    eval_dict['EST_RANGE_CHECK_VARS'] = ''
    fieldnames = sorted(eval_dict.keys())
    try:
        filter_est_dict = {key: value for key, value in est_dict.items()
                if key in fieldnames}
    except AttributeError:
        return False

    save_dir_path = '../check_cvt_data'
    if not os.path.isdir(save_dir_path):
        os.mkdir(save_dir_path)
        with open(os.path.join(save_dir_path, 'cvt_est_eval_dat.csv'), 'w') as f:  # Just use 'w' mode in 3.x
            w = csv.DictWriter(f, fieldnames=fieldnames)
            w.writeheader()
            w.writerow(filter_est_dict)
            w.writerow(eval_dict)
    else:
        with open(os.path.join(save_dir_path, 'cvt_est_eval_dat.csv'), 'a') as f:  # Just use 'w' mode in 3.x
            w = csv.DictWriter(f, fieldnames=fieldnames)
            w.writerow(filter_est_dict)
            w.writerow(eval_dict)

    return True

    # for key in eval_dict.keys():
    #     try:
    #         est_dat.append([key, est_dict[key]])
    #         eval_dat.append([key, eval_dict[key]])
    #     except ValueError as e:
    #         logger.info(e) if logger else print(e)
    #     except KeyError as e:
    #         logger.info(e) if logger else print(e)

    pass


def extract_rows_from_table(dataset, col_names):
    """ Extract rows from DB table.

    :param dataset:
    :param col_names:
    :return:
    """
    trans_dataset = transpose_list(dataset)
    rows = []
    if type(col_names).__name__ == 'str':
        col_names = [col_names]
    for col_name in col_names:
        idx = dataset[0].index(col_name)
        rows.append(trans_dataset[idx])
    if len(col_names) == 1:
        return rows[0]
    else:
        return transpose_list(rows)


def extract_cols_from_table(dataset, col_names):
    """ Extract columns from DB table.
    :param dataset:
    :param col_names:
    :return:
    """
    trans_dataset = transpose_list(dataset)
    rows = []
    if type(col_names).__name__ == 'str':
        col_names = [col_names]
    for col_name in col_names:
        idx = dataset[0].index(col_name)
        rows.append(trans_dataset[idx])
    if len(col_names) == 1:
        return rows[0]
    else:
        return transpose_list(rows)


def get_col_from_table(dataset, idx_name, idx_array, tar_name):
    ref_idx = dataset[0].index(idx_name)
    tar_idx = dataset[0].index(tar_name)
    trans_dataset = transpose_list(dataset)
    arr = []
    for i in idx_array:
        # print(i)
        pos = trans_dataset[ref_idx].index(str(i))
        arr.append(trans_dataset[tar_idx][pos])

    return arr


def get_model_dir(root_dir, category, method, version, result_file=""):
    """ Get the directory having model.

    :param root_dir:
    :param category:
    :param method:
    :param version:
    :return:
    """
    root_dirs = root_dir.split('/')
    dir_path = os.path.join(*root_dirs, category, method)
    versions = sorted(next(os.walk(dir_path))[1], reverse=True)
    if not os.path.isdir(os.path.join(dir_path, version)):
        i = 0
        model_dir = os.path.join(dir_path, versions[i])
        while not os.path.exists(os.path.join(model_dir, result_file)):
            i += 1
            model_dir = os.path.join(dir_path, versions[i])

    return model_dir


def get_learning_dataset_dir(root_dir, category):
    """ Get learning dataset directory.

    :param root_dir:
    :param category:
    :return:
    """

    root_dirs = root_dir.split('/')
    dir_path = os.path.join(*root_dirs, category)

    return dir_path


def run_estimation_CVT(method, model_dir, stream_dict, logger=None):
    """ Run estimation operation.

    :param method:
    :param model_dir:
    :param stream_dict:
    :param logger:
    :return:
    """
    output = None
    if method == 'RFR':
        msg = 'RFR estimation operation with {}'.format(model_dir)
        logger.info(msg) if logger else print(msg)
        model = pickle.load(open(os.path.join(model_dir, method + '.bin'), 'rb'))
        inputs = []
        for key in model[1]:
            try:
                inputs.append(stream_dict[key])
            except ValueError as e:
                logger.info(e) if logger else print(e)
        output = model[0].predict([inputs])[0]
    elif method == 'DNN':
        msg = 'EST DNN operation'
        logger.info(msg) if logger else print(msg)
        model = pickle.load(open(os.path.join(model_dir, method + '.bin'), 'rb'))
        inputs = []
        norm_dict = []
        norm_key = []
        norm_val = []

        for idx in range(len(model[1])):
            key = model[4][0][idx]
            val = RealToNorm(stream_dict[key], model[4][1][idx], model[4][2][idx])
            norm_key.append(key)
            norm_val.append(val)

        norm_dict = temp_dict(norm_val, norm_key)
        for key in model[1]:
            try:
                inputs.append(norm_dict[key])
            except ValueError as e:
                logger.info(e) if logger else print(e)

        norm_output = model[0].predict([inputs])[0]
        output = NormToReal(model[5], model[6], norm_output)

    else:
        msg = 'Invalid method, {}'.format(method)
        logger.info(msg) if logger else print(msg)
    return output


def normalize_list(stream_list, minmax_list, min_val=0, max_val=1):
    minmax_dict = {}
    for i in range(len(minmax_list[0])):
        minmax_dict[minmax_list[0][i]] = (minmax_list[1][i], minmax_list[2][i])

    for idx in range(len(stream_list)):

        val = stream_list[idx][1]

        if isinstance(val, (int, float)):  # if number

            val = float(val)

            var_name = stream_list[idx][0]
            minmax = minmax_dict[var_name]
            max_thresh = minmax[1]
            min_thresh = minmax[0]

            val_range = float(max_thresh - min_thresh)

            if val > max_thresh:
                val = max_thresh
            elif val < min_thresh:
                val = min_thresh
            elif val_range != 0:
                val = (val - min_thresh) / val_range * (max_val - min_val) + min_val

        stream_list[idx][1] = val


# #def encode_by_one_hot(stream_list, Feat):
def MinMaxScaler(data):
    numerator = data - np.min(data)
    denominator = np.max(data) - np.min(data)
    return numerator / (denominator + 1e-7)

def dnn_estimation(model_dir, stream_dict, Feat=None, old_var='', new_var='', logger=None):
    """ Run estimation operation 2.

    :param method:
    :param model_dir:
    :param stream_dict:
    :param logger:
    :return:
    """


    # var_names = reg_args['var_names']
    # Feat = dict['features']

    reg_args = pickle.load(open(os.path.join(model_dir, 'DNN.bin'), 'rb'))
    # reg_args = pickle.load(open(os.path.join('model\\RH\\DNN\\181012', 'DNN.bin'), 'rb'))
    msg = 'DNN operation'
    logger.info(msg) if logger else print(msg)

    dataset = reg_args['dataset'] # original code
    params = reg_args['params']
    # dataset = reg_args.dataset # 181029 modified
    # params = reg_args.params # 181029 modified
    name_list = []
    val_list = []
    for var_name in dataset[0]:  # dataset[0] contains variable names
        try:
            if var_name == old_var:
                val = stream_dict[new_var]
            else:
                val = stream_dict[var_name]
            name_list.append(var_name)
            val_list.append(val)
        except KeyError as e:
            print(e)
    target_dataset = [name_list, val_list]

    Learn = learn_handler.MachineLearner(Feat=Feat, dataset=target_dataset)
    Learn.normalize_number_features()
    Learn.encode_symbol_dataset_by_one_hot_encoder()

    target_dataset = Learn.dataset[1]
    target_dataset = np.array([target_dataset])
    target_dataset.shape = (1, len(Learn.dataset[1]))

    dnn_feat_col = [tf.contrib.layers.real_valued_column("", dimension=len(target_dataset[0]))]

    dnn_unit = params['unit'].split(',')
    dnn_unit = [int(u) for u in dnn_unit]

    if params['active_fn'] == "relu":
        dnn_active_fn = tf.nn.relu
    elif params['active_fn'] == "sigmoid":
        dnn_active_fn = tf.sigmoid
    else:
        dnn_active_fn = tf.tanh

    dnn_learn_rate = float(params['learn_rate'])
    if params['optimizer'] == "adadelta":
        dnn_optimizer = tf.train.AdadeltaOptimizer(learning_rate=dnn_learn_rate)
    elif params['optimizer'] == "adam":
        dnn_optimizer = tf.train.AdamOptimizer(learning_rate=dnn_learn_rate)
    elif params['optimizer'] == "ftrl":
        dnn_optimizer = tf.train.FtrlOptimizer(learning_rate=dnn_learn_rate)
    elif params['optimizer'] == "gd":
        dnn_optimizer = tf.train.GradientDescentOptimizer(learning_rate=dnn_learn_rate)
    elif params['optimizer'] == "proximalGrad":
        dnn_optimizer = tf.train.ProximalGradientDescentOptimizer(learning_rate=dnn_learn_rate)
    elif params['optimizer'] == "proximalAdagrad":
        dnn_optimizer = tf.train.ProximalAdagradOptimizer(learning_rate=dnn_learn_rate)
    elif params['optimizer'] == "RMSProp":
        dnn_optimizer = tf.train.RMSPropOptimizer(learning_rate=dnn_learn_rate)
    else:
        dnn_optimizer = tf.train.AdagradOptimizer(learning_rate=dnn_learn_rate)

    dnn_dropout = None
    if params['dropout'] != 'None':
        dnn_dropout = float(params['dropout'])

    config_proto = tf.ConfigProto(allow_soft_placement=True, log_device_placement=True)
    config_proto.gpu_options.allow_growth = False
    reg = tf.contrib.learn.DNNRegressor(feature_columns=dnn_feat_col,
                                        activation_fn=dnn_active_fn,
                                        hidden_units=dnn_unit,
                                        model_dir=model_dir + "/dnn_model",
                                        optimizer=dnn_optimizer, dropout=dnn_dropout)

    predict_arr = reg.predict(target_dataset)
    predict_arr = np.array(list(predict_arr))

    result = denormalize_output(predict_arr, Feat.vars[dataset[0][0]].max_thresh, Feat.vars[dataset[0][0]].min_thresh)
    print("DNN estimation: " + str(result[0]))
    return result[0]


def dnn_estimation_multiply_y(model_dir, stream_dict, Feat=None, logger=None):
    """ Run estimation operation 2.

    :param method:
    :param model_dir:
    :param stream_dict:
    :param logger:
    :return:
    """

    # var_names = reg_args['var_names']
    # Feat = dict['features']

    reg_args = pickle.load(open(os.path.join(model_dir, 'DNN.bin'), 'rb'))
    msg = 'DNN operation'
    logger.info(msg) if logger else print(msg)

    dataset = reg_args['dataset']
    params = reg_args['params']
    name_list = []
    val_list = []
    for var_name in dataset[0]:  # dataset[0] contains variable names
        try:
            val = stream_dict[var_name]
            name_list.append(var_name)
            val_list.append(val)
        except KeyError as e:
            print(e)
    target_dataset = [name_list, val_list]

    Learn = learn_handler.MachineLearner(Feat=Feat, dataset=target_dataset)
    Learn.normalize_number_features()
    Learn.encode_symbol_dataset_by_one_hot_encoder()

    target_dataset = Learn.dataset[1]
    target_dataset = np.array([target_dataset])
    target_dataset.shape = (1, len(Learn.dataset[1]))

    dnn_feat_col = [tf.contrib.layers.real_valued_column("", dimension=len(target_dataset[0]))]

    dnn_unit = params['unit'].split(',')
    dnn_unit = [int(u) for u in dnn_unit]

    if params['active_fn'] == "relu":
        dnn_active_fn = tf.nn.relu
    elif params['active_fn'] == "sigmoid":
        dnn_active_fn = tf.sigmoid
    else:
        dnn_active_fn = tf.tanh

    dnn_learn_rate = float(params['learn_rate'])
    if params['optimizer'] == "adadelta":
        dnn_optimizer = tf.train.AdadeltaOptimizer(learning_rate=dnn_learn_rate)
    elif params['optimizer'] == "adam":
        dnn_optimizer = tf.train.AdamOptimizer(learning_rate=dnn_learn_rate)
    elif params['optimizer'] == "ftrl":
        dnn_optimizer = tf.train.FtrlOptimizer(learning_rate=dnn_learn_rate)
    elif params['optimizer'] == "gd":
        dnn_optimizer = tf.train.GradientDescentOptimizer(learning_rate=dnn_learn_rate)
    elif params['optimizer'] == "proximalGrad":
        dnn_optimizer = tf.train.ProximalGradientDescentOptimizer(learning_rate=dnn_learn_rate)
    elif params['optimizer'] == "proximalAdagrad":
        dnn_optimizer = tf.train.ProximalAdagradOptimizer(learning_rate=dnn_learn_rate)
    elif params['optimizer'] == "RMSProp":
        dnn_optimizer = tf.train.RMSPropOptimizer(learning_rate=dnn_learn_rate)
    else:
        dnn_optimizer = tf.train.AdagradOptimizer(learning_rate=dnn_learn_rate)

    dnn_dropout = None
    if params['dropout'] != 'None':
        dnn_dropout = float(params['dropout'])

    config_proto = tf.ConfigProto(allow_soft_placement=True, log_device_placement=True)
    config_proto.gpu_options.allow_growth = False
    reg = tf.contrib.learn.DNNRegressor(feature_columns=dnn_feat_col,
                                        activation_fn=dnn_active_fn,
                                        hidden_units=dnn_unit,
                                        model_dir=model_dir + "/dnn_model",
                                        optimizer=dnn_optimizer, dropout=dnn_dropout,
                                        label_dimension=2,
                                        config=tf.contrib.learn.RunConfig(session_config=config_proto))

    predict_arr = reg.predict(target_dataset)
    predict_arr = np.array(list(predict_arr))

    result_time = denormalize_output(predict_arr[:,0], Feat.vars[dataset[0][0]].max_thresh, Feat.vars[dataset[0][0]].min_thresh)
    result_temp = denormalize_output(predict_arr[:,0], Feat.vars[dataset[0][1]].max_thresh, Feat.vars[dataset[0][1]].min_thresh)

    print("DNN estimation: time - " + str(result_time[0]) + ", temp - " + str(result_temp[0]))

    return result_time[0], result_temp[0]


def rfr_estimation(model_dir, stream_dict, old_var='', new_var='', logger=None):
    msg = 'RFR estimation operation with {}'.format(model_dir)
    logger.info(msg) if logger else print(msg)
    model = pickle.load(open(os.path.join(model_dir, 'RFR.bin'), 'rb'))
    # if new_var:
    #     idx = model.X_name.index(old_var)
    #     model.X_name[idx] = new_var
    inputs = []
    for key in model.X_name:
        try:
            if key == old_var:
                inputs.append([old_var, stream_dict[new_var]])
            else:
                inputs.append([key, stream_dict[key]])
        except ValueError as e:
            logger.info(e) if logger else print(e)
        except KeyError as e:
            logger.info(e) if logger else print(e)
    inputs = transpose_list(inputs)
    model.encode_symbol_dataset_by_int(dataset=inputs)
    return model.model.predict([model.dataset[1]])[0] ## model to ml_model

def lgbmr_estimation(model_dir, stream_dict, old_var='', new_var='', logger=None, process_type='L'):
    msg = 'LGBMR estimation operation with {}'.format(model_dir)
    logger.info(msg) if logger else print(msg)
    if process_type == 'H':
        model = pickle.load(open(os.path.join(model_dir, 'LGBMR_H.bin'), 'rb'))
    else:
        model = pickle.load(open(os.path.join(model_dir, 'LGBMR_L.bin'), 'rb'))
    # if new_var:
    #     idx = model.X_name.index(old_var)
    #     model.X_name[idx] = new_var
    inputs = []
    for key in model.X_name:
        try:
            if key == old_var:
                inputs.append([old_var, stream_dict[new_var]])
            else:
                inputs.append([key, stream_dict[key]])
        except ValueError as e:
            logger.info(e) if logger else print(e)
        except KeyError as e:
            logger.info(e) if logger else print(e)
    inputs = transpose_list(inputs)
    model.encode_symbol_dataset_by_int(dataset=inputs)
    return model.model.predict([model.dataset[1]])[0] ## model to ml_model

def denormalize_output(predicted_output, real_max, real_min, norm_min=-1, norm_max=1):
    return (predicted_output - norm_min) / (norm_max - norm_min) * (real_max - real_min) + real_min

def get_db_table_info(filename):
    return read_csv_file(filename)


def convert_db_to_dict(values, keys):
    db_dict = {}
    for idx in range(len(keys)):
        db_dict[keys[idx][0]] = values[idx]
    return db_dict


def check_process_by_word(word, logger=None):
    for proc in psutil.process_iter():
        msg = " {} ".format(proc.name())
        try:
            for argument in proc.cmdline():
                msg += " {} ".format(argument)
                if word in argument:
                    cmd = "{}".format(proc.name())
                    for arg in proc.cmdline():
                        cmd += "  {}".format(arg)
                    msg = "Found the process, {}".format(cmd)
                    logger.info(msg) if logger else print(msg)
                    return proc.pid, cmd
        except psutil.AccessDenied:
            pass
        print(msg)

    return -1, None


def kill_the_process(cmd_line, pause_time=5, logger=None):
    """ Kill the process.

    :param cmd_line:
    :param pause_time:
    :param logger:
    :return:
    """
    pid, full_cmd = check_process_by_word(cmd_line)
    if pid > 0 and pid != os.getpid():
        psutil.Process(pid).terminate()
        msg = "Terminate existing process, {}".format(full_cmd)
        logger.info(msg) if logger else print(msg)
        time.sleep(pause_time)
    else:
        msg = "No existing process, {}".format(cmd_line)
        logger.info(msg) if logger else print(msg)

    return True


def temp_dict(values, keys):
    db_dict = {}
    for idx in range(len(keys)):
        db_dict[keys[idx]] = values[idx]
    return db_dict


def NormToReal(given_output, predict_output):
    print(max(given_output), min(given_output))
    return predict_output * (max(given_output) - min(given_output)) + min(given_output)


def RealToNorm(given_val, trained_min, trained_max):
    numerator = given_val - trained_min
    denominator = trained_max - trained_min
    return numerator / (denominator + 1e-7)


def calc_MAPE(actual_arr, estimate_arr):
    """ calculate Mean Absolute Percentage Error.

    :param actual_arr:
    :param estimate_arr:
    :return:
    """

    arr = []
    for i in range(len(actual_arr)):
        if actual_arr[i] == 0:
            arr.append(0)
        else:
            arr.append(abs((actual_arr[i] - estimate_arr[i]) / float(actual_arr[i])))

    return 100. * sum(arr) / float(len(arr))


def calc_RMSE(actual_arr, estimate_arr=None):
    """ calculate Root Mean Square Error.
    If the second argument is NOT defined,
    it is assumed that the first argument is error value instead of actual value.

    :param actual_arr:
    :param estimate_arr:
    :return:
    """

    if estimate_arr is not None:
        arr = []
        for i in range(len(actual_arr)):
            arr.append((actual_arr[i] - estimate_arr[i]) * (actual_arr[i] - estimate_arr[i]))
        return np.sqrt(sum(arr) / float(len(arr)))
    else:
        arr = []
        for i in range(len(actual_arr)):
            arr.append(actual_arr[i] * actual_arr[i])
        return np.sqrt(sum(arr) / float(len(arr)))


def calc_RMSRE(actual_arr, estimate_arr=None):
    """ calculate Root Mean Square Ratio Error.
    If the second argument is NOT defined,
    it is assumed that the first argument is error value instead of actual value.
    :param actual_arr:
    :param estimate_arr:
    :return:
    """

    if estimate_arr is not None:
        arr = []
        for i in range(len(actual_arr)):
            arr.append(((actual_arr[i] - estimate_arr[i]) / actual_arr[i]) ** 2)
        return np.sqrt(sum(arr) / float(len(arr)))
    else:
        arr = []
        for i in range(len(actual_arr)):
            arr.append((actual_arr[i] / actual_arr[i]) * (actual_arr[i] / actual_arr[i]))
        return np.sqrt(sum(arr) / float(len(arr)))



if __name__ == "__main__":
    kill_the_process("infinite_loop")
    pass
