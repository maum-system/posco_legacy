#!/usr/bin/env python
# -*- coding: utf-8 -*-
import configparser
import datetime
import logging
import os
import time
import traceback
import json
import socket
import psutil

from sys_temp import dataset_handler
from lib import data as data_lib
from mssql.pymssqlwrapper import PyMssqlWrapper
from lib import HoonUtils as utils
import unicodedata
import sys


RECV_BUFF_SIZE = 1024


def print_and_write(msg, console=True, filename=''):
    if console:
        print(msg)
    if filename:
        with open(filename, 'a') as fid:
            fid.write(msg)
    pass


def calc_crop_info_from_ini(cfg, x_start_='x_start', x_end_='x_end', y_start_='y_start', y_end_='y_end'):
    x_start = data_lib.convert_char_to_num(cfg[x_start_])
    x_end = data_lib.convert_char_to_num(cfg[x_end_])
    y_start = int(cfg[y_start_]) - 1
    y_end = int(cfg[y_end_]) - 1
    return (x_start, x_end), (y_start, y_end)


def get_datetime(fmt="%Y-%m-%d-%H-%M-%S"):
    """ Get datetime with format argument.

    :param fmt:
    :return:
    """
    return datetime.datetime.now().strftime(fmt)


def setup_logger(logger_name, 
                 log_prefix_name,
                 level=logging.INFO,
                 folder='.',
                 console_=True):
    """ Setup logger supporting two handlers of stdout and file.

    :param logger_name:
    :param log_prefix_name:
    :param level:
    :param folder:
    :param console_:
    :return:
    """
    crt_time = get_datetime()

    log_file = os.path.join(*folder.split('/'), log_prefix_name + crt_time + '.log')
    log_setup = logging.getLogger(logger_name)

    formatter = logging.Formatter('%(name)-10s | %(asctime)s | %(levelname)-7s | %(message)s',
                                  datefmt='%Y-%m-%d %H:%M:%S')
    fileHandler = logging.FileHandler(log_file, mode='a')
    fileHandler.setFormatter(formatter)
    streamHandler = None
    if console_:
        streamHandler = logging.StreamHandler()
        streamHandler.setFormatter(formatter)
    log_setup.setLevel(level)
    log_setup.addHandler(fileHandler)
    if console_:
        log_setup.addHandler(streamHandler)
    return logging.getLogger(logger_name)


def accept_receive_and_check_in_server(sock, logger=None):
    """

    :param sock:
    :param logger:
    :return:
    """
    logger = utils.what_if_logger_is_not_defined(logger)

    logger.info(" # Waiting for a connection...")
    con, client_address = sock.accept()
    logger.info(" # Connected with {}.".format(client_address))

    str_dat = None
    status = 0

    try:
        str_dat = recv_all(con, logger=logger).decode('utf-8')
        logger.info(" # Received: \"{}\"".format(str_dat))
        if utils.is_json(str_dat):
            dict_dat = json.loads(str_dat)
            cmd = dict_dat['cmd'].lower()
            if cmd == 'check':
                logger.info(" # Check operation")
                result = 'healthy'
            elif cmd == 'stop':
                logger.info(" # Stop operation")
                result = 'Bye'
                status = -1
            else:
                logger.info(" # Invalid command, {}.".format(cmd))
                result = 'Invalid'
            sent_msg = '{"result":"' + result + '"}'
            con.sendall(sent_msg.encode('utf-8'))
            logger.info(" # Sent: {}".format(sent_msg))
        else:
            status = 1
    except Exception as e:
        logger.error(e)
    finally:
        con.close()

    return status, str_dat


def recv_all(connection, timeout_val=10., logger=None, file_prefix=None):
    byte_data = b''
    data_len_list = None  # [16, 15, 527, 837, 842]
    while True:
        try:
            connection.settimeout(timeout_val)
            part = connection.recv(RECV_BUFF_SIZE)
            if data_lib.is_json(str(part)):
                return part
            # if logger:
            #     logger.info("recv_all ({:d} : {}".format(len(part), str(part)))
            if len(part) > 0:
                byte_data += part
                if data_len_list:
                    try:
                        if data_len_list.index(len(byte_data)) >= 0:
                            logger.info("Total packet length is {:d}".format(len(byte_data)))
                            return byte_data
                    except ValueError:
                        pass
            else:
                break
        # except connection.error as e:
        #     if logger:
        #         logger.error("socket error: {}".format(str(e)))
        #     else:
        #         print(e)
        #     break
        except Exception as e:
            if logger:
                logger.error(str(e) + "\n" + traceback.format_exc())
            else:
                print(str(e) + "\n" + traceback.format_exc())
            break

    if file_prefix:
        with open(os.path.join("log", file_prefix + "_" + get_datetime() + ".txt"), "wb") as fid:
            fid.write(byte_data)

    return byte_data


def get_status_msg(msg, logger):
    msg = msg.lower()
    if msg == 'check':
        logger.info("Check operation")
        return 'Healthy'
    elif msg == 'stop':
        logger.info("Stop operation")
        return "Bye"
    else:
        logger.info("Invalid command, {}.".format(msg))
        return "Invalid"


def check_process_by_word(word, logger=None):
    for proc in psutil.process_iter():
        msg = " {} ".format(proc.name())
        try:
            for argument in proc.cmdline():
                msg += " {} ".format(argument)
                if word in argument:
                    cmd = "{}".format(proc.name())
                    for arg in proc.cmdline():
                        cmd += "  {}".format(arg)
                    msg = "Found the process, {}".format(cmd)
                    logger.info(msg) if logger else print(msg)
                    return proc.pid, cmd
        except psutil.AccessDenied:
            pass
        print(msg)

    return -1, None


def kill_process(cmd_line, pause_time=5, logger=None):
    """ Kill the process.

    :param cmd_line:
    :param pause_time:
    :param logger:
    :return:
    """
    pid, full_cmd = check_process_by_word(cmd_line)
    if pid > 0 and pid != os.getpid():
        psutil.Process(pid).terminate()
        msg = "Terminate existing process, {}".format(full_cmd)
        logger.info(msg) if logger else print(msg)
        time.sleep(pause_time)
    else:
        msg = "No existing process, {}".format(cmd_line)
        logger.info(msg) if logger else print(msg)

    return True


def validate_directories(path, root_dir=''):
    joined_dirs = root_dir
    dirs_list = path.split('/')
    for i in range(len(dirs_list)):
        joined_dirs = os.path.join(joined_dirs, dirs_list[i])
        if not os.path.exists(joined_dirs):
            os.makedirs(joined_dirs)


def get_model_dir(root_dir, category, method, version, result_file=""):
    """ Get the directory having model.

    :param root_dir:
    :param category:
    :param method:
    :param version:
    :param result_file:
    :return:
    """
    root_dirs = root_dir.split('/')
    dir_path = os.path.join(*root_dirs, category, method)
    validate_directories(dir_path)
    versions = sorted(next(os.walk(dir_path))[1], reverse=True)

    if len(versions) == 0:
        return None

    model_dir = None
    if not os.path.isdir(os.path.join(dir_path, version)):
        i = 0
        model_dir = os.path.join(dir_path, versions[i])
        while not os.path.exists(os.path.join(model_dir, result_file)):
            i += 1
            model_dir = os.path.join(dir_path, versions[i])

    return model_dir


def get_learning_dataset_dir(root_dir, category):
    """ Get learning dataset directory.

    :param root_dir:
    :param category:
    :return:
    """

    root_dirs = root_dir.split('/')
    dir_path = os.path.join(*root_dirs, category)

    return dir_path


def get_days_in_month(model_date):
    if model_date[2:-2] in ['01', '03', '05', '07', '08', '10', '12']:
        return 31
    if model_date[2:-2] in ['04', '06', '09', '11']:
        return 30
    if int(model_date[:2]) % 4 == 0:
        return 29
    return 28


def time_elapsed(condition, days_passed, days_in_month):
    if condition == 'everyday' and days_passed >= 1:
        return True
    if condition == 'weekly' and days_passed >= 7:
        return True
    if condition == 'biweekly' and days_passed >= 14:
        return True
    if condition == 'monthly' and days_passed >= days_in_month:
        return True

    return False


def write_to_db(category, db_data):
    if category == 'RH':
        idx_RH_LAST_TEMP = db_data[-1][0].index('RH_LAST_TEMP')
        idx_TD_AVG_TEMP = db_data[-1][0].index('CC_TD_AVG_TEMP')
        idx_CALC_OUTPUT = db_data[-1][0].index('CALC_OUTPUT_2')
        for i in range(1, len(db_data[-1])):
            db_data[-1][i][idx_CALC_OUTPUT] = db_data[-1][i][idx_RH_LAST_TEMP] - db_data[-1][i][idx_TD_AVG_TEMP]
    elif category == 'CVT':
        idx_CONV_ENDPNT_TEMP = db_data[-1][0].index('CONV_ENDPNT_TEMP')
        idx_RH_ARR_TEMP = db_data[-1][0].index('RH_ARR_TEMP')
        idx_OUT_DROP = db_data[-1][0].index('OUT_DROP')
        for i in range(1, len(db_data[-1])):
            db_data[-1][i][idx_OUT_DROP] = db_data[-1][i][idx_CONV_ENDPNT_TEMP] - db_data[-1][i][idx_RH_ARR_TEMP]


def create_db_handlers(ini, logger, DB_OP_=True):
    if DB_OP_:
        est_db_handler = PyMssqlWrapper(server_name=ini['EST_DB']['server_name'],
                                        port=ini['EST_DB']['port'],
                                        username=ini['EST_DB']['username'],
                                        password=ini['EST_DB']['password'],
                                        db_name=ini['EST_DB']['db_name'],
                                        table_name=ini['EST_DB']['table_name'],
                                        logger=logger)

        eval_db_handler = PyMssqlWrapper(server_name=ini['EVAL_DB']['server_name'],
                                         port=ini['EVAL_DB']['port'],
                                         username=ini['EVAL_DB']['username'],
                                         password=ini['EVAL_DB']['password'],
                                         db_name=ini['EVAL_DB']['db_name'],
                                         table_name=ini['EVAL_DB']['table_name'],
                                         logger=logger)
    else:
        est_db_handler = False
        eval_db_handler = False

    return est_db_handler, eval_db_handler


def get_feat_handler(var_cfg_file, var_csv_file):
    config = configparser.ConfigParser()
    config.read(var_cfg_file)

    var_mtx = data_lib.read_csv_file(var_csv_file)
    start_pos, end_pos = calc_crop_info_from_ini(config['var_info_csv'])
    roi_var_mtx = data_lib.crop_mtx(var_mtx, start_pos, end_pos)

    feat_handler = dataset_handler.PoscoTempEstModel(roi_var_mtx)
    feat_handler.init_feat_class(config, offset=start_pos[0])
    feat_handler.req_enum_list = data_lib.get_range_list(config['stream']['req_enum_list'])
    feat_handler.rep_enum_list = data_lib.get_range_list(config['stream']['rep_enum_list'])

    return feat_handler


def send_data_to_server_once(dat, ip_address, port, logger=None):
    if not logger:
        logger = utils.what_if_logger_is_not_defined(logger)
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    logger.info(" # Opening socket")
    server_address = (ip_address, port)
    logger.info(" # Connecting socket to %s:%s" % server_address)
    try:
        sock.connect(server_address)
        logger.info(" # Sending \"{}\"".format(dat))
        sock.sendall(dat.encode('utf-8'))
    except ConnectionRefusedError as e:
        logger.error(" @ ConnectionRefusedError " + str(e))
    finally:
        sock.close()
        logger.info(" # Closing socket")


def get_ml_result(mode, model_num, data):
    result = ''
    if mode.upper() == 'EST':
        result = estimate(model_num, data)
    elif mode.upper() == 'EVAL':
        result = evaluate(model_num, data)
    return result


def estimate(model_num, data):
    return 'estimation: ' + data


def evaluate(model_num, data):
    return 'evaluation: ' + data

def unicode_normalize(string):
    if string is None:
        return string
    else:
        return unicodedata.normalize('NFC', string)

def get_ini_parameters(ini_fname, cmt_delimiter="###"):
    ini = configparser.ConfigParser()
    file_exists(ini_fname, exit_=True)
    ini.read(ini_fname, encoding='utf-8')
    return remove_comments_in_ini(ini, cmt_delimiter=cmt_delimiter)

def file_exists(filename, print_=False, exit_=False):
    """
    Check if a file exists or not.
    :param filename:
    :param print_:
    :param exit_:
    :return True/False:
    """
    if not os.path.isfile(filename):
        if print_ or exit_:
            print("\n @ Warning: file not found, {}.\n".format(filename))
        if exit_:
            sys.exit()
        return False
    else:
        return True

def remove_comments_in_ini(ini, cmt_delimiter='###'):
    """
    Remove comments in ini file,
    where comment is text strings rting with comment delimiter.
    :param ini:
    :param cmt_delimiter:
    :return:
    """
    for section in ini.sections():
        for key in ini[section]:
            ini[section][key] = ini[section][key].split(cmt_delimiter)[0].strip()
    return ini

def get_stdout_logger(logger=None):
    if logger is None:
        logger = LoggerWrapper()
        logger.info = print
        logger.error = print
    return logger

class LoggerWrapper:

    def info(self): pass

    def error(self): pass

def setup_logger_with_ini(ini, logging_=True, console_=True):
    backup_count = 0
    if 'backup_count' in ini:
        backup_count = int(ini['backup_count'])

    logger = setup_logger(ini['name'],
                          ini['prefix'],
                          folder=ini['folder'],
                          console_=console_)

    return logger