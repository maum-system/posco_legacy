import pandas as pd

def refine_dataset_by_y_value(feature):

    dataset = feature.dataset

    df = pd.DataFrame(dataset[1:], columns=dataset[0])

    df['CHEM05_F_BAP_C_VAL_1'] = pd.to_numeric(df['CHEM05_F_BAP_C_VAL_1'], errors='coerce')

    df['CHEM05_F_BAP_C_VAL_1'] = df['CHEM05_F_BAP_C_VAL_1'].astype(float) * 10000

    df['OUT_BAP_DEP_CARBON'] = df.pop('CHEM05_F_BAP_C_VAL_1')
    df['OUT_RH_ARR_OXYGEN'] = df.pop('RH_ARR_O2')

    refined_dataset_header = df.columns.tolist()
    refined_dataset = df.values.tolist()

    refined_dataset = [refined_dataset_header] + refined_dataset

    feature.dataset = refined_dataset

def run(feature):
    refine_dataset_by_y_value(feature)


