import pandas as pd

def refine_dataset_by_y_value(feature):

    dataset = feature.dataset

    df = pd.DataFrame(dataset[1:], columns=dataset[0])

    df['OUT_PATTERN_OPERATION'] = df.pop('PATTERN_OPERATION')

    refined_dataset_header = df.columns.tolist()
    refined_dataset = df.values.tolist()

    refined_dataset = [refined_dataset_header] + refined_dataset

    feature.dataset = refined_dataset

def run(feature):
    refine_dataset_by_y_value(feature)


