import pandas as pd

def refine_dataset_by_y_value(feature):

    dataset = feature.dataset

    df = pd.DataFrame(dataset[1:], columns=dataset[0])

    df['F_CC_C'] = pd.to_numeric(df['F_CC_C'], errors='coerce')

    df['F_CC_C'] = df['F_CC_C'].astype(float) * 10000

    df['OUT_TUNDISH_CARBON'] = df.pop('F_CC_C')

    refined_dataset_header = df.columns.tolist()
    refined_dataset = df.values.tolist()

    refined_dataset = [refined_dataset_header] + refined_dataset

    feature.dataset = refined_dataset

def run(feature):
    refine_dataset_by_y_value(feature)

