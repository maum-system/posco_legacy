import csv

# input POSCO metadata as var.csv and output pseudo-generated data
DATA_LEN = 20
INPUT_VAR_CSV_PATH = "./rh_var.csv"
OUTPUT_DATA_CSV_PATH = "./pseudo_data.csv"





# required column names in csv file
VAR_NAME = "object_name"
RANGE_MIN = "range_min"
RANGE_MAX = "range_max"
RANGE_DEFAULT = "range_default"
STR_DOMAIN = "str_domain"
ATTRIBUTE_USAGE = "dataset"
DATA_TYPE = "data_type"

def read_var_csv_stripped(csv_filepath, *required_fields):
    # load metadata csv file into dataframe while checking required fields.
    # raise AttributeError if any of required field names do not exist
    mtx = []
    with open(csv_filepath, "r", encoding='utf-8-sig') as f:
        for row in csv.reader(f, delimiter=","):
            row = [x.strip() if x != "#DIV/0!" else "" for x in row]
            mtx.append(row)

    df_start_row = -1
    for i, row in enumerate(mtx):
        if all(field in row for field in required_fields):
            return mtx[i], mtx[i + 1:]

    print("ERROR: required column names do not exist. check var_csv file or constant values in this script")
    raise AttributeError


# def random_


def main():
    data = []

    cols, var_mtx = read_var_csv_stripped(INPUT_VAR_CSV_PATH,
                                          VAR_NAME, RANGE_MIN, RANGE_MAX, RANGE_DEFAULT, STR_DOMAIN, ATTRIBUTE_USAGE)

    var_name_idx = cols.index(VAR_NAME)
    range_min_idx = cols.index(RANGE_MIN)
    range_max_idx = cols.index(RANGE_MAX)
    range_default_idx = cols.index(RANGE_DEFAULT)
    str_domain_idx = cols.index(STR_DOMAIN)

    # create columns for data generation
    valid_data_cols = []
    for i, var_row in enumerate(var_mtx):
        if var_row[range_default_idx] != "" or var_row[str_domain_idx] != "":
            valid_data_cols.append(var_row[var_name_idx])
    data.append(valid_data_cols)

    # create data
    for n in range(DATA_LEN):
        new_sample = []
        for i, var_row in enumerate(var_mtx):
            if var_row[range_default_idx] != "":
                new_sample.append(var_row[range_default_idx])
                pass
            elif var_row[str_domain_idx] != "":
                new_sample.append(var_row[str_domain_idx].split(",")[0])
                pass
        data.append(new_sample)
    print(data)

    with open(OUTPUT_DATA_CSV_PATH, "w+", newline="") as out_csv:
        writer = csv.writer(out_csv, delimiter=',')
        writer.writerows(data)


if __name__ == "__main__":
    main()