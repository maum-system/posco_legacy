# posco-temp

## Environment
- Python 3.8
- pip install -r requirements.txt


## 주요 실행 코드 위치
* dataset handler: sys_temp/dataset_handler.py
* learning handler: sys_temp/learning_handler.py
    * cvt 학습에 사용할 파일():
        * cvt_cfg_posco.ini 파일의 dataset_file, dataset_file_h 에 적힌 두 파일
    * rh: dataset_file에 적힌 파일
* 모델이 생성되는 위치: sys_temp 폴더에 생성이 된다.
    * 수동으로 아래와 같이 옮겨야함.
    * `sys_temp/model/RH/LGBMR/200612/LGBMR.bin`
    * `sys_temp/model/CVT/LGBMR/200612/LGBMR_H.bin`
    * `sys_temp/model/CVT/LGBMR/200612/LGBMR_L.bin`
* SERVER
    * EST: sys_temp/est_server.py
    * EVAL: sys_temp/eval_server.py
* Client Test
    * sys_temp/ongoing_client.py

## 성능 평가
*  선택한 기간 동안의 Acc와 RMSE 계산.
* raw 데이터와 그래프를 파일로 저장
* db_analysis/analyze_handler.py 코드 사용
    * analyze_df.py
        * DataFrame을 이용해 DB에서 가져온 데이터를 원하는 기준에 맞게 선택한다.
        * 모델 별 조건에 맞게, 결측치와 이상치 제외 등등 ...  
    * analyze_plt.py
        * 성능 평가 결과물인 그래프를 그린다.
        * 시간에 따른 Raw data와 Acc의 변화, 실측값과 예측값 비교 그래프 등
    * analyze_utils.py
        * 각종 도구들
* analyze_ppt.py
    * 성능 평가 결과물을 이용해 ppt를 제작한다.
* count_zero.py, count_zero_and_acc.py
    * 온도 모델 이슈인 CC_TD_AVG_TEMP의 측온 횟수와 정확도의 관계를 파악하기 위한 코드.
    

## 프로젝트 구조
    .
    ├──db_analysis/                     # 성능평가 및 보고서 작성을 위한 DB 데이터 분석
    ├──db_setup/                        # 데이터베이스 테이블 초기 구축
    ├──handlers/                        # CSV데이터 처리 및 학습
    ├──lib/                             # 각종 utility & helper function
    ├──models/                          
    ├──mssql/                           # 각종 mssql 데이터베이스 쿼리 모듈
    ├──servers/                         # 서버 실행 코드
    └──sys_cvt_decarb/                  # 변수정의서 및 각종 DB & CSV & 학습 관련 설정, 서버 테스트용 데이터 등


## 실행 가능 코드
    .
    ├──db_analysis/
    │   ├──analyze.py
    │   ├──analyze_handler.py           # DB 데이터를 불러와서 특정 기간에 대해 성능평가 및 결과물 생성.
    │   ├──analyze_ppt.py               # analyze_handler.py 결과물을 이용해 자동으로 PPT 보고서 생성.
    │   ├──analyze_ppt_table.py         # PPT 보고서에 첨부할 각종 성능평가 결과값 출력.
    │   └──input_var_graph.py           # 선택한 기간만큼 DB 데이터에 대한 그래프 생성. 시간별 데이터의 경향 파악에 유용함.
    ├──db_setup/
    │   └──setup_mssql_db.py            # 데이터베이스 테이블 초기 생성.
    ├──handlers/
    │   ├──analysis_handler.py
    │   ├──compare_features.py
    │   ├──dataset_handler.py           # CSV 데이터 각종 데이터 정제 및 변수중요도 분석.
    │   ├──est_handler.py               
    │   ├──eval_handler.py
    │   ├──learning_handler.py          
    │   └──test_model_handler.py
    ├──servers/
    │   ├──model_server.py              # 단일 서버 실행 (설정에서 모델, EST, EVAL 선택)
    │   ├──ongoing_client.py            # 로컬에서 테스트를 위한 테스트 클라이언트.
    │   ├── ...
    │   └── process_manager.py
    ├──sys_cvt_decarb/
    │   └── learning_handler.py         # 정제된 CSV 데이터를 이용해 학습 및 모델 생성.
    └──

