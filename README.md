## POSCO 2021년 4월자 프로젝트 코드
python 3.7<br>
sklearn<br>
tensorflow<br>
MSSQL<br>
<br>
<br> 
현재 포스코 보안 정책으로 인해, 작업하던 서버에서 외부망이 차단된 상태입니다... <br>
<b>리뷰 목적으로만 이용할 수 있습니다.</b> <br>
구전으로 내려오는 전설에 따르면 이 프로젝트를 잠시라도 거쳐갔던 사람들의 전공만 5개가 넘는다고 합니다...... <br>

https://mindslab.atlassian.net/wiki/spaces/PH/overview?homepageId=4873036327 <br>

현재 로컬에서 테스트를 했던 repo는 posco-cvt-decarb 하나입니다. (나머지는 데이터를 빼오지 못했음)<br>

### posco-cvt-decarb
전로(converter) 탈탄(decarburization)

### posco-rh-decarb
RH 탈탄

### posco-temp
온도모델

