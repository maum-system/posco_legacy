#! /usr/bin/env python
# -*- coding: utf-8 -*-
import sys
import argparse
import pandas as pd
import collections
import os
import configparser
from copy import copy
import copy
from lib import learning as learning_lib
from lib import sys_lib as sys_lib
from lib import data as data_lib
from refine_model import model_1, model_3_85_O, model_3_85_C_O
import matplotlib.pyplot as plt
from models import est_eval
from db_analysis import analyze_plt
from db_analysis.analyze_plt import MakePlot
from db_analysis.analyze_df import EditDF
from db_analysis import analyze_df
from db_analysis.analyze_utils import Evaluation
from db_analysis import analyze_utils
import numpy as np

_this_folder_ = os.path.dirname(os.path.abspath(__file__))
_this_basename_ = os.path.splitext(os.path.basename(__file__))[0]

class AnalyzeHandler():
    def __init__(self):
        self.df_summary = dict()
        self.time_span = list()
        self.time_span_full = list()
        self.real_y = None
        self.pred_y = None
        self.min_th = None
        self.max_th = None
        self.start_date = None
        self.end_date = None
        self.model_unit = None
        self.separate_var = None
        self.acc_header = list()
        self.acc_list = list()
        self.rmse_header = list()
        self.rmse_list = list()
        self.filter_y_df_origin = None
        self.refine_df_origin = None
        self.msg = list()

    def make_folder(self, args, eval):
        folder = sys_lib.get_datetime()[:10]
        if not args.log_dir:
            args.log_dir = os.path.join(eval.ini['logger']['folder'], folder)
        if not os.path.isdir(args.log_dir):
            os.mkdir(args.log_dir)

        if not args.out_path:
            args.out_path = os.path.join(_this_folder_, "./Output")
        if not os.path.isdir(args.out_path):
            os.mkdir(args.out_path)
        self.f = os.path.join(args.out_path, "result.txt")
        if not os.path.isfile(self.f):
            self.file = open(self.f, "w")
            self.file.close()

    def df_preprocessing(self, model_num, filter_y_df, refine_df):
        # model num 이 3인 경우 USE_MOD_EST_OXY에 따라 데이터 추출
        if model_num == '3':
            filter_y_df, refine_df = analyze_df.get_data_by_use_mod_est_oxy(model_num, filter_y_df, refine_df)

        # 실적값이 0이고, 모델 예측값이 999999인 데이터 제거
        filter_y_df, refine_df = analyze_df.remove_unpredictable_data(filter_y_df, refine_df, self.real_y, self.pred_y)

        # var_csv_file에서 실적값의 min, max를 가져오고 실적값이 이상치인 데이터 제외
        filter_y_df, refine_df = analyze_df.remove_outlier_real_y(filter_y_df, refine_df, self.real_y, self.min_th, self.max_th)
        return filter_y_df, refine_df

    def db_preprocessing_min_max(self, model_num, filter_y_df, refine_df):
        filter_y_df, refine_df = self.df_preprocessing(model_num, filter_y_df, refine_df)
        if refine_df.empty:
            db_df = filter_y_df
        else:
            db_df = refine_df
        min_real, max_real = db_df[self.real_y].min(), db_df[self.real_y].max()
        min_pred, max_pred = db_df[self.pred_y].min(), db_df[self.pred_y].max()
        ori_min = min(min_real, min_pred)
        ori_max = max(max_real, max_pred)
        return ori_min, ori_max


    def check_acc_per_tolerance(self, tolerance, filter_y_df, refine_df):
        for idx, db_df in enumerate([filter_y_df, refine_df]):
            db_df['DATE2'] = pd.to_datetime(db_df.DATE, format='%Y%m%d')
            db_df = db_df.set_index("DATE2")
            if db_df.empty:
                continue
            else:
                accuracy, rmse = learning_lib.check_performance_measure(
                    est_val=self.pred_y,
                    real_val=self.real_y,
                    tolerance=tolerance, data_frame=db_df,
                    ys_test=True)
                if idx == 0:
                    for t in self.time_span:
                        edif_df.get_statistic(idx=idx, df_summary_t=self.df_summary[t], pred_y=self.pred_y,
                                              real_y=self.real_y,
                                              t=t, db_df=db_df, tolerance=tolerance)
                else:
                    if not db_df.empty:
                        for t in self.time_span:
                            edif_df.get_statistic(idx=idx, df_summary_t=self.df_summary[t], pred_y=self.pred_y,
                                                  real_y=self.real_y,
                                                  t=t, db_df=db_df, tolerance=tolerance)
            self.msg.append("# Check the model's performance measurement results, tolerance : {:4.8f}".format(tolerance))
            self.msg.append("# Model acc : {:4.2f} %".format(accuracy))
            self.msg.append("# Model RMSE : {:4.4f}".format(rmse))
            self.file = open(self.f, "w")
            self.file.write("\n".join(self.msg))
            self.file.close()

    def wm(self, model_name, date_cond_data, filter_y_df, refine_df, eval, str_domain):

        if PERFORM_EVAL_FORM:  # 주기 별 df summary 틀 생성
            self.df_summary = {'d': None, 'w': None, 'm': None}
            self.time_span, self.time_span_full = ['d', 'w', 'm'], ['day', 'week', 'month']
            for t in self.time_span:
                self.df_summary[t] = pd.DataFrame()

        # data 전처리
        self.real_y, self.pred_y = eval.curr_model_ini['real_y'], eval.curr_model_ini[SERVER_MODE.lower() + '_y']
        self.min_th, self.max_th = eval.feat_handler.vars[self.real_y].min_thresh, eval.feat_handler.vars[self.real_y].max_thresh
        filter_y_df, refine_df = self.df_preprocessing(eval.model_num, filter_y_df, refine_df)

        print("\n[{} results.]".format(eval.model_name))
        self.msg.append("[{} results.]".format(eval.model_name))
        print('\ntotal data : {}, filtered data by Y (Null, Outlier) : {}'.format(len(date_cond_data), len(filter_y_df)))
        self.msg.append('total data : {}, filtered data by Y (Null, Outlier) : {}'.format(len(date_cond_data), len(filter_y_df)))
        print('\ntotal data : {}, filtered data by total variables (Null, Outlier) : {}'.format(len(date_cond_data),
                                                                                                len(refine_df)))
        self.msg.append('total data : {}, filtered data by total variables (Null, Outlier) : {}'.format(len(date_cond_data),
                                                                                                len(refine_df)))
        # Check accuracy per tolerance
        for tol_idx, tolerance in enumerate(eval.tolerances):
            tol_char = analyze_utils.get_tolerance_char(tol_idx)
            self.acc_header.extend((tol_char + '_acc' + '(filtered_by_y)',
                               tol_char + '_acc' + '(filtered_outliers)'))

            # Check accuracy about filter_y, refine_df
            self.check_acc_per_tolerance(tolerance, filter_y_df, refine_df)

        acc_dataset = [self.acc_header] + [self.acc_list]
        rmse_dataset = [self.rmse_header] + [self.rmse_list]
        print("Performance evaluation is completed.")

        # Save used data file
        if eval.save_file_ == True:
            used_data_fname = self.start_date[2:] + '_' + self.end_date[2:] + '_' + model_name + '_' + 'raw_data' + '.csv'
            date_cond_data.to_csv(os.path.join(OUT_PATH, used_data_fname), mode='w')

            # Save evaluated result file
            # get origin min max
            ori_min, ori_max = self.db_preprocessing_min_max(eval.model_num, self.filter_y_df_origin, self.refine_df_origin)

            if refine_df.empty:
                db_df = filter_y_df  # refined df 를 기준으로 하되 refine 데이터가 비어 있다면 filter_y 데이터를 사용한다.
            else:
                db_df = refine_df
            db_df['DATE2'] = pd.to_datetime(db_df.DATE, format='%Y%m%d')  # 형식에 맞춘 날짜를 생성한다.
            db_df = db_df.set_index('DATE2')  # 날짜를 index 로 설정한다.
            db_df['DATE2'] = db_df.index  # index 도 따로 빼준다.
            part_of_fname = self.start_date[2:] + '_' + self.end_date[2:] + '_' + model_name + '_'

            # # proportional expression
            # if '2_50_C' in model_name:
            #     # EST의 DY_OXY1 : EST의 (EST_DYNC - INI C) = EVAL의 DY_OXY1 : X
            #     # NEW DYNC = EST의 INI C - X
            #     db_df_ori = db_df.deepcopy()
            #     db_df_ori['proportion'] = ((db_df_ori['EST_DYNC']-db_df_ori['INI_C_ict']) * db_df_ori['DY_OXY1']) / db_df_ori['DY_OXY1_ict']
            #     db_df_ori = db_df_ori.replace([np.inf, -np.inf], np.nan)
            #     db_df_ori['new_DYNC'] = db_df_ori['proportion'] + db_df_ori['INI_C_ict']  # new dync
            #     db_df_ori = db_df_ori[['No', 'TRANSACTION_CD', 'MTL_NO', 'DATE',
            #                    'DY_OXY1_ict', 'EST_DYNC', 'INI_C_ict', 'DY_OXY1',
            #                    'proportion', 'new_DYNC', 'DYNC']]
            #     accuracy, rmse = learning_lib.check_performance_measure(
            #         est_val='new_DYNC',
            #         real_val='DYNC',
            #         tolerance=0.005, data_frame=db_df_ori,
            #         ys_test=True)
            #     print(accuracy, rmse)
            #     accuracy, rmse = learning_lib.check_performance_measure(
            #         est_val='EST_DYNC',
            #         real_val='DYNC',
            #         tolerance=0.005, data_frame=db_df_ori,
            #         ys_test=True)
            #     print(accuracy, rmse)
            #     db_df_ori.to_csv(("2_50_C_proportion1.csv"), mode='w')

            # if '3_85_C' in model_name:
            #     # EST의 TOTAL_OXY : EST의 (INI C - EST_TERMINAL C) = EVAL의 TOTAL_OXY : X
            #     # NEW Terminal C = EST의 INI C - X
            #     db_df_ori = db_df.deepcopy()
            #     db_df_ori['proportion'] = ((db_df_ori['INI_C_ict'] - db_df_ori['EST_TERMINAL_C']) * db_df_ori['TOTAL_OXY']) / db_df_ori['TOTAL_OXY_ict']
            #     db_df_ori = db_df_ori.replace([np.inf, -np.inf], np.nan)
            #     db_df_ori['new_TERMINAL_C'] = db_df_ori['INI_C_ict'] - db_df_ori['proportion']  # new TERMINAL_C
            #     db_df_ori = db_df_ori[['No', 'TRANSACTION_CD', 'MTL_NO', 'DATE',
            #                    'TOTAL_OXY_ict', 'EST_TERMINAL_C', 'INI_C_ict', 'TERMINAL_C', 'TOTAL_OXY',
            #                    'proportion', 'new_TERMINAL_C']]
            #     accuracy, rmse = learning_lib.check_performance_measure(
            #         est_val='new_TERMINAL_C',
            #         real_val='TERMINAL_C',
            #         tolerance=0.005, data_frame=db_df_ori,
            #         ys_test=True)
            #     print(accuracy, rmse)
            #     accuracy, rmse = learning_lib.check_performance_measure(
            #         est_val='EST_TERMINAL_C',
            #         real_val='TERMINAL_C',
            #         tolerance=0.005, data_frame=db_df_ori,
            #         ys_test=True)
            #     print(accuracy, rmse)
            #
            #     db_df_ori.to_csv(("3_85_C_proportion1.csv"), mode='w')
            #
            # if '3_85_C' in model_name:
            #     # EST의 DYN_OXY_TOTAL : EVAL의 DYNC - EST의 EST_TERMINAL_C = EVAL의 DYN_OXY_TOTAL : X # 실제 DYNC 기준으로 해야하기 대문에 EVAL DYNC 사용
            #     # NEW EST TERMINAL C = EVAL의 DYNC - X
            #     db_df_ori = db_df.deepcopy()
            #     db_df_ori['proportion'] = (db_df_ori['DYN_OXY_TOTAL'] * (db_df_ori['DYNC']-db_df_ori['EST_TERMINAL_C'])) / db_df_ori['DYN_OXY_TOTAL_ict']
            #     db_df_ori = db_df_ori.replace([np.inf, -np.inf], np.nan)
            #     db_df_ori['new_TERMINAL_C'] = db_df_ori['DYNC']-db_df_ori['proportion']
            #     db_df_ori = db_df_ori[['No', 'TRANSACTION_CD', 'MTL_NO', 'DATE',
            #                    'DYN_OXY_TOTAL', 'DYNC', 'DYNC_ict', 'TERMINAL_C',
            #                    'EST_TERMINAL_C', 'DYN_OXY_TOTAL_ict',
            #                    'proportion', 'new_TERMINAL_C']]
            #     accuracy, rmse = learning_lib.check_performance_measure(
            #         est_val='new_TERMINAL_C',
            #         real_val='TERMINAL_C',
            #         tolerance=0.005, data_frame=db_df_ori,
            #         ys_test=True)
            #     print(accuracy, rmse)
            #     accuracy, rmse = learning_lib.check_performance_measure(
            #         est_val='EST_TERMINAL_C',
            #         real_val='TERMINAL_C',
            #         tolerance=0.005, data_frame=db_df_ori,
            #         ys_test=True)
            #     print(accuracy, rmse)
            #
            #     db_df_ori.to_csv(("3_85_C_proportion2.csv"), mode='w')
            #
            # if '3_85_C' in model_name:
            #     # EST의 CO+CO2 : EST의 INI C - EVAL의 DYNC = EVAL의 CO+CO2 : X # 실제 DYNC 기준으로 해야하기 대문에 EVAL DYNC 사용.
            #     # NEW EST TERMINAL C = EST의 INI C - X
            #     db_df_ori = db_df.deepcopy()
            #     db_df_ori['proportion'] = (((db_df_ori['TOTAL_CO']+db_df_ori['TOTAL_CO2']) * (db_df_ori['INI_C_ict'] - db_df_ori['DYNC']))
            #                            / (db_df_ori['TOTAL_CO_ict']+db_df_ori['TOTAL_CO2_ict']))
            #     db_df_ori = db_df_ori.replace([np.inf, -np.inf], np.nan)
            #     db_df_ori['new_TERMINAL_C'] = db_df_ori['INI_C_ict']-db_df_ori['proportion']
            #     db_df_ori = db_df_ori[['No', 'TRANSACTION_CD', 'MTL_NO', 'DATE',
            #                    'TOTAL_CO', 'TOTAL_CO2', 'DYNC', 'DYNC_ict', 'TERMINAL_C',
            #                    'EST_TERMINAL_C', 'TOTAL_CO_ict', 'TOTAL_CO2_ict', 'DYNC',
            #                    'proportion', 'new_TERMINAL_C']]
            #     accuracy, rmse = learning_lib.check_performance_measure(
            #         est_val='new_TERMINAL_C',
            #         real_val='TERMINAL_C',
            #         tolerance=0.005, data_frame=db_df_ori,
            #         ys_test=True)
            #     print(accuracy, rmse)
            #     accuracy, rmse = learning_lib.check_performance_measure(
            #         est_val='EST_TERMINAL_C',
            #         real_val='TERMINAL_C',
            #         tolerance=0.005, data_frame=db_df_ori,
            #         ys_test=True)
            #     print(accuracy, rmse)
            #
            #     db_df_ori.to_csv(("3_85_C_proportion3.csv"), mode='w')


            # 일/주/월 평균 값 plot
            fname_dict = {'d': None, 'w': None, 'm': None}

            # Save As CSV
            for t in self.time_span:
                fname_dict[t] = part_of_fname + '{}_{}_{}_{}.csv'.format(t, SERVER_MODE, self.separate_var, str_domain)
                # column 순서 변경
                self.df_summary[t] = edif_df.order_column(refine_df, self.df_summary[t], self.pred_y, self.real_y, eval.tolerances)
                # csv 저장
                self.df_summary[t].to_csv(os.path.join(OUT_PATH, fname_dict[t]), mode='w')
            fname = part_of_fname + 'result_{}_{}_{}.txt'.format(SERVER_MODE, self.separate_var, str_domain)  # 파일명
            os.rename(self.f, str("/".join(self.f.split("/")[:-1]))+str('/')+fname)

    def main(self, args):
        global accuracy, rmse
        eval = Evaluation(ini=sys_lib.get_ini_parameters(args.ini_fname))

        # 결과가 저장될 folder 를 현재 날짜로 생성
        self.make_folder(args, eval)

        # 사용자가 중단할 때까지 평가 코드 반복
        while True:
            ## ans = input(" \n Enter command, evaluation(1) or exit(0) ? ")
            ans = '1' ##
            try:
                ans = int(ans[0])
            except ValueError:
                print("\n You entered {}. Please enter 0, or 1. Thanks".format(ans))
                continue

            if ans == 0:
                print("\n # Bye")
                break
            # ans 가 1일 경우 평가 수행
            elif ans == 1:
                eval.init_logger(log_dir=args.log_dir, SERVER_MODE=SERVER_MODE)
                eval.logger.info(" # START {} with {}_DB.".format(_this_basename_, SERVER_MODE))
                # 사용자가 model type 선택
                model_type = input(" \n Please enter model type : \n"
                                        "ex) 1        : (TERMINCAL_C)\n"                                    
                                        "ex) 2_50_C   : (DYNC)\n"
                                        "ex) 3_85_OXY : (TOTAL_OXY)\n"
                                        "ex) 3_85_C   : (TERMINCAL_C)\n"
                                        "ex) 3_85_O   : (TERMINCAL_O)\n"
                                        " : "
                                        )
                model_name = 'model_' + model_type
                eval.check_model_list = [model_name]
                for model_idx, model_name in enumerate(eval.check_model_list):
                    # Get model info.
                    eval.get_model_info(model_name=model_name)
                    # Get Machine Learning info.
                    eval.get_ml_info(args.var_ini_file, args.var_csv_file)
                    # Get units (이렇게 하면 안 됨.)
                    units = {'1':'ppm', '2_50_C': 'wt%', '3_85_OXY': 'Nm^3', '3_85_C': 'ppm', '3_85_O': 'ppm'}
                    self.model_unit = units[model_type]

                    # Init. database
                    if DB_OP_:
                        eval.init_db_handler(DB_OP_=DB_OP_)
                    eval.load_est_eval_dataset(DB_OP_=DB_OP_)
                    # eval.extract_match_dataset(SERVER_MODE='ys')
                    eval.extract_match_dataset(SERVER_MODE='EST_EXTRACT')

                    # Set Data Period
                    # self.start_date, self.end_date = analyze_utils.get_date_period()
                    # self.start_date, self.end_date = '20200601', '20210131'
                    self.start_date, self.end_date = '20210115', '20210131'
                    # Get Null/Range check var list
                    ini_null_check_var_list = eval.curr_model_ini['null_check_var_list']
                    ini_range_check_var_list = eval.curr_model_ini['range_check_var_list']
                    null_check_var_list = analyze_utils.get_check_var_list(ini_null_check_var_list)
                    range_check_var_list = analyze_utils.get_check_var_list(ini_range_check_var_list)

                    # filter_y_cond_list = analyze_utils.get_cond_list(db_op_=False,
                    #                                    server_mode='EVAL',
                    #                                    trans_code=eval.trans_code,
                    #                                    start_date=self.start_date, end_date=self.end_date,
                    #                                    null_check_var_list=null_check_var_list,
                    #                                    range_check_var_list=range_check_var_list)

                    # EST EXTRACT
                    filter_y_cond_list = analyze_utils.get_cond_list(db_op_=False,
                                                       server_mode='EST',
                                                       # trans_code='CD_2S_ES',
                                                       trans_code='CD_2S_ES',
                                                       start_date=self.start_date, end_date=self.end_date,
                                                       null_check_var_list=null_check_var_list,
                                                       range_check_var_list=range_check_var_list)

                    date_cond_data = eval.select_data_by_conditions(cond_list=filter_y_cond_list[:3])  # 해당 날짜에 해당되는 데이터
                    used_data_fname = self.start_date[2:] + '_' + self.end_date[
                                                                  2:] + '_' + model_name + '_' + 'raw_data' + '.csv'
                    date_cond_data.to_csv(os.path.join(OUT_PATH, used_data_fname), mode='w')
                    sys.exit(1)
                    filter_y_data = eval.select_data_by_conditions(cond_list=filter_y_cond_list)  # y에 이상치가 있는 data 제외

                    filter_y_cond_list[-1] = ["EVAL_RANGE_CHECK_VARS", 'None']
                    refine_cond_list = filter_y_cond_list
                    refine_data = eval.select_data_by_conditions(cond_list=refine_cond_list)  # y 뿐만 아니라 inpu data에 이상치가 있는 data 제외

                    # Adjust the unit.
                    filter_y_data = analyze_utils.adjust_y_unit(model_name=eval.model_name, dataset=filter_y_data)
                    refine_data = analyze_utils.adjust_y_unit(model_name=eval.model_name, dataset=refine_data)

                    # add condition...
                    self.filter_y_df_origin = filter_y_data.deepcopy()
                    self.refine_df_origin = refine_data.deepcopy()

                    if self.filter_y_df_origin.empty:
                        continue
                    self.wm(model_name, date_cond_data, self.filter_y_df_origin, self.refine_df_origin, eval, '')



            else:
                print("\n You entered {:d}. Please enter 0 or 1. Thanks".format(ans))

def parse_arguments(argv):

    parser = argparse.ArgumentParser()

    parser.add_argument("--op_mode", default='manual', help="operation mode", choices=['manual', 'auto'])
    parser.add_argument("--server_mode", default='EVAL', help="server mode", choices=['EST', 'EVAL', 'ALL'])
    parser.add_argument("--ini_fname", required=True, help="ini filename")
    parser.add_argument("--out_path", default=None, help="Output path")
    parser.add_argument("--logging_", default=False, action='store_true', help="Logging flag")
    parser.add_argument("--console_logging_", default=False, action='store_true', help="Console logging flag")
    parser.add_argument("--log_dir", default=None, help="Log directory")
    parser.add_argument("--perform_eval_form", default=False, help="Performance evaluation form")
    parser.add_argument("--var_ini_file", required=True, help="var ini file")
    parser.add_argument("--var_csv_file", required=True, help="var csv file")

    args = parser.parse_args(argv)
    args.out_path = sys_lib.unicode_normalize(args.out_path)
    args.log_dir = sys_lib.unicode_normalize(args.log_dir)
    # var_mtx = data_lib.read_csv_file(args.var_csv_file)
    return args

# Set Arguments
OP_MODE = 'manual' # manual / auto
SERVER_MODE = 'EST' # EST / EVAL / ALL
INI_FNAME = '../db_analysis/db_setting.ini'
CRT_DATE = sys_lib.get_datetime("%Y%m%d")
OUT_PATH = "../db_analysis/Output/" + CRT_DATE + "/"
LOG_DIR = None

DB_OP_ = True  # True / False
PERFORM_EVAL_FORM = True  # True / False
IN_DEPTH = False  # True / False
separate_var = 'BLOW_TYPE'

if __name__ == "__main__":

    if len(sys.argv) == 1:
        sys.argv.extend([
            "--op_mode", OP_MODE,
            "--server_mode", SERVER_MODE,
            "--ini_fname", INI_FNAME,
            "--out_path", OUT_PATH,
            "--logging_",
            "--console_logging_",
            "--log_dir", LOG_DIR,
            "--var_ini_file", "../sys_cvt_decarb/cvt_decarb_var.ini",
            "--var_csv_file", "../sys_cvt_decarb/cvt_decarb_var.csv",
        ])
    make_plot = MakePlot()
    edif_df = EditDF()
    # Evaluation = Evaluation()
    AnalyzeHandler().main(parse_arguments(sys.argv[1:]))