#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = "Yeongsun Park"
__date__ = "2020-10-13"
__email__ = "parkys@mindslab.ai"

file_name = "D:/mindslab/posco-cvt-decarb/db_analysis/Output/20210224/210115_210223_model_3_85_O_result_EVAL_None_.txt"
f = open(file_name, "r")
my_list = [['model', 'EST/EVAL', 'total_data', 'filtered_y', 'filtered_var', 'tolerance1', 'acc1_y', 'RMSE1_y', 'acc1_v', 'RMSE1_v',
           'tolerance2', 'acc2_y', 'RMSE2_y', 'acc2_v', 'RMSE2_v']]
ff = f.readlines()
for i, line in enumerate(ff):
    if line.startswith("["):
        model = line.split(" ")[0].replace("[","")
        est_eval = "EVAL" if "EVAL" in file_name else "EST"
        total_data = ff[i+1].split(" ")[3].replace(",","")
        filtered_y = ff[i+1].split(" ")[-1].replace("\n","")
        filtered_var = ff[i+2].split(" ")[-1].replace("\n","")
        tolerance1 = ff[i+3].split(" ")[-1].replace("\n","")
        acc1_y = ff[i+4].split(" ")[-2]
        RMSE1_y = ff[i+5].split(" ")[-1].replace("\n","")
        acc1_v = ff[i+7].split(" ")[-2]
        RMSE1_v = ff[i+8].split(" ")[-1].replace("\n","")
        tolerance2 = ff[i+9].split(" ")[-1].replace("\n","")
        acc2_y = ff[i+10].split(" ")[-2]
        RMSE2_y = ff[i+11].split(" ")[-1].replace("\n","")
        acc2_v = ff[i+13].split(" ")[-2]
        RMSE2_v = ff[i+14].split(" ")[-1].replace("\n","")
        my_list.append([model, est_eval, total_data, filtered_y, filtered_var, tolerance1, acc1_y, RMSE1_y, acc1_v, RMSE1_v,
                       tolerance2, acc2_y, RMSE2_y, acc2_v, RMSE2_v])
for m_l in my_list:
    print (m_l)

"""
[model_2_50_C results.]
total data : 3564, filtered data by Y (Null, Outlier) : 3329
total data : 3564, filtered data by total variables (Null, Outlier) : 2044
# Check the model's performance measurement results, tolerance : 0.05000000
# Model acc : 48.00 %
# Model RMSE : 0.0945
# Check the model's performance measurement results, tolerance : 0.05000000
# Model acc : 49.51 %
# Model RMSE : 0.0857
# Check the model's performance measurement results, tolerance : 0.10000000
# Model acc : 77.38 %
# Model RMSE : 0.0945
# Check the model's performance measurement results, tolerance : 0.10000000
# Model acc : 80.14 %
# Model RMSE : 0.0857
"""
"""
 2_50_C 	 est 	9,214	2,006	1,199	0.05	22.58	0.158	23.44	0.1543	0.1	43.12	0.158	43.87	0.1543
 2_50_C     eval 	3,564	3,329	2,044	0.05	48	    0.0945	49.51	0.0857	0.1	77.38	0.0945	80.14	0.0857
"""