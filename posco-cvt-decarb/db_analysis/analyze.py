#! /usr/bin/env python
# -*- coding: utf-8 -*-
import sys
import argparse
import pandas as pd
import collections
import os
from copy import copy
from lib import learning as learning_lib
from lib import sys_lib as sys_lib
from refine_model import model_1, model_3_85_O, model_3_85_C_O

_this_folder_ = os.path.dirname(os.path.abspath(__file__))
_this_basename_ = os.path.splitext(os.path.basename(__file__))[0]

class Evaluation:

    def __init__(self, data_path=None, out_path=None, ini=None, logger=None):

        self.data_path = data_path
        self.out_path = out_path
        self.ini = ini
        self.logger = logger

        self.model_name = None
        self.model_num = None
        self.trans_code = None

        self.init_ini(ini)
        self.est_db_handler = None
        self.eval_db_handler = None
        self.est_dataset = None
        self.eval_dataset = None
        self.match_dataset = None

    def init_ini(self, ini):

        self.est_dataset_fname = ini['general']['est_dataset_fname']
        self.eval_dataset_fname = ini['general']['eval_dataset_fname']
        self.save_file_ = bool(ini['general']['save_file_'])

        self.apply_request_time = bool(ini['general']['apply_request_time'])

        check_model_list = ini['general']['check_model_list'].split(',')
        self.check_model_list = [u.strip() for u in check_model_list]

        self.mssql_ini = ini['mssql_db']
        self.mssql_server_name = ini['mssql_db']['server_name']
        self.mssql_port = ini['mssql_db']['port']
        self.mssql_username = ini['mssql_db']['username']
        self.mssql_password = ini['mssql_db']['password']

        self.model_1_ini = ini['model_1']
        self.model_2_50_C_ini = ini['model_2_50_C']
        self.model_3_85_OXY_ini = ini['model_3_85_OXY']
        self.model_3_85_C_ini = ini['model_3_85_C']
        self.model_3_85_O_ini = ini['model_3_85_O']
        self.curr_model_ini = None

    def init_logger(self, log_dir=None, logging_=True, console_=True):
        try:
            self.ini['logger']['name'] += "." + SERVER_MODE
            self.ini['logger']['prefix'] = SERVER_MODE
            if log_dir:
                self.ini['logger']['folder'] = log_dir
            self.logger = sys_lib.setup_logger_with_ini(self.ini['logger'],
                                                        logging_=logging_,
                                                        console_=console_)
        except Exception as e:
            print(" @ Error in init_logger: {}".format(e))
            self.logger = sys_lib.get_stdout_logger()

    def get_model_info(self, model_name):
        self.model_name = model_name
        self.model_num = self.model_name.split('_')[1]
        self.curr_model_ini = self.get_model_ini(model=self.model_name)
        self.trans_code = self.curr_model_ini['eval_header']
        tolerances = self.curr_model_ini['tolerance'].split(',')
        self.tolerances = [float(u) for u in tolerances]


    def init_db_handler(self):
        if not DB_OP_:
            return False
        self.est_db_handler = sys_lib.PyMssqlWrapper(server_name=self.mssql_server_name,
                                                     port=self.mssql_port,
                                                     username=self.mssql_username,
                                                     password=self.mssql_password,
                                                     db_name=self.curr_model_ini['db_name'],
                                                     table_name=self.curr_model_ini['EST'.lower() + '_' + 'table_name'],
                                                     logger=self.logger)
        self.eval_db_handler = sys_lib.PyMssqlWrapper(server_name=self.mssql_server_name,
                                                     port=self.mssql_port,
                                                     username=self.mssql_username,
                                                     password=self.mssql_password,
                                                     db_name=self.curr_model_ini['db_name'],
                                                     table_name=self.curr_model_ini['EVAL'.lower() + '_' + 'table_name'],
                                                     logger=self.logger)
    def get_model_ini(self, model):

        model_ini_dict = {
            'model_1'       : self.model_1_ini,
            'model_2_50_C'  : self.model_2_50_C_ini,
            'model_3_85_OXY': self.model_3_85_OXY_ini,
            'model_3_85_C': self.model_3_85_C_ini,
            'model_3_85_O': self.model_3_85_O_ini,
        }
        return model_ini_dict[model]

    def load_dataset(self, SERVER_MODE=None):
        if SERVER_MODE == 'EST':
            db_handler = self.est_db_handler
            dataset_fname = self.est_dataset_fname
        elif SERVER_MODE == 'EVAL':
            db_handler = self.eval_db_handler
            dataset_fname = self.eval_dataset_fname

        if DB_OP_:
            db_columns = db_handler.selectColumnNames()
            db_columns = [db_column[0] for db_column in db_columns]
            db_data = db_handler.selectAll()
            dataset = [db_columns] + db_data
            dataset = convert_data_type(dataset, 'df')
        else:
            dataset = pd.read_csv(dataset_fname)
        return dataset

    def load_est_eval_dataset(self):
        for server_mode in ['EST', 'EVAL']:
            dataset = self.load_dataset(SERVER_MODE=server_mode)
            if server_mode is 'EST':
                self.est_dataset = dataset
            elif server_mode is 'EVAL':
                self.eval_dataset = dataset
        return self.est_dataset, self.eval_dataset

    def extract_match_dataset(self):
        est_df = convert_data_type(self.est_dataset, dtype='df')
        eval_df = convert_data_type(self.eval_dataset, dtype='df')

        # # 지시/실적 데이터에서 중복행 제거
        # if self.curr_model_ini.name == 'model_4':
        #     est_no_dup_df = est_df.drop_duplicates(subset=['MTL_NO', 'USE_MOD4_7_TIME'])
        #     eval_no_dup_df = eval_df.drop_duplicates(subset=['MTL_NO', 'USE_MOD4_7_TIME'])
        # else:
        est_no_dup_df = est_df.drop_duplicates(subset=['TRANSACTION_CD','MTL_NO'])
        eval_no_dup_df = eval_df.drop_duplicates(subset=['TRANSACTION_CD','MTL_NO'])

        if SERVER_MODE == 'EST':
            # 실적 매칭되는 지시의 MTL_NO
            match_ch_no = eval_no_dup_df['MTL_NO'].loc[eval_no_dup_df['MTL_NO'].isin(est_no_dup_df['MTL_NO'])].reset_index(drop=True)

            # 실적 데이터에 지시 예측값 추가
            est_y = self.curr_model_ini[SERVER_MODE.lower() + '_y']
            est_values = est_no_dup_df[['MTL_NO', est_y]]
            eval_no_dup_df = eval_no_dup_df.merge(est_values, on='MTL_NO')

            # 지시/실적 MTL_NO 매칭
            match_df = eval_no_dup_df.loc[eval_no_dup_df['MTL_NO'].isin(match_ch_no)]
            self.match_dataset = match_df

        elif SERVER_MODE == 'EVAL':
            self.match_dataset = eval_no_dup_df

    def select_data_by_conditions(self, df=None, cond_list=None):

        if df is None:
            df = self.match_dataset

        for cond in cond_list:
            key = cond[0]
            cond_vals = cond[1]
            if key == 'TRANSACTION_CD':
                df = df.loc[df[key] == cond_vals]
            elif key == 'START_DATE':
                df = df.loc[df['DATE'] >= cond_vals]
            elif key == 'END_DATE':
                df = df.loc[df['DATE'] <= cond_vals]
            elif key == 'NULL_CHECK_VAR_LIST':
                df = df.dropna(subset=cond_vals)
            elif key == SERVER_MODE + '_RANGE_CHECK_VARS':
                if cond_vals == 'None':
                    df = df.loc[df[key] == 'None']
                else:
                    for range_var in cond_vals:
                        df = df.loc[(df[key].str.contains(range_var)) == False]
            elif key == 'C_MAX':
                df = df.loc[(int(cond_vals[0]) <= df[key]) & (df[key] <= int(cond_vals[1]))]
            elif key == 'C_DIFF':
                df = df.loc[abs(df['F_RH_C'] - df['F_CC_C']) <= cond_vals]
            print("Key : {}, condition : {}, df_cnts : {}".format(key, str(cond_vals), len(df)))
            print("Key : {}, condition : {}, df_cnts : {}".format(key, str(cond_vals), len(df)))
        return df

def get_date_period():
    start_date = input(" \n Please enter start date : ex) 20190507\n :")
    end_date = input(" \n Please enter end date : ex) 20191231\n :")

    return start_date, end_date


def get_cond_list(db_op_,
                  server_mode,
                  trans_code,
                  start_date=None, end_date=None,
                  null_check_var_list=None,
                  range_check_var_list=None
                  ):
    if db_op_ is True:
        cond_list = ["TRANSACTION_CD = '" + trans_code + "'"]
        if start_date:
            cond_list.append("DATE >='" + start_date + "'")
        if end_date:
            cond_list.append("DATE <='" + end_date + "'")
        if null_check_var_list:
            for var in null_check_var_list:
                cond_list.append(var + " is not NULL")
        if range_check_var_list:
            for var in range_check_var_list:
                cond_list.append(server_mode+"_RANGE_CHECK_VARS NOT LIKE '%" + var + ":%'")
    else:
        cond_list = [['TRANSACTION_CD', trans_code]]
        if start_date:
            cond_list.append(['START_DATE', start_date])
        if end_date:
            cond_list.append(['END_DATE', end_date])
        if null_check_var_list:
            cond_list.append(['NULL_CHECK_VAR_LIST', null_check_var_list])
        if range_check_var_list:
            cond_list.append([server_mode + "_RANGE_CHECK_VARS", range_check_var_list])

    return cond_list

def convert_data_type(data, dtype='list'):
    """

    :param data:
    :param dtype: list / df
    :return:
    """
    if dtype == 'list':
        if isinstance(data, pd.DataFrame):
            res_data = [data.columns.values.tolist()] + data.values.tolist()
        else:
            res_data = data
    elif dtype =='df':
        if isinstance(data, list):
            res_data = pd.DataFrame(data[1:], columns=data[0])
        else:
            res_data = data
    return res_data

def adjust_y_unit(model_name, dataset):

    # convert data type
    global adjusted_dataset
    dataset = convert_data_type(dataset, 'list')
    if model_name == '1':
        adjusted_dataset = model_1.refine_dataset_by_y_value(dataset=dataset)
    elif model_name == '3_85_OXY':
        adjusted_dataset = model_3_85_O.refine_dataset_by_y_value(dataset=dataset)
    elif model_name == '3_85_C' or model_name == '3_85_O':
        adjusted_dataset = model_3_85_C_O.refine_dataset_by_y_value(dataset=dataset)
    else:
        adjusted_dataset = dataset
    return convert_data_type(adjusted_dataset, 'df')

def decode_symbol_by_dict(dataset, decode_vars, dict):

    for row_idx in range(1, len(dataset)):
        row_dat = dataset[row_idx]
        for col_idx in range(len(row_dat)):
            key = dataset[0][col_idx]
            if key in decode_vars:
                try:
                    dataset[row_idx][col_idx] = model_2.decode_symbol_dict[row_dat[col_idx]]
                except KeyError:
                    print(key + ' ' + 'is' + ' ' + str(row_dat[col_idx]))
                    continue
            else:
                continue
    return dataset

def get_tolerance_char(tol_idx):
    if tol_idx + 1 == 1:
        tol_char = 'first'
    elif tol_idx + 1 == 2:
        tol_char = 'second'
    elif tol_idx + 1 == 3:
        tol_char = 'third'
    return tol_char

def main(args):

    global accuracy, rmse
    eval = Evaluation(ini=sys_lib.get_ini_parameters(args.ini_fname))

    folder = sys_lib.get_datetime()[:10]
    if not args.log_dir:
        args.log_dir = os.path.join(eval.ini['logger']['folder'], folder)
    if not os.path.isdir(args.log_dir):
        os.mkdir(args.log_dir)

    if not args.out_path:
        args.out_path = os.path.join(_this_folder_, "./Output")
    if not os.path.isdir(args.out_path):
        os.mkdir(args.out_path)

    while True:
        ## ans = input(" \n Enter command, evaluation(1) or exit(0) ? ")
        ans = '1' ##
        try:
            ans = int(ans[0])
        except ValueError:
            print("\n You entered {}. Please enter 0, or 1. Thanks".format(ans))
            continue

        if ans == 0:
            print("\n # Bye")
            break
        elif ans == 1:
            eval.init_logger(log_dir=args.log_dir)
            eval.logger.info(" # START {} with {}_DB.".format(_this_basename_, SERVER_MODE))
            model_type = input(" \n Please enter model type : \n"
                                    "ex) 1        : (TERMINCAL_C)\n"                                    
                                    "ex) 2_50_C   : (DYNC)\n"
                                    "ex) 3_85_OXY : (TOTAL_OXY)\n"
                                    "ex) 3_85_C   : (TERMINCAL_C)\n"
                                    "ex) 3_85_O   : (TERMINCAL_O)\n"
                                    "ex) all : (M1 ~ 3)\n"
                                    " : "
                                    )
            model_name = 'model_' + model_type

            # model_type = '1_1' ##
            # model_name = 'model_' + model_type ##

            if model_type != 'all':
                eval.check_model_list = [model_name]
            for model_idx, model_name in enumerate(eval.check_model_list):
                # Get model info.
                eval.get_model_info(model_name=model_name)

                # Init. database
                if DB_OP_:
                    eval.init_db_handler()
                    ## init_db_info()

                eval.load_est_eval_dataset()
                eval.extract_match_dataset()

                start_date, end_date = get_date_period()
                # start_date = '20200401'  ##
                # end_date = "20200427"  ##

                null_check_var_list = [var.strip() for var in eval.curr_model_ini['null_check_var_list'].split(',')] \
                                                        if eval.curr_model_ini['null_check_var_list'] != '' else None
                range_check_var_list = [var.strip() for var in eval.curr_model_ini['range_check_var_list'].split(',')] \
                                                        if eval.curr_model_ini['range_check_var_list'] != '' else None
                filter_y_cond_list = get_cond_list(db_op_=False,
                                                   server_mode='EVAL',
                                                   trans_code=eval.trans_code,
                                                   start_date=start_date, end_date=end_date,
                                                   null_check_var_list=null_check_var_list,
                                                   range_check_var_list=range_check_var_list)

                date_cond_data = eval.select_data_by_conditions(cond_list=filter_y_cond_list[:3])
                filter_y_data = eval.select_data_by_conditions(cond_list=filter_y_cond_list)

                filter_y_cond_list[-1] = ["EVAL_RANGE_CHECK_VARS", 'None']
                refine_cond_list = filter_y_cond_list
                refine_data = eval.select_data_by_conditions(cond_list=refine_cond_list)

                # Adjust the unit.
                filter_y_data = adjust_y_unit(model_name=eval.model_name, dataset=filter_y_data)
                refine_data = adjust_y_unit(model_name=eval.model_name, dataset=refine_data)

                # add condition...
                filter_y_df = filter_y_data.copy()
                refine_df = refine_data.copy()

                acc_header = []
                acc_list = []
                rmse_header = []
                rmse_list = []
                # Check accuracy per tolerance
                for tol_idx, tolerance in enumerate(eval.tolerances):
                    tol_char = get_tolerance_char(tol_idx)
                    acc_header.append(tol_char + '_acc' + '(filtered_by_y)')
                    acc_header.append(tol_char + '_acc' + '(filtered_outliers)')

                    # USE_MOD_EST_OXY에 따라 데이터 추출
                    if eval.model_num == '3':
                        if eval.model_name == 'model_3_85_OXY':
                            filter_y_df = filter_y_df.loc[filter_y_df['USE_MOD_EST_OXY'] == int(-1)].reset_index(drop=True)
                            refine_df = refine_df.loc[refine_df['USE_MOD_EST_OXY'] == int(-1)].reset_index(drop=True)
                        elif eval.model_name == 'model_3_85_C' or eval.model_name == 'model_3_85_O':
                            filter_y_df = filter_y_df.loc[filter_y_df['USE_MOD_EST_OXY'] >= int(-1)].reset_index(drop=True)
                            refine_df = refine_df.loc[refine_df['USE_MOD_EST_OXY'] >= int(-1)].reset_index(drop=True)

                    # 실적값이 0이고, 모델 예측값이 999999인 데이터 제거
                    real_y = eval.curr_model_ini['real_y']
                    pred_y = eval.curr_model_ini[SERVER_MODE.lower() + '_y']
                    filter_y_df = filter_y_df.loc[(0 < filter_y_df[real_y]) & (filter_y_df[pred_y] < 999999)].reset_index(drop=True)
                    refine_df = refine_df.loc[(0 < refine_df[real_y]) & (refine_df[pred_y] < 999999)].reset_index(drop=True)

                    print("\n[{} results.]".format(eval.model_name))
                    print('\ntotal data : {}, filtered data by Y (Null, Outlier) : {}'.format(len(date_cond_data), len(filter_y_df)))
                    print('\ntotal data : {}, filtered data by total variables (Null, Outlier) : {}'.format(len(date_cond_data), len(refine_df)))

                    # Check accuracy about filter_y, refine_df
                    for db_df in [filter_y_df, refine_df]:
                        accuracy = None
                        rmse = None
                        if len(db_df) == 0:
                            print("No data available for the condition.")
                        else:
                            try:
                                accuracy, rmse = learning_lib.check_performance_measure(est_val=eval.curr_model_ini[SERVER_MODE.lower() + '_y'],
                                                                                        real_val=eval.curr_model_ini['real_y'],
                                                                                        tolerance=tolerance, data_frame=db_df)
                            except (ValueError):
                                print("Data contains an outlier value.")
                                pass
                        acc_list.append(accuracy)
                        rmse_list.append(rmse)

                acc_dataset = [acc_header] + [acc_list]
                rmse_dataset = [rmse_header] + [rmse_list]
                print("Performance evaluation is completed.")

                # Save used data file
                if eval.save_file_ == True:
                    used_data_fname = start_date[2:] + '_' + end_date[2:] + '_' + model_name + '_' + 'raw_data' + '.csv'
                    date_cond_data.to_csv(os.path.join(OUT_PATH, used_data_fname), mode='w')

                    # Save evaluated result file
                    eval_data_fname = SERVER_MODE.lower() + '_total_model_evaluation_result.csv'
                    res_dict = collections.OrderedDict()
                    res_dict['model'] = model_name
                    res_dict['start_date'] = start_date
                    res_dict['end_date'] = end_date
                    res_dict['total_data'] = len(date_cond_data)
                    res_dict['filtered_by_y'] = len(filter_y_df)
                    res_dict['filtered_by_outliers'] = len(refine_df)
                    res_dict['tolerances'] = eval.tolerances
                    csv_acc_columns = ['first_acc(filtered_by_y)', 'second_acc(filtered_by_y)', 'third_acc(filtered_by_y)',
                                       'first_acc(filtered_outliers)', 'second_acc(filtered_outliers)', 'third_acc(filtered_outliers)']
                    # acc_titles = [s for s in sorted(list(res_dict.keys())) if "_acc" in s]
                    for ca_idx, ca_name in enumerate(csv_acc_columns):
                        for acc_idx, acc_name in enumerate(acc_header):
                            if acc_name == ca_name:
                                res_dict[ca_name] = acc_dataset[1][acc_idx]
                            elif ca_name not in acc_header:
                                res_dict[ca_name] = 'None'
                    res_dict['RMSE(filtered_by_y)'] = rmse_dataset[1][0]
                    res_dict['RMSE(filtered_outliers)'] = rmse_dataset[1][1]

                    res_df = pd.DataFrame.from_dict(res_dict, orient='index').transpose()
                    save_file_path = os.path.join(OUT_PATH, start_date[2:] + '_' + end_date[2:] + '_' + eval_data_fname)
                    if not(os.path.isfile(save_file_path)):
                        res_df.to_csv(save_file_path, mode='w')
                    else:
                        res_df.to_csv(save_file_path, mode='a', header=False)
        else:
            print("\n You entered {:d}. Please enter 0 or 1. Thanks".format(ans))

def parse_arguments(argv):

    parser = argparse.ArgumentParser()

    parser.add_argument("--op_mode", default='manual', help="operation mode", choices=['manual', 'auto'])
    parser.add_argument("--server_mode", default='EVAL', help="server mode", choices=['EST', 'EVAL', 'ALL'])
    parser.add_argument("--ini_fname", required=True, help="ini filename")
    parser.add_argument("--out_path", default=None, help="Output path")
    parser.add_argument("--logging_", default=False, action='store_true', help="Logging flag")
    parser.add_argument("--console_logging_", default=False, action='store_true', help="Console logging flag")
    parser.add_argument("--log_dir", default=None, help="Log directory")

    args = parser.parse_args(argv)
    args.out_path = sys_lib.unicode_normalize(args.out_path)
    args.log_dir = sys_lib.unicode_normalize(args.log_dir)

    return args

LOG_DIR = None
DB_OP_ = True
OP_MODE = 'manual' # manual / auto
SERVER_MODE = 'EVAL' # EST / EVAL / ALL
INI_FNAME = '../db_analysis/db_setting.ini'
CRT_DATE = sys_lib.get_datetime("%Y%m%d")
OUT_PATH = "../db_analysis/Output/" + CRT_DATE + "/"


if __name__ == "__main__":

    if len(sys.argv) == 1:
        sys.argv.extend([
            "--op_mode", OP_MODE,
            "--server_mode", SERVER_MODE,
            "--ini_fname", INI_FNAME,
            "--out_path", OUT_PATH,
            "--logging_",
            "--console_logging_",
            "--log_dir", LOG_DIR
                         ])
    main(parse_arguments(sys.argv[1:]))
