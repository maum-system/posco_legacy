#! /usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import os
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.dates as mdates

class MakePlot:
    def __init__(self):
        pass
    def raw_plot(self, fname, db_df, real_y, pred_y, SERVER_MODE, model_unit, OUT_PATH, ori_min, ori_max, show=False):
        # 실측 예측 raw data
        # 그래프 크기 설정
        plt.rcParams["figure.figsize"] = [15, 5]
        # 산포도 생성
        db_df = db_df.sort_values(by=['DATE'], axis=0)
        plt.scatter(db_df['DATE2'].dt.strftime('%y-%m-%d'), db_df["{}".format(real_y)], c='blue', alpha=0.5,
                    label='real_y')
        plt.scatter(db_df['DATE2'].dt.strftime('%y-%m-%d'), db_df["{}".format(pred_y)], c='red', alpha=0.5,
                    label='{}_y'.format(SERVER_MODE).lower())
        plt.ylim(ori_min, ori_max+ori_max*0.05)
        plt.legend(loc='upper left')
        plt.title(label=" ".join(fname.split(".")[0].split("_")[2:]), loc='center', fontsize=20)
        plt.xlabel('Date', fontsize=20)
        plt.ylabel('{} Raw Data ({})'.format(real_y, model_unit), fontsize=20)
        plt.gcf().autofmt_xdate()
        plt.xticks(rotation=45)
        plt.xticks(db_df['DATE2'].dt.strftime('%y-%m').duplicated(keep='first').replace(True,"").replace(False, db_df['DATE2'].dt.strftime('%y-%m-%d')))
        # plt.text(25, 80, '상자 속에 글자', fontsize=24, bbox=dict(boxstyle='square', color='lightgray'))
        plt.savefig(os.path.join(OUT_PATH, fname), dpi=300)
        if show:
            plt.show()
        plt.clf()

    def x_real_y_pred_plot(self, fname, db_df, real_y, pred_y, SERVER_MODE, model_unit, OUT_PATH, ori_min, ori_max, show=False):
        # x 축에 실측값, y 축에 예측값인 그래프
        plt.rcParams["figure.figsize"] = [15, 5]
        # 산포도 생성
        plt.scatter(db_df["{}".format(real_y)], # x
                    db_df["{}".format(pred_y)], # y
                    alpha=0.5,
                    c = abs(db_df['{}'.format(real_y)] - db_df['{}'.format(pred_y)]),
                    cmap='Blues_r')
        plt.ylim(ori_min, ori_max+ori_max*0.05)
        plt.xlim(ori_min, ori_max+ori_max*0.05)
        plt.plot([ori_min, ori_max+ori_max*0.05], [ori_min, ori_max+ori_max*0.05], alpha=0.2, color='green', linestyle='dashed')
        plt.title(label=" ".join(fname.split(".")[0].split("_")[2:]), loc='center', fontsize=20)
        plt.xlabel('{} Data ({})'.format(real_y, model_unit), fontsize=20)
        plt.ylabel('{} Data ({})'.format(pred_y, model_unit), fontsize=20)
        plt.xticks(rotation=45)
        plt.savefig(os.path.join(OUT_PATH, fname), dpi=300)
        if show:
            plt.show()
        plt.clf()

    def average_plot(self, fname_dict_t, df_summary_t, real_y, pred_y, model_unit, OUT_PATH, ori_min, ori_max, show=False):
        # 일/주/월 평균 값
        plt.rcParams["figure.figsize"] = [15, 5]
        plt.scatter(df_summary_t['DATE2'].dt.strftime('%y-%m-%d'), df_summary_t["{}_AVERAGE".format(real_y)],
                    c='blue', alpha=0.5,
                    label='real_y_avg')
        plt.scatter(df_summary_t['DATE2'].dt.strftime('%y-%m-%d'), df_summary_t["{}_AVERAGE".format(pred_y)],
                    c='red',
                    alpha=0.5, label='eval_y_avg')
        plt.ylim(ori_min, ori_max+ori_max*0.05)
        plt.legend(loc='upper left')
        plt.title(label=" ".join(fname_dict_t.split(".")[0].split("_")[2:]), loc='center', fontsize=20)
        plt.xlabel('Date', fontsize=20)
        plt.ylabel('{} Raw Data Average ({})'.format(real_y, model_unit), fontsize=20)
        plt.gcf().autofmt_xdate()
        plt.xticks(rotation=45)
        # if 'day' in fname_dict_t:
            # plt.xticks(df_summary_t['DATE2'].dt.strftime('%y-%m').duplicated(keep='first').replace(True, "")
                       # .replace(False, df_summary_t['DATE2'].dt.strftime('%y-%m-%d')))
        plt.savefig(os.path.join(OUT_PATH, fname_dict_t), dpi=300)
        if show:
            plt.show()
        plt.clf()

    def accuracy_plot(self, fname_dict_t, df_summary_t, re_empty, tolerances, real_y, OUT_PATH, show=False):
        # 일/주/월 적중률
        plt.rcParams["figure.figsize"] = [15, 5]
        plt.scatter(df_summary_t['DATE2'].dt.strftime('%y-%m-%d'),
                    df_summary_t["filter_y_ACC_{}".format(tolerances[0])], c='red', alpha=0.5,
                    label='filter_y_ACC_{}'.format(tolerances[0]))
        plt.scatter(df_summary_t['DATE2'].dt.strftime('%y-%m-%d'),
                    df_summary_t["filter_y_ACC_{}".format(tolerances[1])], c='darkorange', alpha=0.5,
                    label='filter_y_ACC_{}'.format(tolerances[1]))
        if re_empty is False:
            plt.scatter(df_summary_t['DATE2'].dt.strftime('%y-%m-%d'),
                        df_summary_t["refined_ACC_{}".format(tolerances[0])], c='green', alpha=0.5,
                        label='refined_ACC_{}'.format(tolerances[0]))
            plt.scatter(df_summary_t['DATE2'].dt.strftime('%y-%m-%d'),
                        df_summary_t["refined_ACC_{}".format(tolerances[1])], c='blue', alpha=0.5,
                        label='refined_ACC_{}'.format(tolerances[1]))
        plt.ylim(0, 1+1*0.05)
        plt.legend(loc='upper left')
        plt.title(label=" ".join(fname_dict_t.split(".")[0].split("_")[2:]), loc='center', fontsize=20)
        plt.xlabel('Date', fontsize=20)
        plt.ylabel('{} Accuracy'.format(real_y), fontsize=20)
        plt.gcf().autofmt_xdate()
        plt.xticks(rotation=45)
        # if 'day' in fname_dict_t:
        #     plt.xticks(df_summary_t['DATE2'].dt.strftime('%y-%m').duplicated(keep='first').replace(True, "")
        #                .replace(False, df_summary_t['DATE2'].dt.strftime('%y-%m-%d')))
        plt.savefig(os.path.join(OUT_PATH, fname_dict_t), dpi=300)
        if show:
            plt.show()
        plt.clf()