#! /usr/bin/env python
# -*- coding: utf-8 -*-

import pymssql
from pandas import DataFrame
import pandas as pd
import matplotlib.pyplot as plt

import sys
if sys.version_info >= (3, 3):
    import collections.abc as collections_abc
else:
    import collections as collections_abc


def get_input_var(dataset_idx=14):  # 55, 70, 73
    # input 으로 쓰이는 변수명을 list 형태로 반환한다.
    # data = pd.read_csv("C:/mindslab/posco-temp-model-component/sys_component/rh_macro_var.csv")
    data = pd.read_csv("D:/mindslab/posco-cvt-decarb/sys_cvt_decarb/cvt_decarb_var_add.csv")
    df = data.iloc[:, [3, dataset_idx]]
    df = df[df.iloc[:,1] == '100']
    df = df.reset_index(drop=True)
    return df.iloc[:,0].values.tolist()

def select_input(select_var):
    conn = pymssql.connect(host="172.24.92.148", user="SA", password="sm2_ai_dev!", database="MINDS_CVT")
    cursor = conn.cursor()
    sql = "SELECT {} FROM [MINDS_CVT].[dbo].[cvt_decarb_est]  " \
          "WHERE TRANSACTION_CD = 'CD_2S_ES' AND DATE >='20210101' AND DATE <='20210428' ORDER BY No"
    cursor.execute(
        sql.format(select_var)
    )
    row = cursor.fetchall()
    row_all = [list(r) for r in row]
    column_names = select_var.split(", ")
    df = DataFrame(row_all, columns = column_names) # (row_all, columns=[])
    for c_name in column_names:
        plt.rcParams["figure.figsize"] = [15, 5]
        plt.scatter(df['DATE'], df["{}".format(c_name)], c='blue', alpha=0.5, label='real_y')
        plt.xticks(rotation=45)
        # plt.savefig(os.path.join(OUT_PATH, fname), dpi=300)
        plt.savefig('yse-ys_{}.jpg'.format(c_name), dpi=300)
        plt.clf()
        # df.to_csv("ys.csv", mode='w')

if __name__ == "__main__":
    var_list = get_input_var()
    var_list.insert(0,'DATE')
    var_list.insert(0,'No')
    # var_list = list()
    var_str = ", ".join(var_list)
    select_input(var_str)