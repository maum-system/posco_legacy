import pandas as pd
from lib import data_frame as df_lib

C_INSERT_BY_ALLOY_TAP = 'C_INSERT_BY_ALLOY_TAP'
C_INSERT_RATIO_TAP = 'C_INSERT_RATIO_TAP'
TERMINAL_C = 'TERMINAL_C'

carbon_in_alloy = { 'HC_SIMN'    : 1.45, 'RECARBURIZER' : 93.7,
                    'FESI_ALLOY' : 0.07, 'FECR_MC'      : 0.75,
                    'FECR_HC'    : 7.9,  'FECR_LC'      : 0.05,
                    'FEV'        : 0.25, 'MNMETAL'      : 0.02,
                    'FEMN_MC'    : 1.74, 'FEMN_LC'      : 0.65,
                    'FEMN_HC'    : 6.87, 'CU'           : 0,
                    'MO'         : 0,    'FE_SI_CR'     : 0.04,
                    'NI'         : 0.25, 'FEP'          : 0.02,
                    'SB'         : 0}

def refine_dataset_by_y_value(dataset):
    if dataset is None:
        return []
    else:
        df = pd.DataFrame(dataset[1:], columns=dataset[0])

        def calc_carbon_insert_amount(alloy, carbon_in_alloy):
            if type(alloy) != float and type(alloy) != int:
                alloy = df_lib.convert_data_type(alloy, float)
            return (alloy * carbon_in_alloy) / 100

        # [3-1-1] ~ [3-1-N]
        alloy_vars = list(carbon_in_alloy.keys())
        alloy_ratio_vars = []
        for alloy in alloy_vars:
            alloy_ratio = alloy + '_RATIO'
            df[alloy_ratio] = df.apply(lambda x: calc_carbon_insert_amount(x[alloy], carbon_in_alloy[alloy]), axis=1)
            alloy_ratio_vars.append(alloy_ratio)

        # [3] = SUM([3-1-1]:[3-1-N])
        df[C_INSERT_BY_ALLOY_TAP] =  df[alloy_ratio_vars].sum(axis=1)

        # [2] = ([3] / [4]) * 100
        df.TAP_WGT = df_lib.convert_data_type(df.TAP_WGT, int)
        df[C_INSERT_RATIO_TAP] = (df[C_INSERT_BY_ALLOY_TAP] / df.TAP_WGT) * 100

        # [Y] = [1] – [2]
        df.BAP_C = df_lib.convert_data_type(df.BAP_C, float)
        df[TERMINAL_C] = df.BAP_C - df[C_INSERT_RATIO_TAP]

        # Drop unnecessary columns
        drop_columns = alloy_ratio_vars
        df.drop(drop_columns, axis=1, inplace=True)

        new_header = df.columns.tolist()
        new_dataset = df.values.tolist()
        return [new_header] + new_dataset

def run(feature, dataset=None, mode=None, logger=None):
    if not dataset:
        dataset = feature.dataset

    feature.dataset = refine_dataset_by_y_value(dataset)