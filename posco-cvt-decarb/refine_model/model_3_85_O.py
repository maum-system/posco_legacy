import pandas as pd
from lib import data_frame as df_lib


def refine_dataset_by_y_value(dataset):
    if dataset is None:
        return []
    else:
        df = pd.DataFrame(dataset[1:], columns=dataset[0])

        def calc_aim_carbon_by_steel_grd(STEEL_GRD):
            if STEEL_GRD == 'U':
                CAIM = 350
            else:
                CAIM = 420
            return CAIM

        df['CAIM'] = df.apply(lambda x: calc_aim_carbon_by_steel_grd(x['STEEL_GRD']), axis=1)

        new_header = df.columns.tolist()
        new_dataset = df.values.tolist()
        return [new_header] + new_dataset

def run(feature, dataset=None, mode=None, logger=None):
    if not dataset:
        dataset = feature.dataset

    feature.dataset = refine_dataset_by_y_value(dataset)
