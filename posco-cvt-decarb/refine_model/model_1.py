import pandas as pd
from lib import data_frame as df_lib
import numpy as np

carbon_in_alloy = { 'HC_SIMN'    : 1.45, 'RECARBURIZER' : 93.7,
                    'FESI_ALLOY' : 0.07, 'FECR_MC'      : 0.75,
                    'FECR_HC'    : 7.9,  'FECR_LC'      : 0.05,
                    'FEV'        : 0.25, 'MNMETAL'      : 0.02,
                    'FEMN_MC'    : 1.74, 'FEMN_LC'      : 0.65,
                    'FEMN_HC'    : 6.87, 'CU'           : 0,
                    'MO'         : 0,    'FE_SI_CR'     : 0.04,
                    'NI'         : 0.25, 'FEP'          : 0.02,
                    'SB'         : 0}

def refine_dataset_by_y_value(dataset):
    if dataset is None:
        return []
    else:
        df = pd.DataFrame(dataset[1:], columns=dataset[0])

        def calc_carbon_insert_amount(alloy, carbon_in_alloy):
            if type(alloy) != float and type(alloy) != int:
                alloy = df_lib.convert_data_type(alloy, float)
            return (alloy * carbon_in_alloy) / 100

        # [3-1-1] ~ [3-1-N]
        alloy_vars = list(carbon_in_alloy.keys())
        alloy_ratio_vars = []
        for alloy in alloy_vars:
            alloy_ratio = alloy + '_RATIO'
            df[alloy_ratio] = df.apply(lambda x: calc_carbon_insert_amount(x[alloy], carbon_in_alloy[alloy]), axis=1)
            alloy_ratio_vars.append(alloy_ratio)

        # [3] = SUM([3-1-1]:[3-1-N])
        df.C_INSERT_BY_ALLOY_TAP =  df[alloy_ratio_vars].sum(axis=1)

        # [2] = ([3] / [4]) * 100
        df.TAP_WGT = df_lib.convert_data_type(df.TAP_WGT, int)
        df.C_INSERT_RATIO_TAP = (df.C_INSERT_BY_ALLOY_TAP / df.TAP_WGT) * 100

        # [Y] = [1] – [2]
        df.BAP_C = df_lib.convert_data_type(df.BAP_C, float)
        df['TERMINAL_C'] = df.BAP_C - df.C_INSERT_RATIO_TAP


        # Drop unnecessary columns
        drop_columns = alloy_ratio_vars
        df.drop(drop_columns, axis=1, inplace=True)

        # ini C
        df.KR_C[df.KR_C=='']='nan'
        df.KR_C=df.KR_C.astype('float')
        df.IRON_WGT[df.IRON_WGT=='']='nan'
        df.IRON_WGT=df.IRON_WGT.astype('float')
        df.TOTAL_INPUT[df.TOTAL_INPUT=='']='nan'
        df.TOTAL_INPUT=df.TOTAL_INPUT.astype('float')

        df['INI_C'] = df.IRON_WGT*df.KR_C/df.TOTAL_INPUT

        # Conversion Factor calculate
        df.TOTAL_CO_CO2_SUM[df.TOTAL_CO_CO2_SUM=='']='nan'
        df.TOTAL_CO_CO2_SUM=df.TOTAL_CO_CO2_SUM.astype('float')
        df.TOTAL_INPUT[df.TOTAL_INPUT == ''] = 'nan'
        df.TOTAL_INPUT = df.TOTAL_INPUT.astype('float')
        df['CF'] = 10000*( df.TERMINAL_C - df.INI_C)/ (df.TOTAL_CO_CO2_SUM)
        #df['CF'] = 10000 * (df.INI_C - df.TERMINAL_C) / (df.TOTAL_CO_CO2_SUM))

        # df.CX01_CONV1_CO_GAS_99[df.CX01_CONV1_CO_GAS_99 == ''] = 'nan'
        # df.CX01_CONV1_CO_GAS_99 = df.CX01_CONV1_CO_GAS_99.astype('float')
        # df.CX01_CONV1_CO2_GAS_99[df.CX01_CONV1_CO2_GAS_99 == ''] = 'nan'
        # df.CX01_CONV1_CO2_GAS_99 = df.CX01_CONV1_CO2_GAS_99.astype('float')
        # temp_sum_100 = df.TOTAL_CO_CO2_SUM-(df.CX01_CONV1_CO2_GAS_99+df.CX01_CONV1_CO_GAS_99)
        # df['CF'] = temp_sum_100/df.TOTAL_CO_CO2_SUM

        #per C
        per = sorted(np.linspace(10, 100, 10, dtype=np.int).tolist() + [85])

        for i in range(11):
            var = per[i]
            df['CX01_CONV1_CO_GAS_' + str(var)][df['CX01_CONV1_CO_GAS_' + str(var)] == ''] = 'nan'
            df['CX01_CONV1_CO_GAS_' + str(var)][df['CX01_CONV1_CO_GAS_' + str(var)]=='0']='nan'
            df['CX01_CONV1_CO_GAS_' + str(var)]=df['CX01_CONV1_CO_GAS_' + str(var)].astype('float')
            df['CX01_CONV1_CO2_GAS_' + str(var)][df['CX01_CONV1_CO2_GAS_' + str(var)] == ''] = 'nan'
            df['CX01_CONV1_CO2_GAS_' + str(var)][df['CX01_CONV1_CO2_GAS_' + str(var)]=='0']='nan'
            df['CX01_CONV1_CO2_GAS_' + str(var)]=df['CX01_CONV1_CO2_GAS_' + str(var)].astype('float')
            df['C'+str(var)] = df.INI_C + 0.0001*df.CF * (df['CX01_CONV1_CO_GAS_' + str(var)] + df['CX01_CONV1_CO2_GAS_' + str(var)])


        new_header = df.columns.tolist()
        new_dataset = df.values.tolist()
        return [new_header] + new_dataset

def run(feature, dataset=None, mode=None, logger=None):
    if not dataset:
        dataset = feature.dataset

    feature.dataset = refine_dataset_by_y_value(dataset)
