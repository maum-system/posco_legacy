import pandas as pd
from lib import data_frame as df_lib


def refine_dataset_by_y_value(dataset):
    if dataset is None:
        return []
    else:
        return dataset

def run(feature, dataset=None, mode=None, logger=None):
    if not dataset:
        dataset = feature.dataset

    feature.dataset = refine_dataset_by_y_value(dataset)
