from lib import data as data_lib


diff_var = 'DEC_TM_DIFF'
compare_vars = ('DEC_TM', 'OUT_DEC_TM')

dataset_file = 'dataset_raw/190909_info/190909_RH_EVAL_DB.csv'
dataset_output_file = 'dataset_raw/190909_info/190909_RH_EVAL_DB_filtered.csv'

filter_cond_list = [['TRANSACTION_CD', 'RH_4S_EV', 'equal'],  # model 4
                    [diff_var, '1', 'equal_or_smaller'],  # time diff
                    # ['RH_NO', '2', 'equal'],
                    ]
data_list = data_lib.read_csv_file(dataset_file)
var_names = data_list[0]

compare_vars_idx = (var_names.index(compare_vars[0]), var_names.index(compare_vars[1]))

for idx, row_data in enumerate(data_list):
    if idx == 0:
        row_data.append(diff_var)
    else:
        try:
            row_data.append(str(abs(int(row_data[compare_vars_idx[0]]) - int(row_data[compare_vars_idx[1]]))))
        except ValueError:
            row_data.append('NULL')

filtered_data = data_list[1:]

for filter_cond in filter_cond_list:
    var_idx = var_names.index(filter_cond[0])
    temp_data_list = []
    for row_data in filtered_data:
        if filter_cond[2] == 'equal':
            if row_data[var_idx] == filter_cond[1]:
                temp_data_list.append(row_data)
        if filter_cond[2] == 'equal_or_smaller':
            if int(row_data[var_idx]) <= int(filter_cond[1]):
                temp_data_list.append(row_data)
    filtered_data = temp_data_list

filtered_data = [var_names] + filtered_data
data_lib.write_list_to_csv(filtered_data, dataset_output_file, overwrite=True)
