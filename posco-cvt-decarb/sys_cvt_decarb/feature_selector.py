#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""This module's docstring summary line.
This is a multi-line docstring. Paragraphs are separated with blank lines.
Lines conform to 79-column limit.
Module and packages names should be short, lower_case_with_underscores.
Notice that this in not PEP8-cheatsheet.py
Seriously, use flake8. Atom.io with https://atom.io/packages/linter-flake8
is awesome!
See http://www.python.org/dev/peps/pep-0008/ for more PEP-8 details

Coding style reference
    - https://www.python.org/dev/peps/pep-0008/
    - http://web.archive.org/web/20111010053227/http://jaynes.colorado.edu
    /PythonGuidelines.html

    HISTORY
    Ver 0.1
        - Initial version
        - Read and parse information csv, configuration ini, and data csv.
        - Build RhStartTempModel with all variables.
    Ver 0.2
        - Analyze statistical properties of variables.
        - Analyze time series of input vectors.
        - Generate histograms of variables.

"""
import numpy as np
from sklearn.feature_selection import f_regression
from sklearn.feature_selection import mutual_info_regression
from sklearn.ensemble import RandomForestRegressor
import matplotlib.pyplot as plt


__author__ = "Hoon Paek, Dong-gi Kim, Ji-young Moon"
__copyright__ = "Copyright 2007, The Cogent Project"
__credits__ = []
__license__ = "GPL"
__version__ = "0.0.1"
__maintainer__ = "Hoon Paek"
__email__ = "hoon.paek@mindslab.ai"
__status__ = "Prototype"  # Prototype / Development / Production

NO_ARG_TEST = True
HIST_BIN_NUM = 128
HIST_DIR = "hist_temp"

ORDER_IDX = 'order_index'
CAT_MAIN_IDX = 'Main'
VAR_NAME = 'object_name'
VAR_IF_NAME = "var_if_name"
DATA_TYPE = 'data_type'
RANGE_MIN = 'range_min'
RANGE_MAX = 'range_max'
STRING_DOMAIN = 'str_domain'
PRIORITY = 'priority'
NULL_DATA = 'null_data_cfg'

DIV_0_ERR = "#DIV/0!"
ANALYTICS_RESULT_FILE = None


class FeatureSelector(object):
    """

    """
    def __init__(self, dataset_X, dataset_Y, offset=1):
        """ Constructor of FeatureSelector class.

        :param dataset_X:
        :param dataset_Y:
        """
        self.dataset = {'X': dataset_X, 'Y': dataset_Y}
        self.offset = offset
        self.dataset_X = self.dataset['X'][self.offset:]
        self.dataset_y = self.dataset['Y'][self.offset:]
        self.deleted_keys = []
        self.log = ""

    def get_log(self):
        return self.log

    def variance_threshold_method(self, thresh=0.0, norm_cond=True, selection_=True, log_=True):
        """ Run feature selection by variance threshold.
        The values of input features are normailzed by output variance.

        :param thresh:
        :param norm_cond:
        :param selection_:
        :param log_:
        :return:
        """
        dataset_dim = [len(self.dataset['X']) - self.offset, len(self.dataset['X'][0])]
        feat_names = self.dataset['X'][0][:]

        if norm_cond:
            y_var = np.array(self.dataset['Y'][1:], dtype=np.float).var()
        else:
            y_var = 1.
        x_dataset = np.array(self.dataset['X'][self.offset:], dtype=np.float)
        x_vars = x_dataset.var(axis=0) / y_var

        del_list = x_vars < thresh
        num_del_list = len(del_list[:]) - sum(del_list)

        if selection_:
            self.dataset['X'], del_keys = del_features(self.dataset['X'], del_list)
            self.deleted_keys.extend(del_keys)

        if not log_:
            self.log = ""

        keys_and_vars = [[feat_names[idx], x_vars[idx]] for idx in range(len(feat_names))]
        sorted_keys_and_vars = sorted(keys_and_vars, key=lambda x:x[1], reverse=True)

        msg  = "\n\n # Feature selection by variance threshold normalized by output variance"
        msg += "\n   > Sorted variance of number type input variables"
        for idx in range(len(feat_names)):
            msg += "\n     {:3d} : {:<30} : {:16.6f}".format(idx+1, sorted_keys_and_vars[idx][0], sorted_keys_and_vars[idx][1])
            if num_del_list == (idx+1):
                msg += "\n     ---------------------------threshold : {:16.6f}".format(thresh)
        msg += "\n\n    > num_type input dataset : {:d} x {:d} -> {:d} x {:d}".format(
            dataset_dim[0], dataset_dim[1], len(self.dataset['X'])-1, len(self.dataset['X'][0]))
        # plot
        # bar_name = []
        # fig_feat = plt.figure(figsize=(25,15))
        # cnt = 0
        # for content in sorted_keys_and_vars:
        #     plt.barh(cnt, int(content[1]))
        #     bar_name.append(content[0])
        #     cnt += 1
        #
        # plt.xlim(0, 3000000)
        # plt.xlabel('Feature importance')
        # plt.ylabel('Variable')
        # plt.yticks([i for i, _ in enumerate(bar_name)], bar_name)
        # plt.title('Variance threshold')
        # fig_feat_name = 'variance_threshold_feature_impt'
        # fig_feat.savefig(fig_feat_name)

        self.log = msg
        return sorted_keys_and_vars


    def f_regression_method(self, p_thresh=1.0, selection_=True, log_=True):
        """ Run f_regression.

        :param p_thresh:
        :param selection_:
        :param log_:
        :return:
        """
        dataset_dim = [len(self.dataset['X']) - self.offset, len(self.dataset['X'][0])]
        feat_names = self.dataset['X'][0][:]
        X = self.dataset['X'][1:]
        y = self.dataset['Y'][1:]
        F_val, p_val = f_regression(X, y)

        del_list = (p_val > p_thresh)
        num_del_list = len(del_list[:]) - sum(del_list)

        if selection_:
            self.dataset['X'], del_keys = del_features(self.dataset['X'], del_list)
            self.deleted_keys.extend(del_keys)

        if not log_:
            return

        keys_and_p = [[feat_names[idx], p_val[idx]] for idx in range(len(feat_names))]
        sorted_keys_and_p = sorted(keys_and_p, key=lambda x:x[1])

        msg  = "\n\n # Feature selection by f_regression"
        msg += "\n   > Sorted variance of number type input variables"
        for idx in range(len(feat_names)):
            msg += "\n     {:3d} : {:<30} : {:16.6f}".format(idx+1, sorted_keys_and_p[idx][0], sorted_keys_and_p[idx][1])
            if num_del_list == (idx+1):
                msg += "\n     -------------------p_value threshold : {:16.6f}".format(p_thresh)

        msg += "\n\n    > num_type input dataset : {:d} x {:d} -> {:d} x {:d}".format(
            dataset_dim[0], dataset_dim[1], len(self.dataset['X'])-1, len(self.dataset['X'][0]))

        # plot
        bar_name = []
        fig_feat = plt.figure(figsize=(25,15))
        cnt = 0
        for content in sorted_keys_and_p:
            try:
                plt.barh(cnt, int(content[1]*100))
                bar_name.append(content[0])
                cnt += 1
            except ValueError:
                pass

        plt.xlim(0, 100)
        plt.xlabel('Feature importance')
        plt.ylabel('Variable')
        plt.yticks([i for i, _ in enumerate(bar_name)], bar_name)
        plt.title('F_regression')
        fig_feat_name = 'f_regression_feature_impt'
        fig_feat.savefig(fig_feat_name)

        self.log = msg
        return sorted_keys_and_p

    def mutual_info_regression_method(self, mi_thresh=0, selection_=True, log_=True):
        """ Run mutual information regression.

        :param mi_thresh:
        :param selection_:
        :param log_:
        :return:
        """
        dataset_dim = [len(self.dataset['X']) - self.offset, len(self.dataset['X'][0])]
        feat_names = self.dataset['X'][0][:]
        X = self.dataset['X'][1:]
        y = self.dataset['Y'][1:]
        mi_val = mutual_info_regression(X, y)

        del_list = (mi_val < mi_thresh)
        num_del_list = len(del_list[:]) - sum(del_list)

        if selection_:
            self.dataset['X'], del_keys = del_features(self.dataset['X'], del_list)
            self.deleted_keys.extend(del_keys)

        if not log_:
            return

        keys_and_p = [[feat_names[idx], mi_val[idx]] for idx in range(len(feat_names))]
        sorted_keys_and_p = sorted(keys_and_p, key=lambda x:x[1], reverse=True)

        msg  = "\n\n # Feature selection by mutual_info_regression"
        msg += "\n   > Sorted variance of number type input variables"
        for idx in range(len(feat_names)):
            msg += "\n     {:3d} : {:<30} : {:16.6f}".format(idx+1, sorted_keys_and_p[idx][0], sorted_keys_and_p[idx][1])
            if num_del_list == (idx+1):
                msg += "\n     ------------------mi_value threshold : {:16.6f}".format(mi_thresh)

        msg += "\n\n    > num_type input dataset : {:d} x {:d} -> {:d} x {:d}".format(
            dataset_dim[0], dataset_dim[1], len(self.dataset['X'])-1, len(self.dataset['X'][0]))

        # plot
        bar_name = []
        fig_feat = plt.figure(figsize=(25,15))
        cnt = 0
        for content in sorted_keys_and_p:
            plt.barh(cnt, int(content[1]*100))
            bar_name.append(content[0])
            cnt += 1

        plt.xlim(0, 100)
        plt.xlabel('Feature importance')
        plt.ylabel('Variable')
        plt.yticks([i for i, _ in enumerate(bar_name)], bar_name)
        plt.title('Mutual_info_regression')
        fig_feat_name = 'mutual_info_regression_feature_impt'
        fig_feat.savefig(fig_feat_name)

        self.log = msg
        return sorted_keys_and_p

    def random_forest_regressor_method(self, thresh=0, selection_=True, log_=True):
        dataset_dim = [len(self.dataset['X']) - self.offset, len(self.dataset['X'][0])]
        feat_names = self.dataset['X'][0][:]
        clf = RandomForestRegressor()
        clf.fit(self.dataset_X, self.dataset_y)
        feat_weights = clf.feature_importances_
        # y_predict = clf.predict(self.dataset_X)
        del_list = list(feat_weights < thresh)
        num_del_list = len(del_list[:]) - sum(del_list)

        if selection_:
            self.dataset['X'], del_keys = del_features(self.dataset['X'], del_list)
            self.deleted_keys.extend(del_keys)

        if not log_:
            return

        key_and_wgt = [[feat_names[idx], feat_weights[idx]] for idx in range(len(feat_names))]
        sorted_key_and_wgt = sorted(key_and_wgt, key=lambda x:x[1], reverse=True)

        msg  = "\n\n # Feature selection by random forest classifier"
        msg += "\n   > Sorted weight of number type input variables"
        for idx in range(len(feat_names)):
            msg += "\n     {:3d} : {:<30} : {:16.6f}".format(idx+1, sorted_key_and_wgt[idx][0], sorted_key_and_wgt[idx][1])
            if num_del_list == (idx+1):
                msg += "\n     ------------------rf_value threshold : {:16.6f}".format(thresh)

        msg += "\n\n    > num_type input dataset : {:d} x {:d} -> {:d} x {:d}".format(
            dataset_dim[0], dataset_dim[1], len(self.dataset['X'])-1, len(self.dataset['X'][0]))

        # plot
        bar_name = []
        fig_feat = plt.figure(figsize=(25,15))
        cnt = 0
        for content in sorted_key_and_wgt:
            plt.barh(cnt, int(content[1]*100))
            bar_name.append(content[0])
            cnt += 1

        plt.xlim(0, 100)
        plt.xlabel('Feature importance')
        plt.ylabel('Variable')
        plt.yticks([i for i, _ in enumerate(bar_name)], bar_name)
        plt.title('Random forest classifier')
        fig_feat_name = 'random_forest_classifier_feat_impt'
        fig_feat.savefig(fig_feat_name)

        self.log = msg
        return sorted_key_and_wgt


def del_features(dataset, del_list):
    del_keys = []
    trans_dataset = list(map(list, zip(*dataset)))
    for idx in range(len(del_list)-1,-1,-1):
        if del_list[idx]:
            del_keys.append(trans_dataset[idx][0])
            del trans_dataset[idx]
    return list(map(list, zip(*trans_dataset))), del_keys

