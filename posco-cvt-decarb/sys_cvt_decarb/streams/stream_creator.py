import argparse
import configparser
import sys

from lib import data as data_lib
from lib import sys_lib

VAR_INFO_CSV = 'var_info_csv'
OUTPUT_PATH = 'stream_cvt_decarb.txt'


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--var_csv_file", required=True, help="csv file for variables")
    parser.add_argument("--var_ini_file", required=True, help="ini file for configuration")
    parser.add_argument("--dat_csv_file", default="", help="csv file for data set")

    parser.add_argument("--create_stream_", default=False, action='store_true',
                        help="Flag to create stream")

    args = parser.parse_args()

    var_ini = configparser.ConfigParser()
    var_ini.read(args.var_ini_file)

    var_mtx = data_lib.read_csv_file(args.var_csv_file)
    dat_mtx = data_lib.read_csv_file(args.dat_csv_file)

    start_pos, end_pos = sys_lib.calc_crop_info_from_ini(var_ini[VAR_INFO_CSV])
    roi_var_mtx = data_lib.crop_mtx(var_mtx, start_pos, end_pos)
    var_name_list = data_lib.transpose_list(roi_var_mtx)[2][1:]
    stream_data_list = []
    dat_mtx_name_list = dat_mtx[0]

    for row_dat in dat_mtx[1:]:
        new_row_dat = []
        for var_name in var_name_list:
            try:
                var_idx = dat_mtx_name_list.index(var_name)
                new_row_dat.append(row_dat[var_idx])
            except ValueError:
                new_row_dat.append('')
        stream_data_list.append(new_row_dat)

    data_lib.write_list_to_csv(stream_data_list, OUTPUT_PATH)


if __name__ == "__main__":

    if len(sys.argv) == 1:
        sys.argv.extend([
            "--var_csv_file", "../cvt_decarb_var.csv",
            "--var_ini_file", "../cvt_decarb_var.ini",
            "--dat_csv_file", "../dataset_raw/200220_test_cvt_carb_dataset.csv",

            "--create_stream_"
        ])

    main()
