import os
import sys
import argparse
import configparser
from tqdm import tqdm

import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np

from lib import data as data_lib

PLOTS_PATH = "./analytics_plots/"  # directory to save generated plots
USE_RAW_DATASET = False   # set this to False for now!!! # use raw or refined dataset (use path defined in .ini file)
DATE_VAR_NAME = "DATE"  # date variable name in csv file

"""
var_ini 파일에서 사용하는 설정값 목록:
model_info.model_number
dataset_{model_info.model_number}.dataset_csv_filename  # refined 데이터셋 경로


"""


def find_single_target_var_name(metadata_mtx, model_num):
    """ metadata에서 목표변수(y인자)로 표기된 변수이름 리턴 (dataset==0)
    Params:
        metadata_mtx: 2d list of str
        model_num: str

    Returns:
        variable_name: str

    Raises:
        ValueError if output variable name cannot be found
    """

    def index_2d(list_2d, item):
        for idx, row in enumerate(list_2d):
            if item in row:
                return idx, row.index(item)

    model_num_col_idx = index_2d(metadata_mtx, model_num)[1]
    for i in range(len(metadata_mtx)):
        if metadata_mtx[i][model_num_col_idx] == "0":
            return metadata_mtx[i][index_2d(metadata_mtx, "var_if_name")[1]]

    raise ValueError  # output variable name cannot be found


def describe_and_plot_single_var(df_data, var_name):
    print("\nbasic description of variable:", var_name)
    print("=========================================================================")
    print(df_data[var_name].describe())
    print("Skewness: %f" % df_data[var_name].skew())
    print("Kurtosis: %f" % df_data[var_name].kurt())
    print("\n")

    if not os.path.exists(PLOTS_PATH):
        os.makedirs(PLOTS_PATH)

    sns.displot(df_data[var_name])
    plt.savefig(os.path.join(PLOTS_PATH, "target_variable_histogram"))


def generate_univariate_plots(df_data):
    """ generate and save histogram plots for all columns
    Params:
        df_data: dataframe
        target_var_name: str

    Issue:
        seaborn.histplot() is too slow
    """
    print("generating univariate plots for all variables...")
    if not os.path.exists(os.path.join(PLOTS_PATH, "univariate_plots/")):
        os.makedirs(os.path.join(PLOTS_PATH, "univariate_plots/"))

    for col in tqdm(df_data.columns):
        try:
            sns.histplot(df_data[col], kde=False)
            plt.savefig(os.path.join(PLOTS_PATH, "univariate_plots/", col + ".png"))
            plt.close()
        except Exception as e:
            print(e)
            pass


def generate_bivariate_plots(df_data, target_var_name):
    """ generate and save scatter plot for all columns vs target var
    Params:
        df_data: dataframe
        target_var_name: str
    """
    print("generating bivariate plots for all variables...")
    if not os.path.exists(os.path.join(PLOTS_PATH, "bivariate_plots/")):
        os.makedirs(os.path.join(PLOTS_PATH, "bivariate_plots/"))

    for col in tqdm(df_data.columns):
        try:
            df_2cols = pd.concat([df_data[target_var_name], df_data[col]], axis=1)
            df_2cols.plot.scatter(x=col, y=target_var_name)
            plt.savefig(os.path.join(PLOTS_PATH, "bivariate_plots/", col + ".png"))
            plt.close()
        except Exception as e:
            print("Exception while scatter plotting", col, " vs", target_var_name, ":\n", e)
            pass


def generate_boxplots(df_data, target_var_name, cols):
    """ generate boxplots for each given column names
        Params:
            df_data: dataframe
            target_var_name: str
            cols: list of str, column names considered
    """
    print("generating boxplots for categorical variables...")
    for col in tqdm(cols):
        box_data1 = pd.concat([df_data[target_var_name], df_data[col]], axis=1)
        sns.boxplot(x=col, y=target_var_name, data=box_data1)
        plt.savefig(os.path.join(PLOTS_PATH, "bivariate_boxplots/", col+".png"))
        plt.close()


def generate_by_date_plots(df_data, date_var_name):
    """ generate and save scatter plot for all columns vs time
    Params:
        df_data: dataframe
        date_var_name: str
    """
    print("generating by_date plots for all variables...")
    try:
        if not os.path.exists(os.path.join(PLOTS_PATH, "plots_by_date/")):
            os.makedirs(os.path.join(PLOTS_PATH, "plots_by_date/"))

        for col in tqdm(df_data.columns):
            df_2cols = pd.concat([df_data[date_var_name].astype("int", errors="ignore"), df_data[col]], axis=1)
            if df_data[col].dtypes == "object":
                pass
                # sns.catplot(x=date_var_name, y=col, data=df_2cols)  # too slow for non-categorical str values
            else:
                df_2cols.plot.scatter(x=date_var_name, y=col)
            plt.savefig(os.path.join(PLOTS_PATH, "plots_by_date/", col + ".png"))
            plt.close()
    except KeyError:
        return


def generate_correlation_heatmap(df_data, target_var_name, correlation_mtx, select_top=10):
    """ compute correlation matrix
        Params:
            df_data: dataframe
            target_var_name: str
            correlation_mtx: dataframe as correlation matrix
            select_top: int, select top x correlated columns (correlated to target var)
    """
    corr_mtx_k_cols = correlation_mtx.nlargest(select_top, target_var_name)[target_var_name].index
    np_correlation_mtx = np.corrcoef(df_data[corr_mtx_k_cols].values.T)
    ax = sns.heatmap(np_correlation_mtx, cbar=True, fmt=".2f", annot=True, annot_kws={"size": 10},
                     yticklabels=corr_mtx_k_cols.values, xticklabels=corr_mtx_k_cols.values)
    plt.savefig(os.path.join(PLOTS_PATH, "correlation_heatmap_top_" + str(select_top) + ".png"))
    plt.close()


def generate_correlation_heatmap(df_data, cols):
    """ generate heatmap plot for given column names
        Params:
            df_data: dataframe
            cols: list of str, column names considered
    """
    print("generating correlation heatmap plots for selected variables...")
    ax = sns.heatmap(np.corrcoef(df_data[cols].values.T), cbar=True, fmt=".2f", annot=True, annot_kws={"size": 10},
                     yticklabels=cols.values, xticklabels=cols.values)
    plt.savefig(os.path.join(PLOTS_PATH, "correlation_heatmap_" + str(len(cols)) + "_vars.png"))
    plt.close()


def generate_pairplot(df_data, cols):
    """ generate pair plot for given column names
        Params:
            df_data: dataframe
            cols: list of str, column names considered
    """
    print("generating pair plots for selected variables...")
    sns.set()
    ax = sns.pairplot(df_data[cols], size=2.5)
    plt.savefig(os.path.join(PLOTS_PATH, "pair_plot_" + str(len(cols)) + "_vars.png"))
    plt.close()




def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--var_csv_file", required=True, help="csv file for variables")
    parser.add_argument("--var_ini_file", required=True, help="ini file for configuration")
    args = parser.parse_args()
    var_ini = configparser.ConfigParser()
    var_ini.read(args.var_ini_file)
    metadata_mtx = data_lib.read_csv_file(args.var_csv_file)

    if USE_RAW_DATASET:
        df_data = pd.read_csv(var_ini["dataset_" + var_ini["model_info"]["model_number"]]["raw_dataset_csv_filename"])
    else:
        df_data = pd.read_csv(var_ini["dataset_" + var_ini["model_info"]["model_number"]]["dataset_csv_filename"])
    target_var_name = find_single_target_var_name(metadata_mtx, var_ini["model_info"]["model_number"])
    correlation_mtx = df_data.corr()
    corr_mtx_10_cols = correlation_mtx.nlargest(10, target_var_name)[target_var_name].index

    print(len(df_data.columns), "columns")

    describe_and_plot_single_var(df_data, target_var_name)
    generate_bivariate_plots(df_data, target_var_name)
    generate_univariate_plots(df_data)
    generate_boxplots(df_data, target_var_name, ["PROCESS", "BLOW_TYPE"])
    generate_correlation_heatmap(df_data, corr_mtx_10_cols)
    generate_pairplot(df_data, corr_mtx_10_cols)
    #generate_by_date_plots(df_data, DATE_VAR_NAME)
    #generate_by_index_plots(df_data, DATE_VAR_NAME)git add
    #generate_missing_values_plots(df_data.iloc[:10], "missing_values_10.png")


    return 0


if __name__ == "__main__":

    if len(sys.argv) == 1:
        sys.argv.extend([
            "--var_csv_file", "../sys_cvt_decarb/cvt_decarb_var.csv",
            "--var_ini_file", "../sys_cvt_decarb/cvt_decarb_var.ini",
        ])

    main()
