#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""
"""
import sys
import os
# os.environ["CUDA_VISIBLE_DEVICES"]="-1" # CPU/GPU option
import argparse
import configparser
import pickle
import math
from operator import itemgetter
import matplotlib.pyplot as plt
from dateutil.parser import parse as date_parse
import datetime
import time
from sklearn.ensemble import RandomForestRegressor, BaggingRegressor, RandomForestClassifier, AdaBoostClassifier
from sklearn.linear_model import Lasso, Ridge
from sklearn.tree import DecisionTreeRegressor, DecisionTreeClassifier
from lightgbm.sklearn import LGBMRegressor, LGBMClassifier
from xgboost.sklearn import XGBRegressor, XGBClassifier
from sklearn.multioutput import MultiOutputRegressor
from sklearn.svm import LinearSVR
from sklearn.metrics import mean_squared_error, r2_score
from sklearn.model_selection import GridSearchCV, cross_val_score
import scipy.stats as st
import pickle
import numpy as np
import tensorflow as tf
from tensorflow.python.client import device_lib
from lib import sys_lib
from lib import data as data_lib
from lib import learning as learning_lib
from handlers import dataset_handler as Dataset


__author__ = "Hoon Paek, Dong-gi Kim"
__copyright__ = "Copyright 2019, The Posco Project"
__credits__ = []
__license__ = "GPL"
__version__ = "0.0.1"
__maintainer__ = "Dong-gi Kim"
__email__ = "motive@mindslab.ai"
__status__ = "Development"  # Prototype / Development / Production

SVR_C = 1.0
SVR_epsilon = 0.1
DATE_FORMAT = "%y%m%d"
DNN_LIST = ['DNNR', 'DNNC']
INT_ENCODING_LIST = ['RFR', 'BAGGING', 'LGBMR', 'XGBR', 'RFC', 'DNNC', 'AdaBoostC', 'XGBC', 'LGBMC', 'Lasso']
ONE_HOT_ENCODING_LIST = ['DNNR']


class MachineLearner(object):
    """

    """
    def __init__(self, Feat=None, dataset=None, out_idx=0):
        self.Feat = Feat
        self.dataset = dataset
        self.out_idx = out_idx
        self.log = ""

        self.X_name = None
        self.y_name = []
        self.y_num = 0
        self.X_dataset = None
        self.y_dataset = None
        self.y_predict = None
        self.params = None

        self.train_mse = 0
        self.train_mse_2 = 0
        self.train_rmse = []
        self.train_rmsre = 0
        self.train_rmsre_2 = 0
        self.train_r2_score = []

        self.test_mse = 0
        self.test_mse_2 = 0
        self.test_rmse = []
        self.test_rmsre = 0
        self.test_rmsre_2 = 0
        self.test_r2_score = []

        self.train_acc = []
        self.train_acc_2 = 0
        self.test_acc = []
        self.test_acc_2 = 0

        self.est_thresh = []
        self.X = [[], []]
        self.y = [[], []]

        self.model = None

    def get_log(self):
        return self.log

    def split_dataset_into_train_and_test(self, dataset_ratio=70, mode='random', num_train=None):
        """ Split dataset into train and test.

        :param dataset_ratio:
        :return:
        """
        dataset_train, dataset_test = learning_lib.split_dataset_into_train_and_test(self.dataset, dataset_ratio, offset=1,
                                                                                     mode=mode, num_train=num_train)
        trans_dataset_train = data_lib.transpose_list(dataset_train)
        trans_dataset_test = data_lib.transpose_list(dataset_test)
        self.X[0] = data_lib.transpose_list(trans_dataset_train[1:])[1:]
        self.X[1] = data_lib.transpose_list(trans_dataset_test[1:])[1:]
        self.y[0] = trans_dataset_train[0][1:]
        self.y[1] = trans_dataset_test[0][1:]
        self.X_name = self.dataset[0][1:]
        self.y_name.append(self.dataset[0][0])
        self.y_num = 1

    def split_dataset_into_train_and_test_for_multiple_y(self, num_y, dataset_ratio=70):
        """ Split dataset into train and test.
        :param dataset_ratio:
        :return:
        """
        dataset_train, dataset_test = learning_lib.split_dataset_into_train_and_test(self.dataset, dataset_ratio, offset=1)
        # dataset_train, dataset_test = learning_lib.split_dataset_into_train_and_test_by_k_fold(self.dataset,
        #                                                                                   dataset_ratio,
        #                                                                                   num_y,
        #                                                                                   offset=1,
        #                                                                                   k_fold_idx=3,
        #                                                                                   k_fold_area_idx=3)
        trans_dataset_train = data_lib.transpose_list(dataset_train)
        trans_dataset_test = data_lib.transpose_list(dataset_test)

        self.X[0] = data_lib.transpose_list(trans_dataset_train[num_y:])[1:]
        self.X[1] = data_lib.transpose_list(trans_dataset_test[num_y:])[1:]
        self.y[0] = data_lib.transpose_list(trans_dataset_train[:num_y])[1:]
        self.y[1] = data_lib.transpose_list(trans_dataset_test[:num_y])[1:]
        self.X_name = self.dataset[0][num_y:]
        self.y_name = self.dataset[0][:num_y]
        self.y_num = num_y

        self.X_dataset = self.X[0] + self.X[1]
        self.y_dataset = self.y[0] + self.y[1]

        pass

    def encode_symbol_dataset_by_int(self, dataset=None):
        """ Encode symbol dataset by integer

        :param dataset:
        :return:
        """
        if not dataset:
            dataset = self.dataset

        trans_dataset = data_lib.transpose_list(dataset)
        arr = []
        for col in trans_dataset:
            if col[0] not in self.Feat.vars:
                continue
            if data_lib.is_class_name(self.Feat.vars[col[0]], 'NumberClass'):
                try:
                    arr.append([col[0]] + [float(x) for x in col[1:]])
                except (TypeError, ValueError) as e:
                    print(e)
                    print(col[0])
            else:
                try:
                    arr.append([col[0]] + [self.Feat.vars[col[0]].domain.index(x) for x in col[1:]])
                except (TypeError, ValueError) as e:
                    print(e)
                    print(col[0])

        self.dataset = data_lib.transpose_list(arr)

        return self.dataset

    def decode_symbol_dataset_by_str_domain(self, dataset=None, target_var=None):
        """ Decode symbol dataset by str domain

        :param dataset:
        :return:
        """
        if not dataset:
            dataset = self.dataset

        trans_dataset = data_lib.transpose_list(dataset)
        arr = []
        for col in trans_dataset:
            if col[0] not in self.Feat.vars:
                continue
            if data_lib.is_class_name(self.Feat.vars[col[0]], 'NumberClass'):
                continue
            else:
                try:
                    if target_var:
                        arr.append([col[0]] + [self.Feat.vars[target_var].domain[x] for x in col[1:]])
                        break
                    else:
                        arr.append([col[0]] + [self.Feat.vars[col[0]].domain[x] for x in col[1:]])
                except (TypeError, ValueError) as e:
                    print(e)
                    print(col[0])

        self.dataset = data_lib.transpose_list(arr)

        return self.dataset

    def encode_symbol_dataset_by_one_hot_encoder(self, dataset=None):
        """ Encode symbol dataset by integer

        :param dataset:
        :return:
        """
        if not dataset:
            dataset = self.dataset

        trans_dataset = data_lib.transpose_list(dataset)
        arr = []
        for col in trans_dataset:
            if data_lib.is_class_name(self.Feat.vars[col[0]], 'NumberClass'):
                arr.append(col)
            else:
                width = len(self.Feat.vars[col[0]].domain)
                sub_arr = [[]]
                for i in range(width):
                    sub_arr[-1].append("{}__{:d}".format(col[0], i))
                for val in col[1:]:
                    sub_arr.append([0.]*width)
                    try:
                        sub_arr[-1][self.Feat.vars[col[0]].domain.index(val)] = 1.
                    except ValueError as e:
                        print(e)
                trans_sub_arr = data_lib.transpose_list(sub_arr)
                for i in range(width):
                    arr.append(trans_sub_arr[i])
        self.dataset = data_lib.transpose_list(arr)

        return self.dataset

    def normalize_number_features(self, dataset=None, min_val=-1, max_val=1, offset=0):
        """ Normalize number features by pre-defined range

        :param dataset:
        :param min_val:
        :param max_val:
        :return:
        """
        if not dataset:
            dataset = self.dataset

        trans_dataset = data_lib.transpose_list(dataset)

        # arr = [[trans_dataset[0][0]] + [float(x) for x in trans_dataset[0][1:]]]
        arr = []
        for col in trans_dataset[offset:]:
            if data_lib.is_class_name(self.Feat.vars[col[0]], 'NumberClass'):
                arr.append([col[0]])
                ptr = self.Feat.vars[col[0]]
                val_range = float(ptr.max_thresh - ptr.min_thresh)
                if val_range == 0:
                    print(col[0])
                    for val in col[1:]:
                        arr[-1].append(float(val))
                else:
                    for val in col[1:]:
                        # print(val)

                        try:
                            val = float(val)
                        except ValueError:
                            val = 0
                        if val < ptr.min_thresh:
                            val = ptr.min_thresh
                        elif val > ptr.max_thresh:
                            val = ptr.max_thresh
                        # if val_range == 0:
                        #    arr[-1].append(col[1:])
                        # else:
                        arr[-1].append((val - ptr.min_thresh) / val_range * (max_val - min_val) + min_val)
            else:
                arr.append(col)

        self.dataset = data_lib.transpose_list(arr)

        return self.dataset

    def denormalize_output(self, predicted_output, real_max, real_min, norm_min=-1, norm_max=1):
        return (predicted_output - norm_min) / (norm_max - norm_min) * (real_max - real_min) + real_min

    def check_est_accuracy(self, error_arr, est_thresh=None):
        """ Check estimation accuracy based on estimation threshold.

        :param error_arr:
        :param est_thresh:
        :return:
        """
        if not est_thresh:
            est_thresh = self.est_thresh

        if self.y_num == 1:
            acc_arr = [x < est_thresh for x in error_arr]
            res_acc_arr = 100. * sum(acc_arr) / float(len(acc_arr))
        else:
            res_acc_arr = []
            for i in range(self.y_num):
                acc_arr = [x < est_thresh[i] for x in error_arr[:,i]]
                res_acc_arr.append(100. * sum(acc_arr) / float(len(acc_arr)))

        if len(acc_arr) == 0:
            return 99.99
        else:
            return res_acc_arr

    def draw_ABS_error_plot(self, err_values, acc_values, est_margin_values, category_list, plot_filename_prefix=''
                            , plot_=True, num=2):

        for i in range(num):
            train_err, test_err = err_values[i]
            train_acc, test_acc = acc_values[i]
            est_margin = est_margin_values[i]
            category = category_list[i]

            max_range = max(max(train_err), max(test_err), est_margin) + 1
            fig = plt.figure()
            plt.subplot(1,2,1)
            plt.plot(train_err, 'b', [est_margin,] * len(train_err), 'r')
            plt.ylim(0, max_range)
            plt.title('{} Training ABS error({:2d}%)'.format(category, int(train_acc)))
            plt.subplot(1,2,2)
            plt.plot(test_err, 'b', [est_margin,] * len(test_err), 'r')
            plt.ylim(0, max_range)
            plt.title('{} Test ABS error({:2d}%)'.format(category, int(test_acc)))
            if plot_filename_prefix:
                fig.savefig(plot_filename_prefix)
            if plot_:
                plt.show()
            else:
                plt.close(fig)

        pass

    def find_feature_importance(self, y_name_list=None, method='RFR'):
        global msg
        y_num = len(y_name_list)
        for i, y_name in enumerate(y_name_list):
            mdl = self.model
            if y_num == 1:
                feat_impt = [[self.dataset[0][y_num + idx], mdl.feature_importances_[idx]]
                             for idx in range(len(self.dataset[0][y_num:]))]
            else:
                feat_impt = [[self.dataset[0][y_num+idx], mdl.estimators_[i].feature_importances_[idx]]
                                for idx in range(len(self.dataset[0][y_num:]))]
            sorted_feat_impt = sorted(feat_impt, key=itemgetter(1), reverse=True)

            msg = "\n   > [{}] Sorted feature importance".format(y_name)
            for idx, content in enumerate(sorted_feat_impt):
                msg += "\n   > {:d}. {:<30} : {:6.12f}".format(idx+1, content[0], content[1])

            self.log += msg
            # print(self.log)

            # Draw feature importance
            # index =
            # plt.barh(index, mdl[i].feature_importances_, align='center')
            # plt.yticks(index, cancer.feature_names)
            # plt.ylim(-1, n_feature)
            # plt.xlabel('feature importance', size=15)
            # plt.ylabel('feature', size=15)
            # plt.show()

        pass

    def generate_plot(self, model):
        for k, y_name in enumerate(self.y_name):
            if data_lib.is_class_name(self.Feat.vars[y_name], "NumberClass"):
                xmin = self.Feat.vars[y_name].min_thresh
                xmax = self.Feat.vars[y_name].max_thresh
            else:
                xmin = 0
                xmax = len(self.Feat.vars[y_name].domain)
            if self.y_num == 1:
                train_val = np.array(self.y[0])[:,]
                test_val = np.array(self.y[1])[:,]
                train_pred_val = np.array(self.y_predict[0])[:,]
                test_pred_val = np.array(self.y_predict[1])[:,]
            else:
                train_val = np.array(self.y[0])[:, k]
                test_val = np.array(self.y[1])[:, k]
                train_pred_val = np.array(self.y_predict[0])[:, k]
                test_pred_val = np.array(self.y_predict[1])[:, k]

            for j, acc_th in enumerate(self.est_thresh[k]):
                len_th = len(self.est_thresh[k])
                self.gen_plot(xmin=xmin, xmax=xmax, margin=acc_th,
                              train_val=train_val, test_val=test_val,
                              train_pred_val=train_pred_val,
                              test_pred_val=test_pred_val,
                              train_acc=self.train_acc[(k*len_th)+j], test_acc=self.test_acc[(k*len_th)+j],
                              c_name=y_name, plot_name='Data Distribution Graph', method=model)
        pass

    def gen_plot(self, xmin, xmax, margin, train_val, test_val, train_pred_val, test_pred_val, train_acc, test_acc,
                 c_name, plot_name, method):

        xmin = xmin
        xmax = xmax

        under = []
        under.append((xmin - 30) - margin)
        under.append((xmax + 30) - margin)
        ideal = []
        ideal.append(xmin - 30)
        ideal.append(xmax + 30)
        below = []
        below.append((xmin - 30) + margin)
        below.append((xmax + 30) + margin)

        fig = plt.figure(1)
        fig = plt.figure(figsize=(25, 8))
        plt.subplot(1, 2, 1)
        plt.plot(ideal, under, color='r', linestyle='--', label='{} margin'.format(margin))
        plt.plot(ideal, below, color='r', linestyle='--')
        plt.xlim(xmin, xmax)
        plt.ylim(xmin, xmax)
        half_margin = margin / 2
        mod_train_y = np.array([np.random.uniform(-half_margin, half_margin) for i in range(len(train_val))]) + train_val
        mod_predict_y = np.array([np.random.uniform(-half_margin, half_margin) for i in range(len(train_pred_val))]) + train_pred_val
        plt.plot(ideal, ideal, color='r', linewidth=1, label='ideal value')
        plt.scatter(mod_train_y, mod_predict_y, color='g', s=0.5, label='real value')
        plt.xlabel('Real value of {}'.format(c_name))
        plt.ylabel('Prediction value of {}'.format(c_name))
        plt.title('Training results of {} ({:2d}%)   {} Distribution Graph'.format(method, int(train_acc), c_name))
        plt.legend()

        plt.subplot(1, 2, 2)
        plt.plot(ideal, under, color='r', linestyle='--', label='{} margin'.format(margin))
        plt.plot(ideal, below, color='r', linestyle='--')
        plt.xlim(xmin, xmax)
        plt.ylim(xmin, xmax)
        mod_test_y = np.array([np.random.uniform(-half_margin, half_margin) for i in range(len(test_val))]) + test_val
        mod_predict_test_y = np.array(
            [np.random.uniform(-half_margin, half_margin) for i in range(len(test_pred_val))]) + test_pred_val
        plt.plot(ideal, ideal, color='r', linewidth=1, label='ideal value')
        plt.scatter(mod_test_y, mod_predict_test_y, color='g', s=0.5, label='real value')
        plt.xlabel('Real value of {}'.format(c_name))
        plt.ylabel('Prediction value of {}'.format(c_name))
        plt.title('Test results of {} ({:2d}%)   {} Distribution Graph'.format(method, int(test_acc), c_name))
        plt.legend()

        fig_name = c_name + '_' + plot_name + '_' + str(int(margin))
        fig.savefig(fig_name)
        pass

    def load_model_params(self, model, ml_cfg):
        model_parmas = {}
        model_cfg = ml_cfg[model]
        # For Regression & Classification Model
        if model[:-1] == 'RF':
            self.log = "\n # Random Forest Regression" if model[-1] == 'R' else "\n # Random Forest Classification"
            n_estimators = int(model_cfg['n_estimators'])
            min_samples_leaf = int(model_cfg['min_samples_leaf'])
            max_depth = None if model_cfg['max_depth'] == "none" else int(model_cfg['max_depth'])
            max_leaf_nodes = None if model_cfg['max_leaf_nodes'] == "none" else int(model_cfg['max_leaf_nodes'])
            max_features = int(model_cfg['max_features']) if model_cfg['max_features'] != "auto" and model_cfg['max_features'] != "sqrt"\
                                                             and model_cfg['max_features'] != "log2" \
                                                          else model_cfg['max_features']
            model_parmas['n_estimators'] = n_estimators
            model_parmas['min_samples_leaf'] = min_samples_leaf
            model_parmas['max_depth'] = max_depth
            model_parmas['max_leaf_nodes'] = max_leaf_nodes
            model_parmas['max_features'] = max_features
            for i in range(1, self.y_num+1):
                if i == 1:
                    est_thresh = ml_cfg[model]['est_thresh'].split(',')
                elif i >= 2:
                    est_thresh = ml_cfg[model]['est_thresh_' + str(i)].split(',')
                est_thresh = [float(th) for th in est_thresh]
                self.est_thresh.append(est_thresh)

        elif model[:-1] == 'LGBM':
            self.log = "\n # Light GBM Regression" if model[-1] == 'R' else "\n # Light GBM Classification"
            n_estimators = int(model_cfg['n_estimators'])
            learning_rate = float(model_cfg['learning_rate'])
            num_leaves = int(model_cfg['num_leaves'])
            max_depth = None if model_cfg['max_depth'] == "none" else int(model_cfg['max_depth'])

            model_parmas['n_estimators'] = n_estimators
            model_parmas['learning_rate'] = learning_rate
            model_parmas['num_leaves'] = num_leaves
            model_parmas['max_depth'] = max_depth
            for i in range(1, self.y_num+1):
                if i == 1:
                    est_thresh = ml_cfg[model]['est_thresh'].split(',')
                elif i >= 2:
                    est_thresh = ml_cfg[model]['est_thresh_' + str(i)].split(',')
                est_thresh = [float(th) for th in est_thresh]
                self.est_thresh.append(est_thresh)

        elif model[:-1] == 'XGB':
            self.log = "\n # XGBoost Regression" if model[-1] == 'R' else "\n # XGBoost Classification"
            n_estimators = int(model_cfg['n_estimators'])
            learning_rate = float(model_cfg['learning_rate'])
            max_depth = None if model_cfg['max_depth'] == "none" else int(model_cfg['max_depth'])
            colsample_bytree = float(model_cfg['colsample_bytree'])
            subsample = float(model_cfg['subsample'])
            gamma = float(model_cfg['gamma'])
            reg_alpha = model_cfg['reg_alpha']
            min_child_weight = model_cfg['min_child_weight']

            model_parmas['n_estimators'] = n_estimators
            model_parmas['learning_rate'] = learning_rate
            model_parmas['max_depth'] = max_depth
            model_parmas['colsample_bytree'] = colsample_bytree
            model_parmas['subsample'] = subsample
            model_parmas['gamma'] = gamma
            model_parmas['reg_alpha'] = reg_alpha
            model_parmas['min_child_weight'] = min_child_weight

            for i in range(1, self.y_num+1):
                if i == 1:
                    est_thresh = ml_cfg[model]['est_thresh'].split(',')
                elif i >= 2:
                    est_thresh = ml_cfg[model]['est_thresh_' + str(i)].split(',')
                est_thresh = [float(th) for th in est_thresh]
                self.est_thresh.append(est_thresh)

        elif model[:-1] == 'LASSO':
            self.log = "\n # Lasso Linear Regression" if model[-1] == 'R' else "\n # Lasso Linear Classification"
            n_alpha = float(model_cfg['alpha'])

            model_parmas['n_alpha'] = n_alpha

            for i in range(1, self.y_num+1):
                if i == 1:
                    est_thresh = ml_cfg[model]['est_thresh'].split(',')
                elif i >= 2:
                    est_thresh = ml_cfg[model]['est_thresh_' + str(i)].split(',')
                est_thresh = [float(th) for th in est_thresh]
                self.est_thresh.append(est_thresh)


        elif model[:-1] == 'AdaBoost':
            self.log = "\n # AdaBoost Regression" if model[-1] == 'R' else "\n # AdaBoost Classification"
            n_estimators = int(model_cfg['n_estimators'])
            min_samples_leaf = int(model_cfg['min_samples_leaf'])

            model_parmas['n_estimators'] = n_estimators
            model_parmas['min_samples_leaf'] = min_samples_leaf

            for i in range(1, self.y_num+1):
                if i == 1:
                    est_thresh = ml_cfg[model]['est_thresh'].split(',')
                elif i >= 2:
                    est_thresh = ml_cfg[model]['est_thresh_' + str(i)].split(',')
                est_thresh = [float(th) for th in est_thresh]
                self.est_thresh.append(est_thresh)

        self.params = model_parmas
        return model_parmas

    def load_grid_params(self, model_params):
        grid_params = {}
        ref_params = {
            'n_estimators': [100, 300, 500, 1000],
            'learning_rate': [0.01, 0.1, 0.05, 0.5, 1],
            'num_leaves': [10, 50, 100, 200, 500],
            'min_samples_leaf': [1, 5, 10, 50, 100, 200, 500],
            'max_depth': [None, 10, 25, 50, 100, 200, 500],
            'max_leaf_nodes': [None, 10, 25, 50, 100, 200, 500],
        }
        for key in model_params.keys():
            grid_params[key] = ref_params[key]
        print(grid_params)
        return grid_params

    def select_model(self, model_name, params, y_num):
        # For Regression Model
        global model
        if model_name[-1] == 'R':
            if model_name == 'RFR':
                if y_num == 1:
                    model = RandomForestRegressor(n_estimators=params['n_estimators'], min_samples_leaf=params['min_samples_leaf'],
                                                  max_depth=params['max_depth'], max_leaf_nodes=params['max_leaf_nodes'],
                                                  max_features=params['max_features'], n_jobs=-1)
                else:
                    model = MultiOutputRegressor(RandomForestRegressor(n_estimators=params['n_estimators'],
                                                                       min_samples_leaf=params['min_samples_leaf'],
                                                                       max_depth=params['max_depth'],
                                                                       max_leaf_nodes=params['max_leaf_nodes'],
                                                                       max_features=params['max_features'], n_jobs=-1))
            elif model_name == 'XGBR':
                if y_num == 1:
                    model = XGBRegressor(n_estimators=params['n_estimators'], learning_rate=params['learning_rate'],
                                         max_depth=params['max_depth'], colsample_bytree=params['colsample_bytree'],
                                         subsample=params['subsample'], gamma=params['gamma'],
                                         reg_alpha=params['reg_alpha'], min_child_weight=params['min_child_weight'],
                                         n_jobs=-1)
                else:
                    model = MultiOutputRegressor(XGBRegressor(n_estimators=params['n_estimators'], learning_rate=params['learning_rate'],
                                                              max_depth=params['max_depth'], colsample_bytree=params['colsample_bytree'],
                                                              subsample=params['subsample'], gamma=params['gamma'],
                                                              reg_alpha=params['reg_alpha'], min_child_weight=params['min_child_weight'],
                                                              n_jobs=-1))
            elif model_name == 'LGBMR':
                if y_num == 1:
                    model = LGBMRegressor(n_estimators=params['n_estimators'], learning_rate=params['learning_rate'],
                                          num_leaves=params['num_leaves'], max_depth=params['max_depth'], n_jobs=-1)
                else:
                    model = MultiOutputRegressor(LGBMRegressor(n_estimators=params['n_estimators'], learning_rate=params['learning_rate'],
                                                               num_leaves=params['num_leaves'], max_depth=params['max_depth'], n_jobs=-1))
            elif model_name == 'LASSOR':
                if y_num == 1:
                    model = Lasso(alpha=params['n_alpha'], normalize=params['normalize'])
                else:
                    model = MultiOutputRegressor(Lasso(alpha=params['n_alpha'], normalize=params['normalize']))

        # For Classification Model
        if model_name[-1] == 'C':
            if model_name == 'RFC':
                model = RandomForestClassifier(n_estimators=params['n_estimators'],
                                            min_samples_leaf=params['min_samples_leaf'],
                                            max_depth=params['max_depth'], max_leaf_nodes=params['max_leaf_nodes'],
                                            max_features=params['max_features'], n_jobs=-1)
            elif model_name == 'XGBC':
                model = XGBClassifier(n_estimators=params['n_estimators'], learning_rate=params['learning_rate'],
                                   max_depth=params['max_depth'], colsample_bytree=params['colsample_bytree'],
                                   subsample=params['subsample'], gamma=params['gamma'],
                                   reg_alpha=params['reg_alpha'], min_child_weight=params['min_child_weight'],
                                   n_jobs=-1)
            elif model_name == 'LGBMC':
                model = LGBMClassifier(n_estimators=params['n_estimators'], learning_rate=params['learning_rate'],
                                       num_leaves=params['num_leaves'], max_depth=params['max_depth'], n_jobs=-1)
        return model

    def eval_model_perf(self, model, ml_cfg):
        pred_y = []
        est_err = []
        est_acc = []
        rmse_arr = []
        r2_score_arr = []

        # Test 데이터가 1개이상 존재시 수행
        if len(self.X[1]) > 1:

            # Train & Test measure
            for i in range(2):
                pred_y.append(model.predict(self.X[i]))
                est_err.append(abs(pred_y[i] - np.array(self.y[i])))
                rmse_arr.append(learning_lib.calc_RMSE(self.y[i], pred_y[i]))
                r2_score_arr.append(r2_score(self.y[i], pred_y[i]))

                # Th에 따른 정확도 측정
                for j in range(len(self.est_thresh[0])):
                    # y개수에 따른 threshold 추출
                    th_arr = []
                    for k in range(self.y_num):
                        th_arr.append(self.est_thresh[k][j])
                    est_acc.append(self.check_est_accuracy(est_err[i], th_arr))

            # 모델의 예측속도 측정
            if ml_cfg['general']['print_est_time'].upper() == 'ON':
                start_time = time.time()
                _ = model.predict(self.X[0])
                print(" ## Model's prediction time : {:.3f} sec".format(time.time() - start_time))

            self.y_predict = pred_y
            self.train_rmse.append(rmse_arr[0]), self.test_rmse.append(rmse_arr[1])
            self.train_r2_score.append(r2_score_arr[0]), self.test_r2_score.append(r2_score_arr[1])
            log = ""
            for k, y_name in enumerate(self.y_name):  # For multiple y value
                for j, acc_th in enumerate(self.est_thresh[k]):
                    len_th = len(self.est_thresh[k])
                    self.train_acc.append(est_acc[:len_th][j][k])
                    self.test_acc.append(est_acc[len_th:][j][k])
                    log += "\n # y_name : {}, train acc : {:4.2f}, test acc : {:4.2f}, tolerance : {}".format(y_name, self.train_acc[(k*len_th)+j], self.test_acc[(k*len_th)+j], acc_th)
                    if self.y_num == 1:
                        log += "\n # train rmse : {:4.8f}, test rmse : {:4.8f}".format(self.train_rmse[k], self.test_rmse[k])
                        log += "\n # train r2 score : {:4.2f}, test r2 score : {:4.2f}".format(self.train_r2_score[k], self.test_r2_score[k])
                    else:
                        log += "\n # train rmse : {:4.8f}, test rmse : {:4.8f}".format(self.train_rmse[-1][k], self.test_rmse[-1][k])
                        log += "\n # train r2 score : {:4.2f}, test r2 score : {:4.2f}".format(self.train_r2_score[-1], self.test_r2_score[-1])
            self.log += log
        return self.train_acc, self.test_acc


    def run_model(self, model_params, model, ml_cfg):

        if ml_cfg['general']['grid_search'] == 'on':
            params = self.load_grid_params(model_params=model_params)
            grid = GridSearchCV(estimator=model, param_grid=params, cv=5, verbose=True)
            grid = grid.fit(self.X[0], self.y[0])
            print(grid.best_score_)
            print(grid.best_params_)
            print('Parameter tuning complete')
            sys.exit()
        else:
            model.fit(self.X[0], self.y[0])
            self.model = model

        self.eval_model_perf(model=model, ml_cfg=ml_cfg)


    def select_model_and_run(self, model_name, ml_cfg, dump_dir):
        # For Regression Model
        if model_name[-1] == 'R':
            if model_name == 'DNNR':
                if self.y_num == 1:
                    self.deep_neuralnet_regressor(ml_cfg, dump_dir)
                else:
                    self.deep_neuralnet_regressor_for_multiple_y(ml_cfg)
            else: # Using Scikit-Learn Interface Models
                model_params = self.load_model_params(model_name, ml_cfg)
                model = self.select_model(model_name=model_name, params=model_params, y_num=self.y_num)
                self.run_model(model_params=model_params, model=model, ml_cfg=ml_cfg)

        # For Classification Model
        if model_name[-1] == 'C':
            if model_name == 'DNNC':
                self.deep_neuralnet_classifier(ml_cfg)
            else: # Using Scikit-Learn Interface Models
                model_params = self.load_model_params(model_name, ml_cfg)
                model = self.select_model(model_name=model_name, params=model_params, y_num=self.y_num)
                self.run_model(model_params=model_params, model=model, ml_cfg=ml_cfg)
        pass


    def lasso_regression(self, ml_cfg):

        """ Run light gbm regressor with various environments.

        :param dataset_ratio:
        :param n_estimators:
        :param est_thresh:
        :return:
        """
        lasso_cfg = ml_cfg['Lasso']
        msg = "\n # Lasso Linear Regression"

        n_alpha = float(lasso_cfg['alpha'])
        est_thresh = int(lasso_cfg['est_thresh'])
        self.est_thresh = est_thresh

        GRID_SEARCH_ = False
        if GRID_SEARCH_:
            param_grid = {
                'learning_rate': [0.01, 0.1, 0.05, 0.5, 1],
                'n_estimators': [100, 300, 500, 1000],
                'num_leaves': [10, 50, 100, 200, 500],
                "max_depth": [10, 50, 100, 200, 500],
            }

            mdl = Lasso(alpha=n_alpha, normalize=True)
            grid = GridSearchCV(estimator=mdl, param_grid=param_grid, cv=5)
            grid = grid.fit(self.X[0], self.y[0])
            print('Best score is {:4.4f}' .format(grid.best_score_))
            print('Best params is {}' .format(str(grid.best_params_)))
            print('Parameter tuning complete')

        else:
            mdl = Lasso(alpha=n_alpha, normalize=True)

        mdl.fit(self.X[0], self.y[0])
        self.model = mdl
        scroes = cross_val_score(mdl, self.X[1], self.y[1], cv=5)

        pred_y = []
        est_err = []
        est_acc = []
        mape_arr = []
        rmse_arr = []

        for i in range(2):
            pred_y.append(mdl.predict(self.X[i]))
            est_err.append(abs(pred_y[i] - np.array(self.y[i])))
            est_acc.append(self.check_est_accuracy(est_err[-1], est_thresh))
            mape_arr.append(learning_lib.calc_MAPE(self.y[i], pred_y[-1]))
            rmse_arr.append(learning_lib.calc_RMSE(self.y[i], pred_y[-1]))

        if ml_cfg['general']['print_est_time'].upper() == 'ON':
            # 모델의 예측속도 측정
            start_time = time.time()
            mdl_predict = mdl.predict(self.X[0])
            print(" ## Model's prediction time : {:.3f} sec".format(time.time() - start_time))

        self.y_predict = pred_y
        self.train_acc = self.check_est_accuracy(est_err[0], est_thresh)
        self.test_acc = self.check_est_accuracy(est_err[1], est_thresh)
        self.train_mse = mean_squared_error(self.y[0], pred_y[0].tolist())
        self.test_mse = mean_squared_error(self.y[1], pred_y[1].tolist())

        msg += "\n # train acc : {:4.2f}, test acc : {:4.2f}".format(self.train_acc, self.test_acc)
        msg += "\n # train mse : {:4.2f}, test mse : {:4.2f}".format(self.train_mse, self.test_mse)

        self.log = msg
        self.params = lasso_cfg
        print(msg)
        return self.train_acc, self.test_acc


    def light_gbm_regressor_for_multiple_y(self, ml_cfg):
        """ Run light gbm regressor with various environments.

        :param dataset_ratio:
        :param n_estimators:
        :param est_thresh:
        :return:
        """
        lgbm_cfg = ml_cfg['LGBMR']
        msg = "\n # Light GBM Regression"
        estimator__n_estimators = int(lgbm_cfg['n_estimators'])
        estimator__learning_rate = float(lgbm_cfg['learning_rate'])
        estimator__num_leaves = int(lgbm_cfg['num_leaves'])
        estimator__max_depth = int(lgbm_cfg['max_depth'])

        est_thresh = lgbm_cfg['est_thresh'].split(',')
        self.est_thresh = [float(thresh) for thresh in est_thresh]

        self.est_thresh = est_thresh

        GRID_SEARCH_ = False
        if GRID_SEARCH_:
            param_grid = {
                'estimator__learning_rate': [0.01, 0.05],
                'estimator__n_estimators': [300, 500, 1000],
                'estimator__num_leaves': [5, 10, 20],
                "estimator__max_depth": [5, 10, 20]
            }

            mdl = MultiOutputRegressor(LGBMRegressor(n_estimators=estimator__n_estimators,
                                                     learning_rate=estimator__learning_rate,
                                                     num_leaves=estimator__num_leaves,
                                                     max_depth=estimator__max_depth, n_jobs=-1, verbose=10))
            grid = GridSearchCV(estimator=mdl, param_grid=param_grid, cv=5)
            grid = grid.fit(self.X[0], self.y[0])
            print('Best score is {:4.4f}' .format(grid.best_score_))
            print('Best params is {}' .format(str(grid.best_params_)))
            print('Parameter tuning complete')

        else:
            mdl = MultiOutputRegressor(LGBMRegressor(n_estimators=estimator__n_estimators,
                                                     learning_rate=estimator__learning_rate,
                                                     num_leaves=estimator__num_leaves, max_depth=estimator__max_depth))

        mdl.fit(self.X[0], self.y[0])
        self.model = mdl

        pred_y = []
        est_err = []
        est_acc = []
        mse_arr = []
        rmse_arr = []
        r2_score_arr = []
        if len(self.X[1]) > 1:
            for i in range(2):  # Train & Test
                pred_y.append(mdl.predict(self.X[i]))
                est_err.append(abs(pred_y[i] - np.array(self.y[i])))
                est_acc.append(self.check_est_accuracy(est_err[i], self.est_thresh))
                mse_arr.append(mean_squared_error(self.y[i], pred_y[i]))
                rmse_arr.append(learning_lib.calc_RMSE(self.y[i], pred_y[i]))
                r2_score_arr.append(r2_score(self.y[i], pred_y[i], multioutput='raw_values'))

            if ml_cfg['general']['print_est_time'].upper() == 'ON':
                # 모델의 예측속도 측정
                start_time = time.time()
                mdl_predict = mdl.predict(self.X[0])
                print(" ## Model's prediction time : {:.3f} sec".format(time.time() - start_time))
                fit_start_time = time.time()
                mdl_fit = mdl.fit(self.X[0], self.y[0])
                print(" ## Model's training time : {:.3f} sec".format(time.time() - fit_start_time))

            self.y_predict = pred_y

            self.train_acc, self.test_acc = est_acc
            self.train_mse, self.test_mse = mse_arr
            self.train_rmse, self.test_rmse = rmse_arr
            self.train_r2_score, self.test_r2_score = r2_score_arr

            for j in range(self.y_num):  # For multiple y value
                msg += "\n # type : {}, train acc : {:4.2f}, test acc : {:4.2f}".format(self.y_name[j], self.train_acc[j], self.test_acc[j])
                msg += "\n # type : {}, train rmse : {:4.2f}, test rmse : {:4.2f}".format(self.y_name[j], self.train_rmse[j], self.test_rmse[j])
                msg += "\n # type : {}, train r2 score : {:4.2f}, test r2 score : {:4.2f}".format(self.y_name[j], self.train_r2_score[j], self.test_r2_score[j])

            self.log = msg
            self.params = lgbm_cfg
            print(msg)
            return self.train_acc, self.test_acc

    def light_gbm_classifier(self, ml_cfg):
        """ Run light gbm regressor with various environments.

        :param dataset_ratio:
        :param n_estimators:
        :param est_thresh:
        :return:
        """
        lgbm_cfg = ml_cfg['LGBMC']
        msg = "\n # Light GBM Classification"
        n_estimators = int(lgbm_cfg['n_estimators'])
        learning_rate = float(lgbm_cfg['learning_rate'])
        num_leaves = int(lgbm_cfg['num_leaves'])
        max_depth = int(lgbm_cfg['max_depth'])
        est_thresh = float(lgbm_cfg['est_thresh'])

        self.est_thresh = est_thresh

        GRID_SEARCH_ = False
        if GRID_SEARCH_:
            param_grid = {
                'learning_rate': [0.01, 0.1, 0.05, 0.5, 1],
                'n_estimators': [100, 300, 500, 1000],
                'num_leaves': [3, 5, 10, 20, 100],
                "max_depth":  [3, 5, 10, 20, 100],
            }

            mdl = LGBMClassifier(n_estimators=n_estimators, learning_rate=learning_rate, num_leaves=num_leaves, max_depth=max_depth, n_jobs=-1)
            grid = GridSearchCV(estimator=mdl, param_grid=param_grid, cv=5)
            grid = grid.fit(self.X[0], self.y[0])
            print('Best score is {:4.4f}' .format(grid.best_score_))
            print('Best params is {}' .format(str(grid.best_params_)))
            print('Parameter tuning complete')

        else:
            mdl = LGBMClassifier(n_estimators=n_estimators, learning_rate=learning_rate)

        mdl.fit(self.X[0], self.y[0])
        self.model = mdl


        pred_y = []
        est_err = []
        est_acc = []
        mape_arr = []
        rmse_arr = []
        r2_score_arr = []
        if len(self.X[1]) > 1:
            for i in range(2):
                pred_y.append(mdl.predict(self.X[i]))
                est_err.append(abs(pred_y[i] - np.array(self.y[i])))
                est_acc.append(self.check_est_accuracy(est_err[-1], est_thresh))
                mape_arr.append(learning_lib.calc_MAPE(self.y[i], pred_y[-1]))
                rmse_arr.append(learning_lib.calc_RMSE(self.y[i], pred_y[-1]))
                r2_score_arr.append(r2_score(self.y[i], pred_y[i]))

            self.y_predict = pred_y
            self.train_acc, self.test_acc = est_acc
            self.train_rmse, self.test_rmse = rmse_arr
            self.train_r2_score, self.test_r2_score = r2_score_arr

            msg += "\n # train acc : {:4.2f}, test acc : {:4.2f}".format(self.train_acc, self.test_acc)
            msg += "\n # train rmse : {:4.2f}, test rmse : {:4.2f}".format(self.train_rmse, self.test_rmse)
            msg += "\n # train r2 score : {:4.2f}, test r2 score : {:4.2f}".format(self.train_r2_score, self.test_r2_score)

            self.log = msg
            self.params = lgbm_cfg
            return self.train_acc, self.test_acc


    def xgboost_regressor_for_multiple_y(self, ml_cfg):
        """ Run xgboost regressor with various environments.

        :param dataset_ratio:
        :param n_estimators:
        :param est_thresh:
        :return:
        """
        xgboost_cfg = ml_cfg['XGBR']
        msg = "\n # XGBoost Regression"
        estimator__n_estimators = int(xgboost_cfg['n_estimators'])
        estimator__learning_rate = float(xgboost_cfg['learning_rate'])
        estimator__max_depth = float(xgboost_cfg['max_depth'])

        est_thresh = xgboost_cfg['est_thresh'].split(',')
        self.est_thresh = [float(thresh) for thresh in est_thresh]

        self.est_thresh = est_thresh

        one_to_left = st.beta(10, 1)
        from_zero_positive = st.expon(0, 50)
        GRID_SEARCH_ = False
        if GRID_SEARCH_:
            param_grid = {
                'estimator__n_estimators': [300, 500, 1000],# [300, 500, 1000, 1500],
                'estimator__learning_rate': [0.1, 0.01, 0.05, 0.5], # [0.1, 0.01, 0.05, 0.5],
                'estimator__max_depth': [5, 10, 20, 40], #[5, 10, 20, 40, 60]
                # 'colsample_bytree' : one_to_left,
                # 'subsample' : one_to_left,
                # 'gamma' : st.uniform(0, 10),
                # 'reg_alpha' : from_zero_positive,
                # 'min_child_weight' : from_zero_positive
            }
            mdl = MultiOutputRegressor(XGBRegressor(n_estimators=estimator__n_estimators,
                                                    learning_rate=estimator__learning_rate,
                                                    max_depth=int(estimator__max_depth), n_jobs=-1))
            grid = GridSearchCV(estimator=mdl, param_grid=param_grid, cv=5)
            grid = grid.fit(self.X[0], self.y[0])
            print('Best score is {:4.4s}' .format(str(sum(grid.best_estimator_.predict(self.X[1]) == self.y[1]) / (len(self.y[1])*1.0))))
            print('Best params is {}' .format(str(grid.best_params_)))
            print('Parameter tuning complete')

        else:
            mdl = MultiOutputRegressor(XGBRegressor(n_estimators=estimator__n_estimators,
                                                    learning_rate=estimator__learning_rate,
                                                    max_depth=int(estimator__max_depth)))

        mdl.fit(self.X[0], self.y[0])
        self.model = mdl

        pred_y = []
        est_err = []
        est_acc = []
        mse_arr = []
        rmse_arr = []
        r2_score_arr = []
        if len(self.X[1]) > 1:
            for i in range(2):  # Train & Test
                pred_y.append(mdl.predict(self.X[i]))
                est_err.append(abs(pred_y[i] - np.array(self.y[i], dtype='float64')))
                est_acc.append(self.check_est_accuracy(est_err[i], self.est_thresh))
                mse_arr.append(mean_squared_error(np.array(self.y[i], dtype='float64'), pred_y[i]))
                rmse_arr.append(learning_lib.calc_RMSE(np.array(self.y[i], dtype='float64'), pred_y[i]))
                r2_score_arr.append(r2_score(np.array(self.y[i], dtype='float64'), pred_y[i], multioutput='raw_values'))

            if ml_cfg['general']['print_est_time'].upper() == 'ON':
                # 모델의 예측속도 측정
                start_time = time.time()
                mdl_predict = mdl.predict(self.X[0])
                print(" ## Model's prediction time : {:.3f} sec".format(time.time() - start_time))
                fit_start_time = time.time()
                mdl_fit = mdl.fit(self.X[0], self.y[0])
                print(" ## Model's training time : {:.3f} sec".format(time.time() - fit_start_time))

            self.y_predict = pred_y

            self.train_acc, self.test_acc = est_acc
            self.train_mse, self.test_mse = mse_arr
            self.train_rmse, self.test_rmse = rmse_arr
            self.train_r2_score, self.test_r2_score = r2_score_arr

            for j in range(self.y_num):  # For multiple y value
                msg += "\n # type : {}, train acc : {:4.2f}, test acc : {:4.2f}".format(self.y_name[j], self.train_acc[j],
                                                                                        self.test_acc[j])
                msg += "\n # type : {}, train rmse : {:4.2f}, test rmse : {:4.2f}".format(self.y_name[j],
                                                                                          self.train_rmse[j],
                                                                                          self.test_rmse[j])
                msg += "\n # type : {}, train r2 score : {:4.2f}, test r2 score : {:4.2f}".format(self.y_name[j],
                                                                                                  self.train_r2_score[j],
                                                                                                  self.test_r2_score[j])
            self.log = msg

            return self.train_acc, self.test_acc
            # if True:
            #     self.gen_plot()
        
    def xgboost_classifier(self, ml_cfg):
        """ Run xgboost regressor with various environments.

        :param dataset_ratio:
        :param n_estimators:
        :param est_thresh:
        :return:
        """
        xgboost_cfg = ml_cfg['XGBC']
        msg = "\n # XGBoost Classification"
        n_estimators = int(xgboost_cfg['n_estimators'])
        learning_rate = float(xgboost_cfg['learning_rate'])
        est_thresh = float(xgboost_cfg['est_thresh'])

        self.est_thresh = est_thresh

        one_to_left = st.beta(10, 1)
        from_zero_positive = st.expon(0, 50)
        GRID_SEARCH_ = False
        if GRID_SEARCH_:
            param_grid = {
                'n_estimators': [500, 1000, 1500],# [50, 100, 200, 300, 400, 500],
                'learning_rate': [0.1], # [0.01, 0.1, 0.05, 0.5, 1],
                # 'max_depth' : [5, 10, 20, 40, 60],
                # 'colsample_bytree' : one_to_left,
                # 'subsample' : one_to_left,
                # 'gamma' : st.uniform(0, 10),
                # 'reg_alpha' : from_zero_positive,
                # 'min_child_weight' : from_zero_positive
            }

            mdl = XGBClassifier(n_estimators=n_estimators, learning_rate=learning_rate, n_jobs=-1)
            grid = GridSearchCV(estimator=mdl, param_grid=param_grid, cv=5)
            grid = grid.fit(self.X[0], self.y[0])
            print('Best score is {:4.4f}' .format(sum(grid.best_estimator_.predict(self.X[1]) == self.y[1]) / (len(self.y[1])*1.0)))
            print('Best params is {}' .format(str(grid.best_params_)))
            print('Parameter tuning complete')

        else:
            mdl = XGBClassifier(n_estimators=n_estimators, learning_rate=learning_rate)

        mdl.fit(np.array(self.X[0]), np.array(self.y[0]))
        self.model = mdl

        pred_y = []
        est_err = []
        est_acc = []
        mape_arr = []
        rmse_arr = []
        r2_score_arr = []
        if len(self.X[1]) > 1:
            for i in range(2):
                pred_y.append(mdl.predict(self.X[i]))
                est_err.append(abs(pred_y[i] - np.array(self.y[i])))
                est_acc.append(self.check_est_accuracy(est_err[-1], est_thresh))
                mape_arr.append(learning_lib.calc_MAPE(self.y[i], pred_y[-1]))
                rmse_arr.append(learning_lib.calc_RMSE(self.y[i], pred_y[-1]))
                r2_score_arr.append(r2_score(self.y[i], pred_y[i]))

            self.y_predict = pred_y
            self.train_acc, self.test_acc = est_acc
            self.train_rmse, self.test_rmse = rmse_arr
            self.train_r2_score, self.test_r2_score = r2_score_arr

            msg += "\n # train acc : {:4.2f}, test acc : {:4.2f}".format(self.train_acc, self.test_acc)
            msg += "\n # train rmse : {:4.2f}, test rmse : {:4.2f}".format(self.train_rmse, self.test_rmse)
            msg += "\n # train r2 score : {:4.2f}, test r2 score : {:4.2f}".format(self.train_r2_score, self.test_r2_score)

            self.log = msg
            self.params = xgboost_cfg

            # if True:
            #     self.gen_plot()


    def random_forest_regressor_for_multiple_y(self, ml_cfg):
        """ Run random forest regressor with various environments.
        :param dataset_ratio:
        :param n_estimators:
        :param est_thresh:
        :return:
        """
        rfr_cfg = ml_cfg['RFR']
        msg = "\n # Random Forest Regression"
        n_estimators = int(rfr_cfg['n_estimators'])
        min_samples_leaf = int(rfr_cfg['min_samples_leaf'])

        if rfr_cfg['max_depth'] == "none":
            max_depth = None
        else:
            max_depth = int(rfr_cfg['max_depth'])

        if rfr_cfg['max_leaf_nodes'] == "none":
            max_leaf_nodes = None
        else:
            max_leaf_nodes = int(rfr_cfg['max_leaf_nodes'])

        max_features = rfr_cfg['max_features']
        if max_features != "auto" and max_features != "sqrt" and max_features != "log2":
            max_features = int(max_features)

        est_thresh = rfr_cfg['est_thresh'].split(',')
        self.est_thresh = [float(thresh) for thresh in est_thresh]

        mdl = MultiOutputRegressor(RandomForestRegressor(n_estimators=n_estimators, min_samples_leaf=min_samples_leaf,
                                                         max_depth=max_depth, max_leaf_nodes=max_leaf_nodes,
                                                         max_features=max_features))

        mdl.fit(self.X[0], self.y[0])
        self.RFR_num_estimators = n_estimators
        self.model = mdl

        pred_y = []
        est_err = []
        est_acc = []
        mse_arr = []
        rmse_arr = []
        r2_score_arr = []
        if len(self.X[1]) > 1:
            for i in range(2):  # Train & Test
                pred_y.append(mdl.predict(self.X[i]))
                est_err.append(abs(pred_y[i] - np.array(self.y[i])))
                est_acc.append(self.check_est_accuracy(est_err[i], self.est_thresh))
                mse_arr.append(mean_squared_error(self.y[i], pred_y[i]))
                rmse_arr.append(learning_lib.calc_RMSE(self.y[i], pred_y[i]))
                r2_score_arr.append(r2_score(self.y[i], pred_y[i], multioutput='raw_values'))

            if ml_cfg['general']['print_est_time'].upper() == 'ON':
                # 모델의 예측속도 측정
                start_time = time.time()
                mdl_predict = mdl.predict(self.X[0])
                print(" ## Model's prediction time : {:.3f} sec".format(time.time() - start_time))
                fit_start_time = time.time()
                mdl_fit = mdl.fit(self.X[0], self.y[0])
                print(" ## Model's training time : {:.3f} sec".format(time.time() - fit_start_time))

            self.y_predict = pred_y

            self.train_acc, self.test_acc = est_acc
            self.train_mse, self.test_mse = mse_arr
            self.train_rmse, self.test_rmse = rmse_arr
            self.train_r2_score, self.test_r2_score = r2_score_arr


            for j in range(self.y_num):  # For multiple y value
                msg += "\n # type : {}, train acc : {:4.2f}, test acc : {:4.2f}".format(self.y_name[j], self.train_acc[j], self.test_acc[j])
                msg += "\n # type : {}, train rmse : {:4.2f}, test rmse : {:4.2f}".format(self.y_name[j], self.train_rmse[j], self.test_rmse[j])
                msg += "\n # type : {}, train r2 score : {:4.2f}, test r2 score : {:4.2f}".format(self.y_name[j], self.train_r2_score[j], self.test_r2_score[j])
            self.log = msg
            # print(msg)
            return self.train_acc, self.test_acc

    def random_forest_classifier(self, ml_cfg):
        """ Run random forest classifier with various environments.

        :param dataset_ratio:
        :param n_estimators:
        :param est_thresh:
        :return:
        """
        rfc_cfg = ml_cfg['RFC']
        msg = "\n # Random Forest Classification"
        n_estimators = int(rfc_cfg['n_estimators'])
        min_samples_leaf = int(rfc_cfg['min_samples_leaf'])

        if rfc_cfg['max_depth'] == "none":
            max_depth = None
        else:
            max_depth = int(rfc_cfg['max_depth'])

        if rfc_cfg['max_leaf_nodes'] == "none":
            max_leaf_nodes = None
        else:
            max_leaf_nodes = int(rfc_cfg['max_leaf_nodes'])

        max_features = rfc_cfg['max_features']
        if max_features != "auto" and max_features != "sqrt" and max_features != "log2" :
            max_features = int(max_features)
        # if rfr_cfg['max_features'] == "num_feat":
        #     max_features = len(self.X[0])
        # elif rfr_cfg['max_features'] == "sqrt":
        #     max_features = math.sqrt(len(self.X[0]))
        # elif rfr_cfg['max_features'] == "log2":
        #     max_features = math.log2(len(self.X[0]))
        # else:
        #     max_features = int(rfr_cfg['max_features'])

        est_thresh = float(rfc_cfg['est_thresh'])
        self.est_thresh = est_thresh

        mdl = RandomForestClassifier(n_estimators=n_estimators, min_samples_leaf=min_samples_leaf,
                                    max_depth=max_depth, max_leaf_nodes=max_leaf_nodes,
                                    max_features=max_features)
        mdl.fit(self.X[0], self.y[0])
        self.model = mdl

        if ml_cfg['general']['print_est_time'].upper() == 'ON':
            # 모델의 예측속도 측정
            start_time = time.time()
            mdl_predict = mdl.predict(self.X[0][0])
            print(" ## Model's prediction time : {:.3f} sec".format(time.time() - start_time))

        pred_y = []
        est_err = []
        est_acc = []
        mape_arr = []
        rmse_arr = []
        r2_score_arr = []
        if len(self.X[1]) > 1:
            for i in range(2):
                pred_y.append(mdl.predict(self.X[i]))
                est_err.append(abs(pred_y[i] - np.array(self.y[i])))
                est_acc.append(self.check_est_accuracy(est_err[i], est_thresh))
                mape_arr.append(learning_lib.calc_MAPE(self.y[i], pred_y[i]))
                rmse_arr.append(learning_lib.calc_RMSE(self.y[i], pred_y[i]))
                r2_score_arr.append(r2_score(self.y[i], pred_y[i]))

            self.y_predict = pred_y
            self.train_acc, self.test_acc = est_acc
            self.train_rmse, self.test_rmse = rmse_arr
            self.train_r2_score, self.test_r2_score = r2_score_arr

            msg += "\n # train acc : {:4.2f}, test acc : {:4.2f}".format(self.train_acc, self.test_acc)
            msg += "\n # train rmse : {:4.2f}, test rmse : {:4.2f}".format(self.train_rmse, self.test_rmse)
            msg += "\n # train r2 score : {:4.2f}, test r2 score : {:4.2f}".format(self.train_r2_score, self.test_r2_score)

            self.log = msg
            self.params = ml_cfg

            # if True:
            #     self.gen_plot()

            return

    def bagging_regressor_for_multiple_y(self, ml_cfg):

        cat = ml_cfg['general']['methond']
        bagging_cfg = ml_cfg['BAGGING']

        """ Run random forest regressor with various environments.
        :param dataset_ratio:
        :param n_estimators:
        :param est_thresh:
        :return:
        """
        msg = "\n # Bagging Regression"
        n_estimators = int(bagging_cfg['n_estimators'])
        max_samples = int(bagging_cfg['max_samples'])
        max_features = int(bagging_cfg['max_features'])


        if cat == 'RH_PLUS':
            first_est_thresh = float(bagging_cfg['time_est_thresh'])
            second_est_thresh = float(bagging_cfg['temp_est_thresh'])
        elif cat == 'CVT_MACRO_M1':
            first_est_thresh = float(bagging_cfg['carbon_est_thresh'])
            second_est_thresh = float(bagging_cfg['oxygen_est_thresh'])

        mdl = MultiOutputRegressor(BaggingRegressor(base_estimator=DecisionTreeRegressor(),
                                                    n_estimators = n_estimators, max_samples = max_samples,
                                                    bootstrap=False, n_jobs=-1))
                                                    # max_features = max_features))

        # mdl = MultiOutputRegressor(GradientBoostingRegressor(random_state=21, n_estimators=400))

        mdl.fit(self.X[0], self.y[0])
        self.BAGGING_num_estimators = n_estimators
        self.model = mdl
        pred_y = []

        first_est_err = []
        first_est_acc = []
        first_mse_arr = []
        first_rmsre_arr = []

        second_est_err = []
        second_est_acc = []
        second_mse_arr = []
        second_rmsre_arr = []
        if len(self.X[1]) > 1:
            for i in range(2):  # Train & Test
                pred_y.append(mdl.predict(self.X[i]))
                # pred_y.append(mdl.predict(self.X[i]) + np.random.normal(0,sigma[i],len(self.X[i])))
                first_est_err.append(abs(np.array(pred_y[i])[:, 0] - np.array(self.y[i])[:, 0]))
                first_est_acc.append(self.check_est_accuracy(first_est_err[-1], first_est_thresh))
                first_mse_arr.append(mean_squared_error(np.array(self.y[i])[:, 0], np.array(pred_y[-1])[:, 0]))
                first_rmsre_arr.append(learning_lib.calc_RMSRE(np.array(self.y[i])[:, 0], np.array(pred_y[-1])[:, 0])) ## check point

                second_est_err.append(abs(np.array(pred_y[i])[:, 1] - np.array(self.y[i])[:, 1]))
                second_est_acc.append(self.check_est_accuracy(second_est_err[-1], second_est_thresh))
                second_mse_arr.append(mean_squared_error(np.array(self.y[i])[:, 1], np.array(pred_y[-1])[:, 1]))
                second_rmsre_arr.append(learning_lib.calc_RMSRE(np.array(self.y[i])[:, 1], np.array(pred_y[-1])[:, 1])) ## check point

            self.train_acc, self.test_acc = first_est_acc
            self.train_acc_2, self.test_acc_2 = second_est_acc

            self.train_mse, self.test_mse = first_mse_arr
            self.train_mse_2, self.test_mse_2 = second_mse_arr

            self.train_rmsre, self.test_rmsre = first_rmsre_arr
            self.train_rmsre_2, self.test_rmsre_2 = second_rmsre_arr

            # Calculate mean, std value
            first_real_y = np.concatenate((np.array(self.y[0])[:, 0], np.array(self.y[1])[:, 0]), axis=0)
            first_y_mean = np.mean(first_real_y)
            first_y_std = np.std(first_real_y)

            second_real_y = np.concatenate((np.array(self.y[0])[:, 1], np.array(self.y[1])[:, 1]), axis=0)
            second_y_mean = np.mean(second_real_y)
            second_y_std = np.std(second_real_y)

            msg += "\n # type : carbon, train acc : {:4.2f}, test acc : {:4.2f}".format(self.train_acc, self.test_acc)
            # msg += "\n # type : carbon, train mse : {:4.2f}, test mse : {:4.2f}".format(self.train_mse, self.test_mse)
            msg += "\n # type : carbon, train rmsre : {:4.2f}, test rmsre : {:4.2f}".format(self.train_rmsre, self.test_rmsre)
            msg += "\n # type : carbon, mean value : {:4.2f}, std value : {:4.2f}".format(first_y_mean, first_y_std)

            msg += "\n # type : oxygen, train acc : {:4.2f}, test acc : {:4.2f}".format(self.train_acc_2, self.test_acc_2)
            # msg += "\n # type : oxygen, train mse : {:4.2f}, test mse : {:4.2f}".format(self.train_mse_2, self.test_mse_2)
            msg += "\n # type : oxygen, train rmsre : {:4.2f}, test rmsre : {:4.2f}".format(self.train_rmsre_2, self.test_rmsre_2)
            msg += "\n # type : oxygen, mean value : {:4.2f}, std value : {:4.2f}".format(second_y_mean, second_y_std)
            self.log = msg
            print(msg)
            return self.train_acc, self.test_acc, self.train_acc_2, self.test_acc_2

    def support_vector_regressor(self, est_margin=sys.float_info.epsilon, plot_filename_prefix='', plot_=True, pkl_filename=''):
        # reg = SVR(C=SVR_C, epsilon=SVR_epsilon)
        reg = LinearSVR()
        reg.fit(self.train_X, self.train_y)
        train_y_predict = reg.predict(self.train_X)
        test_y_predict = reg.predict(self.test_X)
        err_train = abs(train_y_predict - self.train_y)
        err_test  = abs(test_y_predict  - self.test_y)
        train_result = err_train <= est_margin
        test_result  = err_test  <= est_margin
        train_accuracy = 100. * sum(train_result) / float(len(train_result))
        test_accuracy  = 100. * sum(test_result)  / float(len(test_result))

        num_train = len(self.train_X)
        num_test  = len(self.test_X)
        num_ratio = int(100. * num_train / float(num_train + num_test))
        msg  = "\n"
        msg += "\n # Machine learning by support vector regressor"
        msg += "\n   > Train vs test = {:d} vs {:d} ({:d}%)".format(num_train, num_test, num_ratio)
        msg += "\n   > Train dataset statistics: mean = {:5.2f}, var = {:7.2f}".format(err_train.mean(), err_train.var())
        msg += "\n   > Test  dataset statistics: mean = {:5.2f}, var = {:7.2f}".format(err_test.mean(),  err_test.var())
        msg += "\n   > Estimator accuracy for train dataset = {:6.2f}".format(train_accuracy)
        msg += "\n   > Estimator accuracy for test  dataset = {:6.2f}".format(test_accuracy)
        self.log = msg
        print(msg)
        print(" real  = ({:f} {:f}) -> ({:f} {:f})".format(min(self.train_y), max(self.train_y),
                                                           min(train_y_predict), max(train_y_predict)))
        print(" real  = ({:f} {:f}) -> ({:f} {:f})".format(min(self.test_y), max(self.test_y),
                                                           min(test_y_predict), max(test_y_predict)))

        max_range = max(max(err_train), max(err_test), est_margin) + 1
        fig = plt.figure()
        plt.subplot(1,2,1)
        plt.plot(err_train, 'b', [est_margin,] * len(err_train), 'r')
        plt.ylim(0, max_range)
        plt.title('Training ABS error({:2d}%)'.format(int(train_accuracy)))
        plt.subplot(1,2,2)
        plt.plot(err_test, 'b', [est_margin,] * len(err_test), 'r')
        plt.ylim(0, max_range)
        plt.title('Test ABS error({:2d}%)'.format(int(test_accuracy)))
        if plot_filename_prefix:
            fig.savefig(plot_filename_prefix)
        if plot_:
            plt.show()
        else:
            plt.close(fig)

        if pkl_filename:
            pickle.dump([reg, self.name_X, self.name_y, msg], open(pkl_filename, 'wb'))
        pass

    def deep_neuralnet_regressor(self, ml_cfg, dump_dir=None):

        # with tf.device('/cpu:0'):
        dnn_cfg = ml_cfg['DNNR']
        train_X_n = np.array(self.X[0])
        test_X_n = np.array(self.X[1])
        train_y_n = np.array(self.y[0])
        test_y_n = np.array(self.y[1])

        train_y_n.shape = (train_y_n.shape[0], 1)
        test_y_n.shape = (test_y_n.shape[0], 1)

        def get_train_data():
            X_train_tf = tf.constant(train_X_n)
            Y_train_tf = tf.constant(train_y_n)
            return X_train_tf, Y_train_tf

        nsteps = int(dnn_cfg['nsteps'])

        dnn_num_feat = train_X_n.shape[1]
        dnn_feat_col = [tf.contrib.layers.real_valued_column("", dimension=dnn_num_feat)]

        if dnn_cfg['active_fn'] == "relu":
            dnn_active_fn = tf.nn.relu
        elif dnn_cfg['active_fn'] == "sigmoid":
            dnn_active_fn = tf.sigmoid
        else:
            dnn_active_fn = tf.tanh
        dnn_unit = dnn_cfg['unit'].split(',')
        dnn_unit = [int(u) for u in dnn_unit]
        dnn_learn_rate = float(dnn_cfg['learn_rate'])
        if dnn_cfg['optimizer'] == "adadelta":
            dnn_optimizer = tf.train.AdadeltaOptimizer(learning_rate=dnn_learn_rate)
        elif dnn_cfg['optimizer'] == "adam":
            dnn_optimizer = tf.train.AdamOptimizer(learning_rate=dnn_learn_rate)
        elif dnn_cfg['optimizer'] == "ftrl":
            dnn_optimizer = tf.train.FtrlOptimizer(learning_rate=dnn_learn_rate)
        elif dnn_cfg['optimizer'] == "gd":
            dnn_optimizer = tf.train.GradientDescentOptimizer(learning_rate=dnn_learn_rate)
        elif dnn_cfg['optimizer'] == "proximalGrad":
            dnn_optimizer = tf.train.ProximalGradientDescentOptimizer(learning_rate=dnn_learn_rate)
        elif dnn_cfg['optimizer'] == "proximalAdagrad":
            dnn_optimizer = tf.train.ProximalAdagradOptimizer(learning_rate=dnn_learn_rate)
        elif dnn_cfg['optimizer'] == "RMSProp":
            dnn_optimizer = tf.train.RMSPropOptimizer(learning_rate=dnn_learn_rate)
        else:
            dnn_optimizer = tf.train.AdagradOptimizer(learning_rate=dnn_learn_rate)

        dnn_dropout = None
        if dnn_cfg['dropout'] != 'None':
            dnn_dropout = float(dnn_cfg['dropout'])

        est_thresh = dnn_cfg['est_thresh'].split(',')
        est_thresh = [float(th) for th in est_thresh]
        self.est_thresh.append(est_thresh)
        msg = "\n"
        msg += "------------------------------------\n"

        model_dir = os.path.join(dump_dir, dnn_cfg['model_dir']) if dump_dir else dnn_cfg['model_dir']

        reg = tf.contrib.learn.DNNRegressor(feature_columns=dnn_feat_col, activation_fn=dnn_active_fn,
                                            hidden_units=dnn_unit, # model_dir=model_dir,
                                            optimizer=dnn_optimizer, dropout=dnn_dropout)

        reg.fit(input_fn=get_train_data, steps=nsteps)
        self.model = reg

        if ml_cfg['general']['print_est_time'].upper == 'ON':
            # with tf.device("/cpu:0"):
            print(device_lib.list_local_devices())
            # 모델의 예측속도 측정
            start_time = time.time()
            mdl_predict = reg.predict(train_X_n[:1])
            print(" ## Model's prediction time : {:.3f} sec".format(time.time() - start_time))

        if len(self.X[1]) >1:
            train_y_predict_n = reg.predict(train_X_n)
            train_y_predict_n = np.array(list(train_y_predict_n))
            test_y_predict_n = reg.predict(test_X_n)
            test_y_predict_n = np.array(list(test_y_predict_n))

            y_name = self.y_name[0]
            train_y_predict = self.denormalize_output(train_y_predict_n, self.Feat.vars[y_name].max_thresh,
                                                      self.Feat.vars[y_name].min_thresh)
            test_y_predict = self.denormalize_output(test_y_predict_n, self.Feat.vars[y_name].max_thresh,
                                                     self.Feat.vars[y_name].min_thresh)

            real_y_0, real_y_1 = self.denormalize_y(y_name=y_name)

            err_train = abs(train_y_predict - real_y_0)
            err_test = abs(test_y_predict - real_y_1)
            self.train_mse = mean_squared_error(real_y_0, train_y_predict)
            self.test_mse = mean_squared_error(real_y_1, test_y_predict)
            self.train_rmse.append(learning_lib.calc_RMSE(real_y_0, train_y_predict))
            self.test_rmse.append(learning_lib.calc_RMSE(real_y_1, test_y_predict))

            train_result = err_train <= float(est_thresh[0])
            test_result = err_test <= float(est_thresh[0])

            train_accuracy = 100. * sum(train_result) / float(len(train_result))
            test_accuracy = 100. * sum(test_result) / float(len(test_result))

            self.train_acc.append(train_accuracy)
            self.test_acc.append(test_accuracy)

            self.log += "\n # train acc : {:4.2f}, test acc : {:4.2f}, tolerance : {}".format(self.train_acc[0], self.test_acc[0], self.est_thresh)
            self.log += "\n # train rmse : {:4.8f}, test rmse : {:4.8f}".format(self.train_rmse[0], self.test_rmse[0])

            trans_dataset = data_lib.transpose_list(self.dataset[1:])
            self.X_dataset = data_lib.transpose_list(trans_dataset[1:])
            self.y_dataset = trans_dataset[0]

            self.params = ml_cfg
            return self.train_acc, self.test_acc


    def deep_neuralnet_regressor_for_multiple_y(self, ml_cfg):
        dnn_cfg = ml_cfg['DNNR']
        output_num = int(ml_cfg['general']['output_num'])
        train_X_n = np.array(self.X[0])
        test_X_n = np.array(self.X[1])
        train_y_n = np.array(self.y[0])
        test_y_n = np.array(self.y[1])

        train_y_n.shape = (train_y_n.shape[0], output_num)
        test_y_n.shape = (test_y_n.shape[0], output_num)

        def get_train_data():
            X_train_tf = tf.constant(train_X_n)
            Y_train_tf = tf.constant(train_y_n)
            return X_train_tf, Y_train_tf

        nsteps = int(dnn_cfg['nsteps'])

        dnn_num_feat = train_X_n.shape[1]
        dnn_feat_col = [tf.contrib.layers.real_valued_column("", dimension=dnn_num_feat)]

        if dnn_cfg['active_fn'] == "relu":
            dnn_active_fn = tf.nn.relu
        elif dnn_cfg['active_fn'] == "sigmoid":
            dnn_active_fn = tf.sigmoid
        else:
            dnn_active_fn = tf.tanh
        dnn_unit = dnn_cfg['unit'].split(',')
        dnn_unit = [int(u) for u in dnn_unit]
        dnn_learn_rate = float(dnn_cfg['learn_rate'])
        if dnn_cfg['optimizer'] == "adadelta":
            dnn_optimizer = tf.train.AdadeltaOptimizer(learning_rate=dnn_learn_rate)
        elif dnn_cfg['optimizer'] == "adam":
            dnn_optimizer = tf.train.AdamOptimizer(learning_rate=dnn_learn_rate)
        elif dnn_cfg['optimizer'] == "ftrl":
            dnn_optimizer = tf.train.FtrlOptimizer(learning_rate=dnn_learn_rate)
        elif dnn_cfg['optimizer'] == "gd":
            dnn_optimizer = tf.train.GradientDescentOptimizer(learning_rate=dnn_learn_rate)
        elif dnn_cfg['optimizer'] == "proximalGrad":
            dnn_optimizer = tf.train.ProximalGradientDescentOptimizer(learning_rate=dnn_learn_rate)
        elif dnn_cfg['optimizer'] == "proximalAdagrad":
            dnn_optimizer = tf.train.ProximalAdagradOptimizer(learning_rate=dnn_learn_rate)
        elif dnn_cfg['optimizer'] == "RMSProp":
            dnn_optimizer = tf.train.RMSPropOptimizer(learning_rate=dnn_learn_rate)
        else:
            dnn_optimizer = tf.train.AdagradOptimizer(learning_rate=dnn_learn_rate)

        dnn_dropout = None
        if dnn_cfg['dropout'] != 'None':
            dnn_dropout = float(dnn_cfg['dropout'])

        msg = "\n"
        msg += "------------------------------------\n"
        # config_proto = tf.ConfigProto(allow_soft_placement=True, log_device_placement=True)
        # config_proto.gpu_options.allow_growth = True

        reg = tf.contrib.learn.DNNRegressor(feature_columns=dnn_feat_col, activation_fn=dnn_active_fn,
                                            hidden_units=dnn_unit, # model_dir=dnn_cfg['model_dir'],
                                            optimizer=dnn_optimizer, dropout=dnn_dropout,
                                            label_dimension=2)
        # config=tf.contrib.learn.RunConfig(session_config=config_proto))

        reg.fit(input_fn=get_train_data, steps=nsteps)
        train_y_predict_n = reg.predict(train_X_n)
        train_y_predict_n = np.array(list(train_y_predict_n))
        test_y_predict_n = reg.predict(test_X_n)
        test_y_predict_n = np.array(list(test_y_predict_n))
        train_y_vals = {}
        test_y_vals = {}
        for idx in range(output_num):
            train_y_vals[self.y_name[idx]] = self.denormalize_output(train_y_predict_n[:, idx],
                                                                     self.Feat.vars[self.y_name[idx]].max_thresh,
                                                                     self.Feat.vars[self.y_name[idx]].min_thresh)
            test_y_vals[self.y_name[idx]] = self.denormalize_output(test_y_predict_n[:, idx],
                                                                    self.Feat.vars[self.y_name[idx]].max_thresh,
                                                                    self.Feat.vars[self.y_name[idx]].min_thresh)
        #real_y_list = self.denormalize_multiple_y()
        real_y_0_time, real_y_1_time, real_y_0_temp, real_y_1_temp = self.denormalize_multiple_y()

        err_train_time = abs(train_y_vals[self.y_name[0]] - real_y_0_time)
        err_train_temp = abs(train_y_vals[self.y_name[1]] - real_y_0_temp)
        err_test_time = abs(test_y_vals[self.y_name[0]] - real_y_1_time)
        err_test_temp = abs(test_y_vals[self.y_name[1]] - real_y_1_temp)

        rmse_train_time = learning_lib.calc_RMSE(real_y_0_time, train_y_vals[self.y_name[0]])
        rmse_train_temp = learning_lib.calc_RMSE(real_y_0_temp, train_y_vals[self.y_name[1]])
        rmse_test_time = learning_lib.calc_RMSE(real_y_1_time, test_y_vals[self.y_name[0]])
        rmse_test_temp = learning_lib.calc_RMSE(real_y_1_temp, test_y_vals[self.y_name[1]])

        for i in range(1, self.y_num + 1):
            if i == 1:
                est_thresh = ml_cfg['DNNR']['est_thresh'].split(',')
            elif i >= 2:
                est_thresh = ml_cfg['DNNR']['est_thresh_' + str(i)].split(',')
            est_thresh = [float(th) for th in est_thresh]
            self.est_thresh.append(est_thresh)

        train_result_time = err_train_time <= float(dnn_cfg['est_thresh'])
        train_result_temp = err_train_temp <= float(dnn_cfg['est_thresh_2'])
        test_result_time = err_test_time <= float(dnn_cfg['est_thresh'])
        test_result_temp = err_test_temp <= float(dnn_cfg['est_thresh_2'])

        train_accuracy_time = 100. * sum(train_result_time) / float(len(train_result_time))
        train_accuracy_temp = 100. * sum(train_result_temp) / float(len(train_result_temp))
        test_accuracy_time = 100. * sum(test_result_time) / float(len(test_result_time))
        test_accuracy_temp = 100. * sum(test_result_temp) / float(len(test_result_temp))

        msg += "\n y_name : TERMINAL_C, train acc : {:4.2f}, test acc : {:4.2f}".format(train_accuracy_time, test_accuracy_time)
        msg += "\n # train rmse : {:4.8f}, test rmse : {:4.8f}".format(rmse_train_time, rmse_test_time)
        msg += "\n y_name : TERMINAL_O, train acc : {:4.2f}, test acc : {:4.2f}".format(train_accuracy_temp, test_accuracy_temp)
        msg += "\n # train rmse : {:4.8f}, test rmse : {:4.8f}".format(rmse_train_temp, rmse_test_temp)
        print(msg)
        self.log = msg

        self.train_acc.append(float(train_accuracy_time))
        self.train_acc.append(float(train_accuracy_temp))
        self.test_acc.append(float(test_accuracy_time))
        self.test_acc.append(float(test_accuracy_temp))

        self.train_rmse.append([rmse_train_time, rmse_train_temp])
        self.test_rmse.append([rmse_test_time, rmse_test_temp])


        trans_dataset = data_lib.transpose_list(self.dataset[1:])
        self.X_dataset = data_lib.transpose_list(trans_dataset[1:])
        self.y_dataset = trans_dataset[0]

        self.params = ml_cfg

    def deep_neuralnet_classifier(self, ml_cfg):
        dnnc_cfg = ml_cfg['DNNC']
        train_X_n = np.array(self.X[0])
        test_X_n = np.array(self.X[1])
        train_y_n = np.array(self.y[0], dtype=np.int)
        test_y_n = np.array(self.y[1], dtype=np.int)

        train_y_n.shape = (train_y_n.shape[0], 1)
        test_y_n.shape = (test_y_n.shape[0], 1)

        def get_train_data():
            X_train_tf = tf.constant(train_X_n)
            Y_train_tf = tf.constant(train_y_n)
            return X_train_tf, Y_train_tf

        nsteps = int(dnnc_cfg['nsteps'])

        dnn_num_feat = train_X_n.shape[1]
        dnn_feat_col = [tf.contrib.layers.real_valued_column("", dimension=dnn_num_feat)]

        if dnnc_cfg['active_fn'] == "relu":
            dnn_active_fn = tf.nn.relu
        elif dnnc_cfg['active_fn'] == "sigmoid":
            dnn_active_fn = tf.sigmoid
        else:
            dnn_active_fn = tf.tanh
        dnn_unit = dnnc_cfg['unit'].split(',')
        dnn_unit = [int(u) for u in dnn_unit]
        dnn_learn_rate = float(dnnc_cfg['learn_rate'])
        if dnnc_cfg['optimizer'] == "adadelta":
            dnn_optimizer = tf.train.AdadeltaOptimizer(learning_rate=dnn_learn_rate)
        elif dnnc_cfg['optimizer'] == "adam":
            dnn_optimizer = tf.train.AdamOptimizer(learning_rate=dnn_learn_rate)
        elif dnnc_cfg['optimizer'] == "ftrl":
            dnn_optimizer = tf.train.FtrlOptimizer(learning_rate=dnn_learn_rate)
        elif dnnc_cfg['optimizer'] == "gd":
            dnn_optimizer = tf.train.GradientDescentOptimizer(learning_rate=dnn_learn_rate)
        elif dnnc_cfg['optimizer'] == "proximalGrad":
            dnn_optimizer = tf.train.ProximalGradientDescentOptimizer(learning_rate=dnn_learn_rate)
        elif dnnc_cfg['optimizer'] == "proximalAdagrad":
            dnn_optimizer = tf.train.ProximalAdagradOptimizer(learning_rate=dnn_learn_rate)
        elif dnnc_cfg['optimizer'] == "RMSProp":
            dnn_optimizer = tf.train.RMSPropOptimizer(learning_rate=dnn_learn_rate)
        else:
            dnn_optimizer = tf.train.AdagradOptimizer(learning_rate=dnn_learn_rate)

        dnn_dropout = None
        if dnnc_cfg['dropout'] != 'None':
            dnn_dropout = float(dnnc_cfg['dropout'])

        msg = "\n"
        msg += "------------------------------------\n"

        reg = tf.contrib.learn.DNNClassifier(feature_columns=dnn_feat_col, activation_fn=dnn_active_fn,
                                             hidden_units=dnn_unit, model_dir=dnnc_cfg['model_dir'],
                                             optimizer=dnn_optimizer, dropout=dnn_dropout,
                                             n_classes=int(dnnc_cfg['n_classes']))

        reg.fit(input_fn=get_train_data, steps=nsteps)
        self.model = reg

        if ml_cfg['general']['print_est_time'].upper == 'ON':
            # with tf.device("/cpu:0"):
            print(device_lib.list_local_devices())
            # 모델의 예측속도 측정
            start_time = time.time()
            mdl_predict = reg.predict(train_X_n[:1])
            print(" ## Model's prediction time : {:.3f} sec".format(time.time() - start_time))

        train_y_predict_n = reg.predict(train_X_n)
        train_y_predict_n = np.array(list(train_y_predict_n))
        test_y_predict_n = reg.predict(test_X_n)
        test_y_predict_n = np.array(list(test_y_predict_n))

        train_y_predict = train_y_predict_n
        test_y_predict = test_y_predict_n

        real_y_0, real_y_1 = self.y[0], self.y[1]

        err_train = abs(train_y_predict - real_y_0)
        err_test = abs(test_y_predict - real_y_1)
        self.train_mse = mean_squared_error(real_y_0, train_y_predict)
        self.test_mse = mean_squared_error(real_y_1, test_y_predict)

        train_result = err_train < float(dnnc_cfg['est_margin'])
        test_result = err_test < float(dnnc_cfg['est_margin'])

        train_accuracy = 100. * sum(train_result) / float(len(train_result))
        test_accuracy = 100. * sum(test_result) / float(len(test_result))

        self.train_acc = train_accuracy
        self.test_acc = test_accuracy

        num_train = len(self.X[0])
        num_test = len(self.X[1])
        num_ratio = int(100. * num_train / float(num_train + num_test))

        msg += "{:6.2f}".format(self.train_acc)
        msg += "{:6.2f}".format(self.test_acc)

        # filename = "result_" + dnn_cfg['train_ratio'] + "_" +\
        #            dnn_cfg['nsteps'] + "_" + dnn_cfg['unit'] + "_" +\
        #            dnn_cfg['learn_rate'] + "_" + dnn_cfg['dropout'] + "_" +\
        #            str(dnn_num_feat) + dnn_cfg['filename_postfix']
        #
        # with open(dnn_cfg['result_dir'] + filename, 'a') as f:
        #     f.write("{:6.2f},{:6.2f},{:6.2f},{:6.2f}\n".
        #             format(train_accuracy,
        #                    test_accuracy,
        #                    mean_squared_err_train,
        #                    mean_squared_err_test
        #             ))
        print(msg)
        self.log = msg

        trans_dataset = data_lib.transpose_list(self.dataset[1:])
        self.X_dataset = data_lib.transpose_list(trans_dataset[1:])
        self.y_dataset = trans_dataset[0]

        self.params = ml_cfg

    def adaboost_classifier(self, ml_cfg):
        """ Run adaboost classifier with various environments.

        :param dataset_ratio:
        :param n_estimators:
        :param est_thresh:
        :return:
        """
        ada_cfg = ml_cfg['AdaBoostC']
        msg = "\n # AdaBoost Classification"
        n_estimators = int(ada_cfg['n_estimators'])
        min_samples_leaf = int(ada_cfg['min_samples_leaf'])

        if ada_cfg['max_depth'] == "none":
            max_depth = None
        else:
            max_depth = int(ada_cfg['max_depth'])

        if ada_cfg['max_leaf_nodes'] == "none":
            max_leaf_nodes = None
        else:
            max_leaf_nodes = int(ada_cfg['max_leaf_nodes'])

        max_features = ada_cfg['max_features']
        if max_features != "auto" and max_features != "sqrt" and max_features != "log2" :
            max_features = int(max_features)

        # if rfr_cfg['max_features'] == "num_feat":
        #     max_features = len(self.X[0])
        # elif rfr_cfg['max_features'] == "sqrt":
        #     max_features = math.sqrt(len(self.X[0]))
        # elif rfr_cfg['max_features'] == "log2":
        #     max_features = math.log2(len(self.X[0]))
        # else:
        #     max_features = int(rfr_cfg['max_features'])
        learning_rate = float(ada_cfg['learning_rate'])

        est_thresh = float(ada_cfg['est_thresh'])
        self.est_thresh = est_thresh

        mdl = AdaBoostClassifier(base_estimator=DecisionTreeClassifier(max_depth=max_depth,min_samples_leaf=min_samples_leaf,
                                                                       max_leaf_nodes=max_leaf_nodes, max_features=max_features),
                                 n_estimators=n_estimators, learning_rate=learning_rate)
        mdl.fit(self.X[0], self.y[0])
        self.model = mdl

        if ml_cfg['general']['print_est_time'].upper == 'ON':
            # 모델의 예측속도 측정
            start_time = time.time()
            mdl_predict = mdl.predict(self.X[0][0])
            print(" ## Model's prediction time : {:.3f} sec".format(time.time() - start_time))

        pred_y = []
        est_err = []
        est_acc = []
        mape_arr = []
        rmse_arr = []

        for i in range(2):
            pred_y.append(mdl.predict(self.X[i]))
            est_err.append(abs(pred_y[i] - np.array(self.y[i])))
            est_acc.append(self.check_est_accuracy(est_err[-1], est_thresh))
            mape_arr.append(learning_lib.calc_MAPE(self.y[i], pred_y[-1]))
            rmse_arr.append(learning_lib.calc_RMSE(self.y[i], pred_y[-1]))

        self.y_predict = pred_y
        self.train_acc = self.check_est_accuracy(est_err[0], est_thresh)
        self.test_acc = self.check_est_accuracy(est_err[1], est_thresh)
        self.train_mse = mean_squared_error(self.y[0], pred_y[0].tolist())
        self.test_mse = mean_squared_error(self.y[1], pred_y[1].tolist())

        msg += "\n # train acc : {:4.2f}, test acc : {:4.2f}".format(self.train_acc, self.test_acc)
        msg += "\n # train mse : {:4.2f}, test mse : {:4.2f}".format(self.train_mse, self.test_mse)

        self.log = msg
        self.params = ml_cfg
        return

    def run(self, model_name, ml_cfg, dump_dir=None, num_train=None):
        try:
            num_output = int(ml_cfg['general']['output_num'])
        except KeyError:
            num_output = 1

        if num_output == 1:
            self.split_dataset_into_train_and_test(dataset_ratio=int(ml_cfg[model_name]['train_ratio']))  # mode='seq', num_train=num_train) # for model test
        else:
            self.split_dataset_into_train_and_test_for_multiple_y(dataset_ratio=int(ml_cfg[model_name]['train_ratio']),
                                                                  num_y=num_output)
        self.select_model_and_run(model_name, ml_cfg, dump_dir)

        if ml_cfg['general']['generate_plot'].upper() == 'ON':
            self.generate_plot(model_name)

        if ml_cfg['general']['feature_importance'].upper() == 'ON':
            if num_output == 1:
                y_name_list = [self.y_name]
            elif num_output >= 2:
                y_name_list = self.y_name
            self.find_feature_importance(y_name_list=y_name_list, method=model_name)
            
        return self.train_acc, self.test_acc

    def denormalize_y(self, min_val=-1, max_val=1, y_name=None):
        max_thresh = self.Feat.vars[y_name].max_thresh
        min_thresh = self.Feat.vars[y_name].min_thresh
        real_y_0 = []
        real_y_1 = []
        for val in self.y[0]:
            real_y_0.append( (val - min_val) / (max_val - min_val) * (max_thresh - min_thresh) + min_thresh)
        for val in self.y[1]:
            real_y_1.append( (val - min_val) / (max_val - min_val) * (max_thresh - min_thresh) + min_thresh)
        return real_y_0, real_y_1


    def denormalize_multiple_y(self, min_val=-1, max_val=1):
        max_thresh_time = self.Feat.vars[self.y_name[0]].max_thresh
        min_thresh_time = self.Feat.vars[self.y_name[0]].min_thresh
        max_thresh_temp = self.Feat.vars[self.y_name[1]].max_thresh
        min_thresh_temp = self.Feat.vars[self.y_name[1]].min_thresh
        real_y_0_time = []
        real_y_1_time = []
        real_y_0_temp = []
        real_y_1_temp = []

        trans_y_0 = data_lib.transpose_list(self.y[0])
        trans_y_1 = data_lib.transpose_list(self.y[1])

        for val in trans_y_0[0]:
            real_y_0_time.append(
                (val - min_val) / (max_val - min_val) * (max_thresh_time - min_thresh_time) + min_thresh_time)
        for val in trans_y_1[0]:
            real_y_1_time.append(
                (val - min_val) / (max_val - min_val) * (max_thresh_time - min_thresh_time) + min_thresh_time)
        for val in trans_y_0[1]:
            real_y_0_temp.append(
                (val - min_val) / (max_val - min_val) * (max_thresh_temp - min_thresh_temp) + min_thresh_temp)
        for val in trans_y_1[1]:
            real_y_1_temp.append(
                (val - min_val) / (max_val - min_val) * (max_thresh_temp - min_thresh_temp) + min_thresh_temp)
        return real_y_0_time, real_y_1_time, real_y_0_temp, real_y_1_temp


def MinMaxScaler(data):
    numerator = data - np.min(data)
    denominator = np.max(data) - np.min(data)
    return numerator / (denominator + 1e-7)


def NormToReal(given_output, predict_output):
    print(max(given_output), min(given_output))
    return predict_output * (max(given_output) - min(given_output)) + min(given_output)



def create_ml_model(feature, dataset, general_cfg, ml_cfg, model_save_path=None, num_train=None):
    Learn = MachineLearner(Feat=feature, dataset=dataset, out_idx=0)

    today = sys_lib.get_datetime(DATE_FORMAT)

    ml_model = general_cfg['method']
    if ml_model in DNN_LIST:
        Learn.normalize_number_features()

    if ml_model in INT_ENCODING_LIST:
        Learn.encode_symbol_dataset_by_int()
    elif ml_model in ONE_HOT_ENCODING_LIST:
        Learn.encode_symbol_dataset_by_one_hot_encoder()

    dump_dir = os.path.join(general_cfg['root_dir'], general_cfg['model_name'], general_cfg['method'], today)
    if not os.path.exists(dump_dir):
        os.makedirs(dump_dir)

    Learn.run(general_cfg['method'], ml_cfg, dump_dir, num_train=num_train)

    dump_data = Learn if ml_model in INT_ENCODING_LIST else {'dataset': dataset, 'params': ml_cfg[ml_model]}

    if not os.path.exists(dump_dir):
        os.makedirs(dump_dir)

    if not model_save_path:
        model_save_path = os.path.join(dump_dir, ml_model + general_cfg['bin_extension'])

    pickle.dump(dump_data, open(model_save_path, 'wb'))

    result_file = os.path.join(dump_dir, general_cfg['result_file'])
    sys_lib.print_and_write(Learn.log, console=True, filename=result_file)

    return Learn


def make_sliding_window_dataset(dataset, traindata_size, testdata_size, step_size):
    train_dataset_list = []
    test_dataset_list = []
    col_names = dataset[0]
    for y in range(1, len(dataset)-traindata_size-testdata_size-1, step_size):
        train_dataset = [col_names] + dataset[y:traindata_size+y]
        test_dataset = [col_names] + dataset[traindata_size+y:traindata_size+y+testdata_size]
        train_dataset_list.append(train_dataset)
        test_dataset_list.append(test_dataset)
    return train_dataset_list, test_dataset_list


def make_sliding_window_dataset_by_days(dataset, traindata_size, testdata_size, step_size,
                                        date_var='TAP_WORK_DATE', fix_start_day=False):
    train_dataset_list = []
    test_dataset_list = []
    col_names = dataset[0]
    dataset = dataset[1:]
    date_idx = col_names.index(date_var)
    initial_idx = 0
    idx = initial_idx
    start_date = date_parse(dataset[0][date_idx])

    train_max_date = start_date + datetime.timedelta(days=traindata_size - 1)
    test_max_date = train_max_date + datetime.timedelta(days=testdata_size)

    sub_train_dataset_list = [col_names]
    sub_test_dataset_list = [col_names]
    found_next_step_idx = False
    next_step_idx = 0
    while idx < len(dataset):
        cur_date = date_parse(dataset[idx][date_idx])
        if not found_next_step_idx and (cur_date - start_date).days >= step_size:
            next_step_idx = idx
            found_next_step_idx = True
        if cur_date <= train_max_date:
            sub_train_dataset_list.append(dataset[idx])
        elif cur_date <= test_max_date:
            sub_test_dataset_list.append(dataset[idx])
        else:
            train_dataset_list.append(sub_train_dataset_list)
            test_dataset_list.append(sub_test_dataset_list)
            sub_train_dataset_list = [col_names]
            sub_test_dataset_list = [col_names]
            train_max_date = date_parse(dataset[idx][date_idx]) + datetime.timedelta(days=step_size - testdata_size - 1)
            test_max_date = train_max_date + datetime.timedelta(days=testdata_size)

            found_next_step_idx = False
            if fix_start_day:
                idx = initial_idx
            else:
                idx = next_step_idx
            start_date = date_parse(dataset[idx][date_idx])

        idx += 1
        if idx >= len(dataset):
            train_dataset_list.append(sub_train_dataset_list)
            test_dataset_list.append(sub_test_dataset_list)
            break
    return train_dataset_list, test_dataset_list


def sliding_window(train_dataset_list, test_dataset_list, args):
    msg = ''
    pred_y_list = []
    real_y_list = []
    diff_list = []
    diff_power_list = []

    var_cfg = configparser.ConfigParser()
    var_cfg.read(args.var_ini_file)

    ml_cfg = configparser.ConfigParser()
    ml_cfg.read(args.ml_ini_file)
    general_cfg = ml_cfg['general']
    method = general_cfg['method']
    ml_model = ml_cfg[method]
    est_thresh = ml_model['est_thresh']

    var_mtx = data_lib.read_csv_file(args.var_csv_file)
    start_pos, end_pos = sys_lib.calc_crop_info_from_ini(var_cfg[Dataset.VAR_INFO_CSV])
    roi_var_mtx = data_lib.crop_mtx(var_mtx, start_pos, end_pos)
    feature = Dataset.PoscoDecarbEstModel(roi_var_mtx)
    feature.init_feat_class(var_cfg, model_number_str=general_cfg['model_number'], offset=start_pos[0])

    for idx, dataset in enumerate(train_dataset_list):
        y_name, train_acc_list, test_acc_list, train_rmse_list, test_rmse_list, Learn \
                = create_ml_model(feature, dataset, general_cfg, ml_cfg)
        for i, test_dataset in enumerate(test_dataset_list[idx][1:]):
            pred_y = Learn.model.predict(np.array([test_dataset[2:]]).astype('float32'))
            pred_y_list.append(pred_y)
            real_y = test_dataset[0]
            real_y_list.append(real_y)

    with open(os.path.join(general_cfg['sliding_window_result'], 'sliding_window_result.csv'), 'w') as new_file:
        new_file.write('Real_y,Pred_y,Diff\n')
        for i in range(len(pred_y_list)):
            new_file.write('{},'.format(real_y_list[i]))
            new_file.write('{},'.format(pred_y_list[i][0]))
            diff = abs(float(real_y_list[i]) - float(pred_y_list[i][0]))
            diff_list.append(diff)
            new_file.write('{}\n'.format(diff))

    cnt = 0
    for val in diff_list:
        if val < float(est_thresh):
            cnt += 1
    for diff in diff_list:
        diff_power = diff**2
        diff_power_list.append(diff_power)

    sliding_window_accuracy = round((cnt / len(diff_list)) * 100, 2)
    sliding_window_RMSE = math.sqrt(np.mean(diff_power_list))
    msg += 'sliding_window_accuracy : {} %\n'.format(sliding_window_accuracy)
    msg += 'sliding_window_RMSE : {}\n'.format(sliding_window_RMSE)
    return msg

def add_est_value(x_data=None, X_name=None, y_data=None, y_name=None, y_est_data=None, output_num=1):
    X_train = x_data[0]
    X_test = x_data[1]
    y_train = y_data[0]
    y_test = y_data[1]
    y_est_train =  y_est_data[0]
    y_est_test = y_est_data[1]

    if output_num == 1:
        y_dataset = np.concatenate((y_train, y_test), axis=0).reshape(-1, 1)
        y_pred_dataset = np.concatenate((y_est_train, y_est_test), axis=0).reshape(-1, 1)
        tot_header = [y_name] + ['EST_' + y_name] + X_name
    else:
        y_dataset = np.concatenate((y_train, y_test), axis=0)
        y_pred_dataset = np.concatenate((y_est_train, y_est_test), axis=0)
        tot_header = y_name + ['EST_' + y_name[0], 'EST_' + y_name[1]] + X_name

    X_dataset = np.concatenate((X_train, X_test), axis=0)
    tot_y_dataset = np.concatenate((y_dataset, y_pred_dataset), axis=1)
    tot_dataset = np.concatenate(([tot_y_dataset, X_dataset]), axis=1)

    res_dataset = [tot_header] + tot_dataset.tolist()
    return res_dataset


def main():
    """ Learning handler main code.
    """

    global Learn
    parser = argparse.ArgumentParser()
    parser.add_argument("--var_csv_file", required=True, help="csv file for variables")
    parser.add_argument("--var_ini_file", required=True, help="ini file for configuration")
    parser.add_argument("--ml_ini_file", default="", help="ini file for machine learner")
    parser.add_argument("--sliding_window_", default=False, action='store_true', help="activating sliding window")

    args = parser.parse_args()

    var_cfg = configparser.ConfigParser()
    var_cfg.read(args.var_ini_file)

    ml_cfg = configparser.ConfigParser()
    ml_cfg.read(args.ml_ini_file)
    general_cfg = ml_cfg['general']
    model_method = general_cfg['method']

    var_mtx = data_lib.read_csv_file(args.var_csv_file)
    start_pos, end_pos = sys_lib.calc_crop_info_from_ini(var_cfg[Dataset.VAR_INFO_CSV])
    roi_var_mtx = data_lib.crop_mtx(var_mtx, start_pos, end_pos)
    feature = Dataset.PoscoDecarbEstModel(roi_var_mtx)
    feature.init_feat_class(var_cfg, model_number_str=general_cfg['model_number'], offset=start_pos[0])
    feature.dataset = data_lib.read_csv_file(general_cfg['dataset_file'])
    feature.refine_dataset_col_by_dataset_order()

    model_num_str = general_cfg['model_number']
    print("Model info : M{}".format(model_num_str))

    # # 데이터 개수 조정
    # feature.dataset = feature.dataset[:4000]
    print("변수개수 : {}, 데이터개수 : {}".format(len(feature.dataset[0]), len(feature.dataset)))
    if args.sliding_window_:
        traindata_size = 7
        testdata_size = 1
        step_size = 1
        train_dataset_list, test_dataset_list = make_sliding_window_dataset_by_days(feature.dataset,
                                                                                    traindata_size=traindata_size,
                                                                                    testdata_size=testdata_size,
                                                                                    step_size=step_size,
                                                                                    fix_start_day=True)
        msg = sliding_window(train_dataset_list, test_dataset_list, args)
        print(msg)

    n_thresh = len(ml_cfg[model_method]['est_thresh'].split(','))
    y_num = int(general_cfg['output_num'])

    train_acc_sum = [[0 for _ in range(y_num)] for _ in range(n_thresh)]
    test_acc_sum = [[0 for _ in range(y_num)] for _ in range(n_thresh)]
    train_rmse_sum = [[0 for _ in range(y_num)] for _ in range(n_thresh)]
    test_rmse_sum = [[0 for _ in range(y_num)] for _ in range(n_thresh)]

    iter_num = int(general_cfg['iter_num'])
    for i in range(iter_num):
        print("Iteration num : {}".format(i+1))
        Learn = create_ml_model(feature, feature.dataset, general_cfg, ml_cfg)
        for k, y_name in enumerate(Learn.y_name):  # For multiple y value
            len_th = len(Learn.est_thresh[k])
            for j in range(len_th):
                train_acc_sum[j][k] += Learn.train_acc[(k*len_th)+j]
                test_acc_sum[j][k] += Learn.test_acc[(k*len_th)+j]
                if y_num == 1:
                    train_rmse_sum[j][k] += Learn.train_rmse[k]
                    test_rmse_sum[j][k] += Learn.test_rmse[k]
                else:
                    train_rmse_sum[j][k] += Learn.train_rmse[-1][k]
                    test_rmse_sum[j][k] += Learn.test_rmse[-1][k]

    for k, y_name in enumerate(Learn.y_name):  # For multiple y value
        for j, acc_th in enumerate(Learn.est_thresh[k]):
            train_acc_avg, test_acc_avg = (train_acc_sum[j][k] / iter_num), (test_acc_sum[j][k] / iter_num)
            train_rmse_avg, test_rmse_avg = (train_rmse_sum[j][k] / iter_num), (test_rmse_sum[j][k] / iter_num)

            msg = "\n\n"
            msg += "\n [AVG] # y_name : {}, iter_num : {}, tolerance : {}".format(y_name, iter_num, acc_th)
            msg += "\n # type : {}, train acc average  : {:4.2f}, test acc average  : {:4.2f}".format(y_name, train_acc_avg, test_acc_avg)
            msg += "\n # type : {}, train rmse average : {:4.8f}, test rmse average : {:4.8f}".format(y_name, train_rmse_avg, test_rmse_avg)
            print(msg)

    # # 전체 데이터셋에 모델 예측값을 추가하여 저장
    # output_num = int(ml_cfg['general']['output_num'])
    # res_dataset = add_est_value(x_data=Learn.X, y_data=Learn.y, y_est_data=Learn.y_predict, output_num=output_num)
    # data_lib.write_list_to_csv(res_dataset, 'result_' + ml_cfg['general']['model_name'] + '.csv')

    pass

SELF_TEST_ = True


if __name__ == "__main__":
    if len(sys.argv) == 1:
        if SELF_TEST_:
            sys.argv.extend([
                             "--var_csv_file", "../sys_cvt_decarb/cvt_decarb_var.csv",
                             "--var_ini_file", "../sys_cvt_decarb/cvt_decarb_var.ini",
                             "--ml_ini_file", "../sys_cvt_decarb/config_machine_learning/cvt_decarb_m1.ini"
                             # "--ml_ini_file", "../sys_cvt_decarb/config_machine_learning/cvt_decarb_m2-50-C.ini",
                             # "--ml_ini_file", "../sys_cvt_decarb/config_machine_learning/cvt_decarb_m3-85-O.ini",
                             # "--ml_ini_file", "../sys_cvt_decarb/config_machine_learning/cvt_decarb_m3-85-C_O.ini",


                             # "--sliding_window_"
                             ])
        else:
            sys.argv.extend(["--~help"])
    main()
