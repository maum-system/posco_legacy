#! /usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys
from lib import data as data_lib
from lib import learning
import argparse

EVAL = True

def main():

    parser = argparse.ArgumentParser()
    parser.add_argument("--eval_csv_file", required=True, help="csv file for variables")
    parser.add_argument("--model_bin_file", required=True, help="ini file for configuration")

    args = parser.parse_args()
    file_to_dict = data_lib.read_csv_file_as_dict(args.eval_csv_file)

    eval_result_list = []
    for i in range(len(file_to_dict[list(file_to_dict)[0]])):
        eval_dict = {key: val[i] for key, val in file_to_dict.items()}
        eval_result = learning.lgbmr_estimation(args.model_bin_file, eval_dict)
        eval_result_list.append(eval_result)
    print(eval_result_list)

if __name__ == "__main__":

    if len(sys.argv) == 1:
        if EVAL:
            sys.argv.extend([
                             "--eval_csv_file", "../sys_component/dataset_raw/190809_db/190809_RH_EVAL_DB_refined.csv",
                             "--model_bin_file", "../sys_component/model/RH_MACRO_M5/LGBMR/190812"
                             ])
        else:
            sys.argv.extend(["--~help"])
    main()