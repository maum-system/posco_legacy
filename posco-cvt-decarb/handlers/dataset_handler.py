#!/usr/bin/env python
# -*- coding: utf-8 -*-
import argparse
import configparser
import sys
from operator import itemgetter
import math
import pandas as pd
import copy
import os
from refine_model import model_1, model_3_85_O, model_3_85_C_O, model_2_50_C
from lib import data as data_lib
from lib import sys_lib
from handlers import feat_objects
from sys_cvt_decarb.feature_selector import FeatureSelector

NORMAL_MODE = 100
ANALYSIS_MODE = 2

CAT = 'cvt_decarb'
MODEL_NUMBER = 'model_number'
VAR_INFO_CSV = 'var_info_csv'
ENUM_INDEX = 'no.'
DEFINITION = 'definition'
VAR_IF_NAME = 'var_if_name'
DATA_TYPE = 'data_type'
RANGE_MIN = 'range_min'
RANGE_MAX = 'range_max'
STR_DOMAIN = 'str_domain'
UNIT = 'unit'
RANGE_DEFAULT = 'range_default'
NULL_IS = 'null_is'
DATASET = 'dataset'
PRIORITY = 'priority'
TOLERANCE = 'tolerance'
REMARKS = 'remarks'

REFINEMENT = 'refinement'
NULL_NUM_THRESHOLD = 'null_num_threshold'
PRIORITY_OFF = -1
DIV_CHAR_NUM = 120
DIV_CHAR = '*'
DIV_LINE = ''
for _ in range(DIV_CHAR_NUM):
    DIV_LINE += DIV_CHAR
DIV_PATTERN = "\n\n" + DIV_LINE + "\n"

EXCEPTION_VARS = {'CONV_TAP_AL_ALLOY_INPUT': {'key': 'RH_TREAT_PATTERN', 'val': 'L'},
                  'CONV_TAP_QUICKLIME_ALLOY_INPUT': {'key': 'RH_TREAT_PATTERN', 'val': 'L'}
                  }

_this_folder_ = os.path.dirname(os.path.abspath(__file__))
_this_basename_ = os.path.splitext(os.path.basename(__file__))[0]
class PoscoDecarbEstModel(object):
    """
        CVT Decarbonization Estimation Model
    """

    def __init__(self, mtx):
        self.mtx = mtx
        self.trans_mtx = list(map(list, zip(*self.mtx)))
        self.cell_y = len(self.mtx)
        self.cell_x = len(self.mtx[0])
        self.num_vectors = None
        self.vars = {}
        self.vars_num_in = {}
        self.vars_num_out = {}
        self.vars_str = {}
        self.output_idx = None
        self.dataset = None
        self.datasets = None
        self.req_stream_data = {}
        self.check_process_type = None

        self.recv_enum_list = []
        self.send_enum_list = []
        self.db_est_enum_list = []
        self.db_eval_enum_list = []

        self.log = ""

    def init_feat_class(self, cfg, model_number_str=None, offset=0):
        """ Initialize feature classes.

        :param model_number_str:
        :param cfg:
        :param offset:
        :return:
        """
        self.recv_enum_list = data_lib.get_range_list(cfg['stream']['req_enum_list'])
        self.send_enum_list = data_lib.get_range_list(cfg['stream']['rep_enum_list'])
        self.db_est_enum_list = data_lib.get_range_list(cfg['DB']['est_enum_list'])
        self.db_eval_enum_list = data_lib.get_range_list(cfg['DB']['eval_enum_list'])

        config = cfg[VAR_INFO_CSV]
        model_num_str = model_number_str if model_number_str else cfg['model_info']['model_number']
        enum_pos = data_lib.convert_char_to_num(config[ENUM_INDEX]) - offset
        var_if_name_pos = data_lib.convert_char_to_num(config[VAR_IF_NAME]) - offset
        data_type_pos = data_lib.convert_char_to_num(config[DATA_TYPE]) - offset
        range_min_pos = data_lib.convert_char_to_num(config[RANGE_MIN]) - offset
        range_max_pos = data_lib.convert_char_to_num(config[RANGE_MAX]) - offset
        str_domain_pos = data_lib.convert_char_to_num(config[STR_DOMAIN]) - offset
        range_default_pos = data_lib.convert_char_to_num(config[RANGE_DEFAULT]) - offset
        null_is_pos = data_lib.convert_char_to_num(config[NULL_IS]) - offset
        dataset_pos = data_lib.convert_char_to_num(config[DATASET + '_' + model_num_str]) - offset
        prio_pos = data_lib.convert_char_to_num(config[PRIORITY + '_' + model_num_str]) - offset
        tolerance_pos = data_lib.convert_char_to_num(config[TOLERANCE + '_' + model_num_str]) - offset

        for row_dat in self.mtx[1:]:
            # if not row_dat[dataset_pos]:
            #     continue
            dataset_order = int(row_dat[dataset_pos]) if row_dat[dataset_pos].strip() else -1
            enum_idx = int(row_dat[enum_pos])
            var_if_name = row_dat[var_if_name_pos].strip()

            var_type = row_dat[data_type_pos].strip()
            range_default = row_dat[range_default_pos]
            null_is = row_dat[null_is_pos]
            priority = int(row_dat[prio_pos]) if row_dat[prio_pos] else -1
            tolerance = [float(x.strip()) for x in row_dat[tolerance_pos].split(',')] if row_dat[tolerance_pos] else ''

            # print(' # Initializing {} ({}) ...'.format(var_if_name, var_name))
            # if dataset_order == 1:
            #     continue

            if var_type == 'Int' or var_type == 'Float':
                min_val = float(row_dat[range_min_pos])
                max_val = float(row_dat[range_max_pos])
                min_val = int(min_val) if var_type == 'Int' else min_val
                max_val = int(max_val) if var_type == 'Int' else max_val
                self.vars[var_if_name] = feat_objects.NumberClass(idx=enum_idx,
                                                                  if_name=var_if_name,
                                                                  var_type=var_type,
                                                                  min_val=min_val,
                                                                  max_val=max_val,
                                                                  default_val=range_default,
                                                                  null_is=null_is,
                                                                  dataset_order=dataset_order,
                                                                  priority=priority,
                                                                  tolerance=tolerance)
            elif var_type == 'String':
                str_domain = data_lib.parse_string_domain(row_dat[str_domain_pos])
                self.vars[var_if_name] = feat_objects.StringClass(idx=enum_idx,
                                                                  if_name=var_if_name,
                                                                  domain=str_domain,
                                                                  default_val=range_default,
                                                                  null_is=null_is,
                                                                  dataset_order=dataset_order,
                                                                  priority=priority)
            else:
                pass
        pass

    def decode_stream_comma(self, stream, dbg_=False, logger=None):
        stream = [var.strip() for var in stream.split(',')]
        for idx in self.recv_enum_list:
            key = self.get_key_from_enum_idx(idx)
            if not key:
                continue
            try:
                val = stream[idx-1]
            except IndexError:
                print('IndexError:', key)
                val = -1
            try:
                var = self.vars[key]
            except KeyError:
                print('KeyError:', key)
                val = -2

            if var.stream_dtype.lower() == 'number':
                if val == '' and var.null_is:
                    val = var.null_is
                try:
                    val = float(val) if self.vars[key].var_type == 'Float' else int(val)
                except ValueError as e:
                    logger.error("decode_stream exception of number : " + str(e))
                    val = 0
                except Exception as e:
                    logger.error("decode_stream exception of number : {} = {}".format(key, str(val)) + str(e))

                self.req_stream_data[key] = self.vars[key].val = val

            else:
                try:
                    self.req_stream_data[key] = self.vars[key].val = str(val)
                except Exception as e:
                    if logger:
                        logger.error("decode_stream exception of string : " + str(e))
                    self.req_stream_data[key] = self.vars[key].val = ''

        self.exception_RH_VESSEL_CD(logger=logger)

        if dbg_:
            print("\n # Parsed stream data")
            for key, value in self.req_stream_data.items():
                print(" > {:>30} : {}".format(key, value))

        return self.req_stream_data

    def decode_stream(self, stream, dbg_=False, logger=None):
        """ Parse request stream.

        :param stream:
        :param dbg_:
        :param logger:
        :return:
        """
        self.req_stream_data = {}
        position = 1
        for idx in self.recv_enum_list:
            key = self.get_key_from_enum_idx(idx)
            if ',' in stream:  # Separates strings by commas.
                # logger.info('Separates strings by commas.')
                val = stream.split(',')[idx - 1].strip()
            else:
                val = stream[position - 1:position + self.vars[key].stream_length - 1].strip()
            position += self.vars[key].stream_length
            # print("{} : {} : {:d}".format(key, val, position))
            if self.vars[key].stream_dtype.lower() == 'number':
                if not val:
                    if self.vars[key].null_is:
                        val = self.vars[key].null_is
                        if logger:
                            logger.warning("decode_stream : {} is NULL".format(key))
                    else:
                        val = 0
                        if logger:
                            logger.error("decode_stream : {} is NULL".format(key))
                try:
                    val = float(val)
                except ValueError as e:
                    logger.error("decode_stream exception of number : " + str(e))
                    val = 0
                except Exception as e:
                    logger.error("decode_stream exception of number : {} = {}".format(key, str(val)) + str(e))
                self.req_stream_data[key] = self.vars[key].val = val
            else:
                try:
                    val = str(int(val))
                except ValueError:
                    pass
                try:
                    self.req_stream_data[key] = self.vars[key].val = str(val)
                except Exception as e:
                    if logger:
                        logger.error("decode_stream exception of string : " + str(e))
                    self.req_stream_data[key] = self.vars[key].val = ''

        try:
            list(self.vars.keys()).index('RH_VESSEL_CD')
            try:
                self.vars['RH_VESSEL_CD'].domain.index(self.req_stream_data['RH_VESSEL_CD'])
            except ValueError:
                val = ''
                if self.req_stream_data['LAST_OP'] == "R1" or self.req_stream_data['LAST_OP'] == "R3":
                    if self.req_stream_data['RH_VESSEL_CD'] == '1':
                        val = 'N'
                    elif self.req_stream_data['RH_VESSEL_CD'] == '2':
                        val = 'S'
                    else:
                        pass
                elif self.req_stream_data['LAST_OP'] == 'R2':
                    if self.req_stream_data['RH_VESSEL_CD'] == '1':
                        val = 'E'
                    elif self.req_stream_data['RH_VESSEL_CD'] == '2':
                        val = 'W'
                    else:
                        pass
                else:
                    pass
                if not val:
                    logger.error("RH_VESSEL_CD error: {} and {}".format(self.req_stream_data['LAST_OP'],
                                                                        self.req_stream_data['RH_VESSEL_CD']))
                self.req_stream_data['RH_VESSEL_CD'] = self.vars['RH_VESSEL_CD'].val = val
            except Exception as e:
                logger.error("decode_stream exception RH domain : {}".format(self.req_stream_data['RH_VESSEL_CD']) +
                             str(e))
        except ValueError:
            pass
        except Exception as e:
            logger.error("decode_stream exception RH check : " + str(e))

        if dbg_:
            print("\n # Parsed stream data")
            for key, value in self.req_stream_data.items():
                print(" > {:>30} : {}".format(key, value))

        return self.req_stream_data

    def encode_stream(self, dbg_=False, dataset_order_list=None, additional_items=None):
        """ Encode reply stream.

        :param dbg_:
        :return:
        """
        stream = []
        for idx in self.send_enum_list:
            key = self.get_key_from_enum_idx(idx)
            # additional_items에 없고, dataset_order가 ['0','1']이 아니면 skip
            if key not in additional_items and self.vars[key].dataset_order not in dataset_order_list:
                continue
            try:
                val = self.vars[key].val
            except KeyError:
                # print(key)
                continue
            if self.vars[key].stream_dtype.lower() == 'char':
                stream.append("{v}".format(v=val))
            else:
                stream.append("{v}".format(v=val))

        stream = ','.join(stream)

        if dbg_:
            print("\n # Encoded stream")
            print(' > "{}"'.format(stream))
            print(' > length : {:d}'.format(len(stream)))

        return stream

    def get_key_from_enum_idx(self, enum_num):
        """ Return key by order.

        :return:
        """

        for key in self.vars:
            try:
                if self.vars[key].enum_idx == enum_num:
                    return key
            except AttributeError:
                print(key)
                continue
        return False


    def check_var_process_type(self, var_dict, cat=None, logger=None):
        """ Check variable process type.

        :param cat:
        :param var_dict:
        :param logger:
        :return:
        """
        global PROCESS_TYPE
        if 'RH' in cat:
            return None
        elif 'CVT' in cat:
            pass

        for key in var_dict:
            if data_lib.is_class_name(self.vars[key], 'NumberClass'):
                try:
                    val = abs(float(var_dict[key]))
                    if key == 'AL_TAP': # key == 'CONV_TAP_AL_INPUT': # for 온도모델
                        if val > 100:
                            PROCESS_TYPE = 'H'
                        else:
                            PROCESS_TYPE = 'L'
                except (KeyError, TypeError, ValueError):
                    # print(key)
                    pass

        msg = "Operation process type is {}".format(PROCESS_TYPE)
        if logger:
            logger.info(msg)
        else:
            print(msg)

        return PROCESS_TYPE

    def check_var_ranges(self, var_dict, fix_=False, logger=None):
        range_error_list = {}
        for key in var_dict:
            if self.vars[key].dataset_order != 100:
                continue
            if data_lib.is_class_name(self.vars[key], 'NumberClass'):
                if self.vars[key].min_thresh is not None and self.vars[key].max_thresh is not None:
                    try:
                        val = float(var_dict[key])
                    except Exception as e:
                        logger.error("check_var_ranges : number : " + str(e))
                        val = 0
                    if val < self.vars[key].min_thresh or val > self.vars[key].max_thresh:
                        if fix_:
                            if key in EXCEPTION_VARS:
                                if self.vars[EXCEPTION_VARS[key]['key']].val == EXCEPTION_VARS[key]['val']:
                                    var_dict[key] = float(self.vars[key].default_val.split(',')[0].strip())
                                else:  # default
                                    var_dict[key] = float(self.vars[key].default_val.split(',')[1].strip())
                            elif self.vars[key].default_val:
                                var_dict[key] = float(self.vars[key].default_val)
                            else:
                                var_dict[key] = 0
                        range_error_list[key] = val
            else:  # String
                if not (self.vars[key].domain == '' or self.vars[key].domain is None):
                    try:
                        self.vars[key].domain.index(var_dict[key])
                    except ValueError:
                        if fix_:
                            if self.vars[key].default_val:
                                var_dict[key] = self.vars[key].default_val
                            else:
                                var_dict[key] = self.vars[key].domain[0]
                        range_error_list[key] = var_dict[key]
                    except Exception as e:
                        logger.error("check_var_ranges : number : " + str(e))
                        var_dict[key] = self.vars[key].domain[0]
        msg = "range error list: {}".format(range_error_list)
        if range_error_list:
            logger.warning(msg) if logger else print(msg)
            return False, range_error_list
        else:
            return True, None

    def check_var_tolerances(self, est_dict, var_dict, logger=None):
        """ Check variable tolerances.

        :param est_dict:
        :param var_dict:
        :param logger:
        :return:
        """
        range_error_list = {}
        for key in var_dict:
            # print(key)
            if self.vars[key].dataset_order < 0:
                continue
            if data_lib.is_class_name(self.vars[key], 'NumberClass'):
                if self.vars[key].tolerance:
                    try:
                        val = abs(float(var_dict[key]) - float(est_dict[key]))
                    except (KeyError, TypeError):
                        val = 0
                        print(key)
                    if val < self.vars[key].tolerance[0] or val > self.vars[key].tolerance[1]:
                        range_error_list[key] = val
        msg = "Tolerance error list: {}".format(range_error_list)
        if range_error_list:
            if logger:
                logger.error(msg)
            else:
                print(msg)
            return False, range_error_list
        else:
            return True, None

    def refine_dataset_row_by_process_type(self, dataset=None, cat=None, logger=None):
        """ Refine dataset row by deleting the row vector
                in which at least one sample doesn't satisfy its processing type requirement.

                :param dataset:
                :param logger:
                :return:
                """

        if cat == 'RH' or 'RH' in cat:
            return dataset
        elif cat == 'CVT' or 'CVT' in cat:
            pass

        if not dataset:
            dataset = self.dataset

        dim = [len(dataset), len(dataset[0])]

        del_list = []
        for row_idx in range(1, len(dataset)):
            row_dat = dataset[row_idx]
            for col_idx in range(len(row_dat)):
                key = dataset[0][col_idx]
                # ptr = self.vars[key]
                if key in ['CONV_TAP_AL_INPUT', 'BAP_ARR_O2', 'SM_STEEL_GRD']:
                    if data_lib.is_class_name(self.vars[key], "NumberClass"):
                        val = float(row_dat[col_idx])
                        if key == 'CONV_TAP_AL_INPUT':
                            if not (val <= 100): # 50 to 100
                                del_list.append(row_idx)
                                break
                        # elif key == 'BAP_ARR_O2':
                        #     if not (val >= 100):
                        #         del_list.append(row_idx)
                        #         break
                    # else:
                    #     if 'U' not in row_dat[col_idx]:
                    #         del_list.append(row_idx)
                    #         break
                else:
                    continue

        del_list = sorted(del_list, reverse=True)
        for idx in del_list:
            del dataset[idx]

        msg = "Processing refine_dataset_row_by_process_type : "
        msg += "{:d} x {:d} -> {:d} x {:d}".format(dim[0], dim[1], len(dataset), len(dataset[0]))
        if logger:
            logger.info(msg)
        self.log = msg

        return dataset

    def refine_dataset_group_by_operation_pattern(self, cat, logger=None): # 2, 3, 4
        if 'RH_MACRO_M2' in cat or 'RH_MACRO_M3' in cat or 'RH_MACRO_M4' in cat:

            key = model_2.EXCEPT_OUTPUT_VAR if cat == 'RH_MACRO_M2' else replace_dict_m2_3_4.obj_name

            self.dataset = data_lib.replace_dataset_by_dict(dataset=self.dataset,
                                                            key=key,
                                                            replacement_dict=replace_dict_m2_3_4.replacement_dict,
                                                            logger=logger)
        return self.dataset

    def refine_dataset_col_by_dataset_order(self, dataset=None, filename=None, logger=None, mode=NORMAL_MODE):
        """ Refine dataset column by priority,
            generating the dataset having the features used in training and testing.

            :param dataset:
            :param filename:
            :param logger:
            :return:
        """
        if not dataset:
            dataset = self.dataset

        dataset_trans = data_lib.transpose_list(dataset)

        arr = []
        for key, value in self.vars.items():
            if value.dataset_order == '':
                continue
            dataset_order = int(value.dataset_order)
            priority = int(value.priority)

            if dataset_order < 0:
                continue

            if mode == NORMAL_MODE and dataset_order == 1:
                # print("Key is passed : {}".format(key))
                continue

            else:
                try:
                    idx = dataset[0].index(key)
                    if dataset_order == 0:  # if Y, sort it by priority
                        dataset_order = priority
                    arr.append([dataset_order] + [key] + dataset_trans[idx][1:])
                except (ValueError, TypeError):
                    # print("Key does not exist : {}".format(key))
                    continue

        arr_sorted = data_lib.transpose_list(sorted(arr, key=itemgetter(0)))

        if filename:
            data_lib.write_list_to_csv(arr_sorted[1:], filename)

        msg = "Processing refine_dataset_col_by_priority : "
        msg += "{:d} x {:d} -> {:d} x {:d}".format(len(dataset), len(dataset[0]),
                                                   len(arr_sorted[1:]), len(arr_sorted[0]))
        if logger:
            logger.info(msg)
        self.log = msg

        self.dataset = arr_sorted[1:]

    def extract_dataset_col_by_priority(self, dataset=None, filename=None, logger=None, var_cnt=None):
        """ Extract dataset column by priority,
                generating the dataset having the features used in analysis code.

                :param dataset:
                :param filename:
                :param logger:
                :return:
                """
        if not dataset:
            dataset = self.dataset

        dataset_trans = data_lib.transpose_list(dataset)

        arr = []
        for key, value in self.vars.items():
            if value.priority == '':
                continue

            priority = int(value.priority)
            if priority < 0:
                continue
            else:
                try:
                    idx = dataset[0].index(key)
                    arr.append([priority] + [key] + dataset_trans[idx][1:])
                except (ValueError, TypeError):
                    print("Key does not exist : {}".format(key))
                    continue

        arr_sorted = data_lib.transpose_list(sorted(arr, key=itemgetter(0))[:var_cnt])

        if filename:
            data_lib.write_list_to_csv(arr_sorted[1:], filename)

        msg = "Processing refine_dataset_col_by_priority : "
        msg += "{:d} x {:d} -> {:d} x {:d}".format(len(dataset), len(dataset[0]),
                                                   len(arr_sorted[1:]), len(arr_sorted[0]))
        if logger:
            logger.info(msg)
        self.log = msg

        self.dataset = arr_sorted[1:]

    def refine_dataset_col_by_null_replacement(self, dataset=None, filename=None, logger=None):
        """ Refine dataset column by null replacement
                if the column feature has null replacement value.

                :param dataset:
                :param filename:
                :param logger:
                :return:
                """
        if not dataset:
            dataset = self.dataset

        trans_dataset = data_lib.transpose_list(dataset)
        for idx in range(len(dataset[0])):
            key = dataset[0][idx]
            if self.vars[key].null_is:
                trans_dataset[idx] = [self.vars[key].null_is if x == "" else x for x in trans_dataset[idx]]

        msg = "Processing refine_dataset_col_by_null_replacement..."
        if logger:
            logger.info(msg)
        self.log = msg

        arr = data_lib.transpose_list(trans_dataset)
        if filename:
            data_lib.write_list_to_csv(arr, filename)

        return arr

    def refine_dataset_col_by_null(self, var_ini, console=True):
        """ Refine dataset by deleting feature(s) which has null number greater than threshold.

        :param thresh:
        :param console:
        :return:
        """
        thresh = float(var_ini[REFINEMENT][NULL_NUM_THRESHOLD])
        dataset_dim = [len(self.dataset) - 1, len(self.dataset[0])]
        num_thresh = dataset_dim[0] * thresh
        del_list = []
        for key in self.vars:
            if self.vars[key].null_num > num_thresh:
                del_list.append(key)
        for key in del_list:
            try:
                idx = self.dataset[0].index(self.vars[key].if_name)
            except ValueError:
                # print(key)
                continue
            for row in self.dataset:
                del row[idx]

        msg = "\n\n"
        msg += DIV_LINE
        msg += "\n\n # Dataset refinement process by deleting columns(s) " \
               "which has null number greater than threshold, {:.2f}".format(thresh)
        msg += "\n\n    > {:d} columns detected and deleted.".format(len(del_list))
        for key in del_list:
            msg += "\n      - {:5d}, {} ({})".format(self.vars[key].null_num, self.vars[key].if_name, key)
        msg += "\n\n    > dataset dim : {:d} x {:d} -> {:d} x {:d}".format(dataset_dim[0], dataset_dim[1],
                                                                           dataset_dim[0], len(self.dataset[0]))
        sys_lib.print_and_write(msg, console)
        pass

    def refine_dataset_by_row_range(self, console=True):
        """ Refine dataset row
        by deleting the row vector in which at least one sample doesn't satisfy its range requirement.

        :param console:
        :return:
        """
        dataset_dim = [len(self.dataset) - 1, len(self.dataset[0])]
        error_list = [[], [], []]
        del_list = []
        # ti_aim_cnt = 0
        key_list = []
        for if_name in self.dataset[0]:
            key_list.append(self.get_key_from_if_name(if_name))

        for row_idx in range(1, dataset_dim[0] + 1):
            row_dat = self.dataset[row_idx]
            for idx in range(len(row_dat)):
                key = key_list[idx]
                ptr = self.vars[key_list[idx]]
                if 'OUT_' in key:
                    continue
                if row_dat[idx] == "" and self.vars[key].dataset_order > PRIORITY_OFF:
                    # if key == 'TI_AIM':#
                    #     ti_aim_cnt += 1#
                    #     print("! {:d} -> {:d} and {:d}".format(ti_aim_cnt, row_idx, idx))#
                    error_list[0].append(key)
                    del_list.append(row_idx)
                    break
                if ptr.__class__.__name__ == "NumberClass" and self.vars[key].dataset_order > PRIORITY_OFF:
                    val = float(row_dat[idx])
                    if not (ptr.min_thresh <= val <= ptr.max_thresh):
                        error_list[1].append(key)
                        del_list.append(row_idx)
                        break
                else:
                    # if not (self.get_key_from_if_name(ptr.if_name) == 'Date'):
                    if not ptr.if_name == 'Input_Date_1' and self.vars[key].dataset_order > PRIORITY_OFF:
                        try:
                            ptr.domain.index(row_dat[idx])
                        except ValueError:
                            error_list[2].append(key)
                            del_list.append(row_idx)
                            break
                        except AttributeError:
                            print(key)
                            pass

        del_list = sorted(del_list, reverse=True)
        for idx in del_list:
            del self.dataset[idx]

        msg = "\n\n"
        msg += DIV_LINE
        msg += "\n\n # Dataset refinement process by deleting row vector "
        msg += "in which one of them doesn't satisfy its range requirement."

        features = [[], [], []]
        frequencies = [[], [], []]
        for idx in range(len(error_list)):
            errors = error_list[idx]
            if errors:
                errors = sorted(errors)
                for feature in list(set(errors)):
                    features[idx].append(feature)
                    frequencies[idx].append(errors.count(feature))

        try:
            msg += "\n > Null data in sample vector"
            for idx in range(len(features[0])):
                msg += "\n   - {} : {:d}".format(features[0][idx], frequencies[0][idx])
        except ValueError:
            pass
        try:
            msg += "\n > Number type data in sample vector not satisfying range information"
            for idx in range(len(features[1])):
                msg += "\n   - {} : {:d}".format(features[1][idx], frequencies[1][idx])
        except ValueError:
            pass
        try:
            msg += "\n > String type data in sample vector not satisfying range information"
            for idx in range(len(features[2])):
                msg += "\n   - {} : {:d}".format(features[2][idx], frequencies[2][idx])
        except IndexError:
            pass
        msg += "\n\n    > dataset dim  : {:d} x {:d} -> {:d} x {:d}".format(dataset_dim[0], dataset_dim[1],
                                                                            len(self.dataset) - 1, len(self.dataset[0]))
        sys_lib.print_and_write(msg, console)
        pass

    def refine_dataset_row_by_nan_replacement(self, dataset=None, filename=None, logger=None):
        """ Refine dataset row by nan value replacement.

        :param dataset:
        :param filename:
        :param logger:
        :return:
        """
        if not dataset:
            dataset = self.dataset

        dim = [len(dataset), len(dataset[0])]

        # arr = []
        for row_idx in range(1, len(dataset)):
            row_dat = dataset[row_idx]
            for col_idx in range(len(row_dat)):
                val = row_dat[col_idx]
                if val == '#N/A' or val == 'NaN' or (type(val) == float and math.isnan(val)):
                    row_dat[col_idx] = ""
                else:
                    pass

        msg = "Processing refine_dataset_row_by_nan_replacement..."
        if logger:
            logger.info(msg)
        self.log = msg

        arr = dataset
        if filename:
            data_lib.write_list_to_csv(arr, filename)

        return arr

    def refine_dataset_row_by_null(self, dataset=None, filename=None, logger=None):
        """ Refine dataset row by removing the row having at least one null.

                :param dataset:
                :param filename:
                :param logger:
                :return:
                """
        if not dataset:
            dataset = self.dataset

        arr = []
        for row in dataset:
            try:
                row.index("")
            except ValueError:
                arr.append(row)

        if filename:
            data_lib.write_list_to_csv(arr, filename)

        msg = "Processing refine_dataset_row_by_null : "
        msg += "{:d} x {:d} -> {:d} -> {:d}".format(len(dataset), len(dataset[0]), len(arr), len(arr[0]))
        if logger:
            logger.info(msg)
        self.log = msg

        return arr

    def refine_dataset_row_by_range(self, dataset=None, logger=None, dataset_order_list=None, except_var_list=[]):
        """ Refine dataset row by deleting the row vector
                in which at least one sample doesn't satisfy its range requirement.

                :param dataset_order_list:
                :param dataset:
                :param logger:
                :return:
                """
        if not dataset:
            dataset = self.dataset

        dim = [len(dataset), len(dataset[0])]
        error_list = [[], [], []]
        del_list = []
        for row_idx in range(1, len(dataset)):
            row_dat = dataset[row_idx]
            for col_idx in range(len(row_dat)):
                key = dataset[0][col_idx]
                ptr = self.vars[key]
                if dataset_order_list and ptr.dataset_order not in dataset_order_list:
                    continue
                if key == 'SM_DSTL_LAD_SLU_EA_JDG_GD_TP' or key == 'SM_DSTL_LAD_IN_SLU_JDG_GD_TP' or key in except_var_list:
                    break
                if data_lib.is_class_name(ptr, "NumberClass"):
                    try:
                        val = float(row_dat[col_idx])
                        if not (ptr.min_thresh <= val <= ptr.max_thresh):
                            if row_idx not in del_list:
                                del_list.append(row_idx)
                            error_list[1].append(key)
                            # logger.info(key + " range check: " +
                            #             str(ptr.min_thresh) + "~" + str(ptr.max_thresh))
                            # logger.info("Value: " + str(val))
                            # print(key, row_dat[col_idx])
                            # break
                    except TypeError:
                        if row_idx not in del_list:
                            del_list.append(row_idx)
                        # print(key, row_dat[col_idx])
                        # break
                    except ValueError:
                        if row_idx not in del_list:
                            del_list.append(row_idx)
                        error_list[0].append(key)
                        # break
                else:
                    try:
                        # if key == model_2.EXCEPT_OUTPUT_VAR:
                        #     continue
                        ptr.domain.index(row_dat[col_idx].lstrip('0'))
                    except ValueError:
                        if row_idx not in del_list:
                            del_list.append(row_idx)
                        error_list[2].append(key)
                        # print(key, row_dat[col_idx])
                        # break
                    except AttributeError as e:
                        print(e)
                        # print(key, row_dat[col_idx])

        del_list = sorted(del_list, reverse=True)
        print("Deleted " + str(len(del_list)) + " rows.")
        for idx in del_list:
            del dataset[idx]

        msg = "\n\n"
        msg += DIV_LINE
        msg += "\n\n # Dataset refinement process by deleting row vector "
        msg += "in which one of them doesn't satisfy its range requirement."

        features = [[], [], []]
        frequencies = [[], [], []]
        for idx in range(len(error_list)):
            errors = error_list[idx]
            if errors:
                errors = sorted(errors)
                for feature in list(set(errors)):
                    features[idx].append(feature)
                    frequencies[idx].append(errors.count(feature))

        try:
            msg += "\n > Null data in sample vector"
            for idx in range(len(features[0])):
                msg += "\n   - {} : {:d}".format(features[0][idx], frequencies[0][idx])
        except ValueError:
            pass
        try:
            msg += "\n > Number type data in sample vector not satisfying range information"
            for idx in range(len(features[1])):
                msg += "\n   - {} : {:d}".format(features[1][idx], frequencies[1][idx])
        except ValueError:
            pass
        try:
            msg += "\n > String type data in sample vector not satisfying range information"
            for idx in range(len(features[2])):
                msg += "\n   - {} : {:d}".format(features[2][idx], frequencies[2][idx])
        except IndexError:
            pass

        msg += "\n\n Processing refine_dataset_row_by_range : "
        msg += "\n    {:d} x {:d} -> {:d} x {:d}".format(dim[0], dim[1], len(dataset), len(dataset[0]))
        if logger:
            logger.info(msg)
        self.log = msg

        return dataset



    def update_dataset_by_null_is(self, dataset):
        trans_dataset = data_lib.transpose_list(dataset)
        for if_name in dataset[0]:
            try:
                key = self.get_key_from_if_name(if_name)
                if self.vars[key].null_is:
                    idx = dataset[0].index(if_name)
                    trans_dataset[idx] = [self.vars[key].null_is if x == "" else x for x in trans_dataset[idx]]
            except KeyError:  #
                # print(key)
                continue  #
        return list(map(list, zip(*trans_dataset)))

    def get_key_from_if_name(self, if_name):
        """ Get feature name from feature interface name.

                :param if_name:
                :return:
                """
        for key in self.vars:
            if self.vars[key].if_name == if_name:
                return key
        return False

    def get_key_from_definition(self, definition):
        """ Get feature name from feature definition.

        :param if_name:
        :return:
        """
        for idx, key in enumerate(self.trans_mtx[1][1:]):
            if key  == definition:
                key = self.trans_mtx[2][idx+1]
                return key
        return False

    def init_statistical_properties(self, dataset):
        """ Initialize the statistical properties of all features.

               :param dataset:
               :return:
               """
        self.dataset = dataset
        self.num_vectors = len(self.dataset) - 1
        trans_dat_mtx = list(map(list, zip(*self.dataset)))
        for key in self.vars:
            try:  #
                if self.vars[key].if_name:
                    self.vars[key].get_statistical_properties(
                        trans_dat_mtx[dataset[0].index(self.vars[key].if_name)][1:])
            except ValueError:
                # print("Key does not exist : {}".format(self.vars[key].if_name))
                continue
        pass

    @staticmethod
    def analyze_dataset(dataset, console=True):
        """ Analyze sample vectors.

        :param dataset:
        :param console:
        :return:
        """
        num_vars = len(dataset[0])
        msg = "\n\n # Total number of input vectors = {:d}".format(len(dataset) - 1)
        msg += "\n # Total number of variables = {:d}".format(num_vars)

        null_list = [0, ] * num_vars
        for idx, sample in enumerate(dataset[1:]):
            null_cnt = sample.count("")
            try:
                null_list[null_cnt] += 1
            except IndexError:
                null_list[null_cnt-1] += 1
        msg += "\n # Total null count in input dataset = {}".format(sum(null_list))
        # for idx in range(num_vars):
        #     if null_list[idx] != 0:
        #         msg += "\n {:3d} = {:d}".format(idx, null_list[idx])

        sys_lib.print_and_write(msg, console=console)
        pass

    def print_analytics_result(self, console=True):
        """ Print attribute(s) of data in a smart way.

        :return:
        """
        prt = "\n\n # Total data sample number = {:d}".format(self.num_vectors)
        # prt += "\n\n| {:>30} |".format("Variable IF name")
        prt += "\n\n {:>45} |".format("Variable name")
        prt += " {:>6} |".format("Type")
        prt += " {:>6} |".format("Index")
        prt += " {:>8} |".format("Priority")
        prt += " {:>10} |".format("Null num")
        prt += " {:>10} |".format("Outer num")
        prt += " {:>10} |".format("Null+Outer")
        prt += " {:>10} |".format("Mean")
        prt += " {:>10} |".format("Std")
        prt += DIV_PATTERN

        for key, val in self.vars.items():
            if key: # if ptr.if_name:
                if data_lib.is_class_name(self.vars[key],"NumberClass"):
                    var_type = "number"
                    outer_num = str(val.min_below + val.max_above)
                    waste_num = str(val.null_num + val.min_below + val.max_above)
                    mean_val = "{:.3f}".format(val.mean)
                    std_val = "{:.3f}".format(val.std)
                else:
                    var_type = "string"
                    outer_num = ""
                    waste_num = str(val.null_num)
                    mean_val = ""
                    std_val = ""

                # prt += "\n| {:>30} |".format(ptr.name) # if ptr.name:
                prt += "\n{:>45} |".format(key)
                prt += " {:6} |".format(var_type)
                prt += " {:6d} |".format(val.enum_idx)
                prt += " {:8d} |".format(val.priority)
                prt += " {:10d} |".format(val.null_num)
                prt += " {:>10} |".format(outer_num)
                prt += " {:>10} |".format(waste_num)
                prt += " {:>10} |".format(mean_val)
                prt += " {:>10} |".format(std_val)

        sys_lib.print_and_write(prt, console=console)
        pass

    def split_dataset_by_type_and_dataset_order(self, console=True):
        """ Divide dataset by type such as number vs string and input vs output.

        :param console:
        :return:
        """
        dataset_trans = data_lib.transpose_list(self.dataset)
        datasets = {'in_num': [], 'in_str': [], 'out_num': [], 'out_str': []}
        for key, value in self.vars.items():
            if value.dataset_order == '':
                continue
            if value.dataset_order < 0:
                continue
            try:
                idx = self.dataset[0].index(key)
            except ValueError:
                continue
            if value.dataset_order == 0:
                if data_lib.is_class_name(value, "NumberClass"):
                    datasets['out_num'].append(
                        # [self.vars[key].dataset_prio] + [dataset_trans[idx][0]] + [float(x) for x in dataset_trans[idx][1:]])
                        [value.priority] + [key] + [float(x) for x in dataset_trans[idx][1:]])

                else:
                    datasets['out_str'].append(
                        [value.priority] + [key] + dataset_trans[idx][1:])
            else:
                if data_lib.is_class_name(value, "NumberClass"):
                        # [self.vars[key].dataset_prio] + [dataset_trans[idx][0]] + [float(x) for x in dataset_trans[idx][1:]])
                        datasets['in_num'].append(
                            [value.dataset_order] +
                            [key] +
                            [0 if x == '' else float(x) for x in dataset_trans[idx][1:]]
                        )
                else:
                    # datasets['in_str'].append([self.vars[key].dataset_prio] + [k for k in dataset_trans[idx]])
                    datasets['in_str'].append(
                        [value.dataset_order] + [key] + dataset_trans[idx][1:])

        datasets['in_num'] = sorted(datasets['in_num'], key=itemgetter(0))
        datasets['in_str'] = sorted(datasets['in_str'], key=itemgetter(0))

        len_datasets = {'in_num': len(datasets['in_num']), 'in_str': len(datasets['in_str']),
                        'out_num': len(datasets['out_num']), 'out_str': len(datasets['out_str'])}

        datasets['in_num'] = list(map(list, zip(*datasets['in_num'])))[1:]
        datasets['in_str'] = list(map(list, zip(*datasets['in_str'])))[1:]
        datasets['out_num'] = list(map(list, zip(*datasets['out_num'])))[1:]
        datasets['out_str'] = list(map(list, zip(*datasets['out_str'])))[1:]
        self.datasets = datasets

        msg = "\n\n"
        msg += DIV_LINE
        msg += "\n\n # Divide dataset by type, input vs output and number vs string."
        msg += "\n   > Input  number features = {:d}".format(len_datasets['in_num'])
        msg += "\n   > Input  string features = {:d}".format(len_datasets['in_str'])
        msg += "\n   > Output number features = {:d}".format(len_datasets['out_num'])
        msg += "\n   > Output string features = {:d}".format(len_datasets['out_str'])
        sys_lib.print_and_write(msg, console)


    def exception_RH_VESSEL_CD(self, logger):
        try:
            list(self.vars.keys()).index('RH_VESSEL_CD')
            try:
                self.vars['RH_VESSEL_CD'].domain.index(self.req_stream_data['RH_VESSEL_CD'])
            except ValueError:
                val = ''
                if self.req_stream_data['LAST_OP'] == "R1" or self.req_stream_data['LAST_OP'] == "R3":
                    if self.req_stream_data['RH_VESSEL_CD'] == '1':
                        val = 'N'
                    elif self.req_stream_data['RH_VESSEL_CD'] == '2':
                        val = 'S'
                    else:
                        pass
                elif self.req_stream_data['LAST_OP'] == 'R2':
                    if self.req_stream_data['RH_VESSEL_CD'] == '1':
                        val = 'E'
                    elif self.req_stream_data['RH_VESSEL_CD'] == '2':
                        val = 'W'
                    else:
                        pass
                else:
                    pass
                if not val:
                    logger.error("RH_VESSEL_CD error: {} and {}".format(self.req_stream_data['LAST_OP'],
                                                                        self.req_stream_data['RH_VESSEL_CD']))
                self.req_stream_data['RH_VESSEL_CD'] = self.vars['RH_VESSEL_CD'].val = val
            except Exception as e:
                logger.error("decode_stream exception RH domain : {}".format(self.req_stream_data['RH_VESSEL_CD']) +
                             str(e))
        except ValueError:
            pass
        except Exception as e:
            logger.error("decode_stream exception RH check : " + str(e))

    def refine_dataset_model(self, model_num, feature, dataset=None, logger=None, mode=None):
        if model_num == '1':
            model_1.run(feature, dataset=dataset, logger=logger)
        elif model_num == '2-50-C':
            model_2_50_C.run(feature, dataset=dataset, logger=logger)
        elif model_num == '3-85-O':
            model_3_85_O.run(feature, dataset=dataset, logger=logger)
        elif model_num == '3-85-C_O':
            model_3_85_C_O.run(feature, dataset=dataset, logger=logger)



    def rename_dataset_col(self, filename, dataset=''):
        if not dataset:
            dataset = copy.deepcopy(self.dataset)
        for idx in range(len(dataset[0])):
            # dataset[0][idx] = self.get_key_from_if_name(dataset[0][idx])
            if not(self.get_key_from_definition(dataset[0][idx])) == False:
                dataset[0][idx] = self.get_key_from_definition(dataset[0][idx])
            else:
                print(dataset[0][idx]) ##
        with open(filename, 'w') as f:
            for row_data in dataset:
                try:
                    f.write(','.join(row_data) + '\n')
                except TypeError:
                    print(row_data)
        pass


    def add_dataset(self, filename, dataset='', added_dataset=''):
        if not dataset:
            dataset = copy.deepcopy(self.dataset)

        dataset_df = pd.DataFrame(dataset, columns=dataset[0])
        added_df = pd.DataFrame(added_dataset, columns=added_dataset[0])

        # remove duplicated columns in dataset df
        # added_df = added_df.loc[:, ~dataset_df.columns.duplicated()]

        # remove duplicated rows in df
        dataset_df_no_dups = dataset_df.drop_duplicates()
        added_df_no_dups = added_df.drop_duplicates()

        # refined_df = pd.concat([dataset_df_no_dups, added_df_no_dups], ignore_index=True)
        refined_df = pd.merge(added_df_no_dups, dataset_df_no_dups, how='outer')  # inner : 교집합, outer : 합집합

        # refined_dataset_header = refined_df.columns.tolist()
        refined_dataset = refined_df.values.tolist()

        data_lib.write_list_to_csv(refined_dataset, filename)

        pass

    def refine_dataset_by_feature_selection_result(self, feature_selector):
        """ refine dataset by using feature selection results.
        refer to [refinement] tag of ini file to change feature importance thresholds.
        requirement: feature_selector should run various feature selection functions before running this function.

        parameters:
            feature_selector (feature_selector.FeatureSelector):

        returns:
            void
        """

        #for remaining_key in feature_selector.dataset["X"][0]:
        #    if any(remaining_key==key for key in self.dataset[0]):
        #        # cols to be deleted from dataset
        #        print(remaining_key)

        # delete all columns from self.dataset deleted by FeatureSelector
        for del_key in feature_selector.deleted_keys:
            try:
                del_idx = self.dataset[0].index(del_key)
                for row in self.dataset:
                    del row[del_idx]
            except ValueError:
                continue

        return


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--var_csv_file", required=True, help="csv file for variables")
    parser.add_argument("--var_ini_file", required=True, help="ini file for configuration")

    parser.add_argument("--statistics_", default=False, action='store_true',
                        help="Flag to run statistics function")

    parser.add_argument("--refine_dataset_", default=False, action='store_true',
                        help="Flag to refine dataset")

    parser.add_argument("--feature_selection_", default=False, action='store_true',
                        help="Flag to run feature selection functions")

    parser.add_argument("--generate_dataset_", default=False, action='store_true',
                        help="Flag to generate new dataset")

    parser.add_argument("--add_dataset_col", default="", help="csv file with added column datas")

    args = parser.parse_args()

    g_logger = sys_lib.setup_logger(CAT,
                                    '_',
                                    folder='log',
                                    console_=True)

    var_ini = configparser.ConfigParser()
    var_ini.read(args.var_ini_file)

    var_mtx = data_lib.read_csv_file(args.var_csv_file)
    model_num_str = var_ini['model_info'][MODEL_NUMBER]
    g_logger.info("Model info : M{}".format(model_num_str))
    dat_mtx = data_lib.read_csv_file(var_ini[DATASET + '_' + model_num_str]['raw_dataset_csv_filename'])

    start_pos, end_pos = sys_lib.calc_crop_info_from_ini(var_ini[VAR_INFO_CSV])
    roi_var_mtx = data_lib.crop_mtx(var_mtx, start_pos, end_pos)
    # roi_var_mtx = [roi_var_mtx[0]] + data_lib.remove_row(roi_var_mtx[1:], idx=roi_var_mtx[0].index(DATASET), val='')
    # roi_var_mtx = [roi_var_mtx[0]] + data_lib.remove_row(roi_var_mtx[1:], idx=roi_var_mtx[0].index(DATASET), val='1')
    feature = PoscoDecarbEstModel(roi_var_mtx)
    feature.init_feat_class(var_ini, offset=start_pos[0])

    if args.statistics_:
        dat_mtx = feature.update_dataset_by_null_is(dat_mtx)
        feature.init_statistical_properties(dat_mtx)
        feature.analyze_dataset(dat_mtx)
        feature.print_analytics_result(console=True)

    if args.refine_dataset_:
        feature.refine_dataset_model(model_num=var_ini['model_info']['model_number'], feature=feature, logger=g_logger)  # 전로 탈탄 코드 추가(종점 탄소)
        feature.refine_dataset_col_by_dataset_order()
        feature.refine_dataset_col_by_null_replacement()
        feature.refine_dataset_col_by_null(var_ini)
        feature.refine_dataset_row_by_nan_replacement()
        feature.refine_dataset_row_by_range() # except_var_list=['TAP_WORK_DATE']
        # feature.refine_dataset_row_by_process_type(cat=var_ini['model_info']['model_name']) # 온도 모델 코드
        feature.split_dataset_by_type_and_dataset_order()
        sys_lib.print_and_write(feature.log, filename="refined_dataset_result.txt")

    if args.feature_selection_:
        refine_by_fs = False
        if var_ini.has_option(REFINEMENT, "refine_by_FS_threshold"):
            refine_by_fs = var_ini[REFINEMENT]["refine_by_FS_threshold"]

        feature_selector = FeatureSelector(feature.datasets['in_num'], feature.datasets['out_num'])

        feature_selector.variance_threshold_method(thresh=float(var_ini[REFINEMENT]['FS_var_threshold']))
        sys_lib.print_and_write(DIV_PATTERN + feature_selector.get_log())

        feature_selector.f_regression_method(p_thresh=float(var_ini[REFINEMENT]['FS_f_regression_p_threshold']),
                                             selection_=refine_by_fs, log_=True)
        sys_lib.print_and_write(DIV_PATTERN + feature_selector.get_log())

        feature_selector.mutual_info_regression_method(mi_thresh=float(var_ini[REFINEMENT]['FS_mi_regression_threshold']),
                                                       selection_=refine_by_fs, log_=True)
        sys_lib.print_and_write(DIV_PATTERN + feature_selector.get_log())

        feature_selector.random_forest_regressor_method(thresh=float(var_ini[REFINEMENT]['FS_random_forest_threshold']),
                                                        selection_=refine_by_fs, log_=True)
        sys_lib.print_and_write(DIV_PATTERN + feature_selector.get_log())

        if args.refine_dataset_ and refine_by_fs:
            feature.refine_dataset_by_feature_selection_result(feature_selector)



    if args.generate_dataset_:
        overwrite = True if var_ini[DATASET + '_' + model_num_str]['overwrite'].upper() == 'TRUE' else False

        if  var_ini['model_info']['model_name'][:8] == 'RH_MACRO' and \
                var_ini['model_info']['model_number'][0] == '3':  # or var_ini[VAR_INFO_CSV]['model_number'][0] == '4':
            data_lib.write_list_to_separate_csv(feature.dataset, 'PATTERN_OPERATION',
                                                var_ini[DATASET + '_' + model_num_str]['dataset_csv_filename'],
                                                overwrite=overwrite)
        else:
            data_lib.write_list_to_csv(feature.dataset,
                                       var_ini[DATASET + '_' + model_num_str]['dataset_csv_filename'],
                                       overwrite=overwrite)

    #
    # if args.rename_dataset_col:
    #     print("\n # Write new dataset after replacing column name...")
    #     feature.rename_dataset_col(args.rename_dataset_col, deepcopy.deepcopy(dat_mtx))
    #     return
    #
    if args.add_dataset_col:
        ans_dataset = input('Please enter a filename to add:')
        added_dat_mtx = data_lib.read_csv_file("../sys_component/dataset_raw/" + ans_dataset)

        print("\n # Write new dataset after adding row and column datas...")
        feature.add_dataset(args.add_dataset_col, copy.deepcopy(dat_mtx), copy.deepcopy(added_dat_mtx))
        return


if __name__ == "__main__":

    if len(sys.argv) == 1:
        sys.argv.extend([

            "--var_csv_file", "../sys_cvt_decarb/cvt_decarb_var.csv",
            "--var_ini_file", "../sys_cvt_decarb/cvt_decarb_var.ini",

            "--statistics_",
            "--refine_dataset_",
            "--generate_dataset_",
            "--feature_selection_",
            # "--add_dataset_col", "added_col_dataset.csv",
        ])

    main()
