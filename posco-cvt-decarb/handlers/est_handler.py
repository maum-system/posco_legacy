#! /usr/bin/env python
# -*- coding: utf-8 -*-

import sys
from lib import data as data_lib
from lib import learning
import argparse
import configparser
import math
import matplotlib.pyplot as plt
from sklearn.linear_model import LinearRegression
import numpy as np
from sklearn.metrics import mean_squared_error, r2_score

EST = True
real_value = 'F_RH_C'


def generate_distrib_plot(xmin, xmax, margin, real_val, pred_val, c_name, plot_name, save_path="./"):
    xmin = xmin
    xmax = xmax

    real_val = list(map(float, real_val))
    real_val = np.array(real_val)
    pred_val = np.array(pred_val)
    under = []
    under.append((xmin - 30) - margin)
    under.append((xmax + 30) - margin)
    ideal = []
    ideal.append(xmin - 30)
    ideal.append(xmax + 30)
    below = []
    below.append((xmin - 30) + margin)
    below.append((xmax + 30) + margin)

    # fig = plt.figure(1)
    fig = plt.figure(figsize=(25, 25))
    # plt.subplot(1, 2, 1)
    plt.plot(ideal, under, color='r', linestyle='--', label='{:3d} margin'.format(int(margin)))
    plt.plot(ideal, below, color='r', linestyle='--')
    plt.xlim(xmin, xmax)
    plt.ylim(xmin, xmax)

    linear_regressor = LinearRegression()
    linear_regressor.fit(real_val.reshape(-1, 1), pred_val.reshape(-1, 1))
    y_pred = linear_regressor.predict(real_val.reshape(-1, 1))
    plt.plot(real_val, y_pred, color='blue')

    plt.plot(ideal, ideal, color='r', linewidth=1, label='ideal value')
    plt.scatter(real_val, pred_val, color='g', s=80, label='real value')

    plt.xlabel('Real value of {}'.format(c_name))
    plt.ylabel('Prediction value of {}'.format(c_name))
    plt.title('{} Distribution Graph'.format(c_name))
    plt.legend()
    plt.show()

    fig_name = c_name + '_' + plot_name
    fig.savefig(save_path + fig_name)
    plt.clf()
    pass


def make_est_result_file(est_csv_file, model_dir, result_file, est_method, ml_cfg, feature=None):
    data_dict = data_lib.read_csv_file_as_dict(est_csv_file)
    general_cfg = ml_cfg['general']
    ml_method = general_cfg['method']
    real_val_list = []
    est_val_list = []

    with open(result_file, 'a+') as new_file:
        col_names = [key for key, val in data_dict.items()]
        for name in col_names:
            new_file.write('{},'.format(name))
        new_file.write('{}_{}\n'.format("OUT", col_names[0]))

        correct_count = 0
        for i in range(len(data_dict[col_names[0]])):
            est_dict = {key: val[i] for key, val in data_dict.items()}
            real_y_value = est_dict[real_value]
            est_thresh = ml_cfg[ml_method]['est_thresh']
            real_val_list.append(real_y_value)

            if ml_method == 'RFR':
                est_result = learning.rfr_estimation(model_dir, est_dict)
                diff = abs(float(est_result)-float(real_y_value))
                est_val_list.append(est_result)

                if diff <= float(est_thresh):
                    correct_count += 1

                lines = [val[i] for key, val in data_dict.items()]
                for line in lines:
                    new_file.write('{},'.format(line))
                new_file.write('{}\n'.format(est_result))

            elif ml_method == 'XGBR':
                est_result = learning.xgbr_estimation(model_dir, est_dict)
                diff = abs(float(est_result) - float(real_y_value))
                est_val_list.append(est_result)

                if diff <= float(est_thresh):
                    correct_count += 1

                lines = [val[i] for key, val in data_dict.items()]
                for line in lines:
                    new_file.write('{},'.format(line))
                new_file.write('{}\n'.format(est_result))

            elif ml_method == 'LGBMR':
                est_result = learning.lgbmr_estimation(model_dir, est_dict)
                est_val_list.append(est_result)
                diff = abs(float(est_result) - float(real_y_value))

                if diff <= float(est_thresh):
                    correct_count += 1

                lines = [val[i] for key, val in data_dict.items()]
                for line in lines:
                    new_file.write('{},'.format(line))
                new_file.write('{}\n'.format(est_result))

            elif ml_method == 'DNN':
                est_result = learning.dnn_estimation(model_dir, est_dict, Feat=feature)
                est_val_list.append(est_result)
                diff = abs(float(est_result) - float(real_y_value))

                if diff <= float(est_thresh):
                    correct_count += 1

                lines = [val[i] for key, val in data_dict.items()]
                for line in lines:
                    new_file.write('{},'.format(line))
                new_file.write('{}\n'.format(est_result))

        accuracy = (correct_count / len(data_dict[list(data_dict)[0]])) * 100

        if "mse" in est_method:
            print('mse : {:4.2f}\n'.format(mean_squared_error(np.array(real_val_list).astype(float),
                                                              np.array(est_val_list).astype(float))))
        if "rmse" in est_method:
            print('rmse : {:4.2f}\n'.format(math.sqrt(mean_squared_error(np.array(real_val_list).astype(float),
                                                                         np.array(est_val_list).astype(float)))))
        if "R2" in est_method:
            print('R2_score : {:4.2f}\n'.format(r2_score(np.array(real_val_list).astype(float),
                                                         np.array(est_val_list).astype(float))))
        print('accuracy : {:4.2f} %'.format(accuracy))

        return real_val_list, est_val_list


def main():

    parser = argparse.ArgumentParser()

    parser.add_argument("--est_csv_file", required=True, help="csv file for estimation")
    parser.add_argument("--ml_ini_file", required=True, help="ini file for machine learner")
    parser.add_argument("--model_bin_file", required=True, help="model bin file for training")
    parser.add_argument("--result_file", required=True, help="estimation result file")
    parser.add_argument("--est_method", required=True, help="estimation method")
    parser.add_argument("--gen_plot_",  default=False, action='store_true', help="generate_scatter_plot")

    args = parser.parse_args()
    ml_cfg = configparser.ConfigParser()
    ml_cfg.read(args.ml_ini_file)
    est_method = args.est_method.split(',')

    real_val_list, est_val_list = make_est_result_file(args.est_csv_file, args.model_bin_file, args.result_file,
                                                       est_method, ml_cfg)

    if args.gen_plot_:
        generate_distrib_plot(xmin=int(min(real_val_list)),
                              xmax=int(max(real_val_list)),
                              margin=50,
                              real_val=real_val_list,
                              pred_val=est_val_list,
                              c_name='Carbon', plot_name='{}_distribution plot'.format(real_value),
                              save_path='../sys_component/analysis/')


if __name__ == "__main__":

    if len(sys.argv) == 1:
        if EST:
            sys.argv.extend([
                             "--est_csv_file", "../sys_component/dataset_raw/190909_info/m4_est_data.csv",
                             "--ml_ini_file", "../sys_component/config_machine_learning/rh_macro_m4-7-C.ini",
                             "--model_bin_file", "../sys_component/model/RH_MACRO_M4-7-C/RFR/190909",
                             "--result_file", "../sys_component/analysis/result.csv",
                             "--est_method", "mse,rmse,R2",  # mse, rmse, R2
                             # "--est_csv_file", "../sys_component/analysis/NEW_latest/rh_macro_model5_dataset_all.csv",
                             # "--ml_ini_file", "../sys_component/config_machine_learning/rh_macro_m5.ini",
                             # "--model_bin_file", "../sys_component/model/RH_MACRO_M5/LGBMR/190830",
                             # "--result_file", "../sys_component/analysis/result.csv",
                             # "--est_method", "mse,rmse,R2", #mse, rmse, R2

                             # "--gen_plot_"
                             ])
        else:
            sys.argv.extend(["--~help"])
    main()
