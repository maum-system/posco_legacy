

************************************************************************************************************************

 # Dataset refinement process by deleting row vector in which one of them doesn't satisfy its range requirement.
 > Null data in sample vector
   - GOHAM_TOTAL : 1
   - DY_OXY2 : 2
   - FCE_LIFE : 1
   - FESI_DYN : 1
   - CAO_J1 : 1
   - TERMINAL_TEMP : 265
   - CAO_TOTAL : 1
   - SODIUM_TOTAL : 1
   - AL_TAP : 1
   - DYNC : 139
   - DYN_OXY_TOTAL : 139
   - TOTAL_OB : 3232
   - CAO_2ND : 1
   - DOL_COAT : 1
   - TAPHOLE_LIFE : 1
   - BDO_2ND : 1
   - BLOW_VAIN : 2
   - TD_TARGET_TEMP : 17
   - DOL_DYN : 1
   - LANCE_LIFE : 1
   - MNAIM : 1
   - KR_MN : 583
   - COAT_BLOW : 2
   - KR_C : 583
   - KR_P : 583
   - CAO_TAP : 1
   - DY_OXY1 : 2
   - SM_TIME : 1
   - TOTAL_OXY : 1
   - TOTAL_WGT_INPUT : 2
   - BLOW_TIME : 1
   - CHARGE_WAIT_TIME : 76
   - DYNT : 139
   - SMAX : 1
   - TT : 1
 > Number type data in sample vector not satisfying range information
   - DYNC : 2725
   - CAO_TAP : 30
   - TOTAL_OXY : 809
   - LANCE_LIFE : 6
   - TOTAL_WGT_INPUT : 15
   - DY_OXY2 : 414
   - CAO_2ND : 993
   - CHARGE_WAIT_TIME : 1
   - TERMINAL_TEMP : 392
   - CAO_TOTAL : 1
   - DOL_COAT : 821
   - TT : 22
 > String type data in sample vector not satisfying range information
   - BLOW_TYPE : 716
   - PROCESS : 3

 Processing refine_dataset_row_by_range : 
    24468 x 47 -> 15471 x 47

************************************************************************************************************************

 # Dataset refinement process by deleting row vector in which one of them doesn't satisfy its range requirement.
 > Null data in sample vector
   - IRON_WGT : 1
   - CAO_J1 : 1
   - INPUT_SI_CALC : 4
   - TOTAL_INPUT : 1
   - BDO_2ND : 1
   - EMPTY_LADLE_WGT : 23
   - KR_MN : 583
   - EMP_NUM : 1
   - TERMINAL_TARGET_TEMP : 2
   - TERMINAL_TEMP : 265
   - CHUTE_WGT : 1
   - TOTAL_WGT_INPUT : 2
   - CX01_CONV1_CO_GAS_70 : 6023
   - LANCE_LIFE : 1
   - DYNT : 139
   - FCE_LIFE : 1
   - MNAIM : 1
   - DY_OXY2 : 2
   - CAO_2ND : 1
   - CMAX : 1
   - DYN_OXY_TOTAL : 139
   - OXY_PURITY2 : 2
   - DOL_COAT : 1
   - LADLE_EMPTY_TIME : 13
   - SODIUM_TOTAL : 1
   - BDO_COAT : 1
   - CAO_TAP : 1
   - KR_C : 583
   - TAPHOLE_LIFE : 1
   - TD_TARGET_TEMP : 17
   - TT : 1
   - CAO_TOTAL : 1
   - TOTAL_OXY : 1
   - AL_TAP : 1
   - DY_OXY1 : 2
   - CHARGE_WAIT_TIME : 76
   - SM_TIME : 1
   - BLOW_VAIN : 2
   - COAT_BLOW : 2
   - KR_P : 583
   - BLOW_TIME : 1
   - CX01_CONV1_CO_GAS_80 : 6023
   - DYNC : 139
   - PRP_FCE_MOVING_TIME : 2702
 > Number type data in sample vector not satisfying range information
   - TOTAL_WGT_INPUT : 15
   - CAO_TAP : 30
   - LANCE_LIFE : 6
   - TOTAL_INPUT : 26
   - DYNC : 2725
   - TT : 22
   - CAO_TOTAL : 1
   - DY_OXY2 : 414
   - EMPTY_LADLE_WGT : 3194
   - TERMINAL_TARGET_TEMP : 2
   - TERMINAL_TEMP : 392
   - CAO_2ND : 993
   - TOTAL_OXY : 809
   - CHARGE_WAIT_TIME : 1
   - DOL_COAT : 821
   - LADLE_EMPTY_TIME : 1453
   - PRP_FCE_MOVING_TIME : 172
 > String type data in sample vector not satisfying range information
   - BLOW_TYPE : 716

 Processing refine_dataset_row_by_range : 
    24468 x 51 -> 9323 x 51

************************************************************************************************************************

 # Dataset refinement process by deleting row vector in which one of them doesn't satisfy its range requirement.
 > Null data in sample vector
   - SODIUM_TOTAL : 1
   - CX01_CONV1_CO_GAS_70 : 6023
   - CAO_2ND : 1
   - IRON_WGT : 1
   - TD_TARGET_TEMP : 17
   - CHUTE_WGT : 1
   - LADLE_EMPTY_TIME : 13
   - TT : 1
   - FCE_LIFE : 1
   - BLOW_VAIN : 2
   - INPUT_SI_CALC : 4
   - TAPHOLE_LIFE : 1
   - CAO_TAP : 1
   - LANCE_LIFE : 1
   - CHARGE_WAIT_TIME : 76
   - DYN_OXY_TOTAL : 139
   - KR_MN : 583
   - DYNC : 139
   - KR_C : 583
   - EMP_NUM : 1
   - CX01_CONV1_CO_GAS_80 : 6023
   - DY_OXY2 : 2
   - TERMINAL_TEMP : 265
   - CAO_J1 : 1
   - EMPTY_LADLE_WGT : 23
   - BLOW_TIME : 1
   - DOL_COAT : 1
   - KR_P : 583
   - TOTAL_INPUT : 1
   - TERMINAL_TARGET_TEMP : 2
   - OXY_PURITY2 : 2
   - CMAX : 1
   - BDO_2ND : 1
   - COAT_BLOW : 2
   - TOTAL_WGT_INPUT : 2
   - BDO_COAT : 1
   - PRP_FCE_MOVING_TIME : 2702
   - AL_TAP : 1
   - TOTAL_OXY : 1
   - MNAIM : 1
   - DY_OXY1 : 2
   - DYNT : 139
   - CAO_TOTAL : 1
   - SM_TIME : 1
 > Number type data in sample vector not satisfying range information
   - DY_OXY2 : 414
   - LANCE_LIFE : 6
   - TERMINAL_TEMP : 392
   - CHARGE_WAIT_TIME : 1
   - CAO_2ND : 993
   - EMPTY_LADLE_WGT : 3194
   - TOTAL_WGT_INPUT : 15
   - DYNC : 2725
   - PRP_FCE_MOVING_TIME : 172
   - DOL_COAT : 821
   - TOTAL_OXY : 809
   - LADLE_EMPTY_TIME : 1453
   - TT : 22
   - TOTAL_INPUT : 26
   - TERMINAL_TARGET_TEMP : 2
   - CAO_TOTAL : 1
   - CAO_TAP : 30
 > String type data in sample vector not satisfying range information
   - BLOW_TYPE : 716

 Processing refine_dataset_row_by_range : 
    24468 x 51 -> 9323 x 51

************************************************************************************************************************

 # Dataset refinement process by deleting row vector in which one of them doesn't satisfy its range requirement.
 > Null data in sample vector
   - EMP_NUM : 1
   - DYNC : 139
   - EMPTY_LADLE_WGT : 23
   - TOTAL_OXY : 1
   - COAT_BLOW : 2
   - KR_C : 583
   - IRON_WGT : 1
   - SM_TIME : 1
   - DY_OXY1 : 2
   - BLOW_VAIN : 2
   - CX01_CONV1_CO_GAS_70 : 6023
   - DY_OXY2 : 2
   - CAO_J1 : 1
   - BDO_COAT : 1
   - LADLE_EMPTY_TIME : 13
   - INPUT_SI_CALC : 4
   - MNAIM : 1
   - TERMINAL_TARGET_TEMP : 2
   - TAPHOLE_LIFE : 1
   - CHUTE_WGT : 1
   - TOTAL_INPUT : 1
   - DYN_OXY_TOTAL : 139
   - DYNT : 139
   - OXY_PURITY2 : 2
   - BDO_2ND : 1
   - KR_P : 583
   - TOTAL_WGT_INPUT : 2
   - PRP_FCE_MOVING_TIME : 2702
   - CMAX : 1
   - CX01_CONV1_CO_GAS_80 : 6023
   - CAO_TOTAL : 1
   - KR_MN : 583
   - BLOW_TIME : 1
   - CAO_TAP : 1
   - CHARGE_WAIT_TIME : 76
   - FCE_LIFE : 1
   - DOL_COAT : 1
   - TD_TARGET_TEMP : 17
   - SODIUM_TOTAL : 1
   - TERMINAL_TEMP : 265
   - LANCE_LIFE : 1
   - TT : 1
   - AL_TAP : 1
   - CAO_2ND : 1
 > Number type data in sample vector not satisfying range information
   - TOTAL_INPUT : 26
   - DY_OXY2 : 414
   - DYNC : 2725
   - CAO_TAP : 30
   - EMPTY_LADLE_WGT : 3194
   - TOTAL_OXY : 809
   - CHARGE_WAIT_TIME : 1
   - DOL_COAT : 821
   - LADLE_EMPTY_TIME : 1453
   - TOTAL_WGT_INPUT : 15
   - PRP_FCE_MOVING_TIME : 172
   - TERMINAL_TEMP : 392
   - LANCE_LIFE : 6
   - CAO_TOTAL : 1
   - TT : 22
   - TERMINAL_TARGET_TEMP : 2
   - CAO_2ND : 993
 > String type data in sample vector not satisfying range information
   - BLOW_TYPE : 716

 Processing refine_dataset_row_by_range : 
    24468 x 51 -> 9323 x 51

************************************************************************************************************************

 # Dataset refinement process by deleting row vector in which one of them doesn't satisfy its range requirement.
 > Null data in sample vector
   - CX01_CONV1_CO2_GAS_90 : 6023
   - COAT_BLOW : 2
   - TT : 1
   - CX01_CONV1_CO2_GAS_85 : 6023
   - TOTAL_OB : 3232
   - CX01_CONV1_CO_GAS_90 : 6023
   - CX01_CONV1_CO_GAS_100 : 6023
   - CX01_CONV1_CO2_GAS_70 : 6023
   - MNAIM : 1
   - SM_TIME : 1
   - CHUTE_WGT : 1
   - CHARGE_WAIT_TIME : 76
   - DY_OXY1 : 2
   - TERMINAL_OXY : 324
   - KR_C : 583
   - CX01_CONV1_CO2_GAS_60 : 6023
   - BDO_COAT : 1
   - CX01_CONV1_CO_GAS_80 : 6023
   - INPUT_TEMP : 92
   - DOL_COAT : 1
   - AL_TAP : 1
   - FESI_DYN : 1
   - TOTAL_OXY : 1
   - CX01_CONV1_CO_GAS_85 : 6023
   - CX01_CONV1_CO_GAS_70 : 6023
   - CX01_CONV1_CO2_GAS_100 : 6023
   - TERMINAL_TARGET_TEMP : 2
   - DYN_OXY_TOTAL : 139
   - DYNT : 139
   - OXY_PURITY2 : 2
   - DYNC : 139
   - CX01_CONV1_CO_GAS_60 : 6023
   - BLOW_TIME : 1
   - KR_CR : 583
   - DY_OXY2 : 2
   - CAO_TAP : 1
   - CX01_CONV1_CO2_GAS_80 : 6023
   - TERMINAL_TEMP : 265
   - TAPHOLE_LIFE : 1
 > Number type data in sample vector not satisfying range information
   - DOL_COAT : 821
   - TERMINAL_TARGET_TEMP : 2
   - DYNC : 2725
   - CHARGE_WAIT_TIME : 1
   - TT : 22
   - TERMINAL_OXY : 501
   - TOTAL_OXY : 809
   - DY_OXY2 : 414
   - CAO_TAP : 30
   - TERMINAL_TEMP : 392
 > String type data in sample vector not satisfying range information
   - BLOW_TYPE : 716

 Processing refine_dataset_row_by_range : 
    24468 x 50 -> 12390 x 50

************************************************************************************************************************

 # Dataset refinement process by deleting row vector in which one of them doesn't satisfy its range requirement.
 > Null data in sample vector
   - OXY_PURITY2 : 2
   - CX01_CONV1_CO2_GAS_85 : 6023
   - CX01_CONV1_CO2_GAS_70 : 6023
   - TOTAL_OB : 3232
   - TERMINAL_OXY : 324
   - CX01_CONV1_CO_GAS_70 : 6023
   - CHARGE_WAIT_TIME : 76
   - CX01_CONV1_CO_GAS_90 : 6023
   - DYNT : 139
   - COAT_BLOW : 2
   - BDO_COAT : 1
   - CX01_CONV1_CO2_GAS_60 : 6023
   - CX01_CONV1_CO_GAS_60 : 6023
   - FESI_DYN : 1
   - KR_CR : 583
   - TAPHOLE_LIFE : 1
   - BLOW_TIME : 1
   - DY_OXY2 : 2
   - TOTAL_OXY : 1
   - TT : 1
   - CX01_CONV1_CO2_GAS_80 : 6023
   - CX01_CONV1_CO_GAS_85 : 6023
   - SM_TIME : 1
   - CX01_CONV1_CO2_GAS_90 : 6023
   - KR_C : 583
   - CX01_CONV1_CO_GAS_100 : 6023
   - TERMINAL_TARGET_TEMP : 2
   - TERMINAL_TEMP : 265
   - CX01_CONV1_CO_GAS_80 : 6023
   - DOL_COAT : 1
   - MNAIM : 1
   - CAO_TAP : 1
   - DYNC : 139
   - CX01_CONV1_CO2_GAS_100 : 6023
   - INPUT_TEMP : 92
   - AL_TAP : 1
   - DYN_OXY_TOTAL : 139
   - CHUTE_WGT : 1
   - DY_OXY1 : 2
 > Number type data in sample vector not satisfying range information
   - DY_OXY2 : 414
   - TERMINAL_TARGET_TEMP : 2
   - TERMINAL_TEMP : 392
   - CHARGE_WAIT_TIME : 1
   - TT : 22
   - DOL_COAT : 821
   - CAO_TAP : 30
   - DYNC : 2725
   - TERMINAL_OXY : 501
   - TOTAL_OXY : 809
 > String type data in sample vector not satisfying range information
   - BLOW_TYPE : 716

 Processing refine_dataset_row_by_range : 
    24468 x 50 -> 12390 x 50

************************************************************************************************************************

 # Dataset refinement process by deleting row vector in which one of them doesn't satisfy its range requirement.
 > Null data in sample vector
   - COAT_BLOW : 2
   - TERMINAL_OXY : 324
   - INPUT_TEMP : 92
   - KR_C : 583
   - DYN_OXY_TOTAL : 139
   - DOL_COAT : 1
   - DYNC : 139
   - CX01_CONV1_CO2_GAS_60 : 6023
   - CX01_CONV1_CO_GAS_70 : 6023
   - BLOW_TIME : 1
   - TERMINAL_TEMP : 265
   - SM_TIME : 1
   - DYNT : 139
   - TOTAL_OB : 3232
   - TAPHOLE_LIFE : 1
   - CX01_CONV1_CO_GAS_60 : 6023
   - CHUTE_WGT : 1
   - AL_TAP : 1
   - CHARGE_WAIT_TIME : 76
   - MNAIM : 1
   - DY_OXY1 : 2
   - TERMINAL_TARGET_TEMP : 2
   - TOTAL_OXY : 1
   - CAO_TAP : 1
   - FESI_DYN : 1
   - CX01_CONV1_CO2_GAS_80 : 6023
   - TT : 1
   - CX01_CONV1_CO2_GAS_85 : 6023
   - CX01_CONV1_CO_GAS_85 : 6023
   - DY_OXY2 : 2
   - OXY_PURITY2 : 2
   - CX01_CONV1_CO2_GAS_70 : 6023
   - BDO_COAT : 1
   - KR_CR : 583
   - CX01_CONV1_CO_GAS_80 : 6023
 > Number type data in sample vector not satisfying range information
   - TERMINAL_TEMP : 392
   - TT : 22
   - TERMINAL_TARGET_TEMP : 2
   - TERMINAL_OXY : 501
   - CAO_TAP : 30
   - TOTAL_OXY : 809
   - DY_OXY2 : 414
   - DOL_COAT : 821
   - CHARGE_WAIT_TIME : 1
   - DYNC : 2725
 > String type data in sample vector not satisfying range information
   - BLOW_TYPE : 716

 Processing refine_dataset_row_by_range : 
    24468 x 46 -> 12390 x 46

************************************************************************************************************************

 # Dataset refinement process by deleting row vector in which one of them doesn't satisfy its range requirement.
 > Null data in sample vector
   - DY_OXY1 : 2
   - DY_OXY2 : 2
   - CHUTE_WGT : 1
   - DYNT : 139
   - CX01_CONV1_CO2_GAS_60 : 6023
   - TOTAL_OXY : 1
   - TERMINAL_TEMP : 265
   - BDO_COAT : 1
   - DYNC : 139
   - BLOW_TIME : 1
   - OXY_PURITY2 : 2
   - CX01_CONV1_CO2_GAS_70 : 6023
   - FESI_DYN : 1
   - AL_TAP : 1
   - CX01_CONV1_CO_GAS_85 : 6023
   - KR_CR : 583
   - TOTAL_OB : 3232
   - KR_C : 583
   - INPUT_TEMP : 92
   - CX01_CONV1_CO2_GAS_80 : 6023
   - TERMINAL_TARGET_TEMP : 2
   - SM_TIME : 1
   - CX01_CONV1_CO_GAS_80 : 6023
   - CX01_CONV1_CO_GAS_60 : 6023
   - CX01_CONV1_CO2_GAS_85 : 6023
   - CAO_TAP : 1
   - TERMINAL_OXY : 324
   - TERMINAL_C : 484
   - COAT_BLOW : 2
   - CHARGE_WAIT_TIME : 76
   - TT : 1
   - DYN_OXY_TOTAL : 139
   - MNAIM : 1
   - TAPHOLE_LIFE : 1
   - DOL_COAT : 1
   - CX01_CONV1_CO_GAS_70 : 6023
 > Number type data in sample vector not satisfying range information
   - DY_OXY2 : 414
   - TERMINAL_TARGET_TEMP : 2
   - TT : 22
   - TOTAL_OXY : 809
   - TERMINAL_TEMP : 392
   - CAO_TAP : 30
   - TERMINAL_OXY : 501
   - TERMINAL_C : 3627
   - DYNC : 2725
   - DOL_COAT : 821
   - CHARGE_WAIT_TIME : 1
 > String type data in sample vector not satisfying range information
   - BLOW_TYPE : 716

 Processing refine_dataset_row_by_range : 
    24468 x 47 -> 10742 x 47

************************************************************************************************************************

 # Dataset refinement process by deleting row vector in which one of them doesn't satisfy its range requirement.
 > Null data in sample vector
   - DYNT : 139
   - CX01_CONV1_CO_GAS_60 : 6023
   - DY_OXY2 : 2
   - CAO_TAP : 1
   - CX01_CONV1_CO_GAS_70 : 6023
   - KR_C : 583
   - DY_OXY1 : 2
   - TOTAL_OXY : 1
   - MNAIM : 1
   - CX01_CONV1_CO2_GAS_60 : 6023
   - CX01_CONV1_CO_GAS_80 : 6023
   - BLOW_TIME : 1
   - FESI_DYN : 1
   - TERMINAL_C : 484
   - TERMINAL_TEMP : 265
   - INPUT_TEMP : 92
   - CX01_CONV1_CO2_GAS_70 : 6023
   - CX01_CONV1_CO_GAS_85 : 6023
   - SM_TIME : 1
   - CX01_CONV1_CO2_GAS_80 : 6023
   - TOTAL_OB : 3232
   - TAPHOLE_LIFE : 1
   - OXY_PURITY2 : 2
   - TT : 1
   - AL_TAP : 1
   - CX01_CONV1_CO2_GAS_85 : 6023
   - COAT_BLOW : 2
   - DOL_COAT : 1
   - CHUTE_WGT : 1
   - BDO_COAT : 1
   - KR_CR : 583
   - TERMINAL_OXY : 324
   - TERMINAL_TARGET_TEMP : 2
   - DYN_OXY_TOTAL : 139
   - DYNC : 139
   - CHARGE_WAIT_TIME : 76
 > Number type data in sample vector not satisfying range information
   - TOTAL_OXY : 809
   - DOL_COAT : 821
   - DY_OXY2 : 414
   - CAO_TAP : 30
   - TERMINAL_C : 3627
   - TERMINAL_OXY : 501
   - TERMINAL_TARGET_TEMP : 2
   - TERMINAL_TEMP : 392
   - TT : 22
   - DYNC : 2725
   - CHARGE_WAIT_TIME : 1
 > String type data in sample vector not satisfying range information
   - BLOW_TYPE : 716

 Processing refine_dataset_row_by_range : 
    24468 x 47 -> 10742 x 47

************************************************************************************************************************

 # Dataset refinement process by deleting row vector in which one of them doesn't satisfy its range requirement.
 > Null data in sample vector
   - DYNT : 139
   - CHARGE_WAIT_TIME : 76
   - BDO_2ND : 1
   - KR_MN : 583
   - CAO_TOTAL : 1
   - BDO_COAT : 1
   - LADLE_EMPTY_TIME : 13
   - DOL_COAT : 1
   - AL_TAP : 1
   - CX01_CONV1_CO_GAS_85 : 6023
   - LANCE_LIFE : 1
   - KR_C : 583
   - BLOW_VAIN : 2
   - OXY_PURITY2 : 2
   - CMAX : 1
   - TOTAL_OXY : 1
   - DY_OXY1 : 2
   - TOTAL_WGT_INPUT : 2
   - DYNC : 139
   - KR_P : 583
   - CHUTE_WGT : 1
   - TAPHOLE_LIFE : 1
   - SM_TIME : 1
   - TD_TARGET_TEMP : 17
   - CX01_CONV1_CO2_GAS_70 : 6023
   - CAO_2ND : 1
   - EMPTY_LADLE_WGT : 23
   - CX01_CONV1_CO2_GAS_60 : 6023
   - IRON_WGT : 1
   - CX01_CONV1_CO_GAS_60 : 6023
   - CAO_J1 : 1
   - CX01_CONV1_CO_GAS_70 : 6023
   - CX01_CONV1_CO_GAS_80 : 6023
   - TERMINAL_TARGET_TEMP : 2
   - DY_OXY2 : 2
   - EMP_NUM : 1
   - CAO_TAP : 1
   - PRP_FCE_MOVING_TIME : 2702
   - SODIUM_TOTAL : 1
   - TT : 1
   - CX01_CONV1_CO2_GAS_85 : 6023
   - DYN_OXY_TOTAL : 139
   - FCE_LIFE : 1
   - INPUT_SI_CALC : 4
   - COAT_BLOW : 2
   - MNAIM : 1
   - CX01_CONV1_CO2_GAS_80 : 6023
   - BLOW_TIME : 1
   - TERMINAL_TEMP : 265
   - TOTAL_INPUT : 1
 > Number type data in sample vector not satisfying range information
   - TT : 22
   - TOTAL_OXY : 809
   - TOTAL_WGT_INPUT : 15
   - CHARGE_WAIT_TIME : 1
   - DYNC : 2725
   - EMPTY_LADLE_WGT : 3194
   - TOTAL_INPUT : 26
   - CAO_TOTAL : 1
   - LADLE_EMPTY_TIME : 1453
   - DOL_COAT : 821
   - DY_OXY2 : 414
   - TERMINAL_TARGET_TEMP : 2
   - CAO_TAP : 30
   - LANCE_LIFE : 6
   - TERMINAL_TEMP : 392
   - PRP_FCE_MOVING_TIME : 172
   - CAO_2ND : 993
 > String type data in sample vector not satisfying range information
   - BLOW_TYPE : 716

 Processing refine_dataset_row_by_range : 
    24468 x 61 -> 9323 x 61