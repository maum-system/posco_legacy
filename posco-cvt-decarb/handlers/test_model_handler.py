#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""This module's docstring summary line.
This is a multi-line docstring. Paragraphs are separated with blank lines.
Lines conform to 79-column limit.
Module and packages names should be short, lower_case_with_underscores.
Notice that this in not PEP8-cheatsheet.py
Seriously, use flake8. Atom.io with https://atom.io/packages/linter-flake8
is awesome!
See http://www.python.org/dev/peps/pep-0008/ for more PEP-8 details
Coding style reference
    - https://www.python.org/dev/peps/pep-0008/
    - http://web.archive.org/web/20111010053227/http://jaynes.colorado.edu
    /PythonGuidelines.html
    WHAT TO DO
    > Normalization before learning.
    > RFR, SVR, DNR
    > apply string features to estimation.
"""
import os
import sys
import argparse
import configparser
from lib import sys_lib
from lib import data as data_lib
import pandas as pd
import datetime

import lib.HoonUtils as utils
import handlers.dataset_handler as Dataset
import handlers.learning_handler as Learner
import handlers.analysis_handler as analysis_handler
import sys_component.learning_handler as learning_handler

__author__ = "Hoon Paek, Dong-gi Kim, Ji-young Moon"
__copyright__ = "Copyright 2007, The Cogent Project"
__credits__ = []
__license__ = "GPL"
__version__ = "0.0.1"
__maintainer__ = "Hoon Paek"
__email__ = "hoon.paek@mindslab.ai"
__status__ = "Prototype"  # Prototype / Development / Production

SORT_BY_VAR = 'TAP_WORK_DATE'
LEARNING_TEST = True


def main():
    """ Learning handler main code.
    """

    parser = argparse.ArgumentParser()
    parser.add_argument("--var_csv_file", required=True, help="csv file for variables")
    parser.add_argument("--var_ini_file", required=True, help="ini file for configuration")
    parser.add_argument("--ml_ini_file", default="", help="ini file for machine learner")

    args = parser.parse_args()

    var_cfg = configparser.ConfigParser()
    var_cfg.read(args.var_ini_file)

    ml_cfg = configparser.ConfigParser()
    ml_cfg.read(args.ml_ini_file)
    general_cfg = ml_cfg['general']

    var_mtx = data_lib.read_csv_file(args.var_csv_file)
    start_pos, end_pos = sys_lib.calc_crop_info_from_ini(var_cfg[Dataset.VAR_INFO_CSV])
    roi_var_mtx = data_lib.crop_mtx(var_mtx, start_pos, end_pos)
    feature = Dataset.PoscoTempEstModel(roi_var_mtx)
    feature.init_feat_class(var_cfg, model_number_str=general_cfg['model_number'], offset=start_pos[0])
    feature.dataset = data_lib.read_csv_file(general_cfg['dataset_file'])

    # Create analysis feature object.
    analyisis_feature = analysis_handler.DataAnalysis(feature, ini=utils.get_ini_parameters(args.var_ini_file))
    df = analyisis_feature.apply_dtype_to_data_frame(analyisis_feature.data_frame)

    # 'TAP_WORK_DATE'를 기준으로 정렬
    df.sort_values(by=SORT_BY_VAR, ascending=True, inplace=True)

    # group by month
    df['DATE_TIME'] = pd.to_datetime(df['TAP_WORK_DATE'])
    months = df['DATE_TIME'].dt.strftime("%Y%m").unique().tolist()
    df.drop('DATE_TIME', axis=1, inplace=True)

    # set train properties
    TRAIN_START_DATE = months[0] + '01'
    MONTH_PERIOD = 1

    # generate model by month
    self_learning_dataset = []
    result_list = [['y_name', 'train_acc', 'test_acc', 'train_rmse', 'test_rmse', 'tot_data', 'test_data', 'train_start_date', 'train_end_date', 'test_start_date', 'test_end_date' ]]
    for idx, month in enumerate(months[:-1]):

        # For train dataset by month
        END_DATE_TIME = datetime.datetime.strptime(TRAIN_START_DATE, '%Y%m%d') + pd.DateOffset(months=MONTH_PERIOD + idx) # MONTH_PERIOD is 1
        TRAIN_END_DATE = datetime.datetime.strftime(END_DATE_TIME, '%Y%m%d')
        df_train = df.loc[(df['TAP_WORK_DATE'] >= TRAIN_START_DATE) & (df['TAP_WORK_DATE'] < TRAIN_END_DATE)].reset_index(drop=True) # TRAIN_START_DATE is 2017/01/01
        num_train = len(df_train)

        # For test dataset by next month
        NEXT_END_DATE_TIME = datetime.datetime.strptime(TRAIN_END_DATE, '%Y%m%d') + pd.DateOffset(months=MONTH_PERIOD)  # point next month
        TEST_END_DATE = datetime.datetime.strftime(NEXT_END_DATE_TIME, '%Y%m%d')
        df_test = df.loc[(df['TAP_WORK_DATE'] >= TRAIN_END_DATE) & (df['TAP_WORK_DATE'] < TEST_END_DATE)].reset_index(drop=True)  # TRAIN_START_DATE is 2017/01/01

        # merge train & test dataset
        merged_df = pd.merge(df_train, df_test, how='outer')  # inner : 교집합, outer : 합집합
        merged_df.drop('TAP_WORK_DATE', axis=1, inplace=True)
        merged_data_header = merged_df.columns.tolist()
        merged_data = merged_df.values.tolist()
        merged_dataset = [merged_data_header] + merged_data

        self_learning_dataset = merged_dataset
        # if idx+1 == 1:
        #     self_learning_dataset = merged_dataset
        # else:
        #     self_learning_dataset.extend(merged_data)

        print('generate ({:d}/{:d}) th model'.format(idx+1, len(months[:-1])))
        print('train_start_date ~ train_end_date : {} ~ {}(미만)'.format(TRAIN_START_DATE, TRAIN_END_DATE))
        print('test_start_date  ~ test_end_date  : {} ~ {}'.format(TRAIN_END_DATE, TEST_END_DATE))

        # create mode from added dataset
        y_name, train_acc, test_acc, train_rmse, test_rmse, Learn = learning_handler.create_ml_model(feature, self_learning_dataset, general_cfg, ml_cfg,
                                                                                                     model_save_path='./test_model/latest.bin',
                                                                                                     num_train=num_train)
        print("train_acc : {}, test_acc : {}, train_rmse : {}, test_rmse : {}, "
              "tot_data : {}, test_data : {}, train_start_date : {}, train_end_date : {}, test_start_date : {}, test_end_date : {}".format(
            train_acc, test_acc, train_rmse, test_rmse, len(self_learning_dataset), len(self_learning_dataset)-num_train, TRAIN_START_DATE, TRAIN_END_DATE, TRAIN_END_DATE, TEST_END_DATE))

        result_list.append([y_name, train_acc[0], test_acc[0], train_rmse[0], test_rmse[0], len(self_learning_dataset), len(self_learning_dataset)-num_train, TRAIN_START_DATE, TRAIN_END_DATE, TRAIN_END_DATE, TEST_END_DATE])

    # Write result file
    result_df = pd.DataFrame(result_list)
    result_df.to_csv('./test_model/result.csv', index=False)



if __name__ == "__main__":

    if len(sys.argv) == 1:
        if LEARNING_TEST:
            sys.argv.extend([
                             # "--var_csv_file", "../sys_component/cvt_macro_var.csv",
                             # "--var_ini_file", "../sys_component/cvt_macro_var.ini",
                             # "--ml_ini_file", "../sys_component/config_machine_learning/cvt_macro_m1.ini",

                             "--var_csv_file", "../sys_component/rh_macro_var.csv",
                             "--var_ini_file", "../sys_component/rh_macro_var.ini",
                             "--ml_ini_file", "../sys_component/config_machine_learning/rh_macro_m4-7-C.ini",
                             # "--ml_ini_file", "../sys_component/config_machine_learning/rh_macro_m3-4-OTHER.ini",

                             ])
        else:
            sys.argv.extend(["--~help"])
    main()
