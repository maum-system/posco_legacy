# -*- coding: utf-8 -*-
import sys
import argparse
import configparser
import mssql.pymssqlwrapper

SELF_TEST = True


def main():

    parser = argparse.ArgumentParser()
    parser.add_argument("--db_setup_ini_file", required=True, help="DB generation ini file")
    args = parser.parse_args()

    cfg = configparser.ConfigParser()
    cfg.read(args.db_setup_ini_file)

    db_handler = mssql.pymssqlwrapper.PyMssqlWrapper(server_name=cfg['mssql_db']['server_name'],
                                                     port=cfg['mssql_db']['port'],
                                                     username=cfg['mssql_db']['username'],
                                                     password=cfg['mssql_db']['password'],
                                                     db_name=cfg['mssql_db']['db_name'],
                                                     table_name=cfg['mssql_db']['table_name'])

    db_handler.dropTable(cfg['mssql_db']['table_name'])
    db_handler.createTable(cfg['mssql_db']['table_name'], cfg['mssql_db']['table_info_csv'])

    if True:
        names = db_handler.selectColumnNames()
        for name in names:
            print(name[0])

    if cfg['mssql_db']['dataset_csv']:
        db_handler.insertAll(cfg['mssql_db']['dataset_csv'])
        # Read test from MS-SQL.
        pk = db_handler.selectLastPK()
        dat = db_handler.selectOne(pk)
        print(dat)

    pass


if __name__ == "__main__":
    if len(sys.argv) == 1:
        if SELF_TEST:
            # sys.argv.extend(["--db_setup_ini_file", "mssql_sinter_sut_setup.ini"])
            # sys.argv.extend(["--db_setup_ini_file", "mssql_setup.ini"])
            # sys.argv.extend(["--db_setup_ini_file", "mssql_rh_est_setup.ini"])
            # sys.argv.extend(["--db_setup_ini_file", "mssql_rh_eval_setup.ini"])
            # sys.argv.extend(["--db_setup_ini_file", "mssql_cvt_est_setup.ini"])
            # sys.argv.extend(["--db_setup_ini_file", "mssql_cvt_eval_setup.ini"])
            # sys.argv.extend(["--db_setup_ini_file", "mssql_rh_plus_est_setup.ini"])
            # sys.argv.extend(["--db_setup_ini_file", "mssql_rh_plus_eval_setup.ini"])
            # sys.argv.extend(["--db_setup_ini_file", "mssql_rh_m6_eval_setup.ini"])
            # sys.argv.extend(["--db_setup_ini_file", "mssql_rh_m6_est_setup.ini"])
            # sys.argv.extend(["--db_setup_ini_file", "cvt/est/mssql_cvt_macro_est_setup.ini"])
            # sys.argv.extend(["--db_setup_ini_file", "cvt/eval/mssql_cvt_macro_eval_setup.ini"])
            # sys.argv.extend(["--db_setup_ini_file", "rh/est/mssql_rh_macro_est_setup.ini"])
            # sys.argv.extend(["--db_setup_ini_file", "rh/eval/mssql_rh_macro_eval_setup.ini"])
            sys.argv.extend(["--db_setup_ini_file", "cvt/est/mssql_cvt_decarb_est_setup.ini"])
            # sys.argv.extend(["--db_setup_ini_file", "cvt/eval/mssql_cvt_decarb_eval_setup.ini"])

        else:
            sys.argv.extend(["--help"])

    main()
