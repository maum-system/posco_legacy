import argparse
import configparser
import os
import socket
import sys
import queue
from lib import sys_lib as system_lib
from lib import data as data_lib
from lib import learning as learning_lib
from lib import networking
from handlers import learning_handler
from handlers.learning_handler import MachineLearner
from models import est_eval
import refine_model

DB_OP_ = True
LOGGER_FOLER = 'log'
HEADER_LENGTH = 8
MODEL_REQUEST_TIME = 'MODEL_REQUEST_TIME'
DYN_PERCNT = 'DYN_PERCNT'
USE_MOD_EST_OXY = 'USE_MOD_EST_OXY'
CVT_DECARB_PREFIX = 'cvt_decarb_m'
INI_EXTENSION = '.ini'


def create_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("--server_mode", required=True, help="server mode - EST or EVAL")
    parser.add_argument("--ml_ini_file", required=True, help="machine learning ini file")
    parser.add_argument("--networking_ini_file", required=True, help="networking ini file")
    parser.add_argument("--var_ini_file", required=True, help="var ini file")
    parser.add_argument("--var_csv_file", required=True, help="var csv file")
    return parser

def send_error_message(error_message, client_ip, client_port):
    print(error_message)
    sock_res = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    client_address = (client_ip,
                      client_port)
    sock_res.connect(client_address)

    sock_res.sendall(error_message.encode('utf-8'))
    sock_res.close()

def create_new_filename_words(model_number_str, feat_handler):
    global pattern_number_str
    request_time = 0
    new_filename_words = []

    # 취련시점에 따라 new_filename_words 업데이트(M2, 3)
    new_filename_words.append(model_number_str[0])
    if model_number_str[0] == '2':
        # if feat_handler.vars[DYN_PERCNT].val < 50:
        request_time = 50
    elif model_number_str[0] == '3':
        # if feat_handler.vars[DYN_PERCNT].val == 85:
        request_time = 85
    new_filename_words.append(str(request_time))

    if model_number_str[0] == '2':
        pattern_number_str = 'C'
    elif model_number_str[0] == '3':
        use_mod_est_oxy = feat_handler.vars[USE_MOD_EST_OXY].val
        # 산소 투입량 모델 str 생성
        if use_mod_est_oxy == None or use_mod_est_oxy == '' or use_mod_est_oxy == '-1' or use_mod_est_oxy == 'NULL':
            pattern_number_str = 'O'
        elif use_mod_est_oxy == 0 or use_mod_est_oxy == 1 or use_mod_est_oxy == '0' or use_mod_est_oxy == '1':
            pattern_number_str = 'C_O'
        else:
            pattern_number_str = 'C_O'
    new_filename_words.append(pattern_number_str)
    return new_filename_words

def create_new_model_number_str(model_number_str, new_filename_words):
    new_model_number_str = ''
    for idx, var in enumerate(new_filename_words):
        if idx == len(new_filename_words) - 1:
            new_model_number_str += var
        else:
            new_model_number_str += var + '-'

    # if model_number_str[0] == '2' or model_number_str[0] == '3':
    #     model_number_str_parts = new_model_number_str.split('-')
    #     new_model_number_str = model_number_str_parts[0] + '-' + model_number_str_parts[1]
    return new_model_number_str


def main():
    global refined_db_data
    parser = create_parser()
    args = parser.parse_args()

    networking_ini = configparser.ConfigParser()
    networking_ini.read(args.networking_ini_file)
    ml_ini = configparser.ConfigParser()
    ml_ini.read(args.ml_ini_file)

    var_ini = configparser.ConfigParser()
    var_ini.read(args.var_ini_file)

    var_mtx = data_lib.read_csv_file(args.var_csv_file)

    server_mode = args.server_mode

    if not os.path.exists(LOGGER_FOLER):
        os.makedirs(LOGGER_FOLER)
    logger_name = ml_ini['general']['model_name'] + '_' + server_mode
    logger = system_lib.setup_logger(logger_name,
                                     logger_name.lower(),
                                     folder=LOGGER_FOLER,
                                     console_=True)

    logger.info("START " + ml_ini['general']['model_name'] + "_" + server_mode + " server")

    server_address = (networking_ini[server_mode + '_SERVER']['ip'],
                      int(networking_ini[server_mode + '_SERVER']['port']))

    sock_req = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    logger.info("Starting up on %s port %s..." % server_address)
    sock_req.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    sock_req.bind(server_address)
    sock_req.listen(10)

    while True:
        connected, str_dat, client_address = networking.accept(sock_req, logger)

        if not connected:
            continue
        if "Error" in str_dat:
            client_ip = networking_ini[server_mode + '_CLIENT']['ip']
            client_port = int(networking_ini[server_mode + '_CLIENT']['port'])
            send_error_message(str_dat, client_ip, client_port)
            continue

        model_number_str = ml_ini['general']['model_number']

        feat_handler = system_lib.get_feat_handler(var_ini, var_mtx, model_number_str)
        est_dict = feat_handler.decode_stream_comma(str_dat, dbg_=False, logger=logger)
        est_eval.update_feat_handler(feat_handler, ml_ini, est_dict, server_mode, logger)

        #

        # 모델 2, 3은 세부 number로 ml_ini와 feat_handler를 reload
        if model_number_str[0] != '1':
            # Todo 모델1은 세부 모델로 구분하는지 확인하여 로직 수정필요
            new_filename_words = create_new_filename_words(model_number_str, feat_handler)
            ml_ini = system_lib.update_ml_ini_filepath(new_filename_words, args.ml_ini_file,
                                                       separator='-',
                                                       cvt_decarb_prefix='cvt_decarb_m',
                                                       ini_extension='.ini')
            new_model_number_str = create_new_model_number_str(model_number_str, new_filename_words)

            # feat_handler obj 재생성
            feat_handler = system_lib.get_feat_handler(var_ini, var_mtx, new_model_number_str)

            # 입력된 stream을 ','로 구분하여 dict 재생성
            est_dict = feat_handler.decode_stream_comma(str_dat, dbg_=False, logger=logger)

            # 입력된 변수정보를 기반으로 feat_handler 정보 재업데이트
            est_eval.update_feat_handler(feat_handler, ml_ini, est_dict, server_mode, logger)

        # 모델 2
        # if server_mode == 'EST' and model_number_str == '2-50-C':
        #     est_dict['DY_OXY2'] = est_dict['DY_OXY2_ADD']
        #     est_dict['DYN_OXY_TOTAL'] = est_dict['DYN_OXY_TOTAL_ADD']
        #     est_dict['DYN_OXY_TOTAL'] = est_dict['DYN_OXY_TOTAL_ADD']
        #     est_dict['TOTAL_OXY'] = est_dict['TOTAL_OXY_ADD']
        #     est_dict['TERMINAL_TEMP'] = est_dict['TERMINAL_TEMP_ADD']

        # 모델의 예측값 생성
        output_list = learning_lib.calculate_ml_output(est_dict, ml_ini, feat_handler,
                                                       server_mode=server_mode,
                                                       logger=logger)

        feat_handler.vars['EST_END_TIME'].val = system_lib.get_datetime()

        # DB 연동시 진행
        if DB_OP_:
            # db_handler 초기화
            db_handler = system_lib.get_db_handler(db_ini=networking_ini[server_mode + '_DB'],
                                                   logger=logger,
                                                   DB_OP_=DB_OP_)
            # DB table_info 읽기
            table_info = data_lib.read_csv_file(networking_ini[server_mode + '_DB']['table_info_csv'])

            # 이전 모델 날짜이후의 DB 데이터 추출
            if server_mode == 'EST':
                refined_db_data = None
            elif server_mode == 'EVAL':
                refined_db_data = system_lib.get_refined_db_data(var_ini, ml_ini, networking_ini[server_mode+'_CLIENT'][server_mode.lower()+'_name'],
                                                                 table_info, db_handler, feat_handler,
                                                                 logger)
            # EVAL 서버의경우 날짜 조건 만족시 self-learning 수행
            if system_lib.time_to_self_learn(ml_ini) and refined_db_data is not None:

                dataset = data_lib.read_csv_file(ml_ini['general']['dataset_file'])

                org_dataset_len = len(dataset) - 1
                add_dataset_len = len(refined_db_data) - 1

                # Local dataset의 기준으로 DB dataset에서 데이터를 추출
                sorted_db_data = data_lib.extract_rows_from_table(refined_db_data, dataset[0])
                dataset += sorted_db_data[1:]
                data_lib.write_list_to_csv(dataset, ml_ini['general']['dataset_file'],
                                           overwrite=True)
                msg = "Write new self-learning dataset, {:d} + {:d}".format(org_dataset_len, add_dataset_len)
                logger.info(msg)

                # Evaluation 수행(모델 학습 및 평가)
                est_eval.evaluation_start(ml_ini, dataset, feat_handler, msg, logger)

            # DB에 송신받은 데이터를 write
            name = server_mode.lower() + '_' + 'name'
            if str_dat.split(',')[0] == networking_ini[server_mode + '_CLIENT'][name]:
                db_row_dict = data_lib.gen_db_row_dat(table_info, feat_handler, server_mode)
                db_handler.insertOne(db_row_dict)
                logger.info("{}_DB read done".format(server_mode))
                print("{}_DB read back:".format(server_mode) + str(db_handler.selectOne(db_handler.selectLastPK())))

        logger.info("{} {} operation, ({:d}), ({:d}) - {}.".format(ml_ini['general']['model_name'],
                                                                   server_mode,
                                                                   len(str_dat),
                                                                   len(est_dict),
                                                                   str(est_dict)))

        # Socket 생성 및 연결
        sock_res = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        client_address = (networking_ini[server_mode + '_CLIENT']['ip'],
                          int(networking_ini[server_mode + '_CLIENT']['port']))
        try:
            sock_res.connect(client_address)
        except ConnectionRefusedError:
            logger.error("[WinError 10061] 대상 컴퓨터에서 연결을 거부했으므로 연결하지 못했습니다.")

        # out_stream 생성 및 확인
        out_stream = feat_handler.encode_stream(dbg_=False, dataset_order_list=[1, 0], additional_items=['MTL_NO'])
        logger.info("OUTPUT: " + out_stream)
        stream_flag = out_stream[:8]

        predict_name = networking_ini[args.server_mode + '_CLIENT']['predict_name']
        logger.info("Predict Name:" + predict_name)

        # out_stream 업데이트
        out_stream = predict_name + out_stream[len(predict_name):]

        # out_stream을 9자리로 맞춤
        while True:
            if len(out_stream.split(',')) < 9:
                out_stream += ','
            else:
                break
        if 'CVT_DECARB' in ml_ini['general']['model_name']:
            out_stream += ',' + str(feat_handler.vars[DYN_PERCNT].val) + ',' +  str(feat_handler.vars[USE_MOD_EST_OXY].val) + ',' + feat_handler.vars[server_mode+'_RANGE_CHECK_VARS'].val  + ',' + stream_flag

        # output_str = ''
        # for output in output_list:
        #     output_str += str(output) + ', '
        # output_str = output_str[:-2]
        # print(str(output_str))
        print(out_stream)

        # 데이터 return 및 socket close
        sock_res.sendall(out_stream.encode('utf-8'))
        sock_res.close()

if __name__ == "__main__":
    if len(sys.argv) == 1:
        sys.argv.extend([
            # "--server_mode", "EST",
            # "--var_ini_file", "../sys_cvt_decarb/cvt_decarb_var.ini",
            # "--var_csv_file", "../sys_cvt_decarb/cvt_decarb_var.csv",
            # "--ml_ini_file", "../sys_cvt_decarb/config_machine_learning/cvt_decarb_m1.ini",
            # "--networking_ini_file", "../sys_cvt_decarb/config_networking/cvt_decarb_m1_cfg_sa_posco.ini",

            # "--ml_ini_file", "../sys_cvt_decarb/config_machine_learning/cvt_decarb_m2-50-C.ini",
            # "--networking_ini_file", "../sys_cvt_decarb/config_networking/cvt_decarb_m2_cfg_sa_minds.ini",
            # "--ml_ini_file", "../sys_cvt_decarb/config_machine_learning/cvt_decarb_m3-85-O.ini",
            # "--networking_ini_file", "../sys_cvt_decarb/config_networking/cvt_decarb_m3_cfg_sa_minds.ini",
            # "--ml_ini_file", "../sys_cvt_decarb/config_machine_learning/cvt_decarb_m3-85-C_O.ini",
            # "--networking_ini_file", "../sys_cvt_decarb/config_networking/cvt_decarb_m3_cfg_sa_minds.ini",
            # ys test
            "--server_mode", "EVAL",
            "--var_ini_file", "../sys_cvt_decarb/cvt_decarb_var_add.ini",
            "--var_csv_file", "../sys_cvt_decarb/cvt_decarb_var_add.csv",
            # "--ml_ini_file", "../sys_cvt_decarb/config_machine_learning/cvt_decarb_m2-50-C.ini",
            "--ml_ini_file", "../sys_cvt_decarb/config_machine_learning/cvt_decarb_m3-85-O.ini",
            "--networking_ini_file", "../sys_cvt_decarb/config_networking/cvt_decarb_m3_cfg_posco.ini"
        ])

    main()
